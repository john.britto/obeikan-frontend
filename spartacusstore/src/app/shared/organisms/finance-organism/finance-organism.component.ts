import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { FINANCE_NAV_LINKS } from '../../../services/finance.service'
import { AdminService } from '../../../services/admin.service'
import { CommonUtilityService } from '../../../services/common-utility-service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-finance-organism',
  templateUrl: './finance-organism.component.html',
  styleUrls: ['./finance-organism.component.scss']
})
export class FinanceOrganismComponent implements OnInit {

  @ViewChild('target', { static: false }) target: ElementRef;
  financeNavLinks: any
  panelOpenState = true;
  hrImage: any = '../../assets/images/icn_masterdataicn_masterdata_blacksmall.png'
  scrollPosition: any
  roles: any = []

  constructor(
    private adminService: AdminService,
    private utilityService: CommonUtilityService,
    private route: ActivatedRoute) {

      this.route
      .queryParams
      .subscribe(params => {
        this.adminService.userType = params['roleType']
      });

    let accessbilities = JSON.parse(this.utilityService.getLocalStorage('accessbility'));

    this.financeNavLinks = FINANCE_NAV_LINKS
  }

  getCustomerRoles = () => {
    const orderAPI = environment.subscriptionAPI + this.utilityService.getLocalStorage('displayUid') + '/subscriptions/?view=active';

    const url = orderAPI
    const httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${this.utilityService.getCookie('AuthToken')}`);

    const options = {
      headers: httpHeaders
    };
    this.utilityService.getRequest(url, options).subscribe((res: any) => {
      if (res.subscriptions) {
        res.subscriptions.forEach(role => {
          this.roles.push(role.productCode)
        });
        this.adminService.customerRoles = this.roles
        this.adminService.isAuthorizedUser('finance', this.roles)
      }
    });
  }

  changeAccordian = (tileName, el) => {
    this.scrollPosition = el
    el.nativeElement.scrollIntoView({ behavior: "smooth" });
    this.adminService.changeAdminPageHeaderContext(tileName)
    this.financeNavLinks = this.financeNavLinks.map(link => {
      if (link.name === tileName) {
        return {
          ...link,
          active: true
        }
      }
      return {
        ...link,
        active: false
      }
    })

  }
  ngOnInit() {
    if(!!this.adminService.customerRoles) {
      this.roles = this.adminService.customerRoles
    }

    (!!this.roles && !!this.roles.length && this.roles.length > 0) ? this.adminService.isAuthorizedUser('finance', this.roles) : this.getCustomerRoles()
  }

  scrollToPosition = () => {
    this.scrollPosition.nativeElement.scrollIntoView({ behavior: "smooth" });
  }
}
