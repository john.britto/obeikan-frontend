import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-not-found-organism',
  templateUrl: './not-found-organism.component.html',
  styleUrls: ['./not-found-organism.component.scss']
})
export class NotFoundOrganismComponent implements OnInit {

  returnUrl: any;

  constructor(private router: ActivatedRoute) { }

  ngOnInit() {
    this.returnUrl = this.router.snapshot.queryParams.returnUrl || '/';
  }

}
