import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { AdminService } from '../../../services/admin.service';
import { appConfig } from 'src/app/app.config';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-company-organism',
  templateUrl: './company-organism.component.html',
  styleUrls: ['./company-organism.component.scss']
})
export class CompanyOrganismComponent implements OnInit {

  companyContext: any
  dpInput: any
  isDpSelected: boolean = false
  selectedDpName: any
  hostname: string = environment.hostName

  constructor(private adminService: AdminService) { }

  ngOnInit() {
    this.adminService.getCompanyDetails('company').subscribe((res: any) => {
      if(!!res) this.companyContext = res
    },
    err => {
    })
  }

  refreshData = () => {
     this.ngOnInit()
  }

  onFileChanged(event: any) {
    this.dpInput = event.target.files[0]
    this.selectedDpName = this.dpInput.name
    this.isDpSelected = true
  }

  removeDpSelection= () => {
    this.dpInput = null
    this.isDpSelected = false
  }

  onUploadDp = () => {
    this.adminService.uploadCompanyDpRequest('company/profile-pic', this.dpInput).subscribe((res: any) => {
      this.companyContext = res
      this.isDpSelected = !this.isDpSelected
      this.dialogMsg('success', 'Profile picture has been updated successfully')
    },
    err => {
      if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.dialogMsg('error', err.error.errors[0].message)
      } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    })
  }

  dialogMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true,
      cancelButtonText:
        '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}

