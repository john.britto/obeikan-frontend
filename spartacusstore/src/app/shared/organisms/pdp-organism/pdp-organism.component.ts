import { Component, OnInit, ChangeDetectorRef, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CommonUtilityService } from '../../../services/common-utility-service';
import { environment } from 'src/environments/environment';
import { PdpService } from '../../../services/pdp.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import Swal from 'sweetalert2';
import {  HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-pdp-organism',
  templateUrl: './pdp-organism.component.html',
  styleUrls: ['./pdp-organism.component.scss']
})
export class PdpOrganismComponent implements OnInit {

  hostName: string = environment.hostName;
  imageUrl: any;
  productName: any;
  price: any;
  description: any;
  productCode: any;
  isFinance: any = false;
  succMsg: any = 'Product successfully added to cart !';
  errMsg: any = 'Product not added. Please try again later !';
  maxQtyErr: any = false;
  maxQtyErrMsg: any = 'Please enter any value less than or equal to 50';
  usersQty: number;
  prodCode: any;
  showSubscription: boolean = false;
  isHrms: boolean = false;
  shouldShowAddToCart: boolean = true;
  // range: any = "PRICERANGE1";
  range: any
  _array = Array;
  productDetails: any
  sectionOneBannerContent: any
  sectionOneDesktopBannerContent: any
  sectionOneInfoContent: any
  sectionTwoContent: any
  productThumbnail: any
  usersQtyHR: number = 0
  usersPriceHR: number = 0;
  details: any;

  constructor(
    private route: ActivatedRoute,
    private utilityService: CommonUtilityService,
    private router: Router,
    private pdpService: PdpService,
    private dialog: MatDialog) { }

  ngOnInit() {
    let bundle = '';
    
      this.prodCode = this.route.snapshot.data[0].pageName //log the value of pid
      bundle = this.prodCode === "HRMS" ? 'bundle/' : "";

    let productsData = [
      this.utilityService.getRequest(environment.pdpEndPoint + bundle + this.prodCode, ''),
      this.utilityService.getRequest(`${environment.pdpCMSApi}${this.prodCode.toLowerCase()}ProductPage`, '')
    ]

    forkJoin([...productsData]).subscribe((res: any) => {
      if (!!res && !!res[0]) {
        this.productDetails = res[0]
        this.imageUrl = res[0].picture.url;
        this.productThumbnail = res[0].thumbnail.url
        this.productName = res[0].name;
        this.price = this.prodCode === "HRMS" ? this.getPrice(res[0].variantOptions) : 'SAR ' + res[0].price.value.toFixed(2);
        this.usersPriceHR = this.price*1;
        this.description = res[0].description;
        this.productCode = res[0].code;
        if (this.productCode === 'Finance') {
          this.isFinance = true;
        }
        if (this.productCode === "HRMS") {
          this.isHrms = true;
          this.shouldShowAddToCart = false;
        }
        this.range = this.isHrms ? true : this.range;

        const subsAPI = environment.subscriptionAPI + this.utilityService.getLocalStorage('displayUid') + '/subscriptions/?view=active';
        const httpHeaders = new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Authorization', `Bearer ${this.utilityService.getCookie('AuthToken')}`);
    
        const options = {
          headers: httpHeaders
        };
        this.utilityService.getRequest(subsAPI, options).subscribe((res: any) => {
          if(res.subscriptions) {
            // this.superAdminData = res.subscriptions;
            // this.getProducts(this.superAdminData);
            if(this.isHrms) {
              this.details = res.subscriptions.filter(e => e.productCode==="HRMS_CORE")[0];
            } else {
              this.details = res.subscriptions.filter(e => e.productCode==="Finance_CORE")[0];
            }
          }
        });
      }
      if (!!res && !!res[1]) {
        for (const content of res[1].contentSlots.contentSlot) {
          if (content.slotId.indexOf('ProductSection2Slot') !== -1) this.sectionTwoContent = content.components.component

          if (content.slotId.indexOf('ProductSection1Slot') !== -1) {
            for (const comp of content.components.component) {
              if (comp.uid.indexOf('productBannerComponent') !== -1) this.sectionOneBannerContent = comp
              if (comp.uid.indexOf('ProductVideoComponent') !== -1) this.sectionOneInfoContent = comp
              if (comp.uid.indexOf('productDesktopBannerComponent') !== -1) this.sectionOneDesktopBannerContent = comp
            }
          }
        }
      }
    }, (err) => {
      Swal.fire({
        icon: 'error',
        text: this.errMsg,
        timer: 15000,
        showCloseButton: true,
        cancelButtonText:
          '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
        showConfirmButton: false,
        background: 'black',
        toast: true
      })
    });
    
  }
  getPrice(data) {
    const coreHrms = data.filter(e => e.code == "HRMS_CORE")[0];
    return coreHrms.priceData.value
  }
  handleShowSubscription() {
    this.showSubscription = !this.showSubscription;
    this.shouldShowAddToCart = !this.shouldShowAddToCart;
  }
  handleTerms() {
    this.shouldShowAddToCart = !this.shouldShowAddToCart;
  }
  handleChange(event) {
    this.maxQtyErr = false;
    this.usersQty = event.target.value;
    this.usersQty > 50 ? (this.maxQtyErr = true, this.usersQty = 50) : null;
  }

  handleChangeHR(event) {
    this.maxQtyErr = false;
    this.usersQtyHR = event.target.value
    this.usersPriceHR = event.target.value * this.price;
    this.range = event.target.value > 0 || isNaN(Number(event.target.value));
  }
  checkCart() {
    if (this.utilityService.checkLoggedInOrRedirect() && this.utilityService.getLocalStorage('displayUid')) {
      // Check Cart Exists
      const userId = this.utilityService.getLocalStorage('displayUid');
      const url = environment.hostName + environment.cartAPI + userId + '/carts';
      this.pdpService.checkCartExist(url).subscribe((data: any) => {
        const response = JSON.parse(JSON.stringify(data));
        if (response && response.carts && response.carts.length > 0 && response.carts[0].code && !response.carts.every(e => e.renewOrder)) {
          this.addToCart(url, response.carts[0].code);
        } else {
          // Create new Cart
          this.pdpService.createCart(url).subscribe((res: any) => {
            const result = JSON.parse(JSON.stringify(res));
            if (result && result.code) {
              this.addToCart(url, result.code);
            }
          }, (err) => {
            Swal.fire({
              icon: 'error',
              text: this.errMsg,
              timer: 2000,
              showConfirmButton: false,
              background: 'black',
              toast: true
            })
          });
        }
      }, (err) => {
        Swal.fire({
          icon: 'error',
          text: this.errMsg,
          timer: 2000,
          showConfirmButton: false,
          background: 'black',
          toast: true
        })
      });
    }
  }

  rangeSelect(e) {
    this.range = e.value;
  }

  onSelectRange = range => {
    this.range = range
  }

  addSubscription() {
    const userId = this.utilityService.getLocalStorage('displayUid');
    const url = environment.hostName + environment.cartAPI + userId + '/pricerange';
    this.pdpService.subscription(url, this.range).subscribe((res: any) => {
      const result = JSON.parse(JSON.stringify(res));
      if (result.statusCode === 'success') {
        Swal.fire({
          icon: 'success',
          text: this.succMsg,
          timer: 2000,
          showConfirmButton: false,
          background: 'black',
          toast: true
        })
      }
    }, (err) => {
      Swal.fire({
        icon: 'error',
        text: this.errMsg,
        timer: 15000,
        showCloseButton: true,
        cancelButtonText:
          '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
        showConfirmButton: false,
        background: 'black',
        toast: true
      })
    });
  }

  addToCart(url, cartId) {
    let qty;
    // Add product to the cart
    //this.isHrms && this.addSubscription();
    url = url + '/' + cartId + '/entries';
    this.isFinance ? qty = this.usersQty : qty = this.usersQtyHR;
    const prodCode = this.productCode === "HRMS" ? "HRMS_CORE" : this.productCode === "Finance" ? "Finance_CORE" : this.productCode;
    this.pdpService.addToCart(prodCode, url, qty).subscribe((res: any) => {
      const result = JSON.parse(JSON.stringify(res));
      if (result.statusCode === 'success') {
        this.utilityService.setCookie('cartId', cartId);
        this.router.navigateByUrl('/cart');
      }
    }, (err) => {
      Swal.fire({
        icon: 'error',
        text: this.errMsg,
        timer: 15000,
        showCloseButton: true,
        cancelButtonText:
          '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
        showConfirmButton: false,
        background: 'black',
        toast: true
      })
    });
  }

  onPlayVideo = () => {
    const videoUrl = this.sectionOneInfoContent.media.url
    this.dialog.open(PdpVideoModalComponent, {
      data: { videoUrl }
    });
  }

}

@Component({
  selector: 'app-pdp-video-modal',
  templateUrl: 'pdp-video-modal.html',
  providers: [PdpOrganismComponent]
})
export class PdpVideoModalComponent {

  hostName: string = environment.hostName;
  
  constructor(@Inject(MAT_DIALOG_DATA) public data, public dialog: MatDialog) { }
  onClose(): void {
    this.dialog.closeAll();
  }
}