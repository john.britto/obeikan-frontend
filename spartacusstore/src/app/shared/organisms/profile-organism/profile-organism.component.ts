import { Component, OnInit } from '@angular/core';
import { CommonUtilityService } from '../../../services/common-utility-service';
import { appConfig } from '../../../app.config'
import { AdminService } from '../../../services/admin.service';

@Component({
  selector: 'app-profile-organism',
  templateUrl: './profile-organism.component.html',
  styleUrls: ['./profile-organism.component.scss']
})
export class ProfileOrganismComponent implements OnInit {
  userType: any
  pageTypes: any = appConfig.approverDashboardPageType
  hrPageTypes: any = appConfig.approverDashboardHRPageType
  page: any = 'AP'
  selected = this.pageTypes[0];
  hrSelected = this.hrPageTypes[0]
  isHr: boolean = false
  isFinance: boolean = false

  constructor(
    private utilityService: CommonUtilityService,
    private adminService: AdminService) { }

  ngOnInit() {
    const userType = this.adminService.userType

    this.userType = this.utilityService.getCookie('adminType')

    if (!!userType) {
      this.isFinance = userType === 'Finance'
      this.isHr = userType === 'HR'
    } else {
      this.isHr = (appConfig.DropdownOptions.customerRoles.includes(this.userType) || this.userType === 'HR' || this.userType === 'HREmployee') ? true : false
      this.isFinance = !this.isHr
    }

    if (this.userType === 'HR' || this.userType === 'GeneralManager' || this.userType === 'HREmployee') {
      this.page = 'SE'
    } else this.page = 'AP'
  }

  updatePage = page => {
    this.page = page;
  }
}
