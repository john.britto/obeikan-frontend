import { Component, OnInit} from '@angular/core';
import { FormControl } from '@angular/forms';
import { CommonUtilityService } from '../../../services/common-utility-service';
import * as XLSX from 'xlsx';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from 'src/environments/environment';
import { HttpHeaders } from '@angular/common/http';
import { SuperAdminService } from '../../../services/super-admin.service';
import Swal from 'sweetalert2';
import { appConfig } from 'src/app/app.config';

@Component({
  selector: 'app-finance-manger-organism',
  templateUrl: './finance-manger-organism.component.html',
  styleUrls: ['./finance-manger-organism.component.scss']
})
export class FinanceMangerOrganismComponent implements OnInit {
  downloadForm: any;
  subCustomers: any;
  fileName: any;
  excelValues: any = {};
  selected = new FormControl(0);

  constructor(private utilService: CommonUtilityService, private superAdminService: SuperAdminService) {
    const url = environment.superAdmin.financeManagerAPI;
    const httpHeaders = new HttpHeaders()
    .set('Content-Type', 'application/json')
    .set('Authorization', `Bearer ${this.utilService.getCookie('AuthToken')}`);
    const options = {
      headers: httpHeaders
    };
    this.utilService.getRequest(url, options).subscribe((data: any) => {
      this.downloadForm = data.contentSlots.contentSlot.components.component[0].media.url;
      });
    this.subCustomers = JSON.parse(this.utilService.getCookie('customerList'));
   }

  ngOnInit() {
  }

  downloadFile = e => {
    e.preventDefault();
    const downloadUrl = environment.hostName + this.downloadForm;
    if(this.utilService.isBrowser()) {
      window.open(downloadUrl, '_self')
    }
  }
  fileInputChange(event: any, i, id) {
    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    const file = event.target.files[0];
    reader.onload = () => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: 'binary' });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        initial[name] = XLSX.utils.sheet_to_json(sheet);
        return initial;
      }, {});
      jsonData ? this.sendRequest(jsonData , id) : '';
    };
    reader.readAsBinaryString(file);
  }
  sendRequest(request , id) {
    if (request.itemUpload && request.bpTransUpload && request.openingBalance) {
      request.bpTransUpload = request.bpTransUpload[0];
      request.openingBalance = request.openingBalance[0];
      this.superAdminService.submitFinanceManagerForm(request, id).subscribe((res: any) => {
        if (res) {
          this.dialogMsg('success', 'Data uploaded successfully')
        } else {
          this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
        }
      },
        err => {
          if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
            this.dialogMsg('error', err.error.errors[0].message)
          } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
        });
    } else {
      this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    }
  }
  dialogMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}
