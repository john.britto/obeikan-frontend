import { Component, OnInit } from '@angular/core';
import { CommonUtilityService } from '../../../services/common-utility-service';
import { appConfig } from '../../../app.config'
import {FormControl} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import Swal from 'sweetalert2';
import { CartService, SUBS_UPDATE_FAILURE_MSG, UPDATE_CART } from '../../../services/cart.service'

@Component({
  selector: 'app-renewal-organism',
  templateUrl: './renewal-organism.component.html',
  styleUrls: ['./renewal-organism.component.scss']
})
export class RenewalComponent implements OnInit {
    subsData: any;
    constructor(
      private route: ActivatedRoute,
      private subscriptionsService: CartService,
      private matSnack: MatSnackBar
      ) {
        this.subscriptionsService.cartContext.subscribe(context => {
          if(context) {
            this.updateCart()
          }
        });
       }
  
    ngOnInit() {
      this.route.data.subscribe((data: { cartData: any }) => {
         this.subsData = data.cartData;
      });
    }
  
    updateCart = () => {
      this.subscriptionsService.getSubscriptions().subscribe((res: any) => {
        this.subsData = res
      },
      err => {
        Swal.fire({
          icon: 'error',
          text: SUBS_UPDATE_FAILURE_MSG,
          timer: 15000,
          showCloseButton: true, 
          cancelButtonText:
          '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
          showConfirmButton: false,
          background: 'black',
          toast: true
        })
      })
    }
}