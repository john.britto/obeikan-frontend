import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { CommonUtilityService } from '../../../services/common-utility-service';
import { CheckoutService } from '../../../services/checkout.service';

@Component({
  selector: 'app-order-confirmation-organism',
  templateUrl: './order-confirmation-organism.component.html',
  styleUrls: ['./order-confirmation-organism.component.scss']
})
export class OrderConfirmationOrganismComponent implements OnInit {
  orderId: any;
  orderDetails: any

  constructor(
    private utilityService: CommonUtilityService,
    private checkoutService: CheckoutService,
    private router: Router) { }

  ngOnInit() {

    this.orderDetails = this.checkoutService.orderDetails

    if(!this.orderDetails) this.router.navigateByUrl('/cart');
  }

}
