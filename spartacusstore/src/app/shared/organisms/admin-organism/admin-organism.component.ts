import { Component, OnInit, Inject, ChangeDetectorRef, AfterViewInit, NgZone } from '@angular/core';
import { CommonUtilityService } from '../../../services/common-utility-service';
import { environment } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpHeaders } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EmitterService } from '../../../services/emitter.service';
import { AdminService } from '../../../services/admin.service'
import Swal from 'sweetalert2';
import { appConfig } from '../../../app.config'

@Component({
  selector: 'app-admin-organism',
  templateUrl: './admin-organism.component.html',
  styleUrls: ['./admin-organism.component.scss']
})
export class AdminOrganismComponent implements OnInit, AfterViewInit {

  EmployeeDetails: any
  pipe = new DatePipe('en-US');
  setupNavLinks: any
  navLinksSubCategory: any = []
  navLinksSubCategoryHeading: any
  navLinksSubCategoryTwoHeading: any
  navLinksSubCategoryTwo: any = []
  panelOpenState = true;
  hrImage: any = '../../assets/images/icn_masterdataicn_masterdata_blacksmall.png'
  navTab: string = appConfig.hrmsNavLists.mainTiles[0].name;
  employeeList: any;
  enums: any;
  data: any;
  empNumber: any;
  subTileName: any
  terminationReasons: any
  roles: any = []

  constructor(private utilService: CommonUtilityService,
    private route: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    private cd: ChangeDetectorRef,
    private zone: NgZone,
    private emitter: EmitterService,
    private adminService: AdminService) {

    this.adminService.isAuthorizedUser('hrms')

    this.setupNavLinks = appConfig.hrmsNavLists.mainTiles;
    !this.enums ? this.utilService.getEnum().subscribe(enums => { this.enums = enums; }) : '';
    !this.employeeList ? this.getEmployeeList() : '';
    this.emitter.refreshHREployees.subscribe((data) => {
      this.getEmployeeList();
    });
  }

  ngOnInit() {
  }
  ngAfterViewInit() {
    this.cd.detectChanges();
  }
  changeAccordian = (e, tileName) => {
    e.preventDefault()
    this.subTileName = ''
    this.displaySubCategory(tileName)
    this.changeSubCategory(tileName)
    this.adminService.changeAdminPageHeaderContext(tileName)

    this.setupNavLinks = this.setActiveTile(tileName, appConfig.hrmsNavLists.mainTiles)

  }

  displaySubCategory = tileName => {

    this.subTileName = tileName

    switch (tileName) {
      case 'HR Policies Setup':
        this.navLinksSubCategoryHeading = tileName
        this.navLinksSubCategory = appConfig.hrmsNavLists.hrPoliciesSubTiles
        return;

      case 'Payroll Process':
        this.navLinksSubCategoryHeading = tileName
        this.navLinksSubCategory = appConfig.hrmsNavLists.payrollSubTabs
        return;

      default:
        this.navLinksSubCategoryHeading = ''
        this.navLinksSubCategory = []
        return;
    }
  }

  changeSubCategory = (tileName) => {

    this.subTileName = tileName

    this.navLinksSubCategory = this.setActiveTile(tileName, this.navLinksSubCategory)

    switch (tileName) {
      case 'Deductions':
        this.navLinksSubCategoryTwoHeading = tileName
        this.navLinksSubCategoryTwo = appConfig.hrmsNavLists.payrollDeductionTiles
        // this.setActiveTile(tileName, this.navLinksSubCategory)
        return;

      case 'Earnings':
        this.navLinksSubCategoryTwoHeading = tileName
        this.navLinksSubCategoryTwo = appConfig.hrmsNavLists.payrollEarningsTiles
        // this.setActiveTile(tileName, this.navLinksSubCategory)
        return;

      default:
        this.navLinksSubCategoryTwoHeading = ''
        this.navLinksSubCategoryTwo = []
        // this.setActiveTile(tileName, this.navLinksSubCategory)
        return;
    }
  }

  changeSubCategoryTwo = tileName => {
    this.subTileName = tileName
    this.navLinksSubCategoryTwo = this.setActiveTile(tileName, this.navLinksSubCategoryTwo)
  }

  applyFilter = (event) => {
    let filteredData = this.EmployeeDetails.filter(data => {
      let concatFilterColumns = data.emailAddress
      return concatFilterColumns.toLowerCase().indexOf(event.target.value.trim()) != -1
    })
    this.employeeList = filteredData
  }

  setActiveTile = (tileName, category) => {
    if (!!category) {
      return category.map(link => {
        if (link.name === tileName) {
          return {
            ...link,
            active: true,
            
          }
        }
        return {
          ...link,
          active: false
        }
      })
    }
  }

  getEmployeeList() {
    const url = `${environment.superAdmin.hrCreateUserAPI + this.utilService.getLocalStorage('displayUid')}/masterdatasetup`;

    const httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${this.utilService.getCookie('AuthToken')}`);
    const options = {
      headers: httpHeaders
    };
    this.utilService.getRequest(url, options).subscribe((data: any) => {
      this.zone.run(() => {
        data ? this.employeeList = data.hrEmployeeMasterData : '';
        this.EmployeeDetails = this.employeeList
      });
    });
  }
  deleteUserDialog(user) {

    !this.terminationReasons ?
      this.adminService.getLookupData('termination-reason').subscribe((res: any) => {
        if (!!res && !!res.terminationReasonLookups) this.terminationReasons = res.terminationReasonLookups
        this.terminateEmployee(user)
      },
        err => { }) : this.terminateEmployee(user)
  }

  terminateEmployee = user => {

    const terminationTypes = this.terminationReasons

    const dialogRef = this.dialog.open(DeleteHRMSFormModalComponent, {

      data: {
        user,
        terminationDetails: {
          reason: '',
          notifiedDate: '',
          terminationDate: '',
          finalPaymentDate: '',
          lastPayrollProcessDate: ''
        },
        terminationTypes
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      result && this.deleteUser(result.user.employeeNumber, result.terminationDetails);
    });
  }

  deleteUser(id, data) {

    const payload = {
      employeeNumber: id,
      notifiedDate: this.pipe.transform(data.notifiedDate, 'MM/dd/yyyy'),
      terminationDate: this.pipe.transform(data.terminationDate, 'MM/dd/yyyy'),
      finalPaymentDate: this.pipe.transform(data.finalPaymentDate, 'MM/dd/yyyy'),
      lastPayrollProcessDate: this.pipe.transform(data.lastPayrollProcessDate, 'MM/dd/yyyy'),
      terminationReason: {
        code: data.reason
      }
    }


    this.adminService.hrPutRequest('terminate-employee', payload).subscribe((data: any) => {
      this.showSuccessMsg('Employee termination  has been successful !');
      this.getEmployeeList()
    }, (err) => {
      if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.showErrorMsg(err.error.errors[0].message)
      } else this.showErrorMsg(appConfig.apiResponseMessages.GenericErrorMsg)
    });

  }
  refreshEmpList(event) {
    if (event) {
      this.getEmployeeList();
    }
  }
  updateEmployee(id) {

  }
  openDialog(user) {
    const dialogRef = this.dialog.open(EditHRMSFormModalComponent, {
      data: { user, employeeList: this.employeeList, enums: this.enums }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.data = null;
    });
  }


  showSuccessMsg(msg) {
    Swal.fire({
      icon: 'success',
      text: msg,
      timer: 15000,
      showCloseButton: true,
      cancelButtonText:
        '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    });
  }
  showErrorMsg(err) {
    Swal.fire({
      icon: 'error',
      text: err,
      timer: 15000,
      showCloseButton: true,
      cancelButtonText:
        '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    });
  }
}
@Component({
  selector: 'app-edit-hrms-form-modal',
  templateUrl: 'edit-hrms-form-modal.html',
  providers: [AdminOrganismComponent]
})
export class EditHRMSFormModalComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data, public dialog: MatDialog, private emitter: EmitterService) { }
  refresh(event) {
    if (event) {
      this.emitter.refreshHREployees.next(true);
    }
    // this.adminComponent.refreshEmpList(event);
    this.dialog.closeAll();
  }

  onClose(): void {
    this.dialog.closeAll();
  }
}


@Component({
  selector: 'app-delete-hrms-modal',
  templateUrl: 'delete-hrms-modal.html',
  providers: [AdminOrganismComponent]
})
export class DeleteHRMSFormModalComponent {

  terminationDate: any
  joiningDate: any

  constructor(@Inject(MAT_DIALOG_DATA) public data, public dialog: MatDialog) {
    this.joiningDate = new Date(data.user.joiningDate)
  }
  onClose(): void {
    this.dialog.closeAll();
  }

  setTerminationDate = () => {

    const { terminationDate } = this.data.terminationDetails
    this.terminationDate = terminationDate

  }
}
