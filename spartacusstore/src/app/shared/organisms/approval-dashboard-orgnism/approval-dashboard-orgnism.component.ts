import {Component, OnInit} from '@angular/core';
import { ApproverService } from '../../../services/approver.service'
import Swal from 'sweetalert2';
import { appConfig } from 'src/app/app.config';


@Component({
  selector: 'app-approval-dashboard-orgnism',
  templateUrl: './approval-dashboard-orgnism.component.html',
  styleUrls: ['./approval-dashboard-orgnism.component.scss']
})
export class ApprovalDashboardOrgnismComponent implements OnInit {

  approvalList: any
  detailedInfo: any

  constructor(private approverService: ApproverService) {}

  ngOnInit() {
    this.approverService.getApprovalList().subscribe((res: any) => {
      this.approvalList = res.financeCustomerMasterList !== undefined ? res.financeCustomerMasterList : []
    },
    err => {
      if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.dialogMsg('error', err.error.errors[0].message)
      } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    })
  }

  dialogMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}
