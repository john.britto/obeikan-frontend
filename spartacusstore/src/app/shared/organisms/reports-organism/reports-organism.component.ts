import { Component, OnInit } from '@angular/core';
import { SafeResourceUrl } from '@angular/platform-browser';
import { CommonUtilityService } from '../../../services/common-utility-service';
import { AdminService } from '../../../services/admin.service';
import { LoginService } from '../../../services/login.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-reports-organism',
  templateUrl: './reports-organism.component.html',
  styleUrls: ['./reports-organism.component.scss']
})
export class ReportsOrganismComponent implements OnInit {

  url: SafeResourceUrl
  showIframe: any = { activeClass: "", show: false }
  userRoles: any
  isHrUser: boolean = false
  isFinanceUser: boolean = false
  powerBIUrl: any
  pageType: any

  // PowerBI iFrame Urls
  sales: any
  payable: any
  recievable: any
  inventoryOverview: any
  finance: any
  collection: any
  expense: any
  itbr: any
  APReport: any
  ARReport: any
  fixedasset: any
  VATReports: any
  soReport: any
  poReport: any
  inventoryReport: any
  ledgerTransactionDetails: any
  hrms: any
  hrPayrollRegister: any

  //sanad compass reports
  compassReportOne: any
  compassReportTwo: any
  compassReportThree: any
  compassReportFour: any

  constructor(
    private utilityService: CommonUtilityService,
    private adminService: AdminService,
    private loginService: LoginService,
    private activatedroute: ActivatedRoute) { }

  ngOnInit() {

    this.activatedroute.data.subscribe(data => {
      this.pageType = data[0].pageName;
    })

    this.powerBIUrl = this.loginService.powerBiData

    this.utilityService.isNullOrUndefined(this.powerBIUrl) ?
      this.loginService.fetchUserDetails(this.utilityService.getCookie('AuthToken'), this.utilityService.getLocalStorage('displayUid')).subscribe((res: any) => {
        console.log(res)
        this.powerBIUrl = res.powerBiUrls
        this.setDashboardUrl()
      },
        err => {
        }) : this.setDashboardUrl()

    this.userRoles = JSON.parse(this.utilityService.getCookie('customerRoles'))

    this.isHrUser = (this.userRoles.includes('HR') || this.userRoles.includes('GeneralManager')) ? true : false
    this.isFinanceUser = (this.userRoles.includes('Finance') || this.userRoles.includes('FinanceManager')) ? true : false

    this.loadUrl(this.adminService.iframeKey)
  }

  setDashboardUrl = () => {
    if (!!this.powerBIUrl && !!this.powerBIUrl.entry) {
      this.powerBIUrl.entry.forEach(entry => {

        switch (entry.key) {
          case 'salesOverviewUrl':
            this.sales = entry.value
            return;
          case 'payableOverviewUrl':
            this.payable = entry.value
            return;
          case 'receivableOverviewUrl':
            this.recievable = entry.value
            return;
          case 'inventoryReportUrl':
            this.inventoryReport = entry.value
            return;
          case 'inventoryUrl':
            this.inventoryOverview = entry.value
            return;
          case 'financialStmtUrl':
            this.finance = entry.value
            return;
          case 'collectionUrl':
            this.collection = entry.value
            return;
          case 'expenseAnalysisUrl':
            this.expense = entry.value
            return;
          case 'itbrUrl':
            this.itbr = entry.value
            return;
          case 'apReportUrl':
            this.APReport = entry.value
            return;
          case 'arRportUrl':
            this.ARReport = entry.value
            return;
          case 'vatReportUrl':
            this.VATReports = entry.value
            return;
          case 'fixedAssetUrl':
            this.fixedasset = entry.value
            return;
          case 'salesReportUrl':
            this.soReport = entry.value
            return;
          case 'purchaseReportUrl':
            this.poReport = entry.value
            return;
          case 'ledgerTransactionUrl':
            this.ledgerTransactionDetails = entry.value
            return;
          case 'inventoryReportUrl':
            this.inventoryReport = entry.value
            return;
          case 'payrollDashboardUrl':
            this.hrPayrollRegister = entry.value
            return;
          case 'employeeDashboardUrl':
            this.hrms = entry.value
            return;
          case 'compassReportOne':
            this.compassReportOne = entry.value
            return;
          case 'compassReportTwo':
            this.compassReportTwo = entry.value
            return;
          case 'compassReportThree':
            this.compassReportThree = entry.value
            return;
          case 'compassReportFour':
            this.compassReportFour = entry.value
            return;
        }
      });
    }
  }

  loadUrl(url: string) {
    if (this.utilityService.isBrowser() && !!url) {
      this.showIframe.show = true;
      this.showIframe.activeClass = url;
      this.url = !!this[url] ? this[url] : '';
    }
  }
}
