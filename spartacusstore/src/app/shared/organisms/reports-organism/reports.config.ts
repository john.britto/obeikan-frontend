export const iframeUrls = {
        hrms: "https://app.powerbi.com/view?r=eyJrIjoiYjlhMDgwOGQtNDhjMC00MDE3LTljNzEtZTIxZmFkZTE1ZTgyIiwidCI6ImRkODc3MmU1LTF" +
                "iMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9",
        finance: "https://app.powerbi.com/view?r=eyJrIjoiMjY1NWI0ZjktOTNjZS00YmI2LTlhYjgtNTU0NDYyZDllMWFlIiwidCI6ImRkODc3MmU1LTFiMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9",
        collection: "https://app.powerbi.com/view?r=eyJrIjoiNzJjNjIyYjItZTcxMC00M2Y0LTk3ZWEtMDM0YWZlYmRhNTc4IiwidCI6ImRkODc3MmU1L" +
                "TFiMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9",
        expense: "https://app.powerbi.com/view?r=eyJrIjoiNGZlNTc1MDYtOThhYS00ZGRiLTkwZGYtYjEzZDZkYzdlMDNlIiwidCI6ImRkODc3MmU1LTFiMGY" +
                "tNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9",
        itbr: "https://app.powerbi.com/view?r=eyJrIjoiM2Q4ZTY3MjAtYTNhMS00NTYyLWFkMjgtMTkyODYwODg1NDJiIiwidCI6ImRkODc3MmU1LTFiMGYtNDY" +
                "4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9",
        payable: "https://app.powerbi.com/view?r=eyJrIjoiYjAxNWRjMDgtY2VjOS00ZmI0LWJkNjctOTUyODBmYjk1NzEwIiwidCI6ImRkODc3MmU1LTFiMGYtNDY" +
                "4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9",
        recievable: "https://app.powerbi.com/view?r=eyJrIjoiMDQ1ZDRmMjktODgxZS00ZGUyLWFiZGEtNjEwZDM2MzI1M2NlIiwidCI6ImRkODc3MmU1LTFiMGYtNDY" +
                "4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9",
        sales: "https://app.powerbi.com/view?r=eyJrIjoiYTMyYTA0YzEtNmI3OS00Y2NhLTgwYmMtYWQ3NTEyNmQzZTZhIiwidCI6ImRkODc3MmU1LTFiMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9",
        salesTile: 'https://app.powerbi.com/view?r=eyJrIjoiNTZiYWMxZjEtMTZlNy00MWFmLWI3YmYtZWU1NGFjN2QwMGI1IiwidCI6ImRkODc3MmU1LTFiMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9',
        inventoryTile: 'https://app.powerbi.com/view?r=eyJrIjoiMjhmYWM1MzEtMGY2OC00OTc1LWE2YzQtMzg0YTk5MmVlMmExIiwidCI6ImRkODc3MmU1LTFiMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9',
        payableTile: 'https://app.powerbi.com/view?r=eyJrIjoiOGIwY2M4NGYtZWU1Ny00ZGUzLWFjNWEtNzViNjM3MjEyZDJlIiwidCI6ImRkODc3MmU1LTFiMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9',
        receivableTile: 'https://app.powerbi.com/view?r=eyJrIjoiNTFkMGE0MGItOGIxYS00MWIxLWFjMDYtMWI4ZjMwNmFkZWU4IiwidCI6ImRkODc3MmU1LTFiMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9',
        fixedasset: 'https://app.powerbi.com/view?r=eyJrIjoiN2U5MmYxNGQtNzA3MC00NDAxLTljZWMtMjNkOTE2NTZiMjZkIiwidCI6ImRkODc3MmU1LTFiMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9',
        VATReports: 'https://app.powerbi.com/view?r=eyJrIjoiNmJhZTQ1ODktNGIyNi00NmQ3LWIxMDQtZDljYTQ3ZWFlMGI4IiwidCI6ImRkODc3MmU1LTFiMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9',
        APReport: 'https://app.powerbi.com/view?r=eyJrIjoiY2E2Nzk1YTQtMTU1Zi00Yjk5LWE0ODctNjZkN2MxNGVjN2ZjIiwidCI6ImRkODc3MmU1LTFiMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9',
        ARReport: 'https://app.powerbi.com/view?r=eyJrIjoiNjYzNTgwOTItM2IxZS00ZWRkLTljNTctMWM3ODU0NzY3ZDQ5IiwidCI6ImRkODc3MmU1LTFiMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9',
        lastMonthSalary: 'https://app.powerbi.com/view?r=eyJrIjoiMjUxZTAzZWQtYjEwNy00ZDFiLWE2MGItYjUxYmYxMDkzYzVkIiwidCI6ImRkODc3MmU1LTFiMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9',
        lastMonthOnvertime: 'https://app.powerbi.com/view?r=eyJrIjoiNDQ1YTNlNTMtZTU1Ny00MmUxLThjNjMtMTNiNWQ4M2Y3NjZhIiwidCI6ImRkODc3MmU1LTFiMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9',
        lastMonthHousingTravelExp: 'https://app.powerbi.com/view?r=eyJrIjoiYjRmNmZmYmYtNDI3Yi00NzJmLThjODAtYmVhOWFjOTg2YjY3IiwidCI6ImRkODc3MmU1LTFiMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9',
        variableExpenses: 'https://app.powerbi.com/view?r=eyJrIjoiNGI1YWJkZGEtOWFkYy00ODE1LWE4NGItMTUyN2Y1YWViZWFkIiwidCI6ImRkODc3MmU1LTFiMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9',
        noOfEmployees: 'https://app.powerbi.com/view?r=eyJrIjoiMjM4NGM5NTYtYzYyYi00MTg3LWExY2ItOGVmMjIwNTBhZWI5IiwidCI6ImRkODc3MmU1LTFiMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9',
        saudization: 'https://app.powerbi.com/view?r=eyJrIjoiNGM4NzU4MmYtMWQyMS00MmNiLTk2YWMtYjgzMjJkMzg4MDVmIiwidCI6ImRkODc3MmU1LTFiMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9',
        empOnDuty: 'https://app.powerbi.com/view?r=eyJrIjoiOGRlMjAxNGItZmIxNC00MmY1LThmMGQtNjlhN2NmYzFhYWY0IiwidCI6ImRkODc3MmU1LTFiMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9'  ,
        hrPayrollRegister: 'https://app.powerbi.com/view?r=eyJrIjoiMTM1NjBkZDEtYWJiNi00NTM5LWFmOWQtZTFhNDczM2UzYjg1IiwidCI6ImRkODc3MmU1LTFiMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9',
        inventoryOverview: 'https://app.powerbi.com/view?r=eyJrIjoiYmUzNjMwODQtYmQ3Zi00NzM0LWI4M2ItNjgxOTI1Mjc4MjY3IiwidCI6ImRkODc3MmU1LTFiMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9',
        inventoryReport: 'https://app.powerbi.com/view?r=eyJrIjoiZGNkMjAwNmItNTA3OS00OTcyLTg5NGMtNjhjYzFjNWQ1ODU1IiwidCI6ImRkODc3MmU1LTFiMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9',
        ledgerTransactionDetails: 'https://app.powerbi.com/view?r=eyJrIjoiZDU4MmMzZWEtY2RhNC00MmU5LTgzM2YtYWNkYmNkNDBlZjk5IiwidCI6ImRkODc3MmU1LTFiMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9',
        poReport: 'https://app.powerbi.com/view?r=eyJrIjoiYmUyMGI2NGMtZTRiZC00ZTYyLTlkMTItN2MzZmJlNmM4YjExIiwidCI6ImRkODc3MmU1LTFiMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9',
        soReport: 'https://app.powerbi.com/view?r=eyJrIjoiMjExNjgxYzQtMTZjYy00N2ViLWI0MWUtZDAzN2Q4ZTMwODcyIiwidCI6ImRkODc3MmU1LTFiMGYtNDY4ZS05MmFlLTFhMTQzODYzYmJkNyIsImMiOjl9'
}