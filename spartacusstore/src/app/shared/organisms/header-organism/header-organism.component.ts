import { Component, OnInit, HostListener, ElementRef } from '@angular/core';
import { CommonUtilityService } from '../../../services/common-utility-service';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-header-organism',
  templateUrl: './header-organism.component.html',
  styleUrls: ['./header-organism.component.scss'],
})
export class HeaderOrganismComponent implements OnInit {
  headerLinks: string[];
  headerNavLinks: string[];
  logoUrl: string;
  hostName: string = environment.hostName
  isExpanded: boolean = false
  
  constructor(private utilityService: CommonUtilityService,
    private el: ElementRef) {}

  ngOnInit() {
    const headerUrl = environment.headerEndpoint;
    this.utilityService.getRequest(headerUrl, '').subscribe(data => {
     
      const response = JSON.parse(JSON.stringify(data));
      if (
        response &&
        response.contentSlots &&
        response.contentSlots.contentSlot
      ) {
        for (const content of response.contentSlots.contentSlot) {
          if (content.slotId === 'HeaderInfoSlot') {
            this.headerLinks = content.components.component;
          } else if (content.slotId === 'SiteHeaderLogoSlot') {
            this.logoUrl = content.components.component[0].media.url;
          } else if (content.slotId === 'NavigationBarSlot') {
            this.headerNavLinks = content.components.component;
          }
        }
      }
    });
  }

  closeNav = () => {
    if(this.utilityService.isBrowser()) {
      document.getElementById('mySidenav').style.width = '0';
      document.getElementById('header-links').style.display = 'none'
      this.isExpanded = !this.isExpanded
    }    
  }

  openNav = () => {
     
    if(this.utilityService.isBrowser()) {
      document.getElementById('mySidenav').style.width = '200px';
      setTimeout(() => {
        document.getElementById('header-links').style.display = 'block'
      }, 400);
    }
  }

  toggleNav = () => {
    this.isExpanded ? this.closeNav() : this.openNav()
    this.isExpanded = !this.isExpanded
  }

  @HostListener('document:click', ['$event'])
  handleClick(event: Event) {
    if (!this.el.nativeElement.contains(event.target)) {
      if(this.isExpanded) this.closeNav()
    }
  }

}
