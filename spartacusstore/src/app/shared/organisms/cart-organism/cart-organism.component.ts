import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CartService, CART_UPDATE_FAILURE_MSG, UPDATE_CART } from '../../../services/cart.service'
import Swal from 'sweetalert2';

@Component({
  selector: 'app-cart-organism',
  templateUrl: './cart-organism.component.html',
  styleUrls: ['./cart-organism.component.scss']
})
export class CartOrganismComponent implements OnInit {

  cartData: any;
  constructor(
    private route: ActivatedRoute,
    private cartService: CartService,
    private matSnack: MatSnackBar
    ) {
      this.cartService.cartContext.subscribe(context => {
        if(context) {
          this.updateCart()
        }
      });
     }

  ngOnInit() {
    this.route.data.subscribe((data: { cartData: any }) => { console.log(data)
       this.cartData = data.cartData;
    });
  }

  updateCart = () => {
    this.cartService.getCart().subscribe((res: any) => { console.log(res)
      this.cartData = res
    },
    err => {
      Swal.fire({
        icon: 'error',
        text: CART_UPDATE_FAILURE_MSG,
        timer: 15000,
        showCloseButton: true, 
        cancelButtonText:
        '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
        showConfirmButton: false,
        background: 'black',
        toast: true
      })
    })
  }
}
