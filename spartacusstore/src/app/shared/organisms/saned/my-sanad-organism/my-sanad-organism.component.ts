import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../../../services/admin.service'
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-my-sanad-organism',
  templateUrl: './my-sanad-organism.component.html',
  styleUrls: ['./my-sanad-organism.component.scss']
})
export class MySanadOrganismComponent implements OnInit {
  public leaveticketrequest: boolean = false
  public leavecancelation: boolean = false
  public clearance: boolean = false
  public businesstripinternational: boolean = false
  public businesstrip: boolean = false
  public housingadvance: boolean = false
  public expenseclaim: boolean = false
  public leavemanagement: boolean = false
  public companyletters: boolean = false
  public cashadvance: boolean = false
  public resignationTyp: boolean = false
  public timecorrection: boolean = false
  public overTime: boolean = false
  public exitreentry: boolean = false
  public payRollAllToF: boolean = false
  public claimsAllToF: boolean = false
  public leavesAllToF: boolean = false
  public othersAllToF: boolean = false
  public OtShortHrs: boolean = false
  public onetimedeductions: boolean = false
  public onetimeearnings: boolean = false
  public recurringearnings: boolean = false
  public recurringloandeductions: boolean = false
  public wtAllToF: boolean = false
  public btAllToF: boolean = false





  constructor(private adminService: AdminService,private route: ActivatedRoute) {

    this.route
    .queryParams
    .subscribe(params => {
      this.adminService.userType = params['roleType']
    });
  }

  public expenseClaimImg: any = '../../assets/images/icons/hr/last_month_overtime_blue.png'
  public businessTripLocal: any = '../../assets/images/icons/hr/last_month_overtime_blue.png'
  public businessTripInternationalImg: any = '../../assets/images/icons/hr/last_month_overtime_blue.png'
  public leaveManagement: any = '../../assets/images/icons/hr/last_month_overtime_blue.png'
  public leaveManagementImg: any = '../../assets/images/icons/hr/last_month_overtime_blue.png'
  public leaveCancellation: any = '../../assets/images/icons/hr/last_month_overtime_blue.png'
  public leaveTicketRequest: any = '../../assets/images/icons/hr/last_month_overtime_blue.png'
  public clearanceImg: any = '../../assets/images/icons/hr/last_month_overtime_blue.png'
  public exitReentryVisaImg: any = '../../assets/images/icons/hr/last_month_overtime_blue.png'
  public housingAdvanceImg: any = '../../assets/images/icons/hr/last_month_overtime_blue.png'
  public cashAdvanceImg: any = '../../assets/images/icons/hr/last_month_overtime_blue.png'
  public companyLetters: any = '../../assets/images/icons/hr/last_month_overtime_blue.png'
  public resignation: any = '../../assets/images/icons/hr/last_month_overtime_blue.png'
  public overtimeImg: any = '../../assets/images/icons/hr/last_month_overtime_blue.png'
  public timeCorrectionImg: any = '../../assets/images/icons/hr/last_month_overtime_blue.png'
  public payrollImg: any = '../../assets/images/icons/hr/last_month_overtime_blue.png'
  public OneTimeDeductionsImg: any = '../../assets/images/icons/hr/last_month_overtime_blue.png'
  public ShortHrsUploa: any = '../../assets/images/icons/hr/last_month_overtime_blue.png'
  public RecurringLoan: any = '../../assets/images/icons/hr/last_month_overtime_blue.png'
  public OneTimeEarningsImg: any = '../../assets/images/icons/hr/last_month_overtime_blue.png'
  public RecurringEarningsImg: any = '../../assets/images/icons/hr/last_month_overtime_blue.png'
  ngOnInit() {
  }
  allFalse() {
    this.leaveticketrequest = false
    this.leavecancelation = false
    this.clearance = false
    this.businesstripinternational = false
    this.businesstrip = false
    this.housingadvance = false
    this.expenseclaim = false
    this.leavemanagement = false
    this.companyletters = false
    this.cashadvance = false
    this.resignationTyp = false
    this.timecorrection = false
    this.overTime = false
    this.exitreentry = false
    this.OtShortHrs = false
    this.onetimedeductions = false
    this.onetimeearnings = false
    this.recurringearnings = false
    this.recurringloandeductions = false
    this.payRollAllToF = false
    this.leavesAllToF = false
    this.claimsAllToF = false
    this.othersAllToF = false
    this.wtAllToF = false
    this.btAllToF = false
  }
  enableToTrue(x, pageTitle) {
    this.adminService.changeAdminPageHeaderContext(pageTitle)
    this.allFalse()
    if (x == 1) {
      this.claimsAllToF = true
      this.othersAllToF = false
      this.leavesAllToF = false
      this.payRollAllToF = false
      this.expenseclaim = true
    } else if (x == 2) {
      this.claimsAllToF = false
      this.othersAllToF = false
      this.leavesAllToF = false
      this.payRollAllToF = false
      this.businesstrip = true
      this.btAllToF = true
    } else if (x == 3) {
      this.claimsAllToF = false
      this.othersAllToF = false
      this.leavesAllToF = false
      this.payRollAllToF = false
      this.businesstripinternational = true
      this.btAllToF = true
    } else if (x == 4) {
      this.leavemanagement = true
      this.claimsAllToF = false
      this.othersAllToF = false
      this.leavesAllToF = true
      this.payRollAllToF = false
    } else if (x == 5) {
      this.claimsAllToF = false
      this.othersAllToF = false
      this.leavesAllToF = true
      this.payRollAllToF = false
      this.leavecancelation = true
    } else if (x == 6) {
      this.claimsAllToF = false
      this.othersAllToF = false
      this.leavesAllToF = true
      this.payRollAllToF = false
      this.leaveticketrequest = true
    } else if (x == 7) {
      this.clearance = true
      this.claimsAllToF = false
      this.othersAllToF = true
      this.leavesAllToF = false
      this.payRollAllToF = false
    } else if (x == 8) {
      this.claimsAllToF = false
      this.othersAllToF = false
      this.leavesAllToF = true
      this.payRollAllToF = false
      this.exitreentry = true
    } else if (x == 9) {
      this.claimsAllToF = true
      this.othersAllToF = false
      this.leavesAllToF = false
      this.payRollAllToF = false
      this.housingadvance = true
    } else if (x == 10) {
      this.claimsAllToF = true
      this.othersAllToF = false
      this.leavesAllToF = false
      this.payRollAllToF = false
      this.cashadvance = true
    } else if (x == 11) {
      this.claimsAllToF = false
      this.othersAllToF = true
      this.leavesAllToF = false
      this.payRollAllToF = false
      this.companyletters = true
    } else if (x == 12) {
      this.claimsAllToF = false
      this.othersAllToF = true
      this.leavesAllToF = false
      this.payRollAllToF = false
      this.resignationTyp = true
    } else if (x == 13) {
      this.claimsAllToF = false
      this.othersAllToF = false
      this.leavesAllToF = false
      this.payRollAllToF = false
      this.payRollAllToF = false
      this.overTime = true
      this.wtAllToF = true
      this.btAllToF = false
    } else if (x == 14) {
      this.claimsAllToF = false
      this.othersAllToF = false
      this.leavesAllToF = false
      this.payRollAllToF = false
      this.timecorrection = true
      this.wtAllToF = true
      this.btAllToF = false
    } else if (x == 16) {
      this.claimsAllToF = false
      this.othersAllToF = false
      this.leavesAllToF = false
      this.payRollAllToF = true
      this.OtShortHrs = true
    } else if (x == 17) {
      this.claimsAllToF = false
      this.othersAllToF = false
      this.leavesAllToF = false
      this.payRollAllToF = true
      this.recurringloandeductions = true
    } else if (x == 18) {
      this.claimsAllToF = false
      this.othersAllToF = false
      this.leavesAllToF = false
      this.payRollAllToF = true
      this.onetimedeductions = true
    } else if (x == 19) {
      this.claimsAllToF = false
      this.othersAllToF = false
      this.leavesAllToF = false
      this.payRollAllToF = true
      this.onetimeearnings = true
    } else if (x == 20) {
      this.claimsAllToF = false
      this.othersAllToF = false
      this.leavesAllToF = false
      this.payRollAllToF = true
      this.recurringearnings = true
    } else {
      this.allFalse()
    }

  }
  showPP: boolean = false;
  enableToTruePayRoll(pageTitle) {

    this.adminService.changeAdminPageHeaderContext(pageTitle)
    this.showEC = false
    this.showothers = false
    this.showLeaves = false
    this.showPP = !this.showPP;

    this.allFalse()
    if (this.payRollAllToF) {
      this.payRollAllToF = false
    } else {
      this.payRollAllToF = true
    }
  }

  showothers: boolean = false;
  enableToTrueOthers(pageTitle) {

    this.adminService.changeAdminPageHeaderContext(pageTitle)
    this.leavesAllToF = false
    this.showLeaves = false
    this.showPP = false
    this.showothers = !this.showothers;

    this.allFalse()
    if (this.othersAllToF) {
      this.othersAllToF = false
    } else {
      this.othersAllToF = true
    }
  }

  showEC: boolean = false;
  enableToTrueClaimReq(pageTitle) {

    this.adminService.changeAdminPageHeaderContext(pageTitle)
    this.showLeaves = false
    this.showPP = false
    this.showothers = false
    this.showEC = !this.showEC;

    this.allFalse()
    if (this.claimsAllToF) {
      this.claimsAllToF = false
    } else {
      this.claimsAllToF = true
    }
  }
  showLeaves: boolean = false;
  enableToTrueLeaves(pageTitle) {

    this.adminService.changeAdminPageHeaderContext(pageTitle)
    this.showEC = false
    this.showPP = false
    this.showothers = false
    this.showLeaves = !this.showLeaves;

    this.allFalse()
    if (this.leavesAllToF) {
      this.leavesAllToF = false
    } else {
      this.leavesAllToF = true
    }
  }

  showWT: boolean = false;
  enableToTrueWT(pageTitle) {

    this.adminService.changeAdminPageHeaderContext(pageTitle)
    this.showEC = false
    this.showPP = false
    this.showothers = false
    this.showLeaves = false
    this.showWT = !this.showWT
    this.showBT = false
    this.allFalse()
    if (this.wtAllToF) {
      this.wtAllToF = false
    } else {
      this.wtAllToF = true
    }
  }

  showBT: boolean = false;
  enableToTrueBT(pageTitle) {

    this.adminService.changeAdminPageHeaderContext(pageTitle)
    this.showEC = false
    this.showPP = false
    this.showothers = false
    this.showLeaves = false
    this.showWT = false
    this.showBT = !this.showBT

    this.allFalse()
    if (this.btAllToF) {
      this.btAllToF = false
    } else {
      this.btAllToF = true
    }
  }

}

