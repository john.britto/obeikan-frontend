import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home-organism',
  templateUrl: './home-organism.component.html',
  styleUrls: ['./home-organism.component.scss'],
})
export class HomeOrganismComponent implements OnInit {
  homeContent: any;
  sectionOneInfoContent: any
  sectionOneBannerContent: any
  sectionThreeVideoUrl: any
  sectionThreeBanner: any
  sectionThreeParaContent: any
  sanedPlansContent: any
  sanedPlanInfoContent: any
  carousalData: any
  carousalDesktopImages: any

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.data.subscribe((data: { homeData: any }) => {
      const response = data.homeData;
      
      if (
        response &&
        response.contentSlots &&
        response.contentSlots.contentSlot
      ) {
        for (const content of response.contentSlots.contentSlot) {
          if (content.slotId === 'Section3Slot') {
            for(const comp of content.components.component) {
              if(comp.uid === 'SanedFeaturesTabParagraphComponent') this.sectionThreeParaContent = comp
              if(comp.uid === 'SanedHomepageVideoComponent') this.sectionThreeVideoUrl = comp.media.url
              if(comp.uid === 'HomePageVideoBannerComponent') this.sectionThreeBanner = comp.media.url
            }
          }
          if(content.slotId === 'Section1Slot') {
            for(const comp of content.components.component) {
              if(comp.uid === 'HompageBannerComponent') this.sectionOneBannerContent = comp
              if(comp.uid === 'SanedSolutionTabParagraphComponent') this.sectionOneInfoContent = comp
            }
          }
          if(content.slotId === 'Section1BSlot') this.carousalData = content.components.component
          if(content.slotId === 'Section2BSlot') this.sanedPlansContent = content.components.component
          if(content.slotId === 'Section2ASlot') this.sanedPlanInfoContent = content.components.component[0]
          if(content.slotId === 'Section1CSlot') this.carousalDesktopImages = content.components.component
        }
      }
    });
  }
}
