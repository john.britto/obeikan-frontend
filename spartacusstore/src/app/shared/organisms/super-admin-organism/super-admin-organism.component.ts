import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonUtilityService } from '../../../services/common-utility-service';
import { environment } from 'src/environments/environment';
import {  HttpHeaders } from '@angular/common/http';
import { SuperAdminService } from '../../../services/super-admin.service'
@Component({
  selector: 'app-super-admin-organism',
  templateUrl: './super-admin-organism.component.html',
  styleUrls: ['./super-admin-organism.component.scss']
})
export class SuperAdminOrganismComponent implements OnInit {
  superAdminData: any;
  products = [];
  hideProductLinks: any = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private utilityService: CommonUtilityService,
    private superAdminService: SuperAdminService) {

    if (this.router.url === '/superAdmin/hrms' || this.router.url === '/superAdmin/finance') {
      this.hideProductLinks = true;
    } else {
      this.hideProductLinks = false;
    }

  }

  ngOnInit() {
    this.superAdminData = [];
    const codes = JSON.parse(this.utilityService.getCookie('superAdminOrders'));
    const orderAPI = environment.subscriptionAPI + this.utilityService.getLocalStorage('displayUid') + '/subscriptions/?view=active';
    // codes.map((code, i) => {
    //   if(isNaN(Number(code))) return;
      const url = orderAPI //+ code;
      const httpHeaders = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.utilityService.getCookie('AuthToken')}`);

      const options = {
        headers: httpHeaders
      };
      this.utilityService.getRequest(url, options).subscribe((res: any) => {
        if(res.subscriptions) {
          this.superAdminData = res.subscriptions;
          this.getProducts(this.superAdminData);
        }
        //if (i + 1 === codes.length) {
        //}
      });
    // });
  }
  getProducts(data) {
    data.map((each) => {
      // each.map((prod: any) => {
        this.products.push(each.productCode); 
      // })
    });
    this.superAdminService.setSuperAdminRoles(this.products)
  }
  urlChange(event) {
    this.hideProductLinks = event;
  }
}
