import { Component, OnInit, Input, SimpleChanges, OnChanges, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import Swal from 'sweetalert2';
import { AdminService } from '../../../services/admin.service';
import { appConfig } from '../../../app.config';

interface X {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-my-profile-molecule',
  templateUrl: './my-profile-molecule.component.html',
  styleUrls: ['./my-profile-molecule.component.scss']
})
export class MyProfileMoleculeComponent implements OnInit, OnChanges {

  @Input() pageType: any
  @Input() profileContext: any
  @Output() refreshData = new EventEmitter();
  profileInfoForm: FormGroup;
  contactInfoForm: FormGroup;
  emergencyInfoForm: FormGroup;
  familyInfoForm: FormGroup;
  educationInfoForm: FormGroup;
  passwordInfoForm: FormGroup;

  toggleProfileInfo: boolean = false
  toggleContactInfo: boolean = false
  toggleEmergencyInfo: boolean = false
  toggleFamilyInfo: boolean = false
  toggleEducationInfo: boolean = false
  togglePasswordInfo: boolean = true

  selection: X[] = [
    { value: 'education-0', viewValue: 'Education' },
    { value: 'profession-1', viewValue: 'Proffesional Accreditation' },
  ];

  isIncorrectPassword: boolean = false
  isPasswordMismatch: boolean = false

  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private cd: ChangeDetectorRef) { }

  get supply() { return this.familyInfoForm.controls; }
  get familyInfoArr() { return this.supply.familyInfoArray as FormArray; }

  get emergencySupply() { return this.emergencyInfoForm.controls; }
  get emergencyInfoArr() { return this.emergencySupply.emergencyInfoArray as FormArray; }

  get list() { return this.educationInfoForm.controls; }
  get educationInfoArr() { return this.list.educationInfoArray as FormArray; }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.profileContext) {
      this.ngOnInit()
    }
  }

  ngOnInit() {

    if (this.pageType === 'personalInfo') {
      this.profileInfoForm = this.fb.group({
        employeeName: [{value: (!!this.profileContext && !!this.profileContext.name) ? this.profileContext.name : '' , disabled: true} , [Validators.required]],
        jobTitle: [(!!this.profileContext && !!this.profileContext.jobTilte) ? this.profileContext.jobTilte : '', [Validators.required]],
        lastName: [(!!this.profileContext && !!this.profileContext.lastName) ? this.profileContext.lastName : '', [Validators.required]],
        firstName: [(!!this.profileContext && !!this.profileContext.firstName) ? this.profileContext.firstName : '', [Validators.required]],
        middleName: [(!!this.profileContext && !!this.profileContext.middleName) ? this.profileContext.middleName : '', [Validators.required]],
      });
    }

    if (this.pageType === 'contactInfo') {
      this.contactInfoForm = this.fb.group({
        mobileNumber: [(!!this.profileContext && !!this.profileContext.mobileNumber) ? this.profileContext.mobileNumber : '', [Validators.required]],
        homePhoneNumber: [(!!this.profileContext && !!this.profileContext.homePhoneNumber) ? this.profileContext.homePhoneNumber : '', [Validators.required]],
        homeCountryPhone: [(!!this.profileContext && !!this.profileContext.homeCountryNumber) ? this.profileContext.homeCountryNumber : '', [Validators.required]],
        personalEmail: [{value: (!!this.profileContext && !!this.profileContext.emailAddress) ? this.profileContext.emailAddress : '', disabled: true}, [Validators.required]],
        residentialAddress: [(!!this.profileContext && !!this.profileContext.residentialAddresss) ? this.profileContext.residentialAddresss : '', [Validators.required]],
        mailingAddress: [(!!this.profileContext && !!this.profileContext.mailingAddress) ? this.profileContext.mailingAddress : '', [Validators.required]],
      });
    }

    if (this.pageType === 'familyInfo') {
      this.familyInfoForm = this.fb.group({
        familyInfoArray: new FormArray([])
      })
      if (!!this.profileContext && !!this.profileContext.familyInfos && this.profileContext.familyInfos.length > 0) {
        this.profileContext.familyInfos.forEach(info => {
          this.addFamilyInfo(info)
        });
      } else this.addFamilyInfo()
    }

    if (this.pageType === 'emergency') {
      this.emergencyInfoForm = this.fb.group({
        emergencyInfoArray: new FormArray([])
      })
      if (!!this.profileContext && !!this.profileContext.emergencyContactInfos && this.profileContext.emergencyContactInfos.length > 0) {
        this.profileContext.emergencyContactInfos.forEach(info => {
          this.addEmergencyInfo(info)
        });
      } else this.addEmergencyInfo()
    }

    if (this.pageType === 'educationInfo') {
      this.educationInfoForm = this.fb.group({
        educationInfoArray: new FormArray([])
      })

      if (!!this.profileContext && !!this.profileContext.educationInfos && this.profileContext.educationInfos.length > 0) {
        this.profileContext.educationInfos.forEach(info => {
          this.addEducationInfo(info, 'Education')
        });
      } else this.addEducationInfo(null, '')

      if (!!this.profileContext && !!this.profileContext.professionalInfos && this.profileContext.professionalInfos.length > 0) {
        this.profileContext.professionalInfos.forEach(info => {
          this.addEducationInfo(info, 'Proffesional Accreditation')
        });
      }
    }

    if (this.pageType === 'password') {
      this.passwordInfoForm = this.fb.group({
        currentPassword: ['', [Validators.required]],
        newPassword: ['', [Validators.required]],
        confirmNewPassword: ['', [Validators.required]],
      });
    }
  }


  addFamilyInfo = (info?: any) => {

    let newRow = this.fb.group({
      fullName: [(!!info && !!info.fullName) ? info.fullName : '', [Validators.required]],
      relationship: [(!!info && !!info.relationship) ? info.relationship : '', [Validators.required]],
      age: [(!!info && !!info.age) ? info.age : '', [Validators.required]],
    });

    this.familyInfoArr.push(newRow);
  }

  removeFamilyInfo = i => this.familyInfoArr.removeAt(i);

  addEmergencyInfo = (info?: any) => {

    let newRow = this.fb.group({
      fullName: [(!!info && !!info.fullName) ? info.fullName : '', [Validators.required]],
      relationship: [(!!info && !!info.relationship) ? info.relationship : '', [Validators.required]],
      mobileNumber: [(!!info && !!info.mobileNumber) ? info.mobileNumber : '', [Validators.required]],
      phoneNumber: [(!!info && !!info.phoneNumber) ? info.phoneNumber : '', [Validators.required]]
    });

    this.emergencyInfoArr.push(newRow);
  }

  removeEmergencyInfo = i => this.emergencyInfoArr.removeAt(i);


  addEducationInfo = (info?: any, type: any = 'Education') => {

    let newRow = this.fb.group({
      educationType: [(!!info && type === 'Education') ? 'Education' : type === 'Proffesional Accreditation' ? 'Proffesional Accreditation' : ''],
      education: [(!!info && !!info.educationName) ? info.educationName : '', (type === 'Education') ? [Validators.required] : null],
      professionalAccreditation: [(!!info && !!info.educationName) ? info.educationName : '', (type === 'Proffesional Accreditation') ? [Validators.required] : null],
      relationship: [(!!info && !!info.relationship) ? info.relationship : '', [Validators.required]],
      instituteName: [(!!info && !!info.instituteName) ? info.instituteName : '', [Validators.required]],
      yearCompleted: [(!!info && !!info.yearCompleted) ? info.yearCompleted : '', [Validators.required]],
    });

    this.educationInfoArr.push(newRow);
  }

  removeEducationInfo = i => this.educationInfoArr.removeAt(i);

  changeEducationType = (i, type) => {
    if (type === 'Education') {
      this.educationInfoArr.at(i).get('education').setValidators([Validators.required]);
      this.educationInfoArr.at(i).get('professionalAccreditation').clearValidators();
    } else if (type === 'Proffesional Accreditation') {
      this.educationInfoArr.at(i).get('professionalAccreditation').setValidators([Validators.required]);
      this.educationInfoArr.at(i).get('education').clearValidators();
    }
    this.educationInfoArr.at(i).get('education').updateValueAndValidity()
    this.educationInfoArr.at(i).get('professionalAccreditation').updateValueAndValidity()
  }

  validateCurrentPwd = () => {
    if (this.passwordInfoForm.get('currentPassword').value !== this.profileContext.name) {
      this.isIncorrectPassword = true
    } else this.isIncorrectPassword = false
  }

  onConfirmPassword = () => {
    if (this.passwordInfoForm.get('newPassword').value !== this.passwordInfoForm.get('confirmNewPassword').value) {
      this.isPasswordMismatch = true
    } else this.isPasswordMismatch = false
  }

  onSubmitProfileInfo = () => {

    const { employeeName, jobTitle, lastName, firstName, middleName } = this.profileInfoForm.getRawValue()

    let payload = {}

    payload['emailAddress'] = this.profileContext.emailAddress
    payload['name'] = employeeName
    payload['jobTilte'] = jobTitle
    payload['lastName'] = lastName
    payload['firstName'] = firstName
    payload['middleName'] = middleName

    this.submitRequest(payload)
  }

  onSubmitContactInfo = () => {

    const { mobileNumber, homePhoneNumber, homeCountryPhone, personalEmail, residentialAddress, mailingAddress } = this.contactInfoForm.getRawValue()

    let payload = {}

    payload['emailAddress'] = this.profileContext.emailAddress
    payload['mobileNumber'] = mobileNumber
    payload['homePhoneNumber'] = homePhoneNumber
    payload['homeCountryNumber'] = homeCountryPhone
    payload['emailAddress'] = personalEmail
    payload['mailingAddress'] = mailingAddress
    payload['residentialAddresss'] = residentialAddress

    this.submitRequest(payload)
  }

  onSubmitEmergencyInfo = () => {

    let emergencyDetails = []

    this.emergencyInfoArr.getRawValue().map((info) => {
      const data = {...info}
      emergencyDetails.push(data)
    })

    let payload = {}

    payload['emailAddress'] = this.profileContext.emailAddress
    payload['emergencyContactInfos'] = emergencyDetails

    this.submitRequest(payload)
  }

  onSubmitFamilyInfo = () => {

    let familyDetails = []

    this.familyInfoArr.getRawValue().map((info) => {
      const data = {...info}
      familyDetails.push(data)
    })

    let payload = {}

    payload['emailAddress'] = this.profileContext.emailAddress
    payload['familyInfos'] = familyDetails

    this.submitRequest(payload)
  }

  onSubmitEducationInfo = () => {

    let educationDetails = []
    let professionalDetails = []

    this.educationInfoArr.getRawValue().map((info) => {

      if(info.educationType === 'Education') {
        const data = {
          educationName: info.education,
          instituteName: info.instituteName,
          relationship: info.relationship,
          yearCompleted: info.yearCompleted
          }
          educationDetails.push(data)
      } if(info.educationType === 'Proffesional Accreditation') {
        const data = {
          educationName: info.professionalAccreditation,
          instituteName: info.instituteName,
          relationship: info.relationship,
          yearCompleted: info.yearCompleted
          }
          professionalDetails.push(data)
      }    
    })

    let payload = {}

    payload['emailAddress'] = this.profileContext.emailAddress

    payload['educationInfos'] = educationDetails
    payload['professionalInfos'] = professionalDetails

    this.submitRequest(payload)
  }

  onResetPassword = () => {
    const { currentPassword, newPassword  } = this.passwordInfoForm.getRawValue()

    this.adminService.resetPassword(currentPassword, newPassword).subscribe((res: any) => {
      this.dialogMsg('success', 'Password has been reset successfully')
      this.ngOnInit()
    },
    err => {
      if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.dialogMsg('error', err.error.errors[0].message)
      } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    })
  }

  submitRequest = payload => {
    this.adminService.hrPutRequest('employee-info', payload).subscribe((res: any) => {
      if (!!res) {
        this.dialogMsg('success', 'Profile has been updated successfully')
        this.toggleView()
        this.refreshData.emit(true)
      }
    },
      err => {
        if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.dialogMsg('error', err.error.errors[0].message)
        } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      })
  }

  toggleView = () => {
    this.toggleProfileInfo = false
    this.toggleContactInfo = false
    this.toggleEmergencyInfo = false
    this.toggleFamilyInfo = false
    this.toggleEducationInfo = false
    this.togglePasswordInfo = true
  }

  dialogMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true,
      cancelButtonText:
        '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}
