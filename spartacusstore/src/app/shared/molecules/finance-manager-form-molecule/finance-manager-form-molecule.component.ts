import { Component, OnInit, OnChanges, Input } from '@angular/core';
import {
  FormBuilder,
  FormGroup
} from '@angular/forms';
import {
  SuperAdminService
} from '../../../services/super-admin.service';
import Swal from 'sweetalert2';
import { appConfig } from 'src/app/app.config';

@Component({
  selector: 'app-finance-manager-form-molecule',
  templateUrl: './finance-manager-form-molecule.component.html',
  styleUrls: ['./finance-manager-form-molecule.component.scss']
})
export class FinanceManagerFormMoleculeComponent implements OnInit, OnChanges {
  @Input() excelValues: any;
  @Input() userId: any;
  itemUpload: FormGroup;
  bpTransUpload: FormGroup;
  openingBalance: FormGroup;
  buttonName: any = 'Submit';
  constructor(private fb: FormBuilder,
    private superAdminService: SuperAdminService) { }

  ngOnInit() {
    this.itemUpload = this.fb.group({
      itemCode: [null],
      itemDescription: [null],
      itemGroup: [null],
      warehouse: [null],
      inventoryUnit: [null],
      quantity: [null],
      price: [null]
    });

    this.bpTransUpload = this.fb.group({
      documentNumber: [null],
      documentDate: [null],
      businessPartner: [null],
      invoiceNumber: [null],
      currency: [null],
      amountInInvoiceCurrency: [null],
      amountInHC: [null],
      balanceAmount: [null],
      reference: [null],
    });

    this.openingBalance = this.fb.group({
      date: [null],
      ledger: [null],
      dr: [null],
      amount: [null],
      reference: [null]
    });
  }

  ngOnChanges() {
  }

  checkValues(val) {
    return val ? val + '' : '';
  }

  onSubmitForm = () => {

    if (
      this.itemUpload.valid &&
      this.bpTransUpload.valid &&
      this.openingBalance.valid) {
      const itemInput = this.itemUpload.value;
      const bpInput = this.bpTransUpload.value;
      const obInput = this.openingBalance.value;
      const request = {
        itemUpload:
          [{
            itemCode: this.checkValues(itemInput.itemCode),
            itemDescription: this.checkValues(itemInput.itemDescription),
            itemGroup: this.checkValues(itemInput.itemGroup),
            warehouse: this.checkValues(itemInput.warehouse),
            inventoryUnit: this.checkValues(itemInput.inventoryUnit),
            quantity: this.checkValues(itemInput.quantity),
            price: this.checkValues(itemInput.price),
          }],
        bpTransUpload:
        {
          documentNumber: this.checkValues(bpInput.documentNumber),
          documentDate: this.checkValues(bpInput.documentDate),
          businessPartner: this.checkValues(bpInput.businessPartner),
          invoiceNumber: this.checkValues(bpInput.invoiceNumber),
          currency: this.checkValues(bpInput.currency),
          amountInInvoiceCurrency: this.checkValues(bpInput.amountInInvoiceCurrency),
          amountInHC: this.checkValues(bpInput.amountInHC),
          balanceAmount: this.checkValues(bpInput.balanceAmount),
          reference: this.checkValues(bpInput.reference),
        },
        openingBalance:
        {
          date: this.checkValues(obInput.date),
          ledger: this.checkValues(obInput.ledger),
          dr: this.checkValues(obInput.dr),
          amount: this.checkValues(obInput.amount),
          reference: this.checkValues(obInput.reference)
        }

      };

      this.superAdminService.submitFinanceManagerForm(request, this.userId).subscribe((res: any) => {
        if (res) {
          this.dialogMsg('success', appConfig.apiResponseMessages.genericSuccessMsg)
        } else {
          this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
        }
      },
        err => {
          if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
            this.dialogMsg('error', err.error.errors[0].message)
          } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
        });

    }
  }

  dialogMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}
