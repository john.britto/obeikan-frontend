import { Component, OnInit, Input, SimpleChanges, OnChanges, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import Swal from 'sweetalert2';
import { appConfig } from '../../../app.config';
import { AdminService } from '../../../services/admin.service';

@Component({
  selector: 'app-company-molecule',
  templateUrl: './company-molecule.component.html',
  styleUrls: ['./company-molecule.component.scss']
})
export class CompanyMoleculeComponent implements OnInit, OnChanges {

  @Input() pageType: any
  @Input() companyContext: any
  @Output() refreshData = new EventEmitter();
  companyInfoForm: FormGroup;
  contactInfoForm: FormGroup;
  invoiceTemplates: any = []
  statementTemplates: any = []
  reportTemplates: any = []

  toggleCompanyInfo: boolean = false
  toggleContactInfo: boolean = false
  toggleUploadInfo: boolean = false
  
  constructor(private fb: FormBuilder, private adminService: AdminService) { }

  get supply() { return this.companyInfoForm.controls; }
  get branchOfficeArr() { return this.supply.branchOfficeArray as FormArray; }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.companyContext) {
      this.ngOnInit()
    }
  }

  ngOnInit() {

    if(this.pageType === 'companyInfo') {

      this.companyInfoForm = this.fb.group({
        companyFullName: [{value: (!!this.companyContext && !!this.companyContext.name) ? this.companyContext.name : '', disabled: true}, [Validators.required]],
        headOfficeAddress: [(!!this.companyContext && !!this.companyContext.contactAddress && !!this.companyContext.contactAddress.streetname) ? this.companyContext.contactAddress.streetname : '', [Validators.required]],
        branchOfficeArray: new FormArray([])
      });

      if (!!this.companyContext && !!this.companyContext.contactAddress && this.companyContext.contactAddress.branchOfficeAddresses.length > 0) {
        this.companyContext.contactAddress.branchOfficeAddresses.forEach(info => {
          this.addNewBranchInfo(info)
        });
      } else this.addNewBranchInfo()
    }

    if(this.pageType === 'contactInfo') {
      this.contactInfoForm = this.fb.group({
        phoneNumber: [(!!this.companyContext && !!this.companyContext.contactAddress && !!this.companyContext.contactAddress.cellphone) ? this.companyContext.contactAddress.cellphone : '', [Validators.required]],
        fax: [(!!this.companyContext && !!this.companyContext.contactAddress && !!this.companyContext.contactAddress.fax) ? this.companyContext.contactAddress.fax : '', [Validators.required]],
        autorizedEmail: [(!!this.companyContext && !!this.companyContext.contactAddress && !!this.companyContext.contactAddress.email) ? this.companyContext.contactAddress.email : '', [Validators.required]],
        mailingAddress: [(!!this.companyContext && !!this.companyContext.contactAddress && !!this.companyContext.contactAddress.mailingAddress) ? this.companyContext.contactAddress.mailingAddress : '', [Validators.required]],
      });
    }
  }

  addNewBranchInfo = (addr?: any) => {

    let newRow = this.fb.group({
      branchOfficeAddress: [!!addr ? addr : '', [Validators.required]]
    });

    this.branchOfficeArr.push(newRow);
  }

  removeBranchInfo = i => this.branchOfficeArr.removeAt(i);

  onSubmitCompanyInfo = () => {

    const { companyFullName, headOfficeAddress } = this.companyInfoForm.getRawValue()

    let branchAddrDetails = []

    this.branchOfficeArr.getRawValue().map((info) => {
      const data = info.branchOfficeAddress
      branchAddrDetails.push(data)
    })

    let payload = {
      ...this.companyContext
    }

    payload['contactAddress'] = {
      ...payload.contactAddress,
      branchOfficeAddresses: branchAddrDetails,
      streetname: headOfficeAddress
    }
    payload['name'] = companyFullName

    this.submitRequest(payload)
  }

  onSubmitContactInfo = () => {

    const { phoneNumber, fax, autorizedEmail, mailingAddress } = this.contactInfoForm.getRawValue()

    let payload = {
      ...this.companyContext
    }

    payload['contactAddress'] = {
      ...payload.contactAddress,
      cellphone: phoneNumber,
      fax: fax,
      email: autorizedEmail,
      mailingAddress: mailingAddress
    }

    this.submitRequest(payload)
  }

  submitRequest = payload => {
    this.adminService.updateCompanyRequest('company', payload).subscribe((res: any) => {
      if (!!res) {
        this.dialogMsg('success', 'Profile has been updated successfully')
        this.toggleView()
        this.refreshData.emit(true)
      }
    },
      err => {
        if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.dialogMsg('error', err.error.errors[0].message)
        } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      })
  }

  toggleView = () => {
    this.toggleCompanyInfo = false
    this.toggleContactInfo = false
    this.toggleUploadInfo = false
  }

  dialogMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true,
      cancelButtonText:
        '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }

  onSelectTemplate(event: any, type) {
    if(type === 'invoice') this.invoiceTemplates = event.target.files[0]
    if(type === 'statement') this.statementTemplates = event.target.files[0]
    if(type === 'report') this.reportTemplates = event.target.files[0]
  }

  removeSelectedTemplate = type => {
    if(type === 'invoice') this.invoiceTemplates = undefined
    if(type === 'statement') this.statementTemplates = undefined
    if(type === 'report') this.reportTemplates = undefined
  }

  onUploadTemplate = () => {
    const payload = {
      invoice: this.invoiceTemplates,
      statement: this.statementTemplates,
      report: this.reportTemplates
    }
    this.adminService.uploadCompanyDpRequest('company/template-upload', payload).subscribe((res: any) => {
    this.invoiceTemplates = undefined
    this.statementTemplates = undefined
    this.reportTemplates = undefined
      this.dialogMsg('success', 'template(s) has been uploaded successfully')
    },
    err => {
      if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.dialogMsg('error', err.error.errors[0].message)
      } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    })
  }

}
