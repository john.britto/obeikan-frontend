import { Component, OnInit, Input } from '@angular/core';
import { CommonUtilityService } from '../../../services/common-utility-service'
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-approver-details-molecule',
  templateUrl: './approver-details-molecule.component.html',
  styleUrls: ['./approver-details-molecule.component.scss']
})
export class ApproverDetailsMoleculeComponent implements OnInit {

  @Input() detailedInfo: any;
  isBpTransUploadInfoDataAvailable: any = false
  bpTransUploadInfoDisplayedColumn: any
  bpTransUploadInfoTableData: any
  isCustomerInfoDataAvailable: any = false
  customerInfoDisplayedColumn: any
  customerInfoTableData: any
  isItemInfoDataAvailable: any = false
  itemInfoDisplayedColumn: any
  itemInfoTableData: any
  isItemUploadInfoDataAvailable: any = false
  itemUploadInfoDisplayedColumn: any
  itemUploadInfoTableData: any
  isOpeningBalanceInfoDataAvailable: any = false
  openingBalanceInfoDisplayedColumn: any
  openingBalanceInfoTableData: any
  isSupplierInfoDataAvailable: any = false
  supplierInfoDisplayedColumn: any
  supplierInfoTableData: any
  fixedAssetTableData: any
  isFixedAssetTableData: boolean = false;
  fixedAssetDisplayedColumn: any;
  fixedAssetInfoTableData: any;
  iswarehouseTableData: boolean = false;
  warehouseDisplayedColumn: any;
  warehouseInfoTableData: any;
  isDimensionTableData: boolean = false;
  dimensionDisplayedColumn: any;
  dimensionTableData: any;
  isGLTableData: boolean = false;
  glDisplayedColumn: any;
  glTableData: any;
  downloadLink: any = environment.hostName;
  isPeriodTableData: boolean = false;
  periodDisplayedColumn: any;
  periodTableData: any;
  isVATTableData: boolean = false;
  vatDisplayedColumn: any;
  vatTableData: any;
  isSalesRepTableData: boolean = false;
  SalesDisplayedColumn: any;
  SalesTableData: any;

  constructor(private utilityService: CommonUtilityService) { }

  ngOnInit() {
    if(!!this.detailedInfo.bpTransUploadInfo) {
      this.isBpTransUploadInfoDataAvailable = true
      this.bpTransUploadInfoDisplayedColumn = ['invoiceNumber', 'reference', 'documentNumber', 'documentDate', 'businessPartner', 'amountInInvoiceCurrency', 'balanceAmount']
      this.bpTransUploadInfoTableData = this.detailedInfo.bpTransUploadInfo
    }
    if(!!this.detailedInfo.customerInfo) {
      this.isCustomerInfoDataAvailable = true
      this.customerInfoDisplayedColumn = ['name', 'vatNumber', 'bpCode', 'currency', 'country', 'city']
      this.customerInfoTableData = this.detailedInfo.customerInfo
    }
    if(!!this.detailedInfo.itemInfo) {
      this.isItemInfoDataAvailable = true
      this.itemInfoDisplayedColumn = ['itemCode', 'itemType', 'itemGroup', 'inventoryUnit', 'description', 'poPrice', 'soPrice', 'currency']
      this.itemInfoTableData = this.detailedInfo.itemInfo
    }
    if(!!this.detailedInfo.itemUploadInfo) {
      this.isItemUploadInfoDataAvailable = true
      this.itemUploadInfoDisplayedColumn = ['itemCode', 'itemDescription', 'itemGroup', 'inventoryUnit', 'poPrice', 'soPrice']
      this.itemUploadInfoTableData = this.detailedInfo.itemUploadInfo
    }
    if(!!this.detailedInfo.openingBalanceInfo) {
      this.isOpeningBalanceInfoDataAvailable = true
      this.openingBalanceInfoDisplayedColumn = ['uid', 'reference', 'ledger', 'dr', 'date', 'amount']
      this.openingBalanceInfoTableData = this.detailedInfo.openingBalanceInfo
    }
    if(!!this.detailedInfo.supplierInfo) {
      this.isSupplierInfoDataAvailable = true
      this.supplierInfoDisplayedColumn = ['name', 'bpCode', 'vatNumber', 'financialGroup', 'currency', 'country', 'city']
      this.supplierInfoTableData = this.detailedInfo.supplierInfo
    }
    if(!!this.detailedInfo.asset) {
      this.isFixedAssetTableData = true
      this.fixedAssetDisplayedColumn = ['assetCode', 'assetExtension', 'assetName', 'category', 'assetDescription', 'group', 'purchaseDate', 'serviceDate', 'transactionCost', 'transactionCurrency']
      this.fixedAssetInfoTableData = this.detailedInfo.asset
    }
    if(!!this.detailedInfo.warehouse) {
      this.iswarehouseTableData = true
      this.warehouseDisplayedColumn = ['SNo', 'warehouseCode', 'description']
      this.warehouseInfoTableData = this.detailedInfo.warehouse
    }
    if(!!this.detailedInfo.dimension) {
      this.isDimensionTableData = true
      this.dimensionDisplayedColumn = ['dimensionCode', 'description', 'subLevel', 'dimensionType', 'parentDimension']
      this.dimensionTableData = this.detailedInfo.dimension
    }
    if(!!this.detailedInfo.period) {
      this.isPeriodTableData = true
      this.periodDisplayedColumn = ['periodType', 'year', 'period', 'startDate', 'periodDescription', 'correctionPeriod', 'status1', 'status2', 'status3', 'status4', 'status5']
      this.periodTableData = this.detailedInfo.period
    }
    if(!!this.detailedInfo.ledgerAccount) {
      this.isGLTableData = true
      this.glDisplayedColumn = ['accountNumber', 'description', 'searchKey', 'subLevel', 'parentLedger', 'accountType', 'type', 'status', 'dimension1', 'dimension2', 'dimension3', 'dimension4', 'dimension5']
      this.glTableData = this.detailedInfo.ledgerAccount
    }
    if(!!this.detailedInfo.financeVAT) {
      this.isVATTableData = true
      this.vatDisplayedColumn = ['vatCode', 'vatPercentage']
      this.vatTableData = this.detailedInfo.financeVAT
    }
    if(!!this.detailedInfo.salesRep) {
      this.isSalesRepTableData = true
      this.SalesDisplayedColumn = ['Code', 'Description']
      this.SalesTableData = this.detailedInfo.salesRep
    }
  }

  downloadFile = () => {
    if(this.utilityService.isBrowser()) {
      window.open(`${this.downloadLink}${this.detailedInfo.attachment}`, '_self')
    }
  }

}
