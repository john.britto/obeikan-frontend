import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { MyProfileService } from '../../../../services/my-profile.service'
import { CommonUtilityService } from '../../../../services/common-utility-service'
import { FinanceService } from '../../../../services/finance.service'
import { AdminService } from '../../../../services/admin.service'

@Component({
  selector: 'app-operation-history-molecule',
  templateUrl: './operation-history-molecule.component.html',
  styleUrls: ['./operation-history-molecule.component.scss']
})
export class OperationHistoryMoleculeComponent implements OnInit, OnChanges {

  @Input() processType: any
  @Input() page: any
  tableData: any = {}
  taboneLabel: any
  tabTwoLabel: any
  tabThreeLabel: any
  tabFourLabel: any
  tabFiveLabel: any
  tabSixLabel: any
  tabSevenLabel: any
  tabEightLabel: any
  tabNineLabel: any
  tabTenLabel: any
  tabEleLabel: any
  tabTwelveLabel: any
  tabOneThreeLabel: any
  tabOneFourLabel: any
  tabFourData: any = []
  tabFiveData: any = []
  tabSixData: any = []
  tabSevenData: any = []
  tabEightData: any = []
  tabNineData: any = []
  tabTenData: any = []
  tabEleData: any = []
  tabTwelveData: any = []
  tabOneThreeData: any = []
  tabOneFourData: any = []
  poData: any = []
  materialReceivedData: any = []
  paymentData: any = []
  isDataAvailable: any
  displayedColumn: any
  showGL: boolean = false
  displayedColumnWithoutAmt: any[] = []
  donotShowAmt: boolean = false

  constructor(
    private myProfileService: MyProfileService,
    private utilityService: CommonUtilityService,
    private financeService: FinanceService,
    private adminService: AdminService) {
    this.adminService.refreshDashboard.subscribe(data => {
      this.ngOnInit()
    })
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.page) {
      const customerRoles = JSON.parse(this.utilityService.getCookie('customerRoles'))

      if (!!customerRoles &&
        customerRoles.length === 1 &&
        customerRoles.includes('HREmployee') &&
        (this.page === 'PR' || this.page === 'RP')) {
        this.resetData()
      } else {
        this.ngOnInit()
      }
    }
  }

  ngOnInit() {

    this.displayedColumn = ['code', 'user.name', 'created', 'currentApprover', 'totalPriceWithTax.formattedValue', 'status'];
    this.displayedColumnWithoutAmt = ['code', 'user.name', 'created', 'currentApprover', 'status'];
    this.fetchData();

  }

  fetchData = (e?: any) => {
    if (this.page === 'AP') {
      this.resetData()
      this.myProfileService.getReviewOrders('ACCOUNTS_PAYABLE').subscribe((res: any) => {
        this.tableData = res
        if (Object.keys(this.tableData).length > 0) {
          this.isDataAvailable = true
          this.donotShowAmt = false
          if (!this.utilityService.isNullOrUndefined(this.tableData.tab1)) {
            this.taboneLabel = 'Purchase Order'
            this.poData = this.tableData.tab1
            this.poData.activeTab = 'purchase-order'
          }

          if (!this.utilityService.isNullOrUndefined(this.tableData.tab2)) {
            this.tabTwoLabel = 'Material Received'
            this.materialReceivedData = this.tableData.tab2
            this.materialReceivedData.activeTab = 'material-received'
            this.donotShowAmt = true

            // this.displayedColumn = ['code', 'user.name', 'created', 'currentApprover', 'status'];
          }

          if (!this.utilityService.isNullOrUndefined(this.tableData.tab3)) {
            this.tabFourLabel = 'Invoice Booking'
            this.tabFourData = this.tableData.tab3
            this.tabFourData.activeTab = 'invoice-booking'
          }

          if (!this.utilityService.isNullOrUndefined(this.tableData.tab4)) {
            this.tabThreeLabel = 'Payment'
            this.paymentData = this.tableData.tab4
            this.paymentData.activeTab = 'payments'
          }
        } else {
          this.isDataAvailable = false
          this.donotShowAmt = false
        }
      },
        err => {
          this.isDataAvailable = false
          this.donotShowAmt = false
        })
    } else if (this.page === 'AR') {
      this.resetData()

      this.myProfileService.getReviewOrders('ACCOUNTS_RECEIVABLE').subscribe((res: any) => {
        this.tableData = res
        if (Object.keys(this.tableData).length > 0) {
          this.isDataAvailable = true
          this.donotShowAmt = false
          if (!this.utilityService.isNullOrUndefined(this.tableData.tab1)) {
            this.taboneLabel = 'Sales Orders'
            this.poData = this.tableData.tab1
            this.poData.activeTab = 'sales-order'
          }

          if (!this.utilityService.isNullOrUndefined(this.tableData.tab2)) {
            this.tabTwoLabel = 'Material Shipped'
            this.materialReceivedData = this.tableData.tab2
            this.materialReceivedData.activeTab = 'material-shipped'
            this.donotShowAmt = true

            //this.displayedColumn = ['code', 'user.name', 'created', 'currentApprover', 'status'];
          }

          if (!this.utilityService.isNullOrUndefined(this.tableData.tab3)) {
            this.tabFourLabel = 'Invoice Booking'
            this.tabFourData = this.tableData.tab3
            this.tabFourData.activeTab = 'invoice-booking'
          }

          if (!this.utilityService.isNullOrUndefined(this.tableData.tab4)) {
            this.tabThreeLabel = 'Collections'
            this.paymentData = this.tableData.tab4
            this.paymentData.activeTab = 'payments'
          }
        } else {
          this.isDataAvailable = false
          this.donotShowAmt = false
        }
      },
        err => {
          this.isDataAvailable = false
          this.donotShowAmt = false
        })
    } else if (this.page === 'GL') {
      this.resetData()
      this.financeService.getGLData('financial-entry').subscribe((res: any) => {
        this.tableData = res.financialEntries
        if (!!this.tableData && this.tableData.length > 0) {
          this.isDataAvailable = true
          this.donotShowAmt = false

          if (!this.utilityService.isNullOrUndefined(this.tableData)) {
            this.showGL = true
            this.taboneLabel = 'General Ledger'
            this.poData = this.tableData
            this.poData.activeTab = 'financial-entries'
            this.displayedColumn = ['SNo', 'id', 'ledgerAccount.accountNumber', 'description', 'currentApprover', 'type', 'currency', 'amount', 'status'];
          }
        } else {
          this.isDataAvailable = false
          this.donotShowAmt = false
        }
      },
        err => {
          this.isDataAvailable = false
          this.donotShowAmt = false
        })
    } else if (this.page === 'SE') {
      this.resetData()

      this.adminService.getHrRequest('hr-request').subscribe((res: any) => {
        this.tableData = res
        if (!!this.tableData && Object.keys(this.tableData).length > 0) {
          this.isDataAvailable = true
          this.donotShowAmt = false

          if (!this.utilityService.isNullOrUndefined(this.tableData.businessTripIntRequestListData)) {
            this.taboneLabel = 'Business Trip International'
            this.poData = this.tableData.businessTripIntRequestListData
            this.poData.activeTab = 'btIntntl'
          }

          if (!this.utilityService.isNullOrUndefined(this.tableData.businessTripLocalRequestListData)) {
            this.tabTwoLabel = 'Business Trip Local'
            this.materialReceivedData = this.tableData.businessTripLocalRequestListData
            this.materialReceivedData.activeTab = 'btLocal'
          }

          if (!this.utilityService.isNullOrUndefined(this.tableData.cashAdvanceRequestListData)) {
            this.tabThreeLabel = 'Cash Advance Requests'
            this.paymentData = this.tableData.cashAdvanceRequestListData
            this.paymentData.activeTab = 'cashAdvanceRequestListData'
          }
          if (!this.utilityService.isNullOrUndefined(this.tableData.expenseClaimRequestListData)) {
            this.tabFourLabel = 'Expense Claim'
            this.tabFourData = this.tableData.expenseClaimRequestListData
            this.tabFourData.activeTab = 'expenseClaimRequestListData'
          }
          if (!this.utilityService.isNullOrUndefined(this.tableData.housingAdvanceRequestListData)) {
            this.tabFiveLabel = 'Housing Advance Claim'
            this.tabFiveData = this.tableData.housingAdvanceRequestListData
            this.tabFiveData.activeTab = 'housingAdvanceRequestListData'
          }
          if (!this.utilityService.isNullOrUndefined(this.tableData.companyLetterRequestListData)) {
            this.tabSixLabel = 'Company Letter'
            this.tabSixData = this.tableData.companyLetterRequestListData
            this.tabSixData.activeTab = 'companyLetterRequestListData'
          }
          if (!this.utilityService.isNullOrUndefined(this.tableData.exitReEntryVisaRequestListData)) {
            this.tabSevenLabel = 'Exit Reentry'
            this.tabSevenData = this.tableData.exitReEntryVisaRequestListData
            this.tabSevenData.activeTab = 'exitReEntryVisaRequestListData'
          }
          if (!this.utilityService.isNullOrUndefined(this.tableData.leaveCancellationRequestListData)) {
            this.tabEightLabel = 'Leave Cancellation'
            this.tabEightData = this.tableData.leaveCancellationRequestListData
            this.tabEightData.activeTab = 'leaveCancellationRequestListData'
          }
          if (!this.utilityService.isNullOrUndefined(this.tableData.leaveTicketRequestListData)) {
            this.tabNineLabel = 'Leave Ticket'
            this.tabNineData = this.tableData.leaveTicketRequestListData
            this.tabNineData.activeTab = 'leaveTicketRequestListData'
          }
          if (!this.utilityService.isNullOrUndefined(this.tableData.overtimeRequestListData)) {
            this.tabTenLabel = 'Over Time'
            this.tabTenData = this.tableData.overtimeRequestListData
            this.tabTenData.activeTab = 'overtimeRequestListData'
          }
          if (!this.utilityService.isNullOrUndefined(this.tableData.resignationRequestListData)) {
            this.tabEleLabel = 'Resignation'
            this.tabEleData = this.tableData.resignationRequestListData
            this.tabEleData.activeTab = 'resignationRequestListData'
          }
          if (!this.utilityService.isNullOrUndefined(this.tableData.timeCorrectionRequestListData)) {
            this.tabTwelveLabel = 'Time Correction'
            this.tabTwelveData = this.tableData.timeCorrectionRequestListData
            this.tabTwelveData.activeTab = 'timeCorrectionRequestListData'
          }
          if (!this.utilityService.isNullOrUndefined(this.tableData.clearanceRequestListData)) {
            this.tabOneThreeLabel = 'Clearance'
            this.tabOneThreeData = this.tableData.clearanceRequestListData
            this.tabOneThreeData.activeTab = 'clearanceRequestListData'
          }
          if (!this.utilityService.isNullOrUndefined(this.tableData.leaveManagementRequestListData)) {
            this.tabOneFourLabel = 'Leave Management'
            this.tabOneFourData = this.tableData.leaveManagementRequestListData
            this.tabOneFourData.activeTab = 'leaveManagementRequestListData'
          }
          this.displayedColumn = ['SNo', 'workflowActionPk', 'parent.employeeName', 'parent.employeeNumber', 'parent.reason', 'parent.status', 'parent.creationDate', 'CancelReq']
        } else {
          this.isDataAvailable = false
          this.donotShowAmt = false
        }
      },
        err => {
          this.isDataAvailable = false
          this.donotShowAmt = false
        })
    } else if (this.page === 'RP') {
      this.resetData()
      this.adminService.getPayrollDetails('run-payroll').subscribe((res: any) => {
        this.tableData = res.runPayrolls
        if (!!this.tableData && this.tableData.length > 0) {
          this.isDataAvailable = true
          this.donotShowAmt = false

          if (!this.utilityService.isNullOrUndefined(this.tableData)) {
            this.showGL = true
            this.taboneLabel = 'Run Payroll'
            this.poData = this.tableData
            this.poData.activeTab = 'run-payrol'
            this.displayedColumn = ['SNo', 'pk', 'payrollYear', 'monthInText', 'runPayrollType', 'status'];
          }
        } else {
          this.isDataAvailable = false
          this.donotShowAmt = false
        }
      },
        err => {
          this.isDataAvailable = false
          this.donotShowAmt = false
        })
    } else if (this.page === 'PR') {
      this.resetData()
      this.adminService.getPayrollDetails('payrollentries/').subscribe((res: any) => {
        this.tableData = res

        if (Object.keys(this.tableData).length > 0) {
          this.isDataAvailable = true
          this.donotShowAmt = false
          if (!this.utilityService.isNullOrUndefined(this.tableData.otEntries)) {
            this.taboneLabel = 'One Time Entries'
            this.poData = this.tableData.otEntries
            this.poData.activeTab = 'onetime-entries'
          }

          if (!this.utilityService.isNullOrUndefined(this.tableData.payrollEntries)) {
            this.tabFourLabel = 'Payroll entries'
            this.tabFourData = this.tableData.payrollEntries
            this.tabFourData.activeTab = 'payroll-entries'
            this.donotShowAmt = false
          }
          this.displayedColumn = ['SNo', 'pk', 'employeeName', 'employeeNumber', 'classification', 'entryType', 'startDate', 'endDate', 'status']
        } else {
          this.isDataAvailable = false
          this.donotShowAmt = false
        }
      },
        err => {
          this.isDataAvailable = false
          this.donotShowAmt = false
        })
    } else if (this.page === 'SR') {
      this.resetData()

      this.myProfileService.getReviewOrders('RETURN_ORDER').subscribe((res: any) => {
        this.tableData = res
        
        this.displayedColumn = ['SRNo', 'user.name', 'created', 'currentApprover', 'totalPriceWithTax.formattedValue', 'status'];
        if (Object.keys(this.tableData).length > 0) {
          this.isDataAvailable = true
          this.donotShowAmt = false
          if (!this.utilityService.isNullOrUndefined(this.tableData.tab1)) {
            this.taboneLabel = 'Sales Return Orders'
            this.poData = this.tableData.tab1
            this.poData.activeTab = 'sales-order'
          }

          if (!this.utilityService.isNullOrUndefined(this.tableData.tab2)) {
            this.tabThreeLabel = 'Material Return'
            this.donotShowAmt = true
            this.paymentData = this.tableData.tab2
            this.paymentData.activeTab = 'material-shipped'
          }

          if(!this.utilityService.isNullOrUndefined(this.tableData.tab3)) {
            this.tabFourLabel = 'Credit Note'
            this.tabFourData = this.tableData.tab3
            this.tabFourData.activeTab = 'invoice-booking'
          }
        } else {
          this.isDataAvailable = false
          this.donotShowAmt = false
        }
      },
        err => {
          this.isDataAvailable = false
          this.donotShowAmt = false
        })
    }  else {
      this.resetData() 
    }
  }

  resetData = () => {
    this.poData = []
    this.materialReceivedData = []
    this.paymentData = []
    this.taboneLabel = null
    this.tabTwoLabel = null
    this.tabThreeLabel = null
    this.tabFourLabel = null
    this.tabFiveLabel = null
    this.tabSixLabel = null
    this.tabSevenLabel = null
    this.tabEightLabel = null
    this.tabNineLabel = null
    this.tabTenLabel = null
    this.tabEleLabel = null
    this.tabTwelveLabel = null
    this.tabOneThreeLabel = null
    this.tabOneFourLabel = null
    this.showGL = false
  }

}
