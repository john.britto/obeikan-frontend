import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-material-shipped-molecule',
  templateUrl: './material-shipped-molecule.component.html',
  styleUrls: ['./material-shipped-molecule.component.scss']
})
export class MaterialShippedMoleculeComponent implements OnInit {

  @Input() materialShippedData: any
  @Input() isDataAvailable: any
  @Input() displayedColumn: any

  constructor() { }

  ngOnInit() {
  }

}
