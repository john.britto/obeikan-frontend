import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { MyProfileService } from '../../../../../services/my-profile.service'
import Swal from 'sweetalert2';
import { environment } from 'src/environments/environment';
import { CommonUtilityService } from '../../../../../services/common-utility-service'
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-financial-entry-table-molecule',
  templateUrl: './financial-entry-table-molecule.component.html',
  styleUrls: ['./financial-entry-table-molecule.component.scss']
})
export class FinancialEntryTableMoleculeComponent implements OnInit {

  @Input() pageType: any
  @Input() materialEntryData: any
  @Input() isDataAvailable: any
  @Input() displayedColumn: any
  @Input() page: any
  displayedColumns: string[] = this.displayedColumn
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  ELEMENT_DATA: PeriodicElement[]
  dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);

  constructor(
    private myProfileService: MyProfileService,
    private matSnack: MatSnackBar,
  ) { }

  ngOnChanges() {
    this.ELEMENT_DATA = this.materialEntryData
    this.dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
    this.displayedColumns = this.displayedColumn;
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
    this.ELEMENT_DATA = this.materialEntryData
    this.dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
    this.displayedColumns = this.displayedColumn;
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter = (event) => {
    let filteredData = this.materialEntryData.filter(data => {
      let code = data.order.code2 ? data.order.code2 : data.order.referenceOrder;
      let reference = data.order.referenceOrder2 ? data.order.referenceOrder2 : data.order.referenceOrderData.referenceOrder2
      let status =  data.status ? data.status : data.order.status;
      let desc = data.description;
      let concatFilterColumns = code + reference + desc;
      //+ data.company.bpCode; 
      return concatFilterColumns && concatFilterColumns.toLowerCase().indexOf(event.target.value.trim().toLowerCase()) != -1
    })
    this.ELEMENT_DATA = filteredData
    this.dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
  }

}

export interface PeriodicElement {
  Supplier: string;
  no: number;
  Date: number;
  Amount: string;
}
