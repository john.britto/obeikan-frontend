import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { MyProfileService } from '../../../../services/my-profile.service'
import { CommonUtilityService } from '../../../../services/common-utility-service'
import { FinanceService } from '../../../../services/finance.service'

@Component({
  selector: 'app-financial-entry-molecule',
  templateUrl: './financial-entry-molecule.component.html',
  styleUrls: ['./financial-entry-molecule.component.scss']
})
export class FinancialEntryMoleculeComponent implements OnInit, OnChanges {

  @Input() page: any
  tabTwoLabel: any
  tabOneLabel: any
  tabThreeLabel: any
  tableData: any
  paymentEntryData: any = []
  materialEntryData: any = []
  invoiceEntryData: any = []
  isDataAvailable: any
  meDisplayedColumn: any
  peDisplayedColumn: any
  ieDisplayedColumn: any
  displayedColumn: any
  glData: any = []

  constructor(
    private myProfileService: MyProfileService,
    private utilityService: CommonUtilityService,
    private financeService: FinanceService
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.page) {
      this.ngOnInit()
    }
  }

  ngOnInit() {

    if (this.page === 'AP') {
      this.getData('ACCOUNTS_PAYABLE', 'materialEntries')
      this.getData('ACCOUNTS_PAYABLE', 'paymentEntries')
      this.getData('ACCOUNTS_PAYABLE', 'invoiceEntries')
    } else if (this.page === 'AR') {
      this.getData('ACCOUNTS_RECEIVABLE', 'materialEntries')
      this.getData('ACCOUNTS_RECEIVABLE', 'paymentEntries')
      this.getData('ACCOUNTS_RECEIVABLE', 'invoiceEntries')
    } else if(this.page === 'GLA') {
      this.getGLData()
    } else {
      this.glData = [] 
      this.tabOneLabel = null
      this.materialEntryData = []
      this.tabTwoLabel = null
      this.paymentEntryData = []
      this.invoiceEntryData = []
      this.tabThreeLabel = null
    }
  }

  getData = (pageType, uri) => {

    if (uri === 'materialEntries') {
      this.tabOneLabel = null
      this.materialEntryData = []
    }
    if (uri === 'paymentEntries') {
      this.tabTwoLabel = null
      this.paymentEntryData = []
    }
    if (uri === 'invoiceEntries') {
      this.tabThreeLabel = null
      this.invoiceEntryData = []
    }

    this.myProfileService.getMaterialEntries(pageType, uri).subscribe((res: any) => {
      if (Object.keys(res).length > 0) {

        if (!this.utilityService.isNullOrUndefined(res.materialFinancialEntries)) {
          this.tabOneLabel = 'Material Entry'
          this.materialEntryData = res.materialFinancialEntries
          this.materialEntryData.activeTab = 'material-entry'
          this.meDisplayedColumn = ['S.No', 'date', 'order.code2', 'order.referenceOrder2', 'company.bpCode', 'order.currency2', 'amount', 'totalAmountInSAR', 'accountNumber', 'description', 'creditOrDebit'];
        } else if (!this.utilityService.isNullOrUndefined(res.paymentFinancialEntries)) {
          this.tabTwoLabel = 'Payment Entry'
          this.paymentEntryData = res.paymentFinancialEntries
          this.paymentEntryData.activeTab = 'payment-entry'
          this.peDisplayedColumn = ['S.No', 'date', 'order.referenceOrderData.code2', 'order.referenceOrderData.referenceOrder2', 'order.company.bpCode', 'order.invoiceNumber', 'order.invoiceDate', 'order.referenceOrderData.currency2', 'amount', 'totalAmountInSAR', 'accountNumber', 'description', 'creditOrDebit'];
        } else
         if (!this.utilityService.isNullOrUndefined(res.invoiceFinancialEntries)) {
          this.tabThreeLabel = 'Invoice Entry'
          this.invoiceEntryData = res.invoiceFinancialEntries
          this.invoiceEntryData.activeTab = 'invoice-entry'
          this.ieDisplayedColumn = ['S.No', 'date', 'order.company.bpCode', 'order.referenceOrderData.referenceOrder2', 'order.invoiceNumber', 'order.invoiceDate', 'order.currency', 'amount', 'totalAmountInSAR', 'accountNumber', 'description', 'creditOrDebit'];
        }
      }
    },
      err => {
      })
  }

  getGLData =() => {
    this.tabOneLabel = null
    this.materialEntryData = []
    this.tabTwoLabel = null
    this.paymentEntryData = []

    this.financeService.getGLData('financial-entry').subscribe((res: any) => {
      if(!!res && !!res.financialEntries) {
        this.tabTwoLabel = 'GL Entry'
        this.glData = res.financialEntries
        this.displayedColumn = ['user', 'description', 'type', 'company', 'ledgerAccount.accountNumber', 'currency', 'amount', 'ledgerAccount.accountType'];
      }
    },
    err => {

    })
  }

}
