import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-po-molecule',
  templateUrl: './po-molecule.component.html',
  styleUrls: ['./po-molecule.component.scss']
})
export class PoMoleculeComponent implements OnInit {

  @Input() processType: any
  @Input() pageType: any
  @Input() poData: any
  @Input() isDataAvailable: any
  @Input() displayedColumn: any
  @Output("fetchData") fetchDataPO: EventEmitter<any> = new EventEmitter();  
  
  constructor() { }

  ngOnInit() {
    if(this.pageType === 'Over Time Entries') {
      this.displayedColumn = ['SNo', 'pk', 'effectiveDate', 'entryType', 'classification', 'recurring', 'startDate', 'endDate', 'payrollOTAction']
    }
  }

  fetchData() {
    this.fetchDataPO.emit()
  }
}

