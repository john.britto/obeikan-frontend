import { Component, OnInit, ViewChild, Input, Output, EventEmitter, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { MyProfileService } from '../../../../../services/my-profile.service'
import Swal from 'sweetalert2';
import { environment } from 'src/environments/environment';
import { CommonUtilityService } from '../../../../../services/common-utility-service'
import { DatePipe } from '@angular/common';
import { FinanceService, API_FAILURE_MSG } from '../../../../../services/finance.service'
import { AdminService } from '../../../../../services/admin.service';
import { DeleteHRMSSalaryFormModalComponent } from '../../../admin/hrms/salary-element-entries-single-molecule/salary-element-entries-single-molecule.component';
import { MatDialog } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { appConfig } from 'src/app/app.config';

@Component({
  selector: 'app-table-molecule',
  templateUrl: './table-molecule.component.html',
  styleUrls: ['./table-molecule.component.scss']
})
export class TableMoleculeComponent implements OnInit {

  @Input() processType: any
  @Input() tableData: any
  @Input() displayedColumn: any
  @Input() isDataAvailable: any
  @Input() pageType: any
  @Output() onEmitFCM = new EventEmitter();
  @Output("fetchData") fetchData: EventEmitter<any> = new EventEmitter();

  pipe = new DatePipe('en-US');
  displayedColumns: string[]
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  ELEMENT_DATA: PeriodicElement[]
  dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
  INFO_ELEMENT_DATA: POPeriodicElement[]
  infoDataSource = new MatTableDataSource<POPeriodicElement>(this.INFO_ELEMENT_DATA);
  displayDetailedInfo: any = false
  infoDisplayedColumns: string[]
  infoDisplayedColumnsFamily: string[] = ['key', 'value']
  detailedInfo: any
  showPrint: boolean = false;
  showRemove: boolean = false;
  code: any;
  isLedger: boolean = false
  columnsInPO: any = ['No', 'itemCode', 'itemDescription', 'unit', 'quantity', 'price', 'discountPercentage', 'subTotal', 'vatPercentage', 'itemTotal']
  columnsInSO: any = ['No', 'itemCode', 'itemDescription', 'unit', 'quantity', 'price', 'discountPercentage', 'vatPercentage', 'itemTotal']
  columnsInMR: any = ['No', 'itemCode', 'itemDescription', 'unit', 'quantity']
  columnsInPayment: any = ['No', 'itemCode', 'itemDescription', 'price', 'itemTotal']
  reason: string;

  constructor(
    private myProfileService: MyProfileService,
    private matSnack: MatSnackBar,
    private utilityService: CommonUtilityService,
    private financeService: FinanceService,
    public dialog: MatDialog,
    private adminService: AdminService
  ) {
  }

  ngOnChanges() {
    this.ELEMENT_DATA = this.tableData
    this.dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
    this.displayedColumns = this.displayedColumn;
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
    this.ELEMENT_DATA = this.tableData
    this.dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
    this.displayedColumns = this.displayedColumn;
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  refresh() {
    this.adminService.refreshDashboard.next(true);
  }

  updateDetails = (data, type) => {

    if (type == "reject") {
      const dialogRef = this.dialog.open(ReasonUpdateFormModalComponent, {
        data: { reason: '' }
      });
      dialogRef.afterClosed().subscribe(result => {
        let reason = '';
        if (!!result) {
          reason = `&reason=${result}`
          this.myProfileService.updateStatus(data.code, this.tableData.activeTab, `${type}&workflowActionPk=${data.workflowActionPk}${reason}`).subscribe((res: any) => {
            this.isLedger = false
            this.displayDetailedInfo = false
            this.displayMsg('success', `${data.code} has been rejected`)
            this.adminService.refreshDashboard.next(true)
          },
            err => {
              this.isLedger = false
              this.displayDetailedInfo = false
              this.displayMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
            })
        }
      });
      return;
    }

    this.myProfileService.updateStatus(data.code, this.tableData.activeTab, `${type}&workflowActionPk=${data.workflowActionPk}`).subscribe((res: any) => {
      this.isLedger = false
      this.displayDetailedInfo = false
      this.displayMsg('success', `${data.code} has been ${type === 'approve' ? 'approved' : 'rejected'}`)
      this.adminService.refreshDashboard.next(true)
    },
      err => {
        this.isLedger = false
        this.displayDetailedInfo = false
        this.displayMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      })
  }

  getDimension(data, key) {
    return data[data.findIndex(e => e.key === key)].value;
  }

  getStatus(status, type) {
    if (type === 'status1') {
      return status.filter(e => e.key === "ACP")[0].value;
    }
    if (type === 'status2') {
      return status.filter(e => e.key === "ACR")[0].value;
    }
    if (type === 'status3') {
      return status.filter(e => e.key === "CMG")[0].value;
    }
    if (type === 'status4') {
      return status.filter(e => e.key === "GLD")[0].value;
    }
    if (type === 'status5') {
      return status.filter(e => e.key === "INTEGRATION")[0].value;
    }
  }

  updateLedgerDetails = (info, type) => {
    let uri = `financial-entry/${info.id}?action=${type}&workflowActionPk=${info.workflowActionPk}`;

    if (type == "reject") {
      const dialogRef = this.dialog.open(ReasonUpdateFormModalComponent, {
        data: { reason: '' }
      });
      dialogRef.afterClosed().subscribe(result => {
        let reason = '';
        if (!!result) {
          reason = `&reason=${result}`
          this.financeService.getGLData(uri + reason).subscribe((res: any) => {
            this.isLedger = false
            this.displayDetailedInfo = false
            this.displayMsg('success', `${info.id} has been ${res.status}`)
            this.adminService.refreshDashboard.next(true)
          },
            err => {
              this.displayDetailedInfo = false
              this.isLedger = false
              this.displayMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
            })
        }
      });
      return;
    }

    this.financeService.getGLData(uri).subscribe((res: any) => {
      this.isLedger = false
      this.displayDetailedInfo = false
      this.displayMsg('success', `${info.id} has been ${res.status}`)
      this.adminService.refreshDashboard.next(true)
    },
      err => {
        this.displayDetailedInfo = false
        this.isLedger = false
        this.displayMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      })
  }

  displayInfo = data => {

    this.myProfileService.getPoDetail(data.code, this.tableData.activeTab).subscribe((res: any) => {
      this.isLedger = false
      this.displayDetailedInfo = true
      this.showPrint = res.status === "APPROVED" || res.status === "COMPLETED" ? true : false;
      if (res.status === "CREATED") {
        this.showRemove = this.pageType === 'Purchase Order' || this.pageType === 'Sales Orders' || this.pageType === 'General Ledger' ? true : false;
      }
      this.code = this.pageType === 'General Ledger' ? res.id : res[`${this.utilityService.splitCode(this.tableData.activeTab)}Order`];
      this.INFO_ELEMENT_DATA = res.item
      this.infoDataSource = new MatTableDataSource<POPeriodicElement>(this.INFO_ELEMENT_DATA);
      if (this.pageType === 'Purchase Order') this.infoDisplayedColumns = this.columnsInPO
      if (this.pageType === 'MR') this.infoDisplayedColumns = this.columnsInMR
      if (this.pageType === 'Sales Orders') this.infoDisplayedColumns = this.columnsInSO
      // this.infoDataSource.paginator = this.itemPaginator
      this.detailedInfo = res;
      if (!!this.detailedInfo.transactionEntryDate) this.detailedInfo.transactionEntryDate = this.pipe.transform(this.detailedInfo.transactionEntryDate, 'MM/dd/yyyy');
      if (!!this.detailedInfo.documentDate) this.detailedInfo.documentDate = this.pipe.transform(this.detailedInfo.documentDate, 'MM/dd/yyyy');
    },
      err => {
        this.displayDetailedInfo = false;
        this.displayMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      })
  }

  onCancelRequest = requestId => {
    this.myProfileService.updateStatus(requestId, this.tableData.activeTab, 'close').subscribe((res: any) => {
      if (!!res) {
        this.isLedger = false
        this.displayDetailedInfo = false
        this.displayMsg('success', `${requestId} has been closed successfully`)
        this.adminService.refreshDashboard.next(true)
      }
    },
      err => {
        this.displayDetailedInfo = false
        this.isLedger = false
        this.displayMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
       })
  }

  remove = () => {
    const dialogRef = this.dialog.open(DeleteHRMSSalaryFormModalComponent, {
      data: { code: this.code }
    });

    dialogRef.afterClosed().subscribe(result => {
      result && this.removeItem(this.code);
    });
  }

  removeItem = (selectedCode) => {
    const endpoint = `remove-${this.tableData.activeTab}`
    this.financeService.removeOrder(selectedCode, endpoint).subscribe((res: any) => {
      this.displayMsg('success', `${this.pageType} deleted successfully`);
      this.fetchData.emit()
    }, err => {
      if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.displayMsg('error', err.error.errors[0].message)
      } else this.displayMsg('error', API_FAILURE_MSG)
    });
  }


  onPrint = (divName) => {
    let prinatble = document.getElementsByClassName("detailed-info") as HTMLCollectionOf<HTMLElement>;

    document.body.style.visibility = "hidden";
    prinatble[0].style.visibility = "visible";
    window.print();
    document.body.style.visibility = "visible";
    prinatble[0].style.visibility = "visible";
  }

  displayLedgerInfo = (info) => {
    let uri = `financial-entry/${info.id}?action=view`
    this.financeService.getGLData(uri).subscribe((res: any) => {
      this.isLedger = true
      this.displayDetailedInfo = true
      this.detailedInfo = res;
      this.detailedInfo.date = this.pipe.transform(this.detailedInfo.date, 'MM/dd/yyyy')
    },
      err => {

      })
  }

  displayHRInfo = detailedInfo => {
    this.displayDetailedInfo = true
    this.detailedInfo = detailedInfo;
    this.INFO_ELEMENT_DATA = detailedInfo.familyDetails
    this.infoDataSource = new MatTableDataSource<POPeriodicElement>(this.INFO_ELEMENT_DATA);
  }

  updatePayrollDetails = (pk, action, uri) => {
    let pkNo = `${pk}?action=${action}`

    if (action == "reject") {
      const dialogRef = this.dialog.open(ReasonUpdateFormModalComponent, {
        data: { reason: '' }
      });
      dialogRef.afterClosed().subscribe(result => {
        let reason = '';
        if (!!result) {
          reason = `reason=${result}`
          this.adminService.getPayrollEntry(pkNo, uri, reason).subscribe((res: any) => {
            this.isLedger = false
            this.displayDetailedInfo = false
            this.displayMsg('success', `${pk} has been ${res.status}`)
            this.adminService.refreshDashboard.next(true)
          },
            err => {
              this.displayDetailedInfo = false
              this.isLedger = false
              this.displayMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
            })
        }
      });
      return;
    }
    this.adminService.getPayrollEntry(pkNo, uri, null).subscribe((res: any) => {
      this.isLedger = false
      this.displayDetailedInfo = false
      this.displayMsg('success', `${pk} has been ${res.status}`)
      this.adminService.refreshDashboard.next(true)
    },
      err => {
        this.displayDetailedInfo = false
        this.isLedger = false
        this.displayMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      })
  }

  cancelRequest = (data) => {
    if (data.parent.status === "PENDING_APPROVAL") {
      this.adminService.updateCancelEntries(`status-cancel?pk=${data.parent.pk}`).subscribe((res: any) => {
        this.fetchData.emit()
        this.displayMsg('success', `${data.parent.pk} has been ${res ? 'cancelled' : 'rejected'}`)
        this.adminService.refreshDashboard.next(true)
      },
        err => {
          this.displayMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
        })
    }
  }

  updateRunPayroll = (pk, action) => {
    if (action == "reject") {
      const dialogRef = this.dialog.open(ReasonUpdateFormModalComponent, {
        data: { reason: '' }
      });
      dialogRef.afterClosed().subscribe(result => {
        let reason = '';
        if (!!result) {
          reason = `${result}`
          this.adminService.updateRunPayroll(`update-payroll-run/${pk}?action=${action}&reason=${reason}`).subscribe((res: any) => {
            this.isLedger = false
            this.displayDetailedInfo = false
            this.displayMsg('success', `${pk} has been ${action === 'approve' ? 'approved' : 'rejected'}`)
            this.adminService.refreshDashboard.next(true)
          },
            err => {
              this.displayDetailedInfo = false
              this.isLedger = false
              this.displayMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
            })
        }
      });
      return;
    }
    this.adminService.updateRunPayroll(`update-payroll-run/${pk}?action=${action}`).subscribe((res: any) => {
      this.isLedger = false
      this.displayDetailedInfo = false
      this.displayMsg('success', `${pk} has been ${action === 'approve' ? 'approved' : 'rejected'}`)
      this.adminService.refreshDashboard.next(true)
    },
      err => {
        this.displayDetailedInfo = false
        this.isLedger = false
        this.displayMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      })
  }

  updateHrDetails = (pk, action) => {
    let request = `${pk}?action=${action}`

    if (action == "reject") {
      const dialogRef = this.dialog.open(ReasonUpdateFormModalComponent, {
        data: { reason: '' }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (!!result) {
          request = `${pk}?action=${action}&reason=${result}`
          this.adminService.postHRRequest('update-hr-request', request).subscribe((res: any) => {
            this.isLedger = false
            this.displayDetailedInfo = false
            this.displayMsg('success', `${pk} has been ${action === 'approve' ? 'approved' : 'rejected'}`)
            this.adminService.refreshDashboard.next(true)
          },
            err => {
              this.displayDetailedInfo = false
              this.isLedger = false
              this.displayMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
            })
        }
      });
      return;
    }

    this.adminService.postHRRequest('update-hr-request', request).subscribe((res: any) => {
      this.isLedger = false
      this.displayDetailedInfo = false
      this.displayMsg('success', `${pk} has been ${action === 'approve' ? 'approved' : 'rejected'}`)
      this.adminService.refreshDashboard.next(true)
    },
      err => {
        this.displayDetailedInfo = false
        this.isLedger = false
        this.displayMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      })
  }

  displayMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true,
      cancelButtonText:
        '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }

  onDownloadFile = (e, url) => {
    e.preventDefault();
    if (this.utilityService.isBrowser()) {
      window.open(environment.hostName + url, '_blank')
    }
  }

  getRoundOff(value: any) {
    return Number(value).toFixed(2);
  }

  applyFilter = (event) => {
    let filteredData = [];
    if(!!this.utilityService.getCookie('isSuperAdmin') && JSON.parse(this.utilityService.getCookie('isSuperAdmin'))) {
      filteredData = this.tableData.filter(data => {
        let code = data.bpCode || data.assetCode || data.itemCode || data.code || data.accountNumber || data.vatCode || data.key; 
        let desc = data.vatNumber || data.searchKey || data.periodType || data.description || data.vatDescription || data.value;
        let concatFilterColumns = code + data.name + desc
        return concatFilterColumns.toLowerCase().indexOf(event.target.value.trim()) != -1
      })
    } else if(this.displayedColumn.includes('pk') && this.displayedColumn.includes('employeeName') && this.displayedColumn.includes('employeeNumber')) {
      filteredData = this.tableData.filter(data => {
        let concatFilterColumns = data.pk + data.employeeName + data.employeeNumber + data.status
        return concatFilterColumns.toLowerCase().indexOf(event.target.value.trim()) != -1
      })
    } else if(this.tableData.activeTab === 'run-payrol'){
      filteredData = this.tableData.filter(data => {
        let concatFilterColumns = data.pk + data.payrollYear + data.status + data.monthInText + data.runPayrollType
        return concatFilterColumns.toLowerCase().indexOf(event.target.value.trim()) != -1
      })
    } else {
      filteredData = this.tableData.filter(data => {
        let concatFilterColumns = data.code + data.user.name + data.status
        return concatFilterColumns.toLowerCase().indexOf(event.target.value.trim()) != -1
      })
    }
    this.ELEMENT_DATA = filteredData
    this.dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
  }

  onEditFCM = data => {
    this.onEmitFCM.emit({
      ...data,
      index: this.tableData.index
    })
  }
}

export interface PeriodicElement {
  Supplier: string;
  no: number;
  Date: string;
  Amount: string;
  Status: string;
  workflowActionPk: string;
  hrStatus: string;
  'parent.transactionId': string
}

export interface POPeriodicElement {
  itemCode: string;
  itemDescription: number;
  itemTotal: number;
  price: string;
}

export interface MRPeriodicElement {
  Supplier: string;
  no: number;
  Date: number;
  Amount: string;
}

@Component({
  selector: 'reason-update',
  templateUrl: '../reasonUpdate.html',
  providers: [TableMoleculeComponent]
})
export class ReasonUpdateFormModalComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data, public dialog: MatDialog) { }
  onNoClick(): void {
    this.dialog.closeAll();
  }

}


