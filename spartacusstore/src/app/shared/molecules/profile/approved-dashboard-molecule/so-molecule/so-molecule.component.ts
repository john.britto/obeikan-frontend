import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-so-molecule',
  templateUrl: './so-molecule.component.html',
  styleUrls: ['./so-molecule.component.scss']
})
export class SoMoleculeComponent implements OnInit {

  @Input() tableRes: any
  tableData: any
  displayedColumn: any

  constructor() { }

  ngOnInit() {
    this.tableData = [
      {no: 5, Supplier: 'Hydrogen', Date: 1.0079, Amount: 'H'},
      {no: 2, Supplier: 'Helium', Date: 4.0026, Amount: 'He'},
      {no: 3, Supplier: 'Lithium', Date: 6.941, Amount: 'Li'},
      {no: 3, Supplier: 'Beryllium', Date: 9.0122, Amount: 'Be'},
      {no: 1, Supplier: 'Boron', Date: 10.811, Amount: 'B'},
      {no: 6, Supplier: 'Carbon', Date: 12.0107, Amount: 'C'},
      {no: 7, Supplier: 'Nitrogen', Date: 14.0067, Amount: 'N'},
      {no: 8, Supplier: 'Oxygen', Date: 15.9994, Amount: 'O'},
      {no: 9, Supplier: 'Fluorine', Date: 18.9984, Amount: 'F'},
      {no: 10, Supplier: 'Neon', Date: 20.1797, Amount: 'Ne'},
      {no: 11, Supplier: 'Sodium', Date: 22.9897, Amount: 'Na'},
      {no: 12, Supplier: 'Magnesium', Date: 24.305, Amount: 'Mg'},
      {no: 13, Supplier: 'Aluminum', Date: 26.9815, Amount: 'Al'},
      {no: 14, Supplier: 'Silicon', Date: 28.0855, Amount: 'Si'},
      {no: 15, Supplier: 'Phosphorus', Date: 30.9738, Amount: 'P'},
      {no: 16, Supplier: 'Sulfur', Date: 32.065, Amount: 'S'},
      {no: 17, Supplier: 'Chlorine', Date: 35.453, Amount: 'Cl'},
      {no: 18, Supplier: 'Argon', Date: 39.948, Amount: 'Ar'},
      {no: 19, Supplier: 'Potassium', Date: 39.0983, Amount: 'K'},
      {no: 20, Supplier: 'Calcium', Date: 40.078, Amount: 'Ca'},
    ];
    this.displayedColumn = ['no', 'Supplier', 'Date', 'Amount'];
  }

}
