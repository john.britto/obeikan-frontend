import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-profile-payment-molecule',
  templateUrl: './payment-molecule.component.html',
  styleUrls: ['./payment-molecule.component.scss']
})
export class ProfilePaymentMoleculeComponent implements OnInit {
  
  @Input() pageType: any
  @Input() paymentData: any
  @Input() isDataAvailable: any
  @Input() displayedColumn: any
  @Output("fetchData") fetchDataPay: EventEmitter<any> = new EventEmitter();  

  constructor() { }

  ngOnInit() {
  }
  fetchData() {
    this.fetchDataPay.emit()
  }
}
