import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-material-received-molecule',
  templateUrl: './material-received-molecule.component.html',
  styleUrls: ['./material-received-molecule.component.scss']
})
export class MaterialReceivedMoleculeComponent implements OnInit {

  @Input() pageType: any
  @Input() materialReceivedData: any
  @Input() isDataAvailable: any
  @Input() displayedColumn: any
  @Output("fetchData") fetchDataMR: EventEmitter<any> = new EventEmitter();  

  constructor() { }

  ngOnInit() {
    if(this.pageType === 'Payroll Entries') {
      this.displayedColumn = ['SNo', 'pk', 'effectiveDate', 'entryType', 'classification', 'recurring', 'startDate', 'endDate', 'payrollAction']
    }
  }
  fetchData() {
    this.fetchDataMR.emit()
  }
}
