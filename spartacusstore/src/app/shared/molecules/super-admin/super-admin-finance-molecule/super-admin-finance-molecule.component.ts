import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { CommonUtilityService } from '../../../../services/common-utility-service';
import * as XLSX from 'xlsx';
import { MatTabGroup } from '@angular/material'
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { SuperAdminService } from '../../../../services/super-admin.service';
import Swal from 'sweetalert2';
import { appConfig } from 'src/app/app.config';
import { FinanceService } from '../../../../services/finance.service'
import { Router, NavigationEnd  } from '@angular/router';
import { AdminService } from '../../../../services/admin.service';

@Component({
  selector: 'app-super-admin-finance-molecule',
  templateUrl: './super-admin-finance-molecule.component.html',
  styleUrls: ['./super-admin-finance-molecule.component.scss']
})
export class SuperAdminFinanceMoleculeComponent implements OnInit {

  @ViewChild('tabGroupEl', { static: false }) tabGroupEl: MatTabGroup;
  @ViewChild('f', { static: false }) employeeForm;
  selected = new FormControl(0);
  createUserForm: FormGroup;
  financePortalData: any;
  subCustomersData: any = [];
  hostName = environment.hostName;
  disableCreateUserForm = false;
  matSubCustomersData: any;
  validationMessages = {
    firstName: [{ type: 'required', message: 'First name is required' }],
    lastName: [{ type: 'required', message: 'First name is required' }],
    emailId: [
      { type: 'required', message: 'Email is required' },
      { type: 'email', message: 'Enter a valid email' }
    ],
    employeeId: [
      { type: 'required', message: 'Employee ID is required' },
      { type: 'pattern', message: 'Please enter a valid Employee ID' }
    ],
    iqamaId: [
      { type: 'required', message: 'ID is required' },
      { type: 'pattern', message: 'Please enter a valid Iqama Id' }
    ],
    customerRole: [{ type: 'required', message: 'Please select any option' }],
    verificationId: [{ type: 'required', message: 'Verification Id is required' }],
  };
  customerRoles: any = appConfig.DropdownOptions.customerRoles
  errMsg = 'Sorry ! Please try again later';
  succMsg = 'User Created successfully !';
  options: any;
  hrImage: any = '../../assets/images/icn_masterdataicn_masterdata_blacksmall.png';
  verificationId: any;
  dropdownList: any = [];
  toggle: any = [];
  approver1: any; approver2: any; approver3: any;
  downloadxl: any;
  fileSelected: any = '';
  fileToUpload: any;
  formGroup: FormGroup;
  fileName: any;
  excelValues: any;
  detailedInfo: any;
  isBpTransUploadInfoDataAvailable: any = false
  bpTransUploadInfoDisplayedColumn: any
  bpTransUploadInfoTableData: any
  isCustomerInfoDataAvailable: any = false
  customerInfoDisplayedColumn: any
  customerInfoTableData: any
  isItemInfoDataAvailable: any = false
  itemInfoDisplayedColumn: any
  itemInfoTableData: any
  isItemUploadInfoDataAvailable: any = false
  itemUploadInfoDisplayedColumn: any
  itemUploadInfoTableData: any
  isOpeningBalanceInfoDataAvailable: any = false
  openingBalanceInfoDisplayedColumn: any
  openingBalanceInfoTableData: any
  isSupplierInfoDataAvailable: any = false
  supplierInfoDisplayedColumn: any
  supplierInfoTableData: any
  fixedAssetTableData: any
  isFixedAssetTableData: boolean = false;
  fixedAssetDisplayedColumn: any;
  fixedAssetInfoTableData: any;
  iswarehouseTableData: boolean = false;
  warehouseDisplayedColumn: any;
  warehouseInfoTableData: any;
  isDimensionTableData: boolean = false;
  dimensionDisplayedColumn: any;
  dimensionTableData: any;
  isGLTableData: boolean = false;
  glDisplayedColumn: any;
  glTableData: any;
  isPeriodTableData: boolean = false;
  periodDisplayedColumn: any;
  periodTableData: any;
  isVATTableData: boolean = false;
  vatDisplayedColumn: any;
  vatTableData: any;
  isSalesRepableData: boolean = false;
  salesDisplayedColumn: any;
  salesTableData: any;
  editItem: any
  processNames: any = []
  activeProcessIndex: any
  approverMatrixFormGroup: FormGroup
  isMaxLimitReached: any = false
  accessibilities: any = appConfig.DropdownOptions.accessibilities
  levelOfApprovals: any = appConfig.DropdownOptions.financeApproverTypes
  roleTitles: any = []

  constructor(
    private fb: FormBuilder,
    private financeService: FinanceService,
    private utilService: CommonUtilityService,
    private superAdminService: SuperAdminService,
    private route: Router,
    private adminService: AdminService) {
    const hrUrl = environment.superAdmin.financePortalAPI;
    const httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${this.utilService.getCookie('AuthToken')}`);
    this.options = {
      headers: httpHeaders
    };
    this.utilService.getRequest(hrUrl, this.options).subscribe((data: any) => {
      const contentSlot = data.contentSlots.contentSlot;
      if (contentSlot.length > 0) {
        contentSlot.map(each => {
          if (each.slotId === 'FinancePortalVideoSlot') {
            this.financePortalData = each.components.component;
          } else if (each.slotId === 'FinanceVerificationUserSlot') {
            this.verificationId = each.components.component[0].content;
          } else if (each.slotId === 'FinancePortalExcelSlot') {
            this.downloadxl = each.components.component[0].media.url;
          }
        });
      }
      this.constructForm();
    });
    this.getSubCustomers();
  }

  get supply() { return this.approverMatrixFormGroup.controls; }
  get approversArr() { return this.supply.approversArray as FormArray; }

  ngOnInit() {
    this.formGroup = this.fb.group({
      file: [null, Validators.required]
    });

    this.financeService.getSupplierDetails('customermasterdatasetup?action=view').subscribe((res: any) => {
      if(!!res) {
        this.detailedInfo = res
        this.previewUploadedForm()
      }
    },
    err => {

    })

    this.adminService.getRunPayroll('dropdownValues-commonApproval').subscribe((res: any) => {
      if(!!res) {
        if(!!res.financeProcessNames) this.processNames = res.financeProcessNames
        if(!!res.roleTitles) this.roleTitles = res.roleTitles
        this.processNames.forEach(element => { this.toggle.push(false) });
      }
    },
    err => {

    })

    this.approverMatrixFormGroup = this.fb.group({
      approversArray: new FormArray([]),
    });

  }

  constructForm() {
    this.createUserForm = this.fb.group({
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      emailId: [null, Validators.compose([
        Validators.required,
        Validators.email
      ])],
      employeeId: [null, Validators.compose([
        Validators.required,
      ])],
      iqamaId: [null, Validators.compose([
        Validators.required,
      ])],
      customerRole: [null, Validators.required],
      verificationId: [this.verificationId, Validators.required],
      accessbility: [null, Validators.required],
    });
  }

  getSubCustomers() {
    this.toggle = []; this.subCustomersData = []; this.dropdownList = [];
    const customerUrl = environment.superAdmin.hrGetCustomerAPI + this.utilService.getLocalStorage('displayUid') + '/customerInfo';
    this.utilService.getRequest(customerUrl, this.options).subscribe((data: any) => {
      if (data.subEmployeesInfo && data.subEmployeesInfo.subEmployeesInfoList) {
        const customersData = data.subEmployeesInfo.subEmployeesInfoList;
        customersData.map(cust => cust.customerRole ? (cust.customerRole.customerRoleList.map(role => {
          role && role.toUpperCase() === 'FINANCE' ? this.subCustomersData.push(cust) : '';
        })) : '');
        if (this.subCustomersData && this.subCustomersData.length > 0) {
          this.subCustomersData.map(each => { this.dropdownList.push(each.emailId) });
        }
      }
    });
  }

  addLevel = () => {

    if( !this.isMaxLimitReached) {
      let newLevel = this.fb.group({
        key: [''],
        value: ['']
      });
      this.approversArr.push(newLevel)
    }
    this.setLimit()
  }

  removeLevel = i => {
    this.approversArr.removeAt(i)
    this.setLimit()
  };

  setLimit = () => this.isMaxLimitReached = this.approversArr.value.length <= 6 ? false : true

  getProcessDetails = (process, i) => {
    this.activeProcessIndex = i
    this.approversArr.clear()

    this.adminService.getHrRequest(`processApprovalMatrix/${process}`).subscribe((res: any) => {
      if (!!res && !!res.levelOfApprovals) {
        res.levelOfApprovals.forEach(level => {
          let newLevel = this.fb.group({
            key: [level.value1 === 'SpecificEmployee' ? 'Specific Employee' : 'Role Title'],
            value: [level.value2]
          });
          this.approversArr.push(newLevel)
        });
      } else {
        this.addLevel()
      }
      this.toggleIcon(i, 'add')
    },
    err => {
      
    })
  }

  setLevel = process => {
    if(this.approversArr.value.length > 0) {
      let levels = []
      this.approversArr.value.forEach((level, i) => {
        if(!!level.key && (!!level.value || level.key === 'Supervisor')) {
          levels.push({
            key: `L${i + 1}`,
            value1: this.adminService.setCode(level.key, this.levelOfApprovals),
            value2: level.value
          })
        }
      });
      const request = { 
        levelOfApprovals: levels,
        processName: process
       }

       this.adminService.postHrRequest('processApprovalMatrix', request).subscribe((res: any) => {
        if(!!res && !!res.processName) {
          this.dialogMsg('success', `Levels has been set for the process ${res.processName}`)
          this.toggle[this.activeProcessIndex] = false
        }
        else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
       },
       err => {
        if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.dialogMsg('error', err.error.errors[0].message)
        } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
       })
    }
  }

  getMatCustomers(id, i) {
    this.approver1 = undefined; this.approver2 = undefined; this.approver3 = undefined;
    const customerUrl = environment.superAdmin.hrGetCustomerAPI + id + '/customerInfo';
    this.utilService.getRequest(customerUrl, this.options).subscribe((data: any) => {
      this.matSubCustomersData = data;
      data.firstApprover ? (this.dropdownList.push(data.firstApprover), this.approver1 = data.firstApprover) : '';
      data.secondApprover ? (this.dropdownList.push(data.secondApprover), this.approver2 = data.secondApprover) : '';
      data.thirdApprover ? (this.dropdownList.push(data.thirdApprover), this.approver3 = data.thirdApprover) : '';
      const unique = new Set(this.dropdownList);
      this.dropdownList = [...unique]
      this.toggleIcon(i, 'add');
    });
  }
  toggleIcon(i, fn) {
    if (fn === 'add') {
      this.toggle.map((each, index) => {
        if (i === index) {
          this.toggle[i] = true;
        } else {
          this.toggle[index] = false;
        }
      });
    } else {
      this.toggle[i] = false;
    }
  }
  navigateToFinance() {
    this.route.navigate(['/admin/finance'])
  }
  createAdmin(user) {
    const { firstName, lastName, emailId, employeeId, iqamaId, customerRole, verificationId, accessbility } = user;
    const url = environment.superAdmin.financeCreateUserAPI + this.utilService.getLocalStorage('displayUid') + '/register';
    const body = {
      emailId,
      firstName,
      lastName,
      employeeId,
      iqamaId,
      customerRole,
      accessbility
    };
    const httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded;')
      .set('Authorization', `Bearer ${this.utilService.getCookie('AuthToken')}`);
    const options = {
      headers: httpHeaders
    };
    const request: HttpParams = this.utilService.buildQueryParams(body);
    this.utilService.postRequest(url, request.toString(), options).subscribe((res) => {
      this.constructForm();
      this.getSubCustomers();
      this.employeeForm.resetForm();
      this.dialogMsg('success', this.succMsg)
    }, (err) => {
      if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.dialogMsg('error', err.error.errors[0].message)
      } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    });
  }

  dialogMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }

  retrigger(emailId, endpoint) {
    endpoint = endpoint === "retriggerEmail" ? endpoint : `${endpoint}?emailId=${emailId}&action=disable`
    const url = endpoint === "retriggerEmail" ? environment.superAdmin.hrGetCustomerAPI + emailId + '/' + endpoint : environment.superAdmin.financeGetCustomerAPI + emailId + '/' + endpoint;
    const body = { emailId };
    const httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded;')
      .set('Authorization', `Bearer ${this.utilService.getCookie('AuthToken')}`);
    const options = {
      headers: httpHeaders
    };
    const request: HttpParams = this.utilService.buildQueryParams(body);
    this.utilService.postRequest(url, endpoint === "retriggerEmail" && request.toString(), options).subscribe((res) => {
      this.getSubCustomers()
      Swal.fire({
        icon: 'success',
        text: endpoint === "retriggerEmail" ? 'Mail has been triggerred successfully' : 'Employee has been disabled successfully',
        timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
        showConfirmButton: false,
        background: 'black',
        toast: true
      })
    }, (err) => {
      Swal.fire({
        icon: 'error',
        text: appConfig.apiResponseMessages.GenericErrorMsg,
        timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
        showConfirmButton: false,
        background: 'black',
        toast: true
      })
    });
  }
  addApprover(level, id) {
    const url = environment.superAdmin.financeCreateUserAPI + id + '/setAuthMatrix';
    const body = {
      approverLevel1: level === '1' ? this.approver1 : '',
      approverLevel2: level === '2' ? this.approver2 : '',
      approverLevel3: level === '3' ? this.approver3 : ''
    };
    const httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded;')
      .set('Authorization', `Bearer ${this.utilService.getCookie('AuthToken')}`);
    const options = {
      headers: httpHeaders
    };
    const request: HttpParams = this.utilService.buildQueryParams(body);
    this.utilService.putRequest(url, request.toString(), options).subscribe((res) => {
      Swal.fire({
        icon: 'success',
        text: 'Approver has been added successfully',
        timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
        showConfirmButton: false,
        background: 'black',
        toast: true
      })
    }, (err) => {
      Swal.fire({
        icon: 'error',
        text: appConfig.apiResponseMessages.GenericErrorMsg,
        timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
        showConfirmButton: false,
        background: 'black',
        toast: true
      })
    });
  }

  openNextLevel() {
    !this.approver1 ? this.approver1 = 'x' : ((this.approver1 && !this.approver2) ? this.approver2 = 'x' : this.approver3 = 'x');
  }
  downloadFile = e => {
    e.preventDefault();
    const downloadUrl = environment.hostName + this.downloadxl;
    if (this.utilService.isBrowser()) {
      window.open(downloadUrl, '_self')
    }
  }

  toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });

  fileInputChange(event: any) {
    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    const file = event.target.files[0];
    reader.onload = () => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: 'binary' });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        initial[name] = XLSX.utils.sheet_to_json(sheet);
        return initial;
      }, {});
      jsonData ? this.validateData(jsonData, file) : '';
    };
    reader.readAsBinaryString(file);
  }

  validateData = (json, file) => {
    const { isCustomerValidationFailed,
      isItemUploadValidationFailed,
      isItemInfoValidationFailed,
      isBPValidationFailed,
      isDimensionValidationFailed,
      isPeriodValidationFailed,
      isLedgerValidationFailed,
      isAssetValidationFailed,
      isWarehouseValidationFailed,
      isSupplierValidationFailed,
      isOBValidationFailed,
      errors } = this.superAdminService.validateUploadedTemplate(json)

    if (!isCustomerValidationFailed && !isSupplierValidationFailed && !isItemInfoValidationFailed && !isItemUploadValidationFailed
      && !isBPValidationFailed && !isDimensionValidationFailed && !isPeriodValidationFailed && !isLedgerValidationFailed
      && !isAssetValidationFailed && !isWarehouseValidationFailed && !isOBValidationFailed) {
      this.sendRequest(json, file)
    } else {

      Swal.fire({
        icon: 'error',
        text: `${errors.join(',')} . Please correct the data as per the instructions tab & reupload the excel file`,
        timer: 15000,
        showCloseButton: true, 
        cancelButtonText:
        '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
        showConfirmButton: false,
        background: 'black',
        toast: true
      })
    }
  }

  sendRequest(request, file) {
    if (request.customerInfo && request.supplierInfo && request.itemInfo) {
      var formData: any = new FormData();
      formData.set("attachment", file)
      formData.set("customerFinanceMasterSetupData", new Blob([JSON.stringify(request)], { type: "application/json" }))
      this.superAdminService.submitFinanceMasterFormUpload(formData).subscribe((res: any) => {
        if (res) {
          this.dialogMsg('success', appConfig.apiResponseMessages.genericSuccessMsg)
          this.detailedInfo = res
          this.previewUploadedForm()
          this.tabGroupEl.selectedIndex = 2
        } else {
          this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
        }
      },
        err => {
          if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
            this.dialogMsg('error', err.error.errors[0].message)
          } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
        });
    } else {
      this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    }
  }

  previewUploadedForm = () => {
    if(!!this.detailedInfo.bpTransUploadInfo) {
      this.isBpTransUploadInfoDataAvailable = true
      this.bpTransUploadInfoDisplayedColumn = ['invoiceNumber', 'reference', 'documentNumber', 'documentDate', 'businessPartner', 'amountInInvoiceCurrency', 'balanceAmount', 'edit']
      this.bpTransUploadInfoTableData = this.detailedInfo.bpTransUploadInfo
      this.bpTransUploadInfoTableData.index = 2
    }
    if(!!this.detailedInfo.customerInfo) {
      this.isCustomerInfoDataAvailable = true
      this.customerInfoDisplayedColumn = ['name', 'vatNumber', 'bpCode', 'currency', 'country', 'city', 'edit']
      this.customerInfoTableData = this.detailedInfo.customerInfo
      this.customerInfoTableData.index = 0
    }
    if(!!this.detailedInfo.itemInfo) {
      this.isItemInfoDataAvailable = true
      this.itemInfoDisplayedColumn = ['itemCode', 'itemType', 'itemGroup', 'inventoryUnit', 'description', 'currency', 'poPrice', 'soPrice', 'edit']
      this.itemInfoTableData = this.detailedInfo.itemInfo
      this.itemInfoTableData.index = 3
    }
    if(!!this.detailedInfo.itemUploadInfo) {
      this.isItemUploadInfoDataAvailable = true
      this.itemUploadInfoDisplayedColumn = ['itemCode', 'itemDescription', 'itemGroup', 'inventoryUnit', 'poPrice', 'soPrice', 'edit']
      this.itemUploadInfoTableData = this.detailedInfo.itemUploadInfo
      this.itemUploadInfoTableData.index = 4
    }
    if(!!this.detailedInfo.openingBalanceInfo) {
      this.isOpeningBalanceInfoDataAvailable = true
      this.openingBalanceInfoDisplayedColumn = ['uid', 'reference', 'ledger', 'dr', 'date', 'amount', 'edit']
      this.openingBalanceInfoTableData = this.detailedInfo.openingBalanceInfo
      this.openingBalanceInfoTableData.index = 5
    }
    if(!!this.detailedInfo.supplierInfo) {
      this.isSupplierInfoDataAvailable = true
      this.supplierInfoDisplayedColumn = ['name', 'bpCode', 'vatNumber', 'financialGroup', 'currency', 'country', 'city', 'edit']
      this.supplierInfoTableData = this.detailedInfo.supplierInfo
      this.supplierInfoTableData.index = 1
    }
    if(!!this.detailedInfo.asset) {
      this.isFixedAssetTableData = true
      this.fixedAssetDisplayedColumn = ['assetCode', 'assetExtension', 'assetName', 'category', 'assetDescription', 'group', 'purchaseDate', 'serviceDate', 'transactionCost', 'transactionCurrency', 'assetLifeMonth', 'assetLifeYear', 'edit']
      this.fixedAssetInfoTableData = this.detailedInfo.asset
      this.fixedAssetInfoTableData.index = 6
    }
    if(!!this.detailedInfo.warehouse) {
      this.iswarehouseTableData = true
      this.warehouseDisplayedColumn = ['SNo', 'warehouseCode', 'description', 'edit']
      this.warehouseInfoTableData = this.detailedInfo.warehouse
      this.warehouseInfoTableData.index = 7
    }
    if(!!this.detailedInfo.dimension) {
      this.isDimensionTableData = true
      this.dimensionDisplayedColumn = ['dimensionCode', 'description', 'subLevel', 'dimensionType', 'parentDimension', 'edit']
      this.dimensionTableData = this.detailedInfo.dimension
      this.dimensionTableData.index = 10
    }
    if(!!this.detailedInfo.period) {
      this.isPeriodTableData = true
      this.periodDisplayedColumn = ['periodType', 'year', 'period', 'startDate', 'periodDescription', 'correctionPeriod', 'status1', 'status2', 'status3', 'status4', 'status5', 'edit']
      this.periodTableData = this.detailedInfo.period
      this.periodTableData.index = 9
    }
    if(!!this.detailedInfo.ledgerAccount) {
      this.isGLTableData = true
      this.glDisplayedColumn = ['accountNumber', 'description', 'searchKey', 'subLevel', 'parentLedger', 'accountType', 'type', 'status', 'dimension1', 'dimension2', 'dimension3', 'dimension4', 'dimension5', 'edit']
      this.glTableData = this.detailedInfo.ledgerAccount
      this.glTableData.index = 8
    }
    if(!!this.detailedInfo.financeVAT) {
      this.isVATTableData = true
      this.vatDisplayedColumn = ['vatCode', 'vatPercentage', 'vatDescription', 'edit']
      this.vatTableData = this.detailedInfo.financeVAT
      this.vatTableData.index = 11
    }
    if(!!this.detailedInfo.salesRep) {
      this.isSalesRepableData = true
      this.salesDisplayedColumn = ['Code', 'Description', 'edit']
      this.salesTableData = this.detailedInfo.salesRep
      this.salesTableData.index = 12
    }
  }

  onEditFCM = e => {
    this.tabGroupEl.selectedIndex = 0
    this.editItem = e
  }
}
