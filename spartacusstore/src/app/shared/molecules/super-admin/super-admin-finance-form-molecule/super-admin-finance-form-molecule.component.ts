import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import Swal from 'sweetalert2';
import { COUNTRIES } from '../../../../../assets/js/countries.js'
import { PAYMENT_TERMS, DELIVERY_TERMS } from '../../../../../assets/js/paymentTerms.js'
import {
  SuperAdminService,
  VALIDATION_MESSAGES,
  assetCategory
} from '../../../../services/super-admin.service';
import { appConfig } from '../../../../app.config'
import { DatePipe } from '@angular/common';
import { CURRENCIES } from '../../../../../assets/js/currencies';
import { FinanceService } from '../../../../services/finance.service'
import {Observable} from 'rxjs';
import { CommonUtilityService } from '../../../../services/common-utility-service';

@Component({
  selector: 'app-super-admin-finance-form-molecule',
  templateUrl: './super-admin-finance-form-molecule.component.html',
  styleUrls: ['./super-admin-finance-form-molecule.component.scss']
})
export class SuperAdminFinanceFormMoleculeComponent implements OnInit, OnChanges {

  @Input() editItem: any
  genders: any;
  countries: any = COUNTRIES
  paymentTerms: any = PAYMENT_TERMS
  deliveryTerms: any = DELIVERY_TERMS
  maritalStatus: any;
  religions: any;
  currencies: any = CURRENCIES
  inventoryUnits: any = appConfig.DropdownOptions.inventoryUnit
  financialGroups: any = appConfig.DropdownOptions.financialGroup
  accountTypes: any = appConfig.DropdownOptions.accountTypes
  transactionTypes: any = appConfig.DropdownOptions.transactionTypes
  ledgerStatus: any = appConfig.DropdownOptions.ledgerStatus
  dimensionStatus: any = appConfig.DropdownOptions.dimensionStatus
  dimensionTypes: any = appConfig.DropdownOptions.dimensionTypes
  pipe = new DatePipe('en-US');
  customerInfo: FormGroup;
  supplierInfo: FormGroup;
  itemInfo: FormGroup;
  itemUpload: FormGroup;
  ledgerAccount: FormGroup;
  periodFormGroup: FormGroup;
  dimensionFormGroup: FormGroup;
  wareHouseInfo: FormGroup;
  fixedAssetInfo: FormGroup;
  SalesRepFormGroup: FormGroup;
  currencyFormGroup: FormGroup;
  filteredCurrencyCode: Observable<string[]>;
  VATFormGroup: FormGroup
  itemGroup: any = [];
  validationMessages = VALIDATION_MESSAGES;
  buttonName: any = 'Submit';
  showItemGroupField: boolean = false;
  assetCategory: any = assetCategory;
  periods: any = [];
  periodTypeDropDown: any = ['Fiscal', 'Tax', 'Reporting'];
  correctionPeriodDropDown: any = ['YES', 'NO']
  vatArrays: any = appConfig.DropdownOptions.vatCodes;
  enums: any
  currenciesRate: any
  availableBanks: any = appConfig.DropdownOptions.bankLists
  warehouses: any = []

  constructor(private fb: FormBuilder,
              private superAdminService: SuperAdminService,
              private matSnack: MatSnackBar,
              private financeService: FinanceService,
              private utilityService: CommonUtilityService) {
               }

              get supply() { return this.VATFormGroup.controls; }
              get vatArr() { return this.supply.vatArray as FormArray; }

              get currencyConv() { return this.currencyFormGroup.controls; }
              get rateArr() { return this.currencyConv.rate as FormArray; }

  ngOnChanges(changes: SimpleChanges) {
    if(!!changes && !!changes.editItem && !!changes.editItem.currentValue) {
      this.autoPopulateItem(changes.editItem.currentValue)
    }
  }

  ngOnInit() {

    this.getConversionRate();

    this.financeService.getSupplierDetails('customermasterdatasetup?action=view').subscribe((res: any) => {
      if (res && res.supplierInfo && res.itemInfo) {
        this.itemGroup = res.itemInfo;
        this.periods = res.period;
      }
      //this.itemGroup = [...new Set(this.itemGroup.map(obj => obj.itemGroup))];
      let unique = [];
      let distinct = [];
      for( let i=0; i<this.itemGroup.length; i++) {
        if( !unique[this.itemGroup[i].itemGroup]){
          distinct.push(this.itemGroup[i]);
          unique[this.itemGroup[i].itemGroup] = 1;
        }
    }

    this.itemGroup = distinct;

    console.log(distinct, this.itemGroup)

      if(!!res && !!res.warehouse) this.warehouses = res.warehouse
    },
    err => {
    });

    this.customerInfo = this.fb.group({
      bpCode: [null, Validators.required],
      name: [null, Validators.required],
      streetName: [null, Validators.required],
      country: [null, Validators.required],
      city: [null, Validators.required],
      vatNumber: [null, Validators.required],
      currency: [null, Validators.required],
      financialGroup: [null, Validators.required],
      paymentTerms: [null, Validators.required],
      deliveryTerms: [null, Validators.required],
      bank: [null, Validators.required]
    });

    this.supplierInfo = this.fb.group({
      bpCode: [null, Validators.required],
      name: [null, Validators.required],
      country: [null, Validators.required],
      city: [null, Validators.required],
      streetName: [null, Validators.required],
      vatNumber: [null, Validators.required],
      currency: [null, Validators.required],
      financialGroup: [null, Validators.required],
      paymentTerm: [null, Validators.required],
      bank: [null, Validators.required]
    });

    this.itemInfo = this.fb.group({
      itemCode: [null, Validators.required],
      description: [null, Validators.required],
      itemType: [null, Validators.required],
      itemGroup: [null, Validators.required],
      itemGroupDescription: [null, Validators.required],
      inventoryUnit: [null, Validators.required],
      currency: [null, Validators.required],
      purchasePrice: [null, Validators.required],
      salesPrice: [null, Validators.required],
      othersitemgroup: [''],
      warehouse: [null, Validators.required],
      quantity: [null, Validators.required],
    });

    this.wareHouseInfo = this.fb.group({
      code: [null, Validators.compose([
        Validators.required,
        Validators.pattern(appConfig.pattern.wareHouseCodeMaxlimit)
        
      ])],
      description: [null, Validators.compose([
        Validators.required,
        Validators.pattern(appConfig.pattern.wareHouseDescMaxlimit)
        
      ])],
    });

    this.fixedAssetInfo = this.fb.group({
      assetCode: [null, Validators.required],
      assetName: [null, Validators.required],
      assetExtension: [null, Validators.required],
      group: [null, Validators.required],
      category: [null, Validators.required],
      description: [null, Validators.required],
      serviceDate: [null, Validators.required],
      purchaseDate: [null, Validators.required],
      transactionCost: [null, Validators.required],
      transactionCurrency: [null, Validators.required],
      // assetLifeYear: ['', [Validators.required, Validators.pattern(appConfig.pattern.yearMaxlimit)]],
      assetLifeYear: ['', [Validators.required]],
      assetLifeMonth: ['', [Validators.required, Validators.pattern(appConfig.pattern.monthMaxlimit)]]
    });

    this.itemUpload = this.fb.group({
      itemCode: [null],
      itemDescription: [null],
      itemGroup: [null],
      warehouse: [null],
      inventoryUnit: [null],
      quantity: [null],
      price: [null]
    });

    this.ledgerAccount = this.fb.group({
      accountNumber: [null, Validators.required],
      description: [null, Validators.required],
      searchKey: [null, Validators.required],
      subLevel: [null, Validators.required],
      parentLedger: [null],
      accountType: [null, Validators.required],
      type: [null, Validators.required],
      status: [null, Validators.required],
      dimension1: [null, Validators.required],
      dimension2: [null, Validators.required],
      dimension3: [null, Validators.required],
      dimension4: [null, Validators.required],
      dimension5: [null, Validators.required],
    });

    this.periodFormGroup = this.fb.group({
      periodType: [null, Validators.required],
      year: [null, Validators.required],
      period: [null, Validators.required],
      startDate: [null, Validators.required],
      description: [null, Validators.required],
      correctionPeriod: [null, Validators.required],
      status1: [null, Validators.required],
      status2: [null, Validators.required],
      status3: [null, Validators.required],
      status4: [null, Validators.required],
      status5: [null, Validators.required],
    });

    this.dimensionFormGroup = this.fb.group({
      code: [null, Validators.required],
      description: [null, Validators.required],
      subLevel: [null, Validators.required],
      dimensionType: [null, Validators.required],
      parentDimension: [null]
    });

    this.SalesRepFormGroup = this.fb.group({
      key: [null, Validators.required],
      value: [null, Validators.required]
    });

    this.currencyFormGroup = this.fb.group({
      currency: [null, Validators.required],
      rate: new FormArray([])
    });

    this.VATFormGroup = this.fb.group({
      vatArray: new FormArray([])
    })

    this.vatArrays.forEach(vat => {
      let newRow = this.fb.group({
        vatCode: [{ value: vat.vatCode, disabled: true }],
        vatDescription: [{ value: vat.vatDescription, disabled: true }],
        vatPercentage: [vat.vatPercentage]
      });
  
      this.vatArr.push(newRow);
    });

  }

  optionalValidator(validators?: (ValidatorFn | null | undefined)[]): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {

        return control.value ? Validators.compose(validators)(control) : null;
      };
  }

  updateSelect = (e, item) => {

    if(e.isUserInput) {
      if(e.source.selected) {
        let newRow = this.fb.group({
          key: [{ value: item.isocode + '-' + item.name, disabled: true }, Validators.required],
          value: [item.conversion, Validators.required]
        });
    
        this.rateArr.push(newRow);
      } else {
        const index = this.rateArr.getRawValue().findIndex(e => e.key === item.isocode + '-' + item.name)
        this.rateArr.removeAt(index);
      }
    }

  }

  remove(i, item) {
    this.rateArr.removeAt(i);
  }

  getConversionRate() {
    this.financeService.getSupplierDetails('currencies').subscribe((res: any) => { 
      if (res && res.currencies) {
        this.currenciesRate = res.currencies;
      }
    },
    err => {
    });
  }

  checkValues(val) {
    return val ? val + '' : '';
  }

  checkLimit = (formGroup, key, limit) => {
    String(this[formGroup].get([key]).value).length > limit ? this[formGroup].patchValue({[key]: parseInt(String(this[formGroup].get([key]).value).slice(0, limit))}) : null
  }

  checkOthers(e, value) {
    if (e.isUserInput) {
      if (value === "Others") {
        this.showItemGroupField = true;
        this.itemInfo.get('othersitemgroup').setValidators([Validators.required]);
        this.itemInfo.get('itemGroupDescription').setValue('')
      } else {
        this.showItemGroupField = false;
        this.itemInfo.get('othersitemgroup').clearValidators();
        this.itemInfo.get('itemGroupDescription').setValue(value.itemGroupDescription)
      }
    }
  }
  
  onSubmitForm = () => {
        let request = {};

      let customerInfo = [{
          bpCode: this.checkValues(this.customerInfo.value.bpCode) ,
          name: this.checkValues(this.customerInfo.value.name) ,
          streetName: this.checkValues(this.customerInfo.value.streetName),
          country: this.checkValues(this.customerInfo.value.country) ,
          city: this.checkValues(this.customerInfo.value.city) ,
          vatNumber: this.checkValues(this.customerInfo.value.vatNumber)  ,
          currency: this.checkValues(this.customerInfo.value.currency) ,
          financialGroup: this.checkValues(this.customerInfo.value.financialGroup),
          paymentTerms: this.checkValues(this.customerInfo.value.paymentTerms),
          deliveryTerms: this.checkValues(this.customerInfo.value.deliveryTerms),
          bank: `${!!this.customerInfo.value.bank ? this.customerInfo.value.bank : ''}`
        }]

        let supplierInfo = [{
           bpCode:  this.checkValues(this.supplierInfo.value.bpCode) ,
           name:  this.checkValues(this.supplierInfo.value.name) ,
           country:  this.checkValues(this.supplierInfo.value.country) ,
           city:  this.checkValues(this.supplierInfo.value.city) ,
           streetName: this.checkValues(this.supplierInfo.value.streetName),
           vatNumber:  this.checkValues(this.supplierInfo.value.vatNumber),
           currency:  this.checkValues(this.supplierInfo.value.currency) ,
           financialGroup:  this.checkValues(this.supplierInfo.value.financialGroup),
           paymentTerms: this.checkValues(this.supplierInfo.value.paymentTerm),
           bank: `${!!this.supplierInfo.value.bank ? this.supplierInfo.value.bank : ''}`
          }]

         let itemInfo = [{
          itemCode:  this.checkValues(this.itemInfo.value.itemCode) ,
          description:  this.checkValues(this.itemInfo.value.description) ,
          itemType:  this.checkValues(this.itemInfo.value.itemType) ,
          itemGroup:  this.checkValues(this.itemInfo.value.itemGroup) === "Others" ? this.itemInfo.value.othersitemgroup : this.checkValues(this.itemInfo.value.itemGroup) ,
          itemGroupDescription:  this.checkValues(this.itemInfo.value.itemGroupDescription) ,
          inventoryUnit:  this.checkValues(this.itemInfo.value.inventoryUnit) ,
          currency:  this.checkValues(this.itemInfo.value.currency),
          price: this.checkValues(this.itemInfo.value.price),
          purchasePrice: this.checkValues(this.itemInfo.value.purchasePrice),
          salesPrice: this.checkValues(this.itemInfo.value.salesPrice), 
          warehouse: this.checkValues(this.itemInfo.value.warehouse),
          quantity: this.itemInfo.value.quantity,
        }]

        let  warehouseInfo =  [{
            code:  this.checkValues(this.wareHouseInfo.value.code) ,
            description:  this.checkValues(this.wareHouseInfo.value.description)
          }]

          let asset =  [{
            assetCode: this.checkValues(this.fixedAssetInfo.value.assetCode),
            assetName: this.checkValues(this.fixedAssetInfo.value.assetName),
            assetExtension: this.checkValues(this.fixedAssetInfo.value.assetExtension),
            group: this.checkValues(this.fixedAssetInfo.value.group),
            category: this.checkValues(this.fixedAssetInfo.value.category),
            description: this.checkValues(this.fixedAssetInfo.value.description),
            serviceDate: this.checkValues(this.pipe.transform(this.fixedAssetInfo.value.serviceDate, 'MM/dd/yyyy')),
            purchaseDate: this.checkValues(this.pipe.transform(this.fixedAssetInfo.value.purchaseDate, 'MM/dd/yyyy')),
            transactionCost: this.checkValues(this.fixedAssetInfo.value.transactionCost),
            transactionCurrency: this.checkValues(this.fixedAssetInfo.value.transactionCurrency),
            assetLifeYear: this.fixedAssetInfo.value.assetLifeYear,
            assetLifeMonth: this.fixedAssetInfo.value.assetLifeMonth
          }]
      
      if(this.periodFormGroup.valid) {
        this.periodFormGroup.value.startDate = this.pipe.transform(this.periodFormGroup.value.startDate, 'MM/dd/yyyy');
      }
      
      if(this.customerInfo.valid) request['customerInfo'] = customerInfo
      if(this.supplierInfo.valid) request['supplierInfo'] = supplierInfo
      if(this.itemInfo.valid) request['itemInfo'] = itemInfo
      if(this.wareHouseInfo.valid) request['warehouse'] = warehouseInfo
      if(this.fixedAssetInfo.valid) request['asset'] = asset
      if(this.ledgerAccount.valid) request['ledgerAccount'] = [{...this.ledgerAccount.value}]
      if(this.dimensionFormGroup.valid) request['dimension'] = [{...this.dimensionFormGroup.value}]
      if(this.periodFormGroup.valid) request['period'] = [{...this.periodFormGroup.value}]
      if(this.SalesRepFormGroup.valid) request['salesRep'] = [{...this.SalesRepFormGroup.value}]
      if(this.VATFormGroup.valid) {
        let vatDetails = []
        this.VATFormGroup.getRawValue().vatArray.forEach(vat => {
          if(!!vat.vatPercentage) {
            vatDetails.push({
              vatCode: vat.vatCode,
              vatPercentage: vat.vatPercentage,
              vatDescription: vat.vatDescription
            })
          }
        });
        request['financeVAT'] = vatDetails
      }
      if(this.currencyFormGroup.valid && this.rateArr.valid && this.rateArr.getRawValue().length > 0) request["conversionRate"] = this.rateArr.getRawValue();
        let formData: any = new FormData();
        formData.set('attachment', '');
        formData.set('customerFinanceMasterSetupData', new Blob([JSON.stringify(request)], {type : 'application/json'}));
        this.superAdminService.submitFinanceMasterFormUpload(formData).subscribe((res: any) => {
        if (res) {
          this.dialogMsg('success', 'Form submitted successfully');
          this.rateArr.reset();
          this.currencyFormGroup.reset()
          while (this.rateArr.length !== 0) {
            this.rateArr.removeAt(0)
          }
        } else {
          this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
        }
        this.SalesRepFormGroup.reset();
      },
        err => {
          if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
            this.dialogMsg('error', err.error.errors[0].message)
          } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
        });
  }

  autoPopulateItem = item => {
    switch(item.index) {
      case 0:
        this.customerInfo.patchValue({
          bpCode: item.bpCode,
          name: item.name,
          country: item.country,
          city: item.city,
          streetName: item.streetName,
          vatNumber: item.vatNumber,
          currency: item.currency,
          financialGroup: item.financialGroup,
          deliveryTerms: item.deliveryTerms,
          paymentTerms: item.paymentTerms,
          bank: item.bank
        })
        return
      case 1:
        this.supplierInfo.patchValue({
          bpCode: item.bpCode,
          name: item.name,
          country: item.country,
          city: item.city,
          streetName: item.streetName,
          vatNumber: item.vatNumber,
          currency: item.currency,
          financialGroup: item.financialGroup,
          paymentTerm: item.paymentTerms,
          bank: item.bank
        })
        return
      case 3:
        this.itemGroup.push({itemGroup:item.itemGroup});
        let unique = [];
        let distinct = [];
        for( let i=0; i<this.itemGroup.length; i++) {
          if( !unique[this.itemGroup[i].itemGroup]){
            distinct.push(this.itemGroup[i]);
            unique[this.itemGroup[i].itemGroup] = 1;
          }
        } 
        this.itemGroup = distinct
        this.itemInfo.patchValue({
          itemCode: item.itemCode,
          description: item.description,
          itemType: item.itemType,
          itemGroup: item.itemGroup,
          inventoryUnit: item.inventoryUnit,
          currency: item.currency,
          warehouse: '',
          quantity: item.quantity,
          itemGroupDescription: item.itemGroupDescription,
          purchasePrice: item.purchasePrice,
          salesPrice: item.salesPrice
        })
        return
      case 4:
        this.itemUpload.patchValue({
          itemCode: item.itemCode,
          itemDescription: item.itemDescription,
          itemGroup: item.itemGroup,
          inventoryUnit: item.inventoryUnit,
          price: item.price
        })
        return
      case 6:
        this.fixedAssetInfo.patchValue({
          assetCode: item.assetCode,
          assetName: item.assetName,
          assetExtension: item.assetExtension,
          group: item.group,
          category: item.category,
          description: item.description,
          serviceDate: new Date(item.serviceDate),
          purchaseDate: new Date(item.purchaseDate),
          transactionCost: item.transactionCost,
          transactionCurrency: item.transactionCurrency,
          assetLifeMonth: item.assetLifeMonth, //? new Date(Date.parse(item.assetLifeMonth +" 1, 2012")).getMonth()+1 : item.assetLifeMonth,
          assetLifeYear: item.assetLifeYear
        })
        return
      case 7:
        this.wareHouseInfo.patchValue({
          code: item.code,
          description: item.description
        })
        return
      case 8:
        this.ledgerAccount.patchValue({
          accountNumber: item.accountNumber,
          description: item.description,
          searchKey: item.searchKey,
          subLevel: item.subLevel,
          parentLedger: item.parentLedger,
          accountType: item.accountType,
          type: item.type,
          status: item.status,
          dimension1: item.dimensionStatus[1].value,
          dimension2: item.dimensionStatus[2].value,
          dimension3: item.dimensionStatus[0].value,
          dimension4: item.dimensionStatus[4].value,
          dimension5: item.dimensionStatus[3].value,                      
        })
        return
      case 9:
        this.periodFormGroup.patchValue({
          periodType: item.periodType,
          year: item.year,
          period: item.period,
          startDate: new Date(item.startDate),
          description: item.description,
          correctionPeriod: item.correctionPeriod,
          status1: item.periodStatus[0].value === 'YES' ? 'Open' : 'Closed',
          status2: item.periodStatus[1].value === 'YES' ? 'Open' : 'Closed',
          status3: item.periodStatus[2].value === 'YES' ? 'Open' : 'Closed',
          status4: item.periodStatus[3].value === 'YES' ? 'Open' : 'Closed',
          status5: item.periodStatus[4].value === 'YES' ? 'Open' : 'Closed',
        })
        return
      case 10: 
      this.dimensionFormGroup.patchValue({
        code: item.code,
        description: item.description,
        subLevel: item.subLevel,
        dimensionType: item.dimensionType,
        parentDimension: item.parentDimension
      })
      return
      case 11:
        this.vatArr.clear()
        
          let newRow = this.fb.group({
            vatCode: [{ value: item.vatCode, disabled: true }],
            vatDescription: [{ value: item.vatDescription, disabled: true }],
            vatPercentage: [item.vatPercentage]
          });
      
          this.vatArr.push(newRow);
      return
      case 12:
          this.SalesRepFormGroup.patchValue({
            key: item.key,
            value: item.value
          })
    return
    default:
      return
    }
  }

  dialogMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }

}
