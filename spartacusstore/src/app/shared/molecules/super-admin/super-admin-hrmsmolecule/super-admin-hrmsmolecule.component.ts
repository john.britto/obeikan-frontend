import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { CommonUtilityService } from '../../../../services/common-utility-service';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import Swal from 'sweetalert2';
import { appConfig } from '../../../../app.config';
import { AdminService } from '../../../../services/admin.service';

@Component({
  selector: 'app-super-admin-hrmsmolecule',
  templateUrl: './super-admin-hrmsmolecule.component.html',
  styleUrls: ['./super-admin-hrmsmolecule.component.scss']
})
export class SuperAdminHRMSMoleculeComponent implements OnInit {
  @ViewChild('f', { static: false }) employeeForm;
  @ViewChild('adminForm', { static: false }) adminForm;
  selected = new FormControl(0);
  createUserForm: FormGroup;
  superAdminForm: FormGroup
  hrPortalData: any;
  subCustomersData: any = [];
  customerRoles: any = appConfig.DropdownOptions.customerRoles
  hostName = environment.hostName;
  disableCreateUserForm = false;
  validationMessages = {
    firstName: [{ type: 'required', message: 'First name is required' }],
    lastName: [{ type: 'required', message: 'Last name is required' }],
    email: [
      { type: 'required', message: 'Email is required' },
      { type: 'email', message: 'Enter a valid email' }
    ],
    employeeId: [{ type: 'required', message: 'Employee ID is required' }],
    employeeNumber: [{ type: 'required', message: 'Employee Number is required' }],
    iqamaId: [{ type: 'required', message: 'Iqama Id is required' }],
    customerRole: [{ type: 'required', message: 'Please select any option' }]
  };
  options: any;

  // Authorization Matrix states
  approverMatrixFormGroup: FormGroup
  processNames: any = []
  toggle: any = []
  levelOfApprovals: any = appConfig.DropdownOptions.approverTypes
  jobTitles: any = []
  employees: any = []
  activeProcessIndex: any
  isMaxLimitReached: any = false

  constructor(
    private fb: FormBuilder,
    private utilService: CommonUtilityService,
    private adminService: AdminService) {
    const hrUrl = environment.superAdmin.hrPortalAPI;
    const httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${this.utilService.getCookie('AuthToken')}`);
    this.options = {
      headers: httpHeaders
    };
    this.utilService.getRequest(hrUrl, this.options).subscribe((data: any) => {
      this.hrPortalData = data.contentSlots.contentSlot.components.component;
    });
    this.getSubCustomers();
    this.constructForm();
  }

  get supply() { return this.approverMatrixFormGroup.controls; }
  get approversArr() { return this.supply.approversArray as FormArray; }

  ngOnInit() {

    this.adminService.getRunPayroll('dropdownValues-commonApproval').subscribe((res: any) => {
      if(!!res) {
        if(!!res.processNames) this.processNames = res.processNames
        if(!!res.jobTitles) this.jobTitles = res.jobTitles
        if(!!res.employees) this.employees = res.employees
        this.processNames.forEach(element => { this.toggle.push(false) });
        this.processNames.sort();
      }
    },
    err => {

    })

    this.superAdminForm = this.fb.group({
      emailAddress: [{value: this.utilService.getLocalStorage('displayUid'), disabled: true}, Validators.required],
      employeeNumber: [null, Validators.required],
      iqamaNo: [null, Validators.required]
    });

    this.approverMatrixFormGroup = this.fb.group({
      approversArray: new FormArray([]),
    });
  }

  constructForm() {
    this.createUserForm = this.fb.group({
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      emailId: [null, Validators.compose([
        Validators.required,
        Validators.email
      ])],
      employeeId: [null, Validators.required],
      iqamaId: [null, Validators.required],
      customerRole: [null, Validators.required]
    });
  }

  getSubCustomers() {
    this.subCustomersData = [];
    const customerUrl = environment.superAdmin.hrGetCustomerAPI + this.utilService.getLocalStorage('displayUid') + '/customerInfo';
    this.utilService.getRequest(customerUrl, this.options).subscribe((data: any) => {
      if (data.subEmployeesInfo && data.subEmployeesInfo.subEmployeesInfoList) {
        const customersData = data.subEmployeesInfo.subEmployeesInfoList;
        customersData.map(cust => cust.customerRole ? (cust.customerRole.customerRoleList.map(role => {
          role && (appConfig.DropdownOptions.customerRoles.includes(role) || role === 'HR') ? this.subCustomersData.push(cust) : '';
        })) : ''); console.log(this.subCustomersData)
        var flags = [], output = [], l = this.subCustomersData.length, i;
        for( i=0; i<l; i++) {
            if( flags[this.subCustomersData[i].emailId]) continue;
            flags[this.subCustomersData[i].emailId] = true;
            output.push(this.subCustomersData[i]);
        }
        this.subCustomersData = output;
      }
    });
  }
  createAdmin(user) {
    const { firstName, lastName, emailId, employeeId, iqamaId, customerRole } = user;
    const url = environment.superAdmin.hrCreateUserAPI + this.utilService.getLocalStorage('displayUid') + '/register';
    const body = {
      emailId,
      firstName,
      lastName,
      employeeId,
      iqamaId,
      customerRole
    };
    const httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded;')
      .set('Authorization', `Bearer ${this.utilService.getCookie('AuthToken')}`);
    const options = {
      headers: httpHeaders
    };
    const request: HttpParams = this.utilService.buildQueryParams(body);
    this.utilService.postRequest(url, request.toString(), options).subscribe((res) => {
      this.constructForm();
      this.getSubCustomers();
      this.employeeForm.resetForm();
      this.dialogMsg('success', appConfig.apiResponseMessages.genericSuccessMsg)
    }, (err) => {
      if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.dialogMsg('error', err.error.errors[0].message)
      } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    });
  }

  onSubmitSuperAdmin = () => {

    const payload = {
      ...this.superAdminForm.getRawValue()
    }

    this.adminService.hrPutRequest('employee-info', payload).subscribe((res: any) => {
      if(!!res) {
        this.dialogMsg('success', appConfig.apiResponseMessages.genericSuccessMsg)
        this.adminForm.resetForm();
      }
    },
    err => {
      if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.dialogMsg('error', err.error.errors[0].message)
      } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    })
  }

  dialogMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }

  retrigger(emailId) {
    const url = environment.superAdmin.hrCreateUserAPI + emailId + '/retriggerEmail';
    const body = { emailId };
    const httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded;')
      .set('Authorization', `Bearer ${this.utilService.getCookie('AuthToken')}`);
    const options = {
      headers: httpHeaders
    };
    const request: HttpParams = this.utilService.buildQueryParams(body);
    this.utilService.postRequest(url, request.toString(), options).subscribe((res) => {
      Swal.fire({
        icon: 'success',
        text: 'Mail has been triggerred successfully',
        timer: 15000,
        showCloseButton: true, 
        cancelButtonText:
        '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
        showConfirmButton: false,
        background: 'black',
        toast: true
      })
    }, (err) => {
      if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.dialogMsg('error', err.error.errors[0].message)
      } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    });
  }

  getProcessDetails = (process, i) => {
    this.activeProcessIndex = i
    this.approversArr.clear()

    this.adminService.getHrRequest(`processApprovalMatrix/${process}`).subscribe((res: any) => {
      if (!!res && !!res.levelOfApprovals) {
        res.levelOfApprovals.forEach(level => {
          let newLevel = this.fb.group({
            key: [level.value1 === 'SpecificEmployee' ? 'Nominee' : level.value1],
            value: [level.value2]
          });
          this.approversArr.push(newLevel)
        });
      } else {
        this.addLevel()
      }
      this.toggleIcon(i, 'add')
    },
    err => {
      
    })
  }

  toggleIcon (i, action) {
    if (action === 'add') {
      this.toggle.map((each, index) => {
        if (i === index) {
          this.toggle[i] = true;
        } else {
          this.toggle[index] = false;
        }
      });
    } else {
      this.toggle[i] = false;
    }
  }
  addLevel = () => {

    if( !this.isMaxLimitReached) {
      let newLevel = this.fb.group({
        key: ['Supervisor'],
        value: ['']
      });
      this.approversArr.push(newLevel)
    }
    this.setLimit()
  }

  removeLevel = i => {
    this.approversArr.removeAt(i)
    this.setLimit()
  };

  setLimit = () => this.isMaxLimitReached = this.approversArr.value.length <= 2 ? false : true

  setLevel = process => {
    if(this.approversArr.value.length > 0) {
      let levels = []
      this.approversArr.value.forEach((level, i) => {
        if(!!level.key && (!!level.value || level.key === 'Supervisor')) {
          levels.push({
            key: `L${i + 1}`,
            value1: this.adminService.setCode(level.key, this.levelOfApprovals),
            value2: level.key === 'Supervisor' ? '' : level.value
          })
        }
      });
      const request = { 
        levelOfApprovals: levels,
        processName: process
       }

       this.adminService.postHrRequest('processApprovalMatrix', request).subscribe((res: any) => {
        if(!!res && !!res.processName) {
          this.dialogMsg('success', `Levels has been set for the process ${res.processName}`)
          this.toggle[this.activeProcessIndex] = false
        }
        else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
       },
       err => {
        if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.dialogMsg('error', err.error.errors[0].message)
        } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
       })
    }
  }
}
