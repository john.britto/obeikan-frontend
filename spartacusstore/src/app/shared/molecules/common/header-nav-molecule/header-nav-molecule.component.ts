import { Component, OnInit, Input } from '@angular/core';
import { CommonUtilityService } from '../../../../services/common-utility-service';
import { EmitterService } from '../../../../services/emitter.service';
@Component({
  selector: 'cx-header-nav-molecule',
  templateUrl: './header-nav-molecule.component.html',
  styleUrls: ['./header-nav-molecule.component.scss']
})
export class HeaderNavMoleculeComponent implements OnInit {

  @Input() headerNavLinks: String[]
  @Input() headerLinks: any
  isSuperAdmin: any = false;
  constructor(private utilityService: CommonUtilityService, private emitter: EmitterService) {
    this.emitter.superAdminSource.subscribe((data) => {
      if (data) {
        this.isSuperAdmin = this.utilityService.getCookie('isSuperAdmin') && this.utilityService.getCookie('superAdminOrders');
      }
    })
  }
  ngOnInit() {
    this.isSuperAdmin = this.utilityService.getCookie('isSuperAdmin') && this.utilityService.getCookie('superAdminOrders');
 
  }

  closeNav = () => {
    if(this.utilityService.isBrowser()) {
      document.getElementById("mySidenav").style.width = "0";
    }    
  }

  openNav = () => {
    if(this.utilityService.isBrowser()) {
      document.getElementById("mySidenav").style.width = "150px";
    }
  }

  scrollToProduct = e => {
    e.preventDefault()
    this.emitter.scrollToSaned.next(true);
  }
}
