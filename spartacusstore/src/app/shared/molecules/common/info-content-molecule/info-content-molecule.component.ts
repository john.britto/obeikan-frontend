import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-info-content-molecule',
  templateUrl: './info-content-molecule.component.html',
  styleUrls: ['./info-content-molecule.component.scss']
})
export class InfoContentMoleculeComponent implements OnInit {

  @Input() sectionOneInfoContent: any

  constructor() { }

  ngOnInit() {
  }

}
