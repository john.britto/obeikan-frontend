import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { LoaderService } from '../../../../services/loader.service';

@Component({
  selector: 'app-loader-molecule',
  templateUrl: './loader-molecule.component.html',
  styleUrls: ['./loader-molecule.component.scss']
})
export class LoaderMoleculeComponent implements OnInit {
  color = 'warn';
  mode = 'indeterminate';
  value = 50;
  isLoading: Subject<boolean> = this.loaderService.isLoading;
  constructor(private loaderService: LoaderService) { }

  ngOnInit() {
  }

}
