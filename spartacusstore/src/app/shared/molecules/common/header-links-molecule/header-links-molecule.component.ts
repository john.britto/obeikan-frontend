import { Component, OnInit, Input, ViewEncapsulation, ElementRef, ChangeDetectorRef } from '@angular/core';
import { Subject } from 'rxjs';
import { LoginService } from '../../../../services/login.service'
import { CommonUtilityService } from '../../../../services/common-utility-service'
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { EmitterService } from '../../../../services/emitter.service';
import { SuperAdminService } from '../../../../services/super-admin.service'
import { LoaderService } from '../../../../services/loader.service';
import { AdminService } from '../../../../services/admin.service';
@Component({
  selector: 'app-cx-header-links-molecule',
  templateUrl: './header-links-molecule.component.html',
  styleUrls: ['./header-links-molecule.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderLinksMoleculeComponent implements OnInit {

  @Input() headerLinks: any;
  displayName: any = '';
  displayUID: any = ';'
  isLoggedIn: boolean = false;
  userContext: any;
  customerRoles: any = []
  superAdminRoles: any = []
  isApprover: boolean = false
  isFinanceUser: boolean = false
  isHRMSUser: boolean = false
  subscription: Subscription;
  isLoading: Subject<boolean> = this.loaderService.isLoading;
  isSuperAdmin: any = false;
  roles: any

  constructor(
    private loginService: LoginService,
    private utilityService: CommonUtilityService,
    private router: Router,
    private emitter: EmitterService,
    private superAdminService: SuperAdminService,
    private loaderService: LoaderService,
    private adminService: AdminService) {
    this.isLoggedIn = false;

    this.loginService.customerRoles = this.utilityService.getCookie('customerRoles') !== undefined ? JSON.parse(this.utilityService.getCookie('customerRoles')) : [];
    this.checkCustomerRolesRole()

    this.loginService.userContext.subscribe(context => {
      this.userContext = context;
      this.displayName = context.displayName;
      this.displayUID = context.displayUID;
      this.isSuperAdmin = context.isSuperUser
      this.checkCustomerRolesRole()
    });

    this.superAdminService.getSuperAdminRole().subscribe(roles => {
      if(!!roles) {
        this.roles = roles
        this.adminService.customerRoles = this.roles
        this.isFinanceUser = roles.includes('Finance_CORE')
      } else {
        this.isFinanceUser = false
      }
    })
  }

  ngOnInit() {
    this.loginService.userContext.subscribe(context => {
      this.displayName = context.displayName;
      this.displayUID = context.displayUID;
      this.isSuperAdmin = context.isSuperUser
      this.isLoggedIn = context.displayUID ? true : false;
      this.checkCustomerRolesRole()
    });
  }

  checkCustomerRolesRole = () => {

    const approverRoles = ['ITApprover', 'FinanceApprover']
    this.customerRoles = this.loginService.customerRoles
    this.isHRMSUser = this.customerRoles.includes('HREmployee')
    this.isApprover = false
    this.isSuperAdmin = !this.isSuperAdmin ? this.utilityService.getCookie("isSuperAdmin") : true

    if (Array.isArray(this.customerRoles)) {
      this.customerRoles.map(role => {
        if (role === approverRoles[0] || role === approverRoles[1]) {
          this.isApprover = true
        }
      })
    }
  }

  onLogout(e): void {

    e.preventDefault()

    this.superAdminService.setSuperAdminRoles([])
    this.userContext.displayUID = ''
    this.userContext.displayName = ''
    this.userContext.isAuthenticated = false

    this.loginService.removeLocalStorage()
    
    this.loginService.changeUserContext(this.userContext);
    this.emitter.superAdminSource.next(true);
    this.router.navigateByUrl('/login');
  }

  onToggleLogin(type) {
    if(type) document.getElementById('login-overlay').style.display = 'flex'
    if(!type && !this.loaderService.isInProgress) document.getElementById('login-overlay').style.display = 'none'
  }
}
