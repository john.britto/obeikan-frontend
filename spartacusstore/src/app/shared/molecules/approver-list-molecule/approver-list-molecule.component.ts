import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { ApproverService, SUCCESS_MSG, ATTACHMENT_URL } from '../../../services/approver.service'
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { CommonUtilityService } from '../../../services/common-utility-service'
import { appConfig } from 'src/app/app.config';

@Component({
  selector: 'app-approver-list-molecule',
  templateUrl: './approver-list-molecule.component.html',
  styleUrls: ['./approver-list-molecule.component.scss']
})

export class ApproverListMoleculeComponent implements OnInit {

  @Input() approvalList: any
  displayedColumns: string[]
  ELEMENT_DATA: PeriodicElement[]
  dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
  displayDetailedInfo: any = false
  detailedInfo: any
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  downloadLink: any = `${environment.hostName}${ATTACHMENT_URL}`

  constructor(
    private approverService: ApproverService
  ) { }

  ngOnChanges() {
    this.ELEMENT_DATA = this.approvalList
    this.dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
    this.displayedColumns = ['No', 'code', 'employee', 'createdDate', 'modifiedDate', 'status', 'Approve/Reject'];
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
    this.ELEMENT_DATA = this.approvalList
    this.dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
    this.displayedColumns = ['No', 'code', 'employee', 'createdDate', 'modifiedDate', 'status', 'Approve/Reject'];
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  displayInfo = data => {
    this.detailedInfo = data
    this.displayDetailedInfo = true
  }

  updateDetails = (data, type) => {

    this.approverService.updateStatus(data.code, type).subscribe((res: any) => {
      this.displayMsg('success', `${SUCCESS_MSG}${type}`)
    },
      err => {
        if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.displayMsg('error', err.error.errors[0].message)
        } else this.displayMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      })
  }

  displayMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}

export interface PeriodicElement {
  code: string;
  employee: number
}