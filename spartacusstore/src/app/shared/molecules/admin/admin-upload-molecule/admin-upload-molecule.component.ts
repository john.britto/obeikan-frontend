import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators
} from "@angular/forms";
import { AdminService } from '../../../../services/admin.service'
import { environment } from 'src/environments/environment';
import * as XLSX from 'xlsx';
import { json } from 'body-parser';
import Swal from 'sweetalert2';
import { CommonUtilityService } from '../../../../services/common-utility-service'
import { appConfig } from 'src/app/app.config';

@Component({
  selector: 'app-admin-upload-molecule',
  templateUrl: './admin-upload-molecule.component.html',
  styleUrls: ['./admin-upload-molecule.component.scss']
})

export class AdminUploadMoleculeComponent implements OnInit {

  fileSelected: any = ''
  fileToUpload: any
  formGroup: FormGroup
  fileName: any
  @Output() refreshEmpList = new EventEmitter();


  constructor(
    private adminService: AdminService,
    private fb: FormBuilder,
    private utilityService: CommonUtilityService
  ) {}

  ngOnInit() {
    this.formGroup = this.fb.group({
      file: [null, Validators.required]
    });
  }

  fileInputChange(event: any) {
    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    const file = event.target.files[0];
    reader.onload = () => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: 'binary' });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        initial[name] = XLSX.utils.sheet_to_json(sheet);
        return initial;
      }, {});
      jsonData ? this.sendRequest(jsonData) : '';
    };
    reader.readAsBinaryString(file);
  }

  sendRequest(request) {
    var pattern =/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;
    request.hrEmployeeMasterData.shift()
    for(let row in request.hrEmployeeMasterData) {
      if(!request.hrEmployeeMasterData[row].employeeNumber){
        this.dialogMsg('error', `Employee Number not available at row ${Number(row) + 1}.`); return true;
      }

      if(!request.hrEmployeeMasterData[row].dateOfBirth || !pattern.test(request.hrEmployeeMasterData[row].dateOfBirth)){
        this.dialogMsg('error', `Please Enter valid Date of Birth at row ${Number(row) + 1}.`); return true;
      }

      if(!request.hrEmployeeMasterData[row].joiningDate || !pattern.test(request.hrEmployeeMasterData[row].joiningDate)){
        this.dialogMsg('error', `Please Enter valid Date of Joining at row ${Number(row) + 1}.`); return true;
      }
    }
    this.adminService.submitForm(request , false).subscribe((res: any) => {
       if(!!res && !!res.containsDuplicate && res.containsDuplicate){
         let employeeList = []
         res.hrEmployeeMasterData.forEach(emp => {
           employeeList.push(emp.emailAddress)
         });
        this.dialogMsg('error', `An employee with these values already exist in the system. Kindly check the input values in Employee Number, IQAMA Number, User ID for the employees. ${employeeList}`)
       } else if(!!res) {
        this.dialogMsg('success', appConfig.apiResponseMessages.genericSuccessMsg)
      } else {
        this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      }
      this.refreshEmpList.emit(true);
    },
    err => {
      this.refreshEmpList.emit(true);
      if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.dialogMsg('error', err.error.errors[0].message)
      } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    })
  }

  dialogMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }

  onFileUpload = event => {

    this.adminService.submitForm(this.formGroup.value.file , false).subscribe((res: any) => {
        if (res.hrMasterDataSentToCPI) {
          Swal.fire({
            icon: 'success',
            text: appConfig.apiResponseMessages.genericSuccessMsg,
            timer: 15000,
            showCloseButton: true, 
            cancelButtonText:
            '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
            showConfirmButton: false,
            background: 'black',
            toast: true
          })
        } else {
          this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
        }
      },
      err => {
        if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.dialogMsg('error', err.error.errors[0].message)
        } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      })
  }

  downloadFile = e => {
    e.preventDefault()

    let downloadUrl = environment.hostName
    this.adminService.downloadFile().subscribe((res: any) => {
        let contentRes = []
        res.contentSlots.contentSlot.forEach(content => {
          if (content.slotId === 'HRServiceEmployeeMasterDatasetupSlot') {
            contentRes = content.components.component
          }
        });

        contentRes.forEach(content => {
          if (content.uid === 'HRServicesMasterDataExcelComponent') {
            downloadUrl += content.media.url
          }
        });

        if(this.utilityService.isBrowser()) {
          window.open(downloadUrl, "_self")
        }
      },
      err => {
        if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.dialogMsg('error', err.error.errors[0].message)
        } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      })
  }
}