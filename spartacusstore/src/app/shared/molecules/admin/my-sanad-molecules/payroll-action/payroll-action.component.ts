import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table'
import { FormBuilder, FormGroup, FormControl, Validators, } from '@angular/forms';
import Swal from 'sweetalert2'
import * as _moment from 'moment';
import { DatePipe } from '@angular/common';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';
import { Moment } from 'moment';
import { AdminService } from '../../../../../services/admin.service'
import { appConfig } from 'src/app/app.config';

const moment = _moment;
export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY',
  },
  display: {
    dateInput: 'YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};
@Component({
  selector: 'app-payroll-action',
  templateUrl: './payroll-action.component.html',
  styleUrls: ['./payroll-action.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class PayrollActionComponent implements OnInit {

  @ViewChild('payrollFormEl', { static: false }) payrollFormEl;
  pipe = new DatePipe('en-US');
  public PayrollForm: FormGroup
  public PayrollRegForm: FormGroup
  public YearValE: number = 2020
  employees: any = []
  public displayedColumns: string[] = ['tabYearVal', 'tabMonthVal', 'ProcessName', 'Status', 'ActionVal'];
  submittedPayroll: any = []
  ELEMENT_DATA: PeriodicElement[]
  public dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  monthArr: Array<string> = appConfig.monthArr;
  minDate = new Date(new Date().getFullYear(), 0, 1);
  maxDate = new Date(new Date().getFullYear(), 0, 1);
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(public formBuilder: FormBuilder, private adminService: AdminService) { }
  public dateVal1 = new FormControl(moment());
  public dateVal2 = new FormControl(moment());
  ngOnInit() {

    this.adminService.getMasterData().subscribe((res: any) => {
      if(!!res && !!res.hrEmployeeMasterData) this.employees = res.hrEmployeeMasterData
    },
    err => {

    })

    this.getSubmittedPayroll()

    this.addPayrollForm()

    // this.PayrollRegForm = this.formBuilder.group({
    //   Register: ['', [Validators.required]],
    //   MonthVal1: ['', [Validators.required]],
    //   YearVal1: ['', [Validators.required]],
    // });
    this.dataSource.paginator = this.paginator;

    this.setMonthsArray();
  }

  setMonthsArray() {
    this.monthArr = []
    let i = 0;
    while(i < 6) {
      this.monthArr.push(moment().subtract(i, 'months').format('MMMM'));
      i++
    }
    this.monthArr.push(moment().add(1, 'months').format('MMMM'))

    this.monthArr = this.monthArr.sort(function(a, b){  
      return appConfig.monthArr.indexOf(a) - appConfig.monthArr.indexOf(b);
    });
  }

  setYearRange(month) {
    this.PayrollForm.get("dateVal").setValue('');
    const selectedMonth = new Date(month + new Date().getFullYear()).getMonth();
    const currMonth = new Date().getMonth()
    if(selectedMonth > currMonth && selectedMonth - currMonth > 1) {
      this.minDate = new Date(new Date().getFullYear() - 1, 0, 1);
      this.maxDate = new Date(new Date().getFullYear() - 1, 0, 1);
    } else {
      this.minDate = new Date(new Date().getFullYear(), 0, 1);
      this.maxDate = new Date(new Date().getFullYear(), 0, 1);
    }
  }

  addPayrollForm = () => {
    this.PayrollForm = this.formBuilder.group({
      Payroll: ['', [Validators.required]],
      MonthVal: ['', [Validators.required]],
      dateVal: ['', [Validators.required]],
      empId: ['']
    });
  }

  chosenYearHandler(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.dateVal1.value;
    ctrlValue.year(normalizedYear.year());
    this.PayrollForm.controls.dateVal.setValue(ctrlValue);
    this.dateVal1.setValue(ctrlValue);
    datepicker.close();
  }
  chosenYearHandler1(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.dateVal2.value;
    ctrlValue.year(normalizedYear.year());
    this.PayrollRegForm.controls.YearVal1.setValue(ctrlValue);
    this.dateVal2.setValue(ctrlValue);
    datepicker.close();
  }
  onPayrollSubmit() {
    const { Payroll, MonthVal, dateVal, empId } = this.PayrollForm.getRawValue()

    const request = {
      runPayrollType: Payroll,
      month: MonthVal + 1,
      year: this.pipe.transform(dateVal, 'yyyy'),
      employee: empId
    }

    this.adminService.submitPayroll(request, 'run-payroll').subscribe((res: any) => {
      if (!!res) {
        this.payrollFormEl.resetForm()
        this.showMsg('success', `payroll Process ${res.runPayrollType} started successfully`)
        this.submittedPayroll.unshift(res)
        this.ELEMENT_DATA = this.submittedPayroll
        this.dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
        this.dataSource.paginator = this.paginator;
      }
    },
      err => {
        if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.showMsg('error', err.error.errors[0].message)
        } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      })

  }

  getSubmittedPayroll() {

    this.adminService.getRunPayroll('run-payroll').subscribe((res: any) => {
      if (!!res && !!res.runPayrolls) {
        this.submittedPayroll = res.runPayrolls
        this.ELEMENT_DATA = this.submittedPayroll
        this.dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
        this.dataSource.paginator = this.paginator;
      }
    },
      err => {

      })
  }

  onSubmit() {

  }

  onUpdatePayroll = (pk, action) => {

    const uri = `update-run-payroll/${pk}?action=${action}`

    this.adminService.submitPayrollAction(uri).subscribe((res: any) => {
      this.getSubmittedPayroll()
      this.showMsg('success', `${action} request has been submitted successfully`)
    },
      err => {
        if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.showMsg('error', err.error.errors[0].message)
        } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      }
    )
  }

  showMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}
export interface PeriodicElement {
  runPayrollType: string;
  year: number;
  month: string;
  status: string;
  ActionVal: string;
}
const ELEMENT_DATA: PeriodicElement[] = [];

export class DatepickerViewsSelectionExample {
  dateVal = new FormControl(moment());

  chosenYearHandler(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.dateVal.value;
    ctrlValue.year(normalizedYear.year());
    this.dateVal.setValue(ctrlValue);
    datepicker.close();
  }

}