import { Component, OnInit, Input, SimpleChanges, OnChanges, Output, EventEmitter } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormArray} from '@angular/forms';
import Swal from 'sweetalert2'
import { DatePipe } from '@angular/common';
import { appConfig } from 'src/app/app.config';
import { AdminService } from '../../../../../services/admin.service'

@Component({
  selector: 'app-one-time-deductions',
  templateUrl: './one-time-deductions.component.html',
  styleUrls: ['./one-time-deductions.component.scss']
})
export class OneTimeDeductionsComponent implements OnInit, OnChanges {

  @Input() employeeNumber: any
  @Input() effectiveDate: any
  @Input() editData: any
  @Output() getDetails: EventEmitter<any> = new EventEmitter();

  pipe = new DatePipe('en-US');
  pkNumber: any;
  public oneTimeForm: FormGroup; 
  public deductionTypeList: any = []

  constructor( private fb:FormBuilder, private adminService: AdminService) { }

  get supply() { return this.oneTimeForm.controls; }
  get otDeductionsArr() { return this.supply.otDeductionsArray as FormArray; }

  ngOnChanges(changes: SimpleChanges) {
    if(!!changes.effectiveDate && !!changes.effectiveDate.currentValue) {
      for(let i = 0; i < this.otDeductionsArr.length; i++) {
        this.otDeductionsArr.at(i).patchValue({
          dateVal: this.effectiveDate
        })
      }
    }

    if (!!changes.editData && !!changes.editData.currentValue && changes.editData.currentValue.deductionOnce
      && Object.keys(changes.editData.currentValue.deductionOnce).length > 0) {
      const value = changes.editData.currentValue.deductionOnce;
      this.otDeductionsArr.clear();
      let newRow = this.fb.group({
        deductionType: [{ value: value.entryType, disabled: true }, [Validators.required]],
        reason: [value.reason, [Validators.required]],
        amount: [value.amount, [Validators.required]],
        dateVal: [{ value: new Date(value.startDate), disabled: true }, [Validators.required]],
      });

      this.otDeductionsArr.push(newRow);
      this.pkNumber = value.pk;

    }
  }

  ngOnInit() {

    this.adminService.getPayrollData('recurring=false&earnings=false').subscribe((res: any) => {
      if(!!res && !!res.payrollEntryTypeList) this.deductionTypeList = res.payrollEntryTypeList
    },
    err => {

    })

    this.oneTimeForm=this.fb.group({
      otDeductionsArray: new FormArray([])
    })

    this.addItem()
  }

  addItem = () => {
    let newRow = this.fb.group({
      deductionType : ['', [Validators.required] ],
      dateVal:[this.effectiveDate, [Validators.required] ],
      reason:['', [Validators.required] ],
      amount:['', [Validators.required] ],
    });

    this.otDeductionsArr.push(newRow);
  }

  removeItem = i => this.otDeductionsArr.removeAt(i);

  onCancel() {
    this.pkNumber = undefined;
    this.otDeductionsArr.clear();
    this.addItem();
  }
  
  onSubmit = action => {
    
    if(!!this.employeeNumber && !!this.effectiveDate) {
      let items = []
      this.oneTimeForm.getRawValue().otDeductionsArray.forEach(item => {
        const request: any = {
          entryType: item.deductionType,
          startDate: this.pipe.transform(item.dateVal, 'MM/dd/yyyy'),
          reason: item.reason,
          amount: item.amount,
          currency: 'SAR',
          effectiveDate: this.pipe.transform(this.effectiveDate, 'MM/dd/yyyy'),
          recurring: false
        }
        if(this.pkNumber) {
          request.pk = this.pkNumber;
        }
        items.push(request)
      });
  
      const payload = { payrollEntries: items }
      
      this.adminService.submitPayroll(payload, `create-payrollentries/${this.employeeNumber}?action=${action}`).subscribe((res: any) => {
        if(!!res) {
          this.showMsg('success', action === 'save' ? `Form has been saved successfully`: `Form has been submitted successfully`)
          while (this.otDeductionsArr.length !== 0) {
            this.otDeductionsArr.removeAt(0)
          }
          this.addItem()
        this.pkNumber = undefined;
        this.getDetails.emit()

        }
      },
      err => {
        if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.showMsg('error', err.error.errors[0].message)
        } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      })
    }
  }

  showMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }

}
