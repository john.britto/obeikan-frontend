import { Component, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators,FormArray} from '@angular/forms';
import { AdminService } from '../../../../../services/admin.service'
import Swal from 'sweetalert2';
import { appConfig } from '../../../../../app.config';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-leave-ticket-request',
  templateUrl: './leave-ticket-request.component.html',
  styleUrls: ['./leave-ticket-request.component.scss']
})
export class LeaveTicketRequestComponent implements OnInit {
  @ViewChild('formEl', { static: false }) formEl;

  public leaveticreqForm: FormGroup;
  pipe = new DatePipe('en-US');

  constructor(public formBuilder: FormBuilder, private adminService: AdminService) { }
  get ee() { return this.leaveticreqForm.controls; }
  get familyArr() { return this.ee.familyArray as FormArray; }
  ngOnInit() {
    this.leaveticreqForm = this.formBuilder.group({
      DueDate : ['', [Validators.required] ],
      NoofTc : ['',  [Validators.required]],
      Amount : ['',  [Validators.required]],
      Notes : ['',  [Validators.required]],
      familyArray: new FormArray([])
      });
  }
  onSubmit(){
    const {DueDate, Notes, NoofTc, Amount, familyArray} = this.leaveticreqForm.getRawValue();

    const request = {
      ticketDue: this.pipe.transform(DueDate, 'MM/dd/yyyy'),
      numberOfTickets: NoofTc,
      amount: Amount,
      notes: Notes,
      familyDetails: familyArray,
      parent: { reason: Notes }
    }

    this.adminService.postHrRequest('leave-ticket', request).subscribe((res: any) => {
      if(!!res) {
        this.formEl.resetForm()
        this.showMsg('success', appConfig.apiResponseMessages.genericSuccessMsg)
      }
      else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    },
    err => {
      if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.showMsg('error', err.error.errors[0].message)
      } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    })
  }

  addItem(e) {
    e.preventDefault()
    let initialRow = this.formBuilder.group({
      key: ['', Validators.required],
      value: ['', Validators.required],
    })
    this.familyArr.push(initialRow)
  }

  deleteDialog(i) {
    this.familyArr.removeAt(i);
  }

  cancel() {
    this.leaveticreqForm.reset()
  }

  showMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}
