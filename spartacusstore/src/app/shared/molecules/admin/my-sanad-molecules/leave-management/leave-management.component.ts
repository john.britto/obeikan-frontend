import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, } from '@angular/forms';
import * as moment from 'moment';
import { AdminService } from '../../../../../services/admin.service'
import Swal from 'sweetalert2';
import { appConfig } from '../../../../../app.config';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-leave-management',
  templateUrl: './leave-management.component.html',
  styleUrls: ['./leave-management.component.scss']
})
export class LeaveManagementComponent implements OnInit {
  public leaveManagementForm: FormGroup
  public leaveType: string[] = appConfig.DropdownOptions.leaveTypes
  public dateError: boolean = false
  @ViewChild('formEl', { static: false }) formEl;
  pipe = new DatePipe('en-US')
  empNums: any = [];
  empNumber: any;

  constructor(public formBuilder: FormBuilder, private adminService: AdminService) { }

  ngOnInit() {
    this.leaveManagementForm = this.formBuilder.group({
      StartDate: ['', [Validators.required]],
      EndDate: ['', [Validators.required]],
      leaveTypeVal: ['', [Validators.required]],
      Days: [{ value: '', disabled: true }, [Validators.required]],
      NeedExitReEntry: ['', [Validators.required]],
      ReplaceBy: ['', [Validators.required]],
      NeedTickets: ['', [Validators.required]],
      Mobile: ['', [Validators.required, Validators.pattern('^[0-9]{12}$')]],
      Routing: ['', [Validators.required]],
      Notes: ['', [Validators.required]],
    });

    this.adminService.getRunPayroll('dropdownValues-commonApproval').subscribe((res: any) => {
      this.empNums = res.employeesNumber;
    },
      err => {

      });

    this.adminService.getEmpDetails('employee-info').subscribe((res: any) => {
      this.empNumber = res.employeeNumber;
    },
    err => {
    })
  }
  onSubmit(e) {
    let StartDateCheck = moment(this.leaveManagementForm.get('StartDate').value).format('X');
    let EndDateCheck = moment(this.leaveManagementForm.get('EndDate').value).format('X');
    if (StartDateCheck > EndDateCheck) {
      this.dateError = true
    }
    const {
      StartDate,
      EndDate,
      leaveTypeVal,
      Days,
      NeedExitReEntry,
      ReplaceBy,
      NeedTickets,
      Mobile,
      Routing,
      Notes,
    } = this.leaveManagementForm.getRawValue();

    const request = {

      "leaveType": leaveTypeVal,

      "startDate": this.pipe.transform(StartDate, 'MM/dd/yyyy'),

      "endDate": this.pipe.transform(EndDate, 'MM/dd/yyyy'),

      "days": Days,

      "replaceBy": ReplaceBy.split(":")[0],

      "ticketsNeeded": NeedTickets,

      "exitReEntryVisaNeeded": NeedExitReEntry,

      "routingDetails": Routing,

      "mobileNumber": Mobile,

      "notes": Notes

    }

    this.adminService.postHrRequest('leave-management', request).subscribe((res: any) => {
      if (!!res) {
        this.formEl.resetForm()
        this.showMsg('success', appConfig.apiResponseMessages.genericSuccessMsg)
      }
      else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    },
      err => {
        if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.showMsg('error', err.error.errors[0].message)
        } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      })
  }

  getPeriodData() {
    let StartDate = moment(this.leaveManagementForm.get('StartDate').value).format("DD-MMM-YYYY");
    let EndDate = moment(this.leaveManagementForm.get('EndDate').value).format("DD-MMM-YYYY");
    let { leaveTypeVal } = this.leaveManagementForm.getRawValue();
    let request = {
      "leaveType" : leaveTypeVal,
      "startDate" : StartDate, //Date format day-month-year
      "endDate" : EndDate, //Date format day-month-year
      "parent" : {
        "employeeNumber" : this.empNumber
      }
    }
    if (leaveTypeVal !== '' && StartDate !== "Invalid date" && EndDate !== "Invalid date") {
      this.adminService.postHrLeaveDays('leave-management/validate', request).subscribe((res: any) => {
        this.leaveManagementForm.patchValue({
          Days: res,
        })
      },
        err => {
          if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
            this.showMsg('error', err.error.errors[0].message)
          } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
        })
    }
  }

  cancel() {
    this.leaveManagementForm.reset()
  }

  showMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true,
      cancelButtonText:
        '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}
