import { Component, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators,} from '@angular/forms';
import * as moment from 'moment';
import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { AdminService } from '../../../../../services/admin.service'
import { appConfig } from '../../../../../app.config';
@Component({
  selector: 'app-business-trip',
  templateUrl: './business-trip.component.html',
  styleUrls: ['./business-trip.component.scss']
})
export class BusinessTripComponent implements OnInit {

  @ViewChild('businessFormEl', { static: false }) businessFormEl;
  public bussinesForm: FormGroup;
  pipe = new DatePipe('en-US');
  public dateError:boolean=false
  public citysList:string[]=["Abha","Al Bahah (Al-Baha)","Al Kharj","Al Wajh","Al-`Ula","Al-Hofuf, Al-Ahsa","Al-Jawf","Arar","Bisha","Buraidah","Dammam","Dawadmi","Gurayat (Qurayyat)","Hafar Al-Batin (KKMC)","Ha'il","Jeddah","Jizan (Gizan)","Makkah","Medina (Madinah)","Najran","Others","Qaisumah, Hafar Al-Batin","Qassim","Rafha","Riyadh","Sharurah","Tabuk","Ta'if","Turaif","Wadi al-Dawasir","Yanbu"]
  constructor(
    public formBuilder: FormBuilder,
    private adminService: AdminService) { }

  ngOnInit() {
    this.bussinesForm = this.formBuilder.group({
      startDate : ['', [Validators.required] ],
      endDate : ['',  [Validators.required]],
      city : ['',  [Validators.required]],
      perDiem : ['',  [Validators.required]],
      airTicket: ['',  [Validators.required]],
      mobileNumber: ['',  [Validators.required,Validators.pattern('^[0-9]{12}$')]],
      routing : ['',  [Validators.required]],
      notes : ['',  [Validators.required]],
      reason: ['',  [Validators.required]]
      });

      this.adminService.getEmpDetails('employee-info').subscribe((res: any) => {
        this.bussinesForm.patchValue({
          mobileNumber: res.mobileNumber,
        })
      },
      err => {
      })
  }
  onSubmit(e){    
    let StartDate = moment(this.bussinesForm.get('startDate').value).format('X') ;
    let EndDate = moment(this.bussinesForm.get('endDate').value).format('X') ;
    if(StartDate > EndDate){
      this.dateError=true
    } else {
      const { startDate, endDate, city, perDiem, airTicket, mobileNumber, routing, notes, reason } = this.bussinesForm.getRawValue()

      const request = {
        startDate: this.pipe.transform(startDate, 'MM/dd/yyyy'),
        endDate: this.pipe.transform(endDate, 'MM/dd/yyyy'),
        city,
        perDiem,
        airTicket: airTicket === 'Yes' ? true : false,
        mobileNumber,
        routing,
        notes,
        parent: {
          reason
        }
      }
      this.adminService.postHrRequest('businesstrip-local', request).subscribe((res: any) => {
        if(!!res) {
          this.businessFormEl.resetForm()
          this.showMsg('success', appConfig.apiResponseMessages.genericSuccessMsg)
        }
        else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      },
      err => {
        if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.showMsg('error', err.error.errors[0].message)
        } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      })
    }
  }

  showMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}
