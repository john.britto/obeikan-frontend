import { Component, OnInit, Input, SimpleChanges, OnChanges, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import Swal from 'sweetalert2'
import { DatePipe } from '@angular/common';
import { appConfig } from 'src/app/app.config';
import { AdminService } from '../../../../../services/admin.service'

@Component({
  selector: 'app-recurring-loan-deductions',
  templateUrl: './recurring-loan-deductions.component.html',
  styleUrls: ['./recurring-loan-deductions.component.scss']
})
export class RecurringLoanDeductionsComponent implements OnInit, OnChanges {

  @Input() employeeNumber: any
  @Input() effectiveDate: any
  @Input() editData: any
  @Output() getDetails: EventEmitter<any> = new EventEmitter();

  pkNumber: any

  pipe = new DatePipe('en-US');
  public RecdedForm: FormGroup;
  public deductionTypes: any = []
  constructor(public formBuilder: FormBuilder, private adminService: AdminService) { }

  get supply() { return this.RecdedForm.controls; }
  get recurringDeductionsArr() { return this.supply.recurringDeductionsArray as FormArray; }

  ngOnChanges(changes: SimpleChanges) {
    if(!!changes.effectiveDate && !!changes.effectiveDate.currentValue) {
      for(let i = 0; i < this.recurringDeductionsArr.length; i++) {
        this.recurringDeductionsArr.at(i).patchValue({
          dedDate: this.effectiveDate
        })
      }
    }

    if (!!changes.editData && !!changes.editData.currentValue && changes.editData.currentValue.deductionRecurring
      && Object.keys(changes.editData.currentValue.deductionRecurring).length > 0) {
      const value = changes.editData.currentValue.deductionRecurring;
      this.recurringDeductionsArr.clear();
      let newRow = this.formBuilder.group({
        deductionType: [{ value: value.entryType, disabled: true }, [Validators.required]],
        amount: [value.totalAmount, [Validators.required]],
        endDate: [value.endDate ? new Date(value.endDate) : null],
        notes: [value.reason, [Validators.required]],
        insAmount: [value.amount, [Validators.required]],
        noIns: [value.noOfInstallments, [Validators.required]],
        loanDedDate: [new Date(value.loanDeductionStartDate), [Validators.required]],
        dedDate: [{ value: new Date(value.startDate), disabled: true }, [Validators.required]],
      });

      this.recurringDeductionsArr.push(newRow);
      this.pkNumber = value.pk;

    }
  }

  ngOnInit() {

    this.adminService.getPayrollData('recurring=true&earnings=false').subscribe((res: any) => {
      if (!!res && !!res.payrollEntryTypeList) this.deductionTypes = res.payrollEntryTypeList
    },
      err => {

      })

    this.RecdedForm = this.formBuilder.group({
      recurringDeductionsArray: new FormArray([])
    })

    this.addItem()
  }

  addItem = () => {
    let newRow = this.formBuilder.group({
      deductionType: ['', [Validators.required]],
      endDate: [''],
      notes: ['', [Validators.required]],
      insAmount: ['', [Validators.required]],
      amount: ['', [Validators.required]],
      noIns: ['', [Validators.required]],
      loanDedDate: ['', [Validators.required]],
      dedDate: [this.effectiveDate, [Validators.required]],
    });

    this.recurringDeductionsArr.push(newRow);
  }

  removeItem = i => this.recurringDeductionsArr.removeAt(i);

  onCancel() {
    this.pkNumber = undefined;
    this.recurringDeductionsArr.clear();
    this.addItem();
  }

  onSubmit = action => {

    if (!!this.employeeNumber && !!this.effectiveDate) {
      let items = []
      this.RecdedForm.getRawValue().recurringDeductionsArray.forEach(item => {
        const request: any = {
          entryType: item.deductionType,

          startDate: this.pipe.transform(item.dedDate, 'MM/dd/yyyy'),
          endDate: this.pipe.transform(item.endDate, 'MM/dd/yyyy'),
          loanDeductionStartDate: this.pipe.transform(item.loanDedDate, 'MM/dd/yyyy'),
          reason: item.notes,
          amount: item.insAmount,
          totalAmount: item.amount,
          currency: 'SAR',
          effectiveDate: this.pipe.transform(this.effectiveDate, 'MM/dd/yyyy'),
          noOfInstallments: item.noIns,
          recurring: true
        }
        if(this.pkNumber) {
          request.pk = this.pkNumber;
        }
        items.push(request)
      });

      const payload = { payrollEntries: items }

      this.adminService.submitPayroll(payload, `create-payrollentries/${this.employeeNumber}?action=${action}`).subscribe((res: any) => {
        console.log(res)
        if (!!res) {
          this.showMsg('success', action === 'save' ? `Form has been saved successfully`: `Form has been submitted successfully`)
          while (this.recurringDeductionsArr.length !== 0) {
            this.recurringDeductionsArr.removeAt(0)
          }
          this.addItem()
        this.pkNumber = undefined;
        this.getDetails.emit()

        }
      },
        err => {
          if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
            this.showMsg('error', err.error.errors[0].message)
          } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
        })
    }
  }

  showMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}
