import { Component, OnInit, ViewChild } from '@angular/core';
import { appConfig } from 'src/app/app.config';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdminService } from '../../../../../services/admin.service'
import Swal from 'sweetalert2';

@Component({
  selector: 'app-cash-advance-molecule',
  templateUrl: './cash-advance-molecule.component.html',
  styleUrls: ['./cash-advance-molecule.component.scss']
})
export class CashAdvanceMoleculeComponent implements OnInit {

  cashAdvanceFormGroup: FormGroup;
  displayedColumns: string[]
  ELEMENT_DATA: []
  historyList: any = []
  dataSource = new MatTableDataSource<any>(this.ELEMENT_DATA);
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private fb: FormBuilder,
    private adminService: AdminService) { }

  get supply() { return this.cashAdvanceFormGroup.controls; }
  get selectionArr() { return this.supply.selectionArray as FormArray; }

  ngOnInit() {

    this.getAdvanceHistory()
    
    this.cashAdvanceFormGroup = this.fb.group({
      selectionArray: new FormArray([])
    });

    this.addSelection()
  }

  getAdvanceHistory = () => {
    this.adminService.getPolicyRequest('cash-advance').subscribe((res: any) => {
      if (!!res && !!res.cashAdvancePolicies) {
        this.historyList.unshift(...res.cashAdvancePolicies)
        this.ELEMENT_DATA = this.historyList
        this.dataSource = new MatTableDataSource<any>(this.ELEMENT_DATA);
        this.displayedColumns = ['pk', 'requestedMonth', 'repaymentMonth']
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
    },
      err => {

      })
  }

  addSelection = () => {

    let newRow = this.fb.group({
      requestedMonth: [1, Validators.required],
      repaymentMonth: [3, Validators.required]
    });

    this.selectionArr.push(newRow);
  }

  removeSelection = i => this.selectionArr.removeAt(i);

  validateMonth = (i, control) => {
    const value = Number(this.selectionArr.at(i).get(control).value)

    if(value > 12) this.selectionArr.at(i).get(control).setValue(12)
    if( isNaN(value) || value < 0) this.selectionArr.at(i).get(control).setValue(1)
  }

  onSubmitForm = () => {

    let request = {};
    let items = []
    this.selectionArr.getRawValue().map(selection => {
      items.push(selection)
    })

    request = {'cashAdvancePolicies': items}

    this.adminService.postPolicyRequest('cash-advance', request).subscribe((res: any) => {
      if(!!res) {
        this.dialogMsg('success', 'Cash Advance policy has been submitted successfully')
        this.selectionArr.clear()
        this.addSelection()
        this.historyList.unshift(...res.cashAdvancePolicies)
          this.ELEMENT_DATA = this.historyList
          this.dataSource = new MatTableDataSource<any>(this.ELEMENT_DATA);
          this.dataSource.paginator = this.paginator;
      }
    },
    err => {
      if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.dialogMsg('error', err.error.errors[0].message)
      } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    })
  }

  dialogMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }

}
