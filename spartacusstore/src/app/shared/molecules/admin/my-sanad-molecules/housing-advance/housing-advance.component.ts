import { Component, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators,} from '@angular/forms';
import Swal from 'sweetalert2';
import { AdminService } from '../../../../../services/admin.service'
import { appConfig } from '../../../../../app.config';
@Component({
  selector: 'app-housing-advance',
  templateUrl: './housing-advance.component.html',
  styleUrls: ['./housing-advance.component.scss']
})
export class HousingAdvanceComponent implements OnInit {

  @ViewChild('formEl', { static: false }) formEl;
  public housingForm:FormGroup
  constructor(
    public formBuilder: FormBuilder,
    private adminService: AdminService) { }

  ngOnInit() {
    this.housingForm = this.formBuilder.group({      
      noOfMonths : ['',  [Validators.required]],
      reason : ['',  [Validators.required]],      
      });
  }
  onSubmit(){
    const{ noOfMonths, reason } = this.housingForm.getRawValue()

    const request = {
      noOfMonths,
      parent: {
        reason
      }
    }

    this.adminService.postHrRequest('housing-advance', request).subscribe((res: any) => {
      if(!!res) {
        this.formEl.resetForm()
        this.showMsg('success', appConfig.apiResponseMessages.genericSuccessMsg)
      }
      else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    },
    err => {
      if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.showMsg('error', err.error.errors[0].message)
      } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    })
  }

  showMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}
