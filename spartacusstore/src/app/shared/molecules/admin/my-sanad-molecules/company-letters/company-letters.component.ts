import { Component, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators,} from '@angular/forms';
import { AdminService } from '../../../../../services/admin.service'
import Swal from 'sweetalert2';
import { appConfig } from '../../../../../app.config';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-company-letters',
  templateUrl: './company-letters.component.html',
  styleUrls: ['./company-letters.component.scss']
})
export class CompanyLettersComponent implements OnInit {
  @ViewChild('formEl', { static: false }) formEl;
  pipe = new DatePipe('en-US')
  public CompanyForm:FormGroup
  public resons:string[]=['At his request','Extract a credit card','Money Transfer Centers','Open account','To Buy A Car','To get a loan','Updating data']
  public countryList:string[]=["Algeria","Australia","Austria","Bangladesh","Belgium","Brazil","Bulgaria","Canada","China","Czec","Denmark","Egypt","France","Germany","Greece","Hong Kong","Hungary","India","Indonesia","Iraq","Italy","Japan","Jordan","Kingdom of Bahrain","Kuwait","Lebanon","Malaysia","Morocco","Nepal","Netherland","New Zealand","Norway","Others","Pakistan","Philippines","Qatar","Russia","Saudi Arabia","Singapore","Slovakia","South Africa","South Korea","Spain","Sri Lanka","Sudan","Sultanate of Oman","Switzerland","Syria","Taiwan","Thailand","Tunisia","Turkey","United Arab Emirates (UAE)","United Kingdom (UK)","United States of America (USA)","Vietnam","Yemen"]
  public lettersList:string[]=["Bank Loan","Definition Letter","General Letter","Other (Please Specify)","Service Certificate","Tourist Visa","Transfer of EOS to Bank","Visit Visa Letter - Business Trip"]
  public letterslistType1:string[]=["Detailed Salary","Salary with EOS Benefits","Salary with Security Allowance","Total Salary"]
  public letterslistType2:string[]=["With Salary Information","Without Salary Information"]
  public letterslistType3:string[]=["With Salary Information","Without Salary Information"]
  public letterslistType4:string[]=["N/A"]
  public letterslistType5:string[]=["With Salary Information","Without Salary Information"]
  public letterslistType6:string[]=["Self","With Family"]
  public letterslistType7:string[]=["N/A"]
  public letterslistType8:string[]=["N/A"]
  public isBankLoan:boolean=false
  public isDefinitionLetter:boolean=false
  public isGeneralLetter:boolean=false
  public isOtherPleaseSpecify:boolean=false
  public isServiceCertificate:boolean=false
  public isTouristVisa:boolean=false
  public isTransfer:boolean=false
  public isVisitVisaLetter:boolean=false
  constructor(public formBuilder:FormBuilder, private adminService: AdminService) { }

  ngOnInit() {
    this.CompanyForm = this.formBuilder.group({
      LetterRequired : ['', [Validators.required] ], 
      LetterDetails : ['', [Validators.required] ],
      VisitingCountry : ['', [Validators.required] ],
      PurposeReason : ['', [Validators.required] ],
      Other: ['', [Validators.required] ],
      IssueTo: ['', [Validators.required] ],
      Notes: ['', [Validators.required] ],
      AuthorizingPerson: ['', [Validators.required] ],
      AuthorizingPersonID: ['', [Validators.required] ],
    });
  }
  checkSelectedValue(event){
    this.isBankLoan=false
    this.isDefinitionLetter=false
    this.isGeneralLetter=false
    this.isOtherPleaseSpecify=false
    this.isServiceCertificate=false
    this.isTouristVisa=false
    this.isTransfer=false
    this.isVisitVisaLetter=false
    let selectedValues = this.f.LetterRequired.value
    if(selectedValues.indexOf('Bank Loan')>-1)
    {
      this.isBankLoan = true
    }
    if(selectedValues.indexOf('Definition Letter')>-1)
    {
      this.isDefinitionLetter = true
    }
    if(selectedValues.indexOf('General Letter')>-1)
    {
      this.isGeneralLetter = true
    }
    if(selectedValues.indexOf('Other (Please Specify)')>-1)
    {
      this.isOtherPleaseSpecify = true
    }
    if(selectedValues.indexOf('Service Certificate')>-1)
    {
      this.isServiceCertificate = true
    }
    if(selectedValues.indexOf('Tourist Visa')>-1)
    {
      this.isTouristVisa = true
    }
    if(selectedValues.indexOf('Transfer of EOS to Bank')>-1)
    {
      this.isTransfer = true
    }
    if(selectedValues.indexOf('Visit Visa Letter - Business Trip')>-1)
    {
      this.isVisitVisaLetter = true
    }
  }
  get f() { return this.CompanyForm.controls; }

  onSubmit(event){
    const {
      LetterRequired, 
      LetterDetails,
      VisitingCountry,
      PurposeReason,
      Other,
      IssueTo,
      Notes,
      AuthorizingPerson,
      AuthorizingPersonID,
    } = this.CompanyForm.getRawValue();

    const request =   {

      "parent" : {
    
        "reason" : PurposeReason
    
      },
    
      "letterName" : LetterRequired,
    
      "letterDetail" : LetterDetails,
    
      "country" : VisitingCountry,
    
      "others" : Other,
    
      "issue" : IssueTo,
    
      "notes" : Notes,
    
      "authorizingPersonId" : AuthorizingPersonID,
    
      "authorizingPersonName" : AuthorizingPerson
    
    }

    this.adminService.postHrRequest('company-letter', request).subscribe((res: any) => {
      if(!!res) {
        this.formEl.resetForm()
        this.showMsg('success', appConfig.apiResponseMessages.genericSuccessMsg)
      }
      else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    },
    err => {
      if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.showMsg('error', err.error.errors[0].message)
      } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    })
  }
  cancel() {
    this.CompanyForm.reset()
  }

  showMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }

}
