import { Component, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators,} from '@angular/forms';
import * as moment from 'moment';
import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { AdminService } from '../../../../../services/admin.service'
import { appConfig } from '../../../../../app.config';
@Component({
  selector: 'app-business-trip-international',
  templateUrl: './business-trip-international.component.html',
  styleUrls: ['./business-trip-international.component.scss']
})
export class BusinessTripInternationalComponent implements OnInit {

  @ViewChild('businessFormEl', { static: false }) businessFormEl;
  public bussinesForm: FormGroup;
  pipe = new DatePipe('en-US');
  public dateError:boolean=false
  public CountryList:string[]=["Algeria","Australia","Austria","Bangladesh","Belgium","Brazil","Bulgaria","Canada","China","Czec","Denmark","Egypt","France","Germany","Greece","Hong Kong","Hungary","India","Indonesia","Iraq","Italy","Japan","Jordan","Kingdom of Bahrain","Kuwait","Lebanon","Malaysia","Morocco","Nepal","Netherland","New Zealand","Norway","Others","Pakistan","Philippines","Qatar","Russia","Saudi Arabia","Singapore","Slovakia","South Africa","South Korea","Spain","Sri Lanka","Sudan","Sultanate of Oman","Switzerland","Syria","Taiwan","Thailand","Tunisia","Turkey","United Arab Emirates (UAE)","United Kingdom (UK)","United States of America (USA)","Vietnam","Yemen"]
  constructor(
    public formBuilder: FormBuilder,
    private adminService: AdminService) { }

  ngOnInit() {
    this.bussinesForm = this.formBuilder.group({
      startDate : ['', [Validators.required] ],
      endDate : ['',  [Validators.required]],
      country : ['',  [Validators.required]],
      perDiem : ['',  [Validators.required]],
      airTicket: ['',  [Validators.required]],
      mobileNumber: ['',  [Validators.required,Validators.pattern('^[0-9]{12}$')]],
      routing : ['',  [Validators.required]],
      notes : ['',  [Validators.required]],
      exitEntryVisa:['',  [Validators.required]],
      passportNumber:['',  [Validators.required]],
      passportExpiryDate:['',  [Validators.required]],
      dateOfBirth:['',  [Validators.required]],
      ticketDepatureDate:['',  [Validators.required]],
      ticketReturnDate:['',  [Validators.required]],
      passportName:['',  [Validators.required]],
      reason:['',  [Validators.required]],
      });

      this.adminService.getEmpDetails('employee-info').subscribe((res: any) => {
        this.bussinesForm.patchValue({
          mobileNumber: res.mobileNumber,
          dateOfBirth: new Date(res.dateOfBirth),
        })
      },
      err => {
      })
  }

  onSubmit(e){    
    let StartDate = moment(this.bussinesForm.get('startDate').value).format('X') ;
    let EndDate = moment(this.bussinesForm.get('endDate').value).format('X') ;
    if(StartDate > EndDate){
      this.dateError=true
    } else {
      const {
        startDate,
        endDate,
        country,
        perDiem,
        airTicket,
        mobileNumber,
        routing,
        notes,
        exitEntryVisa,
        passportNumber,
        passportExpiryDate,
        dateOfBirth,
        ticketDepatureDate,
        ticketReturnDate,
        passportName,
        reason
      } = this.bussinesForm.getRawValue()

      const request = {
        startDate: this.pipe.transform(startDate, 'MM/dd/yyyy'),
        endDate: this.pipe.transform(endDate, 'MM/dd/yyyy'),
        country,
        perDiem,
        airTicket: airTicket === 'Yes' ? true : false,
        mobileNumber,
        routing,
        notes,
        exitEntryVisa: exitEntryVisa === 'Yes' ? true : false,
        passportNumber,
        passportExpiryDate: this.pipe.transform(passportExpiryDate, 'MM/dd/yyyy'),
        dateOfBirth: this.pipe.transform(dateOfBirth, 'MM/dd/yyyy'),
        ticketDepatureDate: this.pipe.transform(ticketDepatureDate, 'MM/dd/yyyy'),
        ticketReturnDate: this.pipe.transform(ticketReturnDate, 'MM/dd/yyyy'),
        passportName,
        parent: {
          reason
        }
      }
      this.adminService.postHrRequest('businesstrip-intl', request).subscribe((res: any) => {
        if(!!res) {
          this.businessFormEl.resetForm()
          this.showMsg('success', appConfig.apiResponseMessages.genericSuccessMsg)
        }
        else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      },
      err => {
        if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.showMsg('error', err.error.errors[0].message)
        } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      })
    }
  }

  showMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}
