import { Component, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators,} from '@angular/forms';
import { AdminService } from '../../../../../services/admin.service'
import Swal from 'sweetalert2';
import { appConfig } from '../../../../../app.config';

@Component({
  selector: 'app-leave-cancelation',
  templateUrl: './leave-cancelation.component.html',
  styleUrls: ['./leave-cancelation.component.scss']
})
export class LeaveCancelationComponent implements OnInit {

  @ViewChild('formEl', { static: false }) formEl;
  public leavecancelForm: FormGroup;
  public leaveTypes: string[] = appConfig.DropdownOptions.leaveTypes
  appliedLeaves: any

  constructor(public formBuilder: FormBuilder, private adminService: AdminService) { }

  ngOnInit() {
    this.leavecancelForm = this.formBuilder.group({
      LeaveType : ['',  [Validators.required]],
      Notes : ['',  [Validators.required]],
      ChargeTo: ['',  [Validators.required]],
      transactionId: ['',  [Validators.required]],
      
      });
  }
  onSubmit(event){
    const {LeaveType, Notes, ChargeTo, transactionId} = this.leavecancelForm.getRawValue();

    const request = {
      leaveType: LeaveType,
      parent: { reason: Notes },
      chargeTo: ChargeTo,
      leaveManagement: {
        parent: {
          transactionId: transactionId 
        }
      }
    }

    this.adminService.postHrRequest('leave-cancellation', request).subscribe((res: any) => {
      if(!!res) {
        this.formEl.resetForm()
        this.showMsg('success', appConfig.apiResponseMessages.genericSuccessMsg)
      }
      else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    },
    err => {
      if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.showMsg('error', err.error.errors[0].message)
      } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    })
  }

  getLeaveDetails = type => {
    const uri = `leave-management?leaveType=${type}`

    this.adminService.getHrRequest(uri).subscribe((res: any) => {
      if(res.length > 0) this.appliedLeaves = res
      else this.appliedLeaves = []
    },
    err => {})
  }

  cancel() {
    this.leavecancelForm.reset()
  }

  showMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}

