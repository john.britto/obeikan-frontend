import { Component, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators,} from '@angular/forms';
import { AdminService } from '../../../../../services/admin.service'
import Swal from 'sweetalert2';
import { appConfig } from '../../../../../app.config';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-resignation',
  templateUrl: './resignation.component.html',
  styleUrls: ['./resignation.component.scss']
})
export class ResignationComponent implements OnInit {
  public resignationForm: FormGroup;
  @ViewChild('formEl', { static: false }) formEl;
  pipe = new DatePipe('en-US')
  constructor(public formBuilder: FormBuilder,  private adminService: AdminService) { }
  ngOnInit() {

    this.resignationForm = this.formBuilder.group({
      RequestType : ['',  [Validators.required]],
      IfOthers  : ['',  [Validators.required]],
      NoPeriod  : ['',  [Validators.required]],
      DateofLeaving : ['', [Validators.required] ],
      Exact : ['',  [Validators.required]],
      Mobile: ['',  [Validators.required,Validators.pattern('^[0-9]{12}$')]],
      Reason: ['',  [Validators.required]],
      LastWorkDay : ['',  [Validators.required]],
      Notes : ['',  [Validators.required]],
      });
  }

  onUpdateRequest = value => {
    if(!!value && value === 'Local Transfer') {
      this.resignationForm.get('Exact').disable()
      this.resignationForm.get('Mobile').disable()
      this.resignationForm.get('Mobile').clearValidators()
      this.resignationForm.get('Exact').clearValidators()
    } else {
      this.resignationForm.get('Exact').enable()
      this.resignationForm.get('Mobile').enable()
      this.resignationForm.get('Exact').setValidators([Validators.required])
      this.resignationForm.get('Mobile').setValidators([Validators.required,Validators.pattern('^[0-9]{12}$')])
    }
    this.resignationForm.get('Mobile').updateValueAndValidity()
    this.resignationForm.get('Exact').updateValueAndValidity()
  }

  onSubmit(event){
    const {
      RequestType,
      IfOthers ,
      NoPeriod ,
      DateofLeaving,
      Exact,
      Mobile,
      Reason,
      LastWorkDay,
      Notes,
      } = this.resignationForm.getRawValue();

    const request =   {

      "parent" : {
    
        "reason" : Reason
    
      },
    
      "resignationType" : RequestType,
    
      "leavingDate" : this.pipe.transform(DateofLeaving, 'MM/dd/yyyy'),
    
      "noticePeriod" : NoPeriod,
    
      "others" : IfOthers,
    
      "routingDetails" : Exact,
    
      "notes" : Notes,
    
      "mobileNo" : Mobile,
    
      "lastWorkingDate" : this.pipe.transform(LastWorkDay, 'MM/dd/yyyy')
    
    }

    this.adminService.postHrRequest('resignation', request).subscribe((res: any) => {
      if(!!res) {
        this.formEl.resetForm()
        this.showMsg('success', appConfig.apiResponseMessages.genericSuccessMsg)
      }
      else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    },
    err => {
      if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.showMsg('error', err.error.errors[0].message)
      } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    })
  }

  cancel() {
    this.resignationForm.reset()
  }

  showMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}
