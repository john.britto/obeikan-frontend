import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { appConfig } from 'src/app/app.config';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DatePipe } from '@angular/common';
import { AdminService } from '../../../../../services/admin.service'
import Swal from 'sweetalert2';

@Component({
  selector: 'app-overtime-molecule',
  templateUrl: './overtime-molecule.component.html',
  styleUrls: ['./overtime-molecule.component.scss']
})

export class OvertimeMoleculeComponent implements OnInit {

  @ViewChild('overtimeForm', { static: false }) overtimeForm;
  @Input() employeeList: any
  overTimeFormGroup: FormGroup;
  pipe = new DatePipe('en-US');
  displayedColumns: string[]
  ELEMENT_DATA: []
  historyList: any = []
  dataSource = new MatTableDataSource<any>(this.ELEMENT_DATA);
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  typeOneArray: string[] = appConfig.DropdownOptions.typeOneArray

  constructor(
    private fb: FormBuilder,
    private adminService: AdminService) { }

  get supply() { return this.overTimeFormGroup.controls; }
  get selectionArr() { return this.supply.gradeBasedOverTimePolicies as FormArray; }

  ngOnInit() {

    this.getOvertimeHistory()

    this.overTimeFormGroup = this.fb.group({
      typeOne: [''],
      fromGrade: [''],
      toGrade: [''],
      eligibility: [false],
      ramadanStartDate: ['', Validators.required],
      ramadanEndDate: ['', Validators.required],
      employeeId: [''],
      gradeBasedOverTimePolicies: new FormArray([])
    });

    this.addSelection()
  }

  getOvertimeHistory = () => {
    this.adminService.getPolicyRequest('overtime').subscribe((res: any) => {
      if (!!res) {
        !!res.gradeBasedOverTimePolicies ? this.historyList = [...this.historyList, ...res.gradeBasedOverTimePolicies] : null
        !!res.employeeBasedOverTimePolicies ? this.historyList = [...this.historyList, ...res.employeeBasedOverTimePolicies] : null
        this.ELEMENT_DATA = this.historyList
        this.dataSource = new MatTableDataSource<any>(this.ELEMENT_DATA);
        this.displayedColumns = ['pk', 'employeeId',  'fromGrade', 'toGrade', 'eligibility', 'ramadanStartDate', 'ramadanEndDate']
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
    },
      err => {

      })
  }

  updateSelection = (value) => {

    if (value === 'Grade') {
      this.overTimeFormGroup.get('fromGrade').setValidators([Validators.required])
      this.overTimeFormGroup.get('toGrade').setValidators([Validators.required])
      this.overTimeFormGroup.get('employeeId').clearValidators()
      this.overTimeFormGroup.get('employeeId').updateValueAndValidity()
    } else if (value === 'Employee') {
      this.overTimeFormGroup.get('fromGrade').clearValidators()
      this.overTimeFormGroup.get('toGrade').clearValidators()
      this.overTimeFormGroup.get('fromGrade').updateValueAndValidity()
      this.overTimeFormGroup.get('toGrade').updateValueAndValidity()
      this.overTimeFormGroup.get('employeeId').setValidators([Validators.required])
    }
  }

  addSelection = () => {

    let newRow = this.fb.group({
      weekdays: ['', Validators.required],
      weekends: ['', Validators.required],
      overTimeDueAfter: ['', Validators.required],
      error: [false]
    });

    this.selectionArr.push(newRow);
  }

  removeSelection = i => this.selectionArr.removeAt(i);

  validateMonth = (i, control) => {

    let value = Number(this.selectionArr.at(i).get(control).value)

    if (!isNaN(value)) {
      if (control !== 'overTimeDueAfter') {
        let decimal = value.toString().split('.')

        if (!!value && value > 0 && !!decimal && !!decimal[1]) {

          Number(decimal[1]) < 5 ? this.selectionArr.at(i).get(control).setValue(Number(decimal[0] + '.' + 0)) : Number(decimal[1]) > 5 ? this.selectionArr.at(i).get(control).setValue(Number(decimal[0]) + 1) : null
        }
      } else {
        if (Number(this.selectionArr.at(i).get(control).value) > 48) {
          this.selectionArr.at(i).get('error').setValue(true)
        } else {
          this.selectionArr.at(i).get('error').setValue(false)
        }
      }
    } else this.selectionArr.at(i).get(control).setValue('')
  }

  validateChanges = () => {
    const { fromGrade, toGrade } = this.overTimeFormGroup.getRawValue()

    if (fromGrade > toGrade) {
      this.overTimeFormGroup.get('toGrade').setValue(fromGrade + 1)
    }
  }

  onSubmitForm = () => {

    const { ramadanStartDate, ramadanEndDate, employeeId, fromGrade, toGrade, typeOne, gradeBasedOverTimePolicies, eligibility } = this.overTimeFormGroup.getRawValue()

    let requestItem = {}

    if (typeOne === 'Grade') {
      requestItem = {
        fromGrade: fromGrade,
        toGrade: toGrade
      }
    } else {
      requestItem = {
        employeeId: employeeId
      }
    }

    let gradeBasedOverTimes = []
    gradeBasedOverTimePolicies.forEach(item => {
      gradeBasedOverTimes.push({
        weekdays: item.weekdays,
        weekends: item.weekends,
        overTimeDueAfter: item.overTimeDueAfter
      })
    });

    // let request = {
    //   ...requestItem,
    //   eligibility: eligibility,
    //   ramadanStartDate: this.pipe.transform(ramadanStartDate, 'MM/dd/yyyy'),
    //   ramadanEndDate: this.pipe.transform(ramadanEndDate, 'MM/dd/yyyy'),
    // };
    let request;
    typeOne === 'Grade' ? request =
    {
      gradeBasedOverTimePolicies: gradeBasedOverTimes
    } : request =
      {
        employeeBasedOverTimePolicies: gradeBasedOverTimes
      }

    request = {
      ...request,
      ...requestItem,
      eligibility: eligibility,
      ramadanStartDate: this.pipe.transform(ramadanStartDate, 'MM/dd/yyyy'),
      ramadanEndDate: this.pipe.transform(ramadanEndDate, 'MM/dd/yyyy')
    }

    this.adminService.postPolicyRequest('overtime', request).subscribe((res: any) => {
      if (!!res) {
        this.overtimeForm.resetForm()
        this.selectionArr.clear()
        if(!!res.gradeBasedOverTimePolicies) this.historyList.unshift(...res.gradeBasedOverTimePolicies)
        if(!!res.employeeBasedOverTimePolicies) this.historyList.unshift(...res.employeeBasedOverTimePolicies)
        this.ELEMENT_DATA = this.historyList
        this.dataSource = new MatTableDataSource<any>(this.ELEMENT_DATA);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.dialogMsg('success', 'Overtime policy has been submitted successfully')
      }
    },
      err => {
        if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.dialogMsg('error', err.error.errors[0].message)
        } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      })
  }

  dialogMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true,
      cancelButtonText:
        '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }

}
