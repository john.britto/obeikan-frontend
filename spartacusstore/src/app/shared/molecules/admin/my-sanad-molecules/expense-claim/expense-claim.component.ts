import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, } from '@angular/forms';
import Swal from 'sweetalert2';
import { AdminService } from '../../../../../services/admin.service'
import { appConfig } from '../../../../../app.config';

@Component({
  selector: 'app-expense-claim',
  templateUrl: './expense-claim.component.html',
  styleUrls: ['./expense-claim.component.scss']
})
export class ExpenseClaimComponent implements OnInit {

  @ViewChild('formEl', { static: false }) formEl;
  public expenseForm: FormGroup;
  catList: string[] = ['Certifications', 'Govt. Relation', 'Other', 'TicketRelated'];
  catOneList: string[] = ['Certification Fee'];
  catTwoList: string[] = ['Exit Reentry Payment', 'Iqama Renewal Payment', 'Visa Processing Payment'];
  catThreeList: string[] = ['Business Trip Expenses Claim', 'Car Maintenance', 'Car Parking & Laundry Charges', 'Food Expense Claim', 'Fuel Expense', 'Hotel Bills', 'Mobile Expense Claim', 'Office Stationary', 'Taxi Fare', 'Training Materials Bill', 'Visa Cost for Business Trip'];
  catFourList: string[] = ['Air Ticket Reimbursement Payment'];

  isCertifications: boolean = false
  isGovt: boolean = false
  isOther: boolean = false
  isTicketRelated: boolean = false
  constructor(
    public formBuilder: FormBuilder,
    private adminService: AdminService) { }

  ngOnInit() {
    this.expenseForm = this.formBuilder.group({
      expenseCategory: ['', [Validators.required]],
      certification: [''],
      govtRel: [''],
      other: [''],
      ticketRelated: [''],
      expenseType: ['', [Validators.required]],
      expenseAmount: ['', [Validators.required]],
      reason: ['', [Validators.required]],
      notes: ['', [Validators.required]],
    });
  }
  onSubmit() {

    const { expenseCategory, expenseType, expenseAmount, notes, reason } = this.expenseForm.getRawValue()
    const request = {
      expenseCategory,
      expenseType,
      expenseAmount,
      notes,
      parent: {
        reason
      }
    }
    this.adminService.postHrRequest('expense-claim', request).subscribe((res: any) => {
      if(!!res) {
        this.formEl.resetForm()
        this.showMsg('success', appConfig.apiResponseMessages.genericSuccessMsg)
      }
      else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    },
    err => {
      if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.showMsg('error', err.error.errors[0].message)
      } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    })
  }

  checkSelectedValue(event) {
    this.isCertifications = false
    this.isOther = false
    this.isGovt = false
    this.isTicketRelated = false
    console.log(this.f.expenseCategory.value)

    let selectedValues = this.f.expenseCategory.value
    if (selectedValues.indexOf('Certifications') > -1) {
      this.isCertifications = true
    }
    if (selectedValues.indexOf('Govt. Relation') > -1) {
      this.isGovt = true
    }
    if (selectedValues.indexOf('Other') > -1) {
      this.isOther = true
    } if (selectedValues.indexOf('TicketRelated') > -1) {
      this.isTicketRelated = true
    }

  }
  get f() { return this.expenseForm.controls; }

  showMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }

}