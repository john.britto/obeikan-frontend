import { Component, OnInit, ViewChild } from '@angular/core';
import { AdminService } from '../../../../../services/admin.service'
import { environment } from 'src/environments/environment';
import * as XLSX from 'xlsx';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import Swal from 'sweetalert2';
import { CommonUtilityService } from '../../../../../services/common-utility-service';
import { appConfig } from '../../../../../app.config';

@Component({
  selector: 'app-timesheet-policy-molecule',
  templateUrl: './timesheet-policy-molecule.component.html',
  styleUrls: ['./timesheet-policy-molecule.component.scss']
})
export class TimesheetPolicyMoleculeComponent implements OnInit {

  displayedColumns: string[]
  ELEMENT_DATA: []
  dataSource = new MatTableDataSource<any>(this.ELEMENT_DATA);
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  downloadUrl: any

  constructor(
    private utilityService: CommonUtilityService,
    private adminService: AdminService) { }

  ngOnInit() {

    this.getTimesheetHistory()

    this.adminService.downloadFile().subscribe((res: any) => {
      let contentRes = []
      res.contentSlots.contentSlot.forEach(content => {
        if (content.slotId === 'HRServicePolicySetupSlot') {
          contentRes = content.components.component
        }
      });

      contentRes.forEach(content => {
        if (content.uid === 'HRServicesPolicySetupExcelComponent') {
          this.downloadUrl = content.media.url
        }
      });
    },
    err => {
      if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.dialogMsg('error', err.error.errors[0].message)
      } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    })
  }

  getTimesheetHistory = () => {
    this.adminService.getPolicyRequest('time-sheet').subscribe((res: any) => {
      if (!!res && !!res.timeSheetPolicies) {
        this.ELEMENT_DATA = res.timeSheetPolicies
        this.dataSource = new MatTableDataSource<any>(this.ELEMENT_DATA);
        this.displayedColumns = ['pk', 'employeeId', 'daysWorked', 'overTimeInWeekdays', 'overTimeInWeekend', 'shortHours']
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
    },
      err => {

      })
  }

  fileInputChange(event: any) {
    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    const file = event.target.files[0];
    reader.onload = () => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: 'binary' });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        initial[name] = XLSX.utils.sheet_to_json(sheet);
        return initial;
      }, {});
      jsonData ? this.sendRequest(jsonData, file) : '';
    };
    reader.readAsBinaryString(file);
  }

  sendRequest(request, file) {

    if (!!request && !!request.timeSheetPolicies && request.timeSheetPolicies.length > 0) {

      request.timeSheetPolicies.splice(0, 2)

      this.adminService.postPolicyRequest('time-sheet', request).subscribe((res: any) => {
        if (!!res) {
          this.getTimesheetHistory()
          this.dialogMsg('success', 'Timesheet policies has been uploaded successfully')
        } else {
          this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
        }
      },
        err => {
          if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
            this.dialogMsg('error', err.error.errors[0].message)
          } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
        });
    } else {
      this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    }
  }

  downloadFile = e => {
    e.preventDefault();
    const downloadUrl = environment.hostName + this.downloadUrl;
    if (this.utilityService.isBrowser()) {
      window.open(downloadUrl, '_self')
    }
  }

  dialogMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }

}
