import { Component, OnInit, Input, SimpleChanges, OnChanges, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import Swal from 'sweetalert2'
import { DatePipe } from '@angular/common';
import { appConfig } from 'src/app/app.config'
import { AdminService } from '../../../../../services/admin.service'
@Component({
  selector: 'app-ot-short-hrs',
  templateUrl: './ot-short-hrs.component.html',
  styleUrls: ['./ot-short-hrs.component.scss']
})
export class OtShortHrsComponent implements OnInit, OnChanges {

  @Input() employeeNumber: any
  @Input() effectiveDate: any
  @Input() editData: any
  @Output() getDetails: EventEmitter<any> = new EventEmitter();

  pipe = new DatePipe('en-US');
  public ShortHrsForm: FormGroup;
  public dateError: boolean = false
  otstTypes: any = appConfig.DropdownOptions.otstTypes
  pkNumber: any;

  constructor(
    public formBuilder: FormBuilder,
    private adminService: AdminService) { }

  get supply() { return this.ShortHrsForm.controls; }
  get otshArr() { return this.supply.otshArray as FormArray; }

  ngOnChanges(changes: SimpleChanges) {
    if(!!changes.effectiveDate && !!changes.effectiveDate.currentValue) {
      for(let i = 0; i < this.otshArr.length; i++) {
        this.otshArr.at(i).patchValue({
          date: this.effectiveDate
        })
      }
    }

    if (!!changes.editData && !!changes.editData.currentValue && changes.editData.currentValue.ot
      && Object.keys(changes.editData.currentValue.ot).length > 0) {
      const value = changes.editData.currentValue.ot;
      this.otshArr.clear();
      let newRow = this.formBuilder.group({
        otType: [{ value: value.entryType, disabled: true }, [Validators.required]],
        reason: [value.reason, [Validators.required]],
        hours: [value.hours, [Validators.required]],
        date: [{ value: new Date(value.startDate), disabled: true }, [Validators.required]],
      });

      this.otshArr.push(newRow);
      this.pkNumber = value.pk;

    }
  }

  ngOnInit() {
    this.ShortHrsForm = this.formBuilder.group({
      otshArray: new FormArray([])
    })

    this.addItem()
  }

  addItem = () => {
    let newRow = this.formBuilder.group({
      otType: ['', [Validators.required]],
      date: [this.effectiveDate, [Validators.required]],
      reason: ['', [Validators.required]],
      hours: ['', [Validators.required]],
    });

    this.otshArr.push(newRow);
  }

  onCancel() {
    this.pkNumber = undefined;
    this.otshArr.clear();
    this.addItem();
  }

  removeItem = i => this.otshArr.removeAt(i);

  onSubmit = action => {

    if (!!this.employeeNumber && !!this.effectiveDate) {
      let items = []
      this.ShortHrsForm.getRawValue().otshArray.forEach(item => {
        const request: any = {
          entryType: item.otType,
          reason: item.reason,
          hours: item.hours,
          effectiveDate: this.pipe.transform(this.effectiveDate, 'MM/dd/yyyy'),
        }
        if(this.pkNumber) {
          request.pk = this.pkNumber;
        }
        items.push(request)
      });

      const payload = { otEntries: items }

      this.adminService.submitPayroll(payload, `create-overTimeEntries/${this.employeeNumber}?action=${action}`).subscribe((res: any) => {
        console.log(res)
        if (!!res) {
          this.showMsg('success',action === 'save' ? `Form has been saved successfully`: `Form has been submitted successfully`)
          while (this.otshArr.length !== 0) {
            this.otshArr.removeAt(0)
          }
          this.addItem()
        this.pkNumber = undefined;
        this.getDetails.emit()

        }
      },
        err => {
          if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
            this.showMsg('error', err.error.errors[0].message)
          } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
        })
    }
  }

  showMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}
