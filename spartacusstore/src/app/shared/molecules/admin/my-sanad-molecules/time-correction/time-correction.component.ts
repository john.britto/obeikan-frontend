import { Component, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators,} from '@angular/forms';
import { AdminService } from '../../../../../services/admin.service'
import Swal from 'sweetalert2';
import { appConfig } from '../../../../../app.config';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-time-correction',
  templateUrl: './time-correction.component.html',
  styleUrls: ['./time-correction.component.scss']
})
export class TimeCorrectionComponent implements OnInit {

  public timecorrecForm: FormGroup;
  @ViewChild('formEl', { static: false }) formEl;
  pipe = new DatePipe('en-US')
  constructor(public formBuilder: FormBuilder, private adminService: AdminService) { }

  ngOnInit() {
    this.timecorrecForm = this.formBuilder.group({
      DateWorked : ['', [Validators.required] ],
      TimeIn: ['', [Validators.required] ],
      TimeOut: ['', [Validators.required] ],
      Notes : ['',  [Validators.required]],
      });
  }  
  onSubmit(event){
    const {DateWorked, TimeIn, TimeOut, Notes} = this.timecorrecForm.getRawValue();

    const request =   {
      "dateWorked": this.pipe.transform(DateWorked, 'MM/dd/yyyy'),
      "timeIn": TimeIn,
      "timeOut": TimeOut,
      "notes": Notes,
      "parent": {
        "reason": ''
      }
    }

    this.adminService.postHrRequest('time-correction', request).subscribe((res: any) => {
      if(!!res) {
        this.formEl.resetForm()
        this.showMsg('success', appConfig.apiResponseMessages.genericSuccessMsg)
      }
      else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    },
    err => {
      if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.showMsg('error', err.error.errors[0].message)
      } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    })
  }
  cancel() {
    this.timecorrecForm.reset()
  }

  showMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }

}
