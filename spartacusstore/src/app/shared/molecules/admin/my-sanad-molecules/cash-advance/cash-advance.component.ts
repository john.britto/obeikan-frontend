import { Component, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators,} from '@angular/forms';
import Swal from 'sweetalert2';
import { AdminService } from '../../../../../services/admin.service'
import { appConfig } from '../../../../../app.config';
@Component({
  selector: 'app-cash-advance',
  templateUrl: './cash-advance.component.html',
  styleUrls: ['./cash-advance.component.scss']
})
export class CashAdvanceComponent implements OnInit {

  @ViewChild('formEl', { static: false }) formEl;
  public cashForm:FormGroup
  constructor(
    public formBuilder: FormBuilder,
    private adminService: AdminService) { }

  ngOnInit() {
    this.cashForm = this.formBuilder.group({
      reason : ['', [Validators.required] ],
      requestedMonth: ['', Validators.required],
      repaymentMonth: ['', Validators.required]
    });
  }

  validateMonth = control => {
    if(this.cashForm.get(control).value > 12) this.cashForm.get(control).setValue(12)
  }

  onSubmit(event){
    const{ reason } = this.cashForm.getRawValue()

    const request = {
      parent: {
        reason
      }
    }

    this.adminService.postHrRequest('cash-advance', request).subscribe((res: any) => {
      if(!!res) {
        this.formEl.resetForm()
        this.showMsg('success', appConfig.apiResponseMessages.genericSuccessMsg)
      }
      else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    },
    err => {
      if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.showMsg('error', err.error.errors[0].message)
      } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    })
  }

  showMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}
