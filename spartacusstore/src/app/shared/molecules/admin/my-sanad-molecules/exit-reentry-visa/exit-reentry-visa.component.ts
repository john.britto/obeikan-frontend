import { Component, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormArray} from '@angular/forms';
import { AdminService } from '../../../../../services/admin.service'
import Swal from 'sweetalert2';
import { appConfig } from '../../../../../app.config';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-exit-reentry-visa',
  templateUrl: './exit-reentry-visa.component.html',
  styleUrls: ['./exit-reentry-visa.component.scss']
})
export class ExitReentryVisaComponent implements OnInit {
  public exitreentryForm: FormGroup;
  @ViewChild('formEl', { static: false }) formEl;
  pipe = new DatePipe('en-US');
  public dateError:boolean=false
  public CountryList:string[]=["Algeria","Australia","Austria","Bangladesh","Belgium","Brazil","Bulgaria","Canada","China","Czec","Denmark","Egypt","France","Germany","Greece","Hong Kong","Hungary","India","Indonesia","Iraq","Italy","Japan","Jordan","Kingdom of Bahrain","Kuwait","Lebanon","Malaysia","Morocco","Nepal","Netherland","New Zealand","Norway","Others","Pakistan","Philippines","Qatar","Russia","Saudi Arabia","Singapore","Slovakia","South Africa","South Korea","Spain","Sri Lanka","Sudan","Sultanate of Oman","Switzerland","Syria","Taiwan","Thailand","Tunisia","Turkey","United Arab Emirates (UAE)","United Kingdom (UK)","United States of America (USA)","Vietnam","Yemen"]
  constructor(public formBuilder: FormBuilder, private adminService: AdminService) { }
  get ee() { return this.exitreentryForm.controls; }
  get familyArr() { return this.ee.familyArray as FormArray; }
  ngOnInit() {
    this.exitreentryForm = this.formBuilder.group({
      VisaType : ['',  [Validators.required]],
      VisaReq  : ['',  [Validators.required]],
      Family: ['',  [Validators.required]],
      Days:['',  [Validators.required]],
      Country : ['',  [Validators.required]],
      StartDate : ['', [Validators.required] ],
      Mobile: ['',  [Validators.required,Validators.pattern('^[0-9]{12}$')]],
      Routing : [''],
      Notes : ['',  [Validators.required]],
      familyArray: new FormArray([])
      });
  }
  onSubmit(event){
    const {VisaType, VisaReq, Family, Days, Country, StartDate, Mobile, Routing, Notes, familyArray} = this.exitreentryForm.getRawValue();

    const request =   {
      "visaType": VisaType,
      "country": Country,
      "visaRequiredFor": VisaReq,
      "durationRequired": Days,
      "startDate": this.pipe.transform(StartDate, 'MM/dd/yyyy'),
      "mobileNumber": Mobile,
      "notes": Notes,
      "family": Family,
      "parent": {
        "reason": Routing
      },
      familyDetails: familyArray
    }

    this.adminService.postHrRequest('exit-reentryvisa', request).subscribe((res: any) => {
      if(!!res) {
        this.formEl.resetForm()
        this.showMsg('success', appConfig.apiResponseMessages.genericSuccessMsg)
      }
      else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    },
    err => {
      if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.showMsg('error', err.error.errors[0].message)
      } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    })
  }
  cancel() {
    this.exitreentryForm.reset()
  }

  addItem(e) {
    e.preventDefault()
    let initialRow = this.formBuilder.group({
      key: ['', Validators.required],
      value: ['', Validators.required],
    })
    this.familyArr.push(initialRow)
  }

  deleteDialog(i) {
    this.familyArr.removeAt(i);
  }

  showMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}
