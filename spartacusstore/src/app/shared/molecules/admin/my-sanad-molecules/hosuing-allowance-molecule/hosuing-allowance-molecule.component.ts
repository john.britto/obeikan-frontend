import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { appConfig } from 'src/app/app.config';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdminService } from '../../../../../services/admin.service';
import Swal from 'sweetalert2';
import { CommonUtilityService } from '../../../../../services/common-utility-service';

@Component({
  selector: 'app-hosuing-allowance-molecule',
  templateUrl: './hosuing-allowance-molecule.component.html',
  styleUrls: ['./hosuing-allowance-molecule.component.scss']
})
export class HosuingAllowanceMoleculeComponent implements OnInit {

  @ViewChild('formEntries', { static: false }) formEntries;
  @Input() employeeList: any
  @Input() pageType: any
  displayedColumns: string[]
  ELEMENT_DATA: []
  historyList: any = []
  dataSource = new MatTableDataSource<any>(this.ELEMENT_DATA);
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  hamFormGroup: FormGroup;
  typeOneArray: string[] = appConfig.DropdownOptions.typeOneArray
  typeTwoArray: string[] = appConfig.DropdownOptions.typeTwoArray
  percentageValues: string[]

  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private utilityService: CommonUtilityService) {}

  get supply() { return this.hamFormGroup.controls; }
  get selectionArr() { return this.supply.selectionArray as FormArray; }

  ngOnInit() {

    this.pageType === 'housingAllowance' ? this.percentageValues = ['25', '16.67', '0'] : this.percentageValues = ['15', '10', '0']
    this.getAllowanceHistory()

    this.hamFormGroup = this.fb.group({
      typeOne: ['', Validators.required],
      typeTwo: ['', Validators.required],
      selectionArray: new FormArray([])
    });

    this.addSelection()
  }

  getAllowanceHistory = () => {
    const uri = this.pageType === 'housingAllowance' ? 'housing-allowance' : 'transportation-allowance'

    this.adminService.getPolicyRequest(uri).subscribe((res: any) => {
      if (!!res) {

        if (!!res.employeeBasedPercentage) this.historyList = [...this.historyList, ...res.employeeBasedPercentage]
        if (!!res.gradeBasedAmount) this.historyList = [...this.historyList, ...res.gradeBasedAmount]
        if (!!res.employeeBasedAmount) this.historyList = [...this.historyList, ...res.employeeBasedAmount]
        if (!!res.gradeBasedPercentage) this.historyList = [...this.historyList, ...res.gradeBasedPercentage]

        this.ELEMENT_DATA = this.historyList
        this.dataSource = new MatTableDataSource<any>(this.ELEMENT_DATA);
        this.displayedColumns = ['pk', 'employeeId', 'empoyeeName', 'creationDate', 'percentage', 'fromGrade', 'toGrade', 'amount']
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
    },
      err => {

      })
  }

  addSelection = () => {

    const { typeOne, typeTwo } = this.hamFormGroup.getRawValue()

    let newRow = this.fb.group({
      fromGrade: [''],
      toGrade: [''],
      percentage: [''],
      fixedAmount: [''],
      employeeId: [''],
      otherPercentage: ['']

    });

    (!!typeOne && !!typeTwo) ? this.selectionArr.push(newRow) : null
  }

  removeSelection = i => this.selectionArr.removeAt[i];

  updatePencentage = (nationality, i) => {
    if (nationality === 'Saudi Arabian' && this.pageType === 'housingAllowance') {
      this.selectionArr.controls[i].get('percentage').setValue('Others')
      this.selectionArr.controls[i].get('otherPercentage').setValue(25)
    } else if (nationality === 'Saudi Arabian' && this.pageType === 'travelAllowance') {
      this.selectionArr.controls[i].get('percentage').setValue('10')
    }
  }


  updateSelection = (value) => {

    for (let i = 0; i < this.selectionArr.getRawValue().length; i++) {
      if (value === 'Grade') {
        this.selectionArr.controls[i].get('fromGrade').setValidators([Validators.required])
        this.selectionArr.controls[i].get('toGrade').setValidators([Validators.required])
        this.selectionArr.controls[i].get('employeeId').clearValidators()
      } else if (value === 'Employee') {
        this.selectionArr.controls[i].get('fromGrade').clearValidators()
        this.selectionArr.controls[i].get('toGrade').clearValidators()
        this.selectionArr.controls[i].get('employeeId').setValidators([Validators.required])
      } else if (value === 'Percentage of Salary') {
        this.selectionArr.controls[i].get('fixedAmount').clearValidators()
        this.selectionArr.controls[i].get('percentage').setValidators([Validators.required])
        this.selectionArr.controls[i].get('otherPercentage').setValidators([Validators.required])
      } else if (value === 'Fixed Amount') {
        this.selectionArr.controls[i].get('percentage').clearValidators()
        this.selectionArr.controls[i].get('otherPercentage').clearValidators()
        this.selectionArr.controls[i].get('fixedAmount').setValidators([Validators.required])
      }

    }
  }

  validateChanges = i => {
    const { fromGrade, toGrade } = this.selectionArr.controls[i].value

    if (fromGrade > toGrade) {
      this.selectionArr.controls[i].get('toGrade').setValue(fromGrade + 1)
    }
  }

  validateNumber = (control, i) => {
    if (isNaN(Number(this.selectionArr.controls[i].get(control).value)) || Number(this.selectionArr.controls[i].get(control).value) < 0) {
      this.selectionArr.controls[i].get(control).setValue('')
    }
  }

  updateOthers = (option, i) => {
    if (option === 'Others') {
      this.selectionArr.controls[i].get('otherPercentage').setValidators([Validators.required]);
      this.selectionArr.controls[i].get('otherPercentage').updateValueAndValidity()
    } else {
      this.selectionArr.controls[i].get('otherPercentage').clearValidators()
      this.selectionArr.controls[i].get('otherPercentage').updateValueAndValidity()
    }
  }

  onSubmitForm = () => {

    const { typeOne, typeTwo } = this.hamFormGroup.getRawValue()

    let gradeBasedPercentage = []
    let gradeBasedAmount = []
    let employeeBasedPercentage = []
    let employeeBasedAmount = []

    this.selectionArr.getRawValue().map(item => {
      let selection = {};

      if (typeOne === 'Grade' && typeTwo === 'Percentage of Salary') {
        selection['fromGrade'] = item.fromGrade
        selection['toGrade'] = item.toGrade
        item.percentage !== 'Others' ? selection['percentage'] = item.percentage : selection['percentage'] = item.otherPercentage
        gradeBasedPercentage.push(selection)
      }
      if (typeOne === 'Employee' && typeTwo === 'Percentage of Salary') {
        selection['employeeId'] = item.employeeId
        item.percentage !== 'Others' ? selection['percentage'] = item.percentage : selection['percentage'] = item.otherPercentage
        employeeBasedPercentage.push(selection)
      }
      if (typeOne === 'Grade' && typeTwo === 'Fixed Amount') {
        selection['fromGrade'] = item.fromGrade
        selection['toGrade'] = item.toGrade
        selection['amount'] = item.fixedAmount
        gradeBasedAmount.push(selection)
      }
      if (typeOne === 'Employee' && typeTwo === 'Fixed Amount') {
        selection['employeeId'] = item.employeeId
        selection['amount'] = item.fixedAmount
        employeeBasedAmount.push(selection)
      }

    })
    const request = {
      gradeBasedPercentage,
      gradeBasedAmount,
      employeeBasedPercentage,
      employeeBasedAmount
    }

    const uri = this.pageType === 'housingAllowance' ? 'housing-allowance' : 'transportation-allowance'

    this.adminService.postPolicyRequest(uri, request).subscribe((res: any) => {
      if (!!res) {
        this.selectionArr.clear()
        this.addSelection()
        this.formEntries.resetForm()
        this.hamFormGroup = this.fb.group({
          typeOne: ['', Validators.required],
          typeTwo: ['', Validators.required],
          selectionArray: new FormArray([])
        });
        this.dialogMsg('success', `${this.pageType === 'housingAllowance' ? 'Housing Allowance Policy' : 'Travel Allowance Policy'} has been submitted successfully`)
        if (!!res.employeeBasedAmount) this.historyList.unshift(...res.employeeBasedAmount)
        if (!!res.employeeBasedPercentage) this.historyList.unshift(...res.employeeBasedPercentage)
        if (!!res.gradeBasedAmount) this.historyList.unshift(...res.gradeBasedAmount)
        if (!!res.gradeBasedPercentage) this.historyList.unshift(...res.gradeBasedPercentage)
          this.ELEMENT_DATA = this.historyList
          this.dataSource = new MatTableDataSource<any>(this.ELEMENT_DATA);
          this.dataSource.paginator = this.paginator;
      }
    },
      err => {
        if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.dialogMsg('error', err.error.errors[0].message)
        } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      })
  }

  dialogMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true,
      cancelButtonText:
        '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}
