import { Component, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators,} from '@angular/forms';
import { AdminService } from '../../../../../services/admin.service'
import Swal from 'sweetalert2';
import { appConfig } from '../../../../../app.config';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-clearance',
  templateUrl: './clearance.component.html',
  styleUrls: ['./clearance.component.scss']
})
export class ClearanceComponent implements OnInit {
  public clearanceForm: FormGroup;
  constructor(public formBuilder: FormBuilder, private adminService: AdminService) { }
  @ViewChild('formEl', { static: false }) formEl;
  pipe = new DatePipe('en-US')
  ngOnInit() {
    this.clearanceForm = this.formBuilder.group({
      LastWorkDay : ['', [Validators.required] ],
      ExpectedReturnDate : ['',  [Validators.required]],      
      Type : ['',  [Validators.required]],
      Rfl : ['',  [Validators.required]],
      Itemsgsd: ['',  [Validators.required]],
      Itemsgsdck: ['',  [Validators.required]],
      Itemsgsdte: ['',  [Validators.required]],
      Itemsgsdmo: ['',  [Validators.required]],
      Itemlap: ['',  [Validators.required]],
      Itempc: ['',  [Validators.required]],
      Itemprint: ['',  [Validators.required]],
      Itemid: ['',  [Validators.required]],
      Itemsoft: ['',  [Validators.required]],
      housing : ['',  [Validators.required]],
      Housidet : ['',  [Validators.required]],
      Notes : ['',  [Validators.required]],
      });
  }

  onSubmit(event){
    const {
      LastWorkDay,
      ExpectedReturnDate,      
      Type,
      Rfl,
      Itemsgsd,
      Itemsgsdck,
      Itemsgsdte,
      Itemsgsdmo,
      Itemlap,
      Itempc,
      Itemprint,
      Itemid,
      Itemsoft,
      housing,
      Housidet,
      Notes,
      } = this.clearanceForm.getRawValue();

    const request =   {

      "parent" : {
    
        "reason" : Rfl
    
      },
    
      "clearanceType" : Type,
    
      "lastWorkingDate" : this.pipe.transform(LastWorkDay, 'MM/dd/yyyy'),
    
      "exceptedReturnDate" : this.pipe.transform(ExpectedReturnDate, 'MM/dd/yyyy'),
    
      "office" : Itemsgsd,
    
      "carAndKey" : Itemsgsdck,
    
      "telephone" : Itemsgsdte,
    
      "mobile" : Itemsgsdmo,
    
      "laptop" : Itemlap,
    
      "pc" : Itempc,
    
      "printer" : Itemprint,
    
      "clearingUserId" : Itemid,
    
      "software" : Itemsoft,
    
      "housing" : housing,
    
      "housingDetails" : Housidet,
    
      "notes" : Notes
    
    }

    this.adminService.postHrRequest('clearance', request).subscribe((res: any) => {
      if(!!res) {
        this.formEl.resetForm()
        this.showMsg('success', appConfig.apiResponseMessages.genericSuccessMsg)
      }
      else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    },
    err => {
      if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.showMsg('error', err.error.errors[0].message)
      } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    })
  }

  cancel() {
    this.clearanceForm.reset()
  }

  showMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}



