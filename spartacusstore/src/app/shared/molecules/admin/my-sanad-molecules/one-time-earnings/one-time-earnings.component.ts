import { Component, OnInit, Input, SimpleChanges, OnChanges, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';
import { appConfig } from 'src/app/app.config';
import { AdminService } from '../../../../../services/admin.service'
@Component({
  selector: 'app-one-time-earnings',
  templateUrl: './one-time-earnings.component.html',
  styleUrls: ['./one-time-earnings.component.scss']
})
export class OneTimeEarningsComponent implements OnInit, OnChanges {

  @Input() employeeNumber: any
  @Input() effectiveDate: any;
  @Input() editData: any;
  @Output() getDetails: EventEmitter<any> = new EventEmitter();
  pkNumber: any
  pipe = new DatePipe('en-US');
  public oneTimeForm: FormGroup;
  public earningTypeList: any[]
  constructor(private fb: FormBuilder, private adminService: AdminService) { }

  get supply() { return this.oneTimeForm.controls; }
  get oneTimeEarningsArr() { return this.supply.oneTimeEarningsArray as FormArray; }

  ngOnChanges(changes: SimpleChanges) {
    if (!!changes.effectiveDate && !!changes.effectiveDate.currentValue) {
      for (let i = 0; i < this.oneTimeEarningsArr.length; i++) {
        this.oneTimeEarningsArr.at(i).patchValue({
          startDate: this.effectiveDate
        })
      }
    }

    if (!!changes.editData && !!changes.editData.currentValue && changes.editData.currentValue.earningOnce
      && Object.keys(changes.editData.currentValue.earningOnce).length > 0) {
      const value = changes.editData.currentValue.earningOnce;
      this.oneTimeEarningsArr.clear();
      let newRow = this.fb.group({
        entryType: [{ value: value.entryType, disabled: true }, [Validators.required]],
        reason: [value.reason, [Validators.required]],
        amount: [value.amount, [Validators.required]],
        startDate: [{ value: new Date(value.startDate), disabled: true }, [Validators.required]],
      });

      this.oneTimeEarningsArr.push(newRow);
      this.pkNumber = value.pk;

    }
}

ngOnInit() {

  this.adminService.getPayrollData('recurring=false&earnings=true').subscribe((res: any) => {
    if (!!res && !!res.payrollEntryTypeList) this.earningTypeList = res.payrollEntryTypeList
  },
    err => {

    })

  this.oneTimeForm = this.fb.group({
    oneTimeEarningsArray: new FormArray([])
  })

  this.addForm()
}

onCancel() {
  this.pkNumber = undefined;
  this.oneTimeEarningsArr.clear();
  this.addForm();
}

addForm = () => {
  let newRow = this.fb.group({
    entryType: ['', [Validators.required]],
    startDate: [this.effectiveDate, [Validators.required]],
    reason: ['', [Validators.required]],
    amount: ['', [Validators.required]],
  });

  this.oneTimeEarningsArr.push(newRow);
}

removeItem = i => this.oneTimeEarningsArr.removeAt(i);

onSubmit = action => {

  if (!!this.employeeNumber && !!this.effectiveDate) {
    let items = []
    this.oneTimeForm.getRawValue().oneTimeEarningsArray.forEach(item => {
      const request: any = {
        entryType: item.entryType,
        startDate: this.pipe.transform(item.startDate, 'MM/dd/yyyy'),
        reason: item.reason,
        amount: item.amount,
        currency: 'SAR',
        effectiveDate: this.pipe.transform(this.effectiveDate, 'MM/dd/yyyy'),
        recurring: false
      }
      if(this.pkNumber) {
        request.pk = this.pkNumber;
      }
      items.push(request)
    });

    const payload = { payrollEntries: items }

    this.adminService.submitPayroll(payload, `create-payrollentries/${this.employeeNumber}?action=${action}`).subscribe((res: any) => {
      if (!!res) {
        this.showMsg('success', action === 'save' ? `Form has been saved successfully` : `Form has been submitted successfully`)
        while (this.oneTimeEarningsArr.length !== 0) {
          this.oneTimeEarningsArr.removeAt(0)
        }
        this.addForm()
        this.pkNumber = undefined;
        this.getDetails.emit()
      }
    },
      err => {
        if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.showMsg('error', err.error.errors[0].message)
        } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      })
  }
}

showMsg = (type, msg) => {
  Swal.fire({
    icon: type,
    text: msg,
    timer: 15000,
    showCloseButton: true, 
    cancelButtonText:
    '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
    showConfirmButton: false,
    background: 'black',
    toast: true
  })
}
}
