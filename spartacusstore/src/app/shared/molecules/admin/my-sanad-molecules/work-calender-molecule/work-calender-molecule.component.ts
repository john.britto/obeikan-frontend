import { Component, OnInit, ViewChild } from '@angular/core';
import { appConfig } from 'src/app/app.config';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdminService } from '../../../../../services/admin.service'
import Swal from 'sweetalert2';

@Component({
  selector: 'app-work-calender-molecule',
  templateUrl: './work-calender-molecule.component.html',
  styleUrls: ['./work-calender-molecule.component.scss']
})
export class WorkCalenderMoleculeComponent implements OnInit {

  workCalenderFormGroup: FormGroup;
  calenderDays: string[] = appConfig.DropdownOptions.calenderDays
  displayedColumns: string[]
  ELEMENT_DATA: []
  historyList: any = []
  dataSource = new MatTableDataSource<any>(this.ELEMENT_DATA);
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private fb: FormBuilder,
    private adminService: AdminService) { }

  get supply() { return this.workCalenderFormGroup.controls; }
  get selectionArr() { return this.supply.selectionArray as FormArray; }

  ngOnInit() {

    this.getCalenderHistory()

    this.workCalenderFormGroup = this.fb.group({
      selectionArray: new FormArray([])
    });

    this.addSelection()
  }

  getCalenderHistory = () => {
    this.adminService.getPolicyRequest('work-calender').subscribe((res: any) => {
      if (!!res && !!res.workCalenderPolicies) {
        this.historyList = res.workCalenderPolicies
        this.ELEMENT_DATA = this.historyList
        this.dataSource = new MatTableDataSource<any>(this.ELEMENT_DATA);
        this.displayedColumns = ['pk', 'shiftName', 'startTime', 'finishTime', 'shiftDays']
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
    },
      err => {

      })
  }

  validateName = i => {
    let shiftName = this.selectionArr.at(i).get('shiftName').value
    const shiftHistory: any = this.ELEMENT_DATA
    for (let i = 0; i < shiftHistory.length; i++) {
      if(shiftHistory[i].shiftName === shiftName) {
        this.selectionArr.at(i).get('shiftName').setValue('')
        this.selectionArr.at(i).get('error').setValue(true)
      } else this.selectionArr.at(i).get('error').setValue(false)
    }
  }

  addSelection = () => {

    let newRow = this.fb.group({
      shiftName: ['', Validators.required],
      startTime: ['', Validators.required],
      finishTime: ['', Validators.required],
      shiftDays: ['', Validators.required],
      error: [false]

    });

    this.selectionArr.push(newRow);
  }

  removeSelection = i => this.selectionArr.removeAt(i);

  onSubmitForm = () => {

    let request = {};
    let items = []
    this.selectionArr.getRawValue().map(selection => {
      let formattedStartTime = selection.startTime.split(':')
      formattedStartTime = `${formattedStartTime[0]}.${formattedStartTime[1]}`
      let formattedFinishTime = selection.startTime.split(':')
      formattedFinishTime = `${formattedFinishTime[0]}.${formattedFinishTime[1]}`
      let item = {
        shiftName: selection.shiftName,
        shiftDays: selection.shiftDays,
        startTime: formattedStartTime,
        finishTime: formattedFinishTime
      }
      items.push(item)
    })

    request = {'workCalenderPolicies': items}

    this.adminService.postPolicyRequest('work-calender', request).subscribe((res: any) => {
      if(!!res && !!res.workCalenderPolicies) {
        this.selectionArr.clear()
        this.historyList.unshift(...res.workCalenderPolicies)
        this.ELEMENT_DATA = this.historyList
        this.dataSource = new MatTableDataSource<any>(this.ELEMENT_DATA);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.addSelection()
        this.dialogMsg('success', 'Work Calender policy has been submitted successfully')
      }
    },
    err => {
      if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.dialogMsg('error', err.error.errors[0].message)
      } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    })
  }

  dialogMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}
