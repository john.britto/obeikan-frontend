import { Component, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators,} from '@angular/forms';
import { AdminService } from '../../../../../services/admin.service'
import Swal from 'sweetalert2';
import { appConfig } from '../../../../../app.config';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-over-time',
  templateUrl: './over-time.component.html',
  styleUrls: ['./over-time.component.scss']
})
export class OverTimeComponent implements OnInit {
  @ViewChild('formEl', { static: false }) formEl;

  public overtime: FormGroup;

  constructor(public formBuilder: FormBuilder, private adminService: AdminService) { }

  ngOnInit() {
    this.overtime = this.formBuilder.group({
      month : ['', [Validators.required] ],
      overtime : ['',  [Validators.required]],
      notes : ['',  [Validators.required]],
      });
  }
  onSubmit(){
    const {month, overtime, notes} = this.overtime.getRawValue();

    const request =   {
      "month": month,
      "overtimeHours": overtime,
      "notes": notes,
      "parent": {
        "reason": ''
      }
    }

    this.adminService.postHrRequest('overtime', request).subscribe((res: any) => {
      if(!!res) {
        this.formEl.resetForm()
        this.showMsg('success', appConfig.apiResponseMessages.genericSuccessMsg)
      }
      else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    },
    err => {
      if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.showMsg('error', err.error.errors[0].message)
      } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    })
  }
  cancel() {
    this.overtime.reset()
  }

  showMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}
