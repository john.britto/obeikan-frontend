import { Component, OnInit, Input, SimpleChanges, OnChanges, Output, EventEmitter } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormArray} from '@angular/forms';
import * as moment from 'moment';
import Swal from 'sweetalert2'
import { DatePipe } from '@angular/common';
import { appConfig } from 'src/app/app.config';
import { AdminService } from '../../../../../services/admin.service'

@Component({
  selector: 'app-recurring-earnings',
  templateUrl: './recurring-earnings.component.html',
  styleUrls: ['./recurring-earnings.component.scss']
})
export class RecurringEarningsComponent implements OnInit, OnChanges {

  @Input() employeeNumber: any
  @Input() effectiveDate: any
  @Input() editData: any
  @Output() getDetails: EventEmitter<any> = new EventEmitter();

  pipe = new DatePipe('en-US');
  pkNumber: any;
  public recurringForm: FormGroup; 
  public dateError:boolean=false;
  public recurringTypeList: any = []
  
  constructor( private fb:FormBuilder, private adminService: AdminService) { }

  get supply() { return this.recurringForm.controls; }
  get recurringsArr() { return this.supply.recurringsArray as FormArray; }

  ngOnChanges(changes: SimpleChanges) {
    if(!!changes.effectiveDate && !!changes.effectiveDate.currentValue) {
      for(let i = 0; i < this.recurringsArr.length; i++) {
        this.recurringsArr.at(i).patchValue({
          earningStartDate: this.effectiveDate
        })
      }
    }

    if(!!changes.editData && !!changes.editData.currentValue && changes.editData.currentValue.earningRecurring
       && Object.keys(changes.editData.currentValue.earningRecurring).length > 0) {
      const value = changes.editData.currentValue.earningRecurring;
      this.recurringsArr.clear();
      let newRow = this.fb.group({
        deductionType : [{value: value.entryType, disabled: true}, [Validators.required] ],
        reason:[value.reason, [Validators.required] ],
        amount:[value.amount, [Validators.required] ],
        earningEndDate:[value.endDate ? new Date(value.endDate) : null],
        earningStartDate:[{value: new Date(value.startDate), disabled: true}, [Validators.required] ],
      });
  
      this.recurringsArr.push(newRow);
      this.pkNumber = value.pk;

    }
  }

  ngOnInit() {

    this.adminService.getPayrollData('recurring=true&earnings=true').subscribe((res: any) => {
      if(!!res && !!res.payrollEntryTypeList) this.recurringTypeList = res.payrollEntryTypeList
    },
    err => {

    })

    this.recurringForm=this.fb.group({
      recurringsArray: new FormArray([])
    })

    this.addItem()
  }

  addItem = () => {
    let newRow = this.fb.group({
      deductionType : ['', [Validators.required] ],
      reason:['', [Validators.required] ],
      amount:['', [Validators.required] ],
      earningEndDate:[''],
      earningStartDate:[this.effectiveDate, [Validators.required] ],
    });

    this.recurringsArr.push(newRow);
  }

  removeItem = i => this.recurringsArr.removeAt(i);

  onCancel() {
    this.pkNumber = undefined;
    this.recurringsArr.clear();
    this.addItem();
  }

  onSubmit = action => {
    
    if(!!this.employeeNumber && !!this.effectiveDate) {
      let items = []
      this.recurringForm.getRawValue().recurringsArray.forEach(item => {
        const request: any = {
          entryType: item.deductionType,
          startDate: this.pipe.transform(item.earningStartDate, 'MM/dd/yyyy'),
          endDate: this.pipe.transform(item.earningEndDate, 'MM/dd/yyyy'),
          reason: item.reason,
          amount: item.amount,
          currency: 'SAR',
          effectiveDate: this.pipe.transform(this.effectiveDate, 'MM/dd/yyyy'),
          recurring: true
        }
        if(this.pkNumber) {
          request.pk = this.pkNumber;
        }
        items.push(request)
      });
  
      const payload = { payrollEntries: items }
      
      this.adminService.submitPayroll(payload, `create-payrollentries/${this.employeeNumber}?action=${action}`).subscribe((res: any) => {
        console.log(res)
        if(!!res) {
          this.showMsg('success', action === 'save' ? `Form has been saved successfully`: `Form has been submitted successfully`)
          while (this.recurringsArr.length !== 0) {
            this.recurringsArr.removeAt(0)
          }
          this.addItem()
        this.pkNumber = undefined;
        this.getDetails.emit()

        }
      },
      err => {
        if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.showMsg('error', err.error.errors[0].message)
        } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      })
    }
  }

  showMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}
