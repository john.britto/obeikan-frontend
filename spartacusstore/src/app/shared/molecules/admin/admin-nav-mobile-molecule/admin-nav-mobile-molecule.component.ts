import { Component, OnInit, Input } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { ADMIN_NAV_LINKS} from '../../../../services/admin.service';
import { CommonUtilityService } from '../../../../services/common-utility-service'

@Component({
  selector: 'app-admin-nav-mobile-molecule',
  templateUrl: './admin-nav-mobile-molecule.component.html',
  styleUrls: ['./admin-nav-mobile-molecule.component.scss']
})
export class AdminNavMobileMoleculeComponent implements OnInit {

  @Input() userType: any
  logoUrl: any = '/medias/site-logo.png?context=bWFzdGVyfGltYWdlc3wxMTEyOXxpbWFnZS9wbmd8aDllL2g3YS84ODAxMDA4NzQ2NTI2L3NpdGUtbG9nby5wbmd8NzAyMDlhMzEzZjBiZGMwNDViNjJhNjQ2ZWMwNGFhMzc2YmQzZGEzYjUyNTJiMGIxZDk3NjhlODMzYjlkMTY5Nw'
  hostName: string = environment.hostName;
  adminNavLinks: any;

  constructor(private ultilityService: CommonUtilityService) {
    this.adminNavLinks = ADMIN_NAV_LINKS;
   }

  ngOnInit() {
  }

  openNav = () => {
    if(this.ultilityService.isBrowser()) {
      document.getElementById("myNav").style.width = "15%";
    }
  }

  closeNav = () => {
    if(this.ultilityService.isBrowser()) {
      document.getElementById("myNav").style.width = "0%";

    }
  }

}
