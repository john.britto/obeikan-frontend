import { Component, OnInit, ViewChild, Inject, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { API_FAILURE_MSG, FinanceService, TRANSACTION_TYPES, PAYMENT_SUCCESS_MSG } from '../../../../../../services/finance.service'
import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { englishEnums } from '../../../../../../app.enums';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { CommonUtilityService } from '../../../../../../services/common-utility-service';
import { appConfig } from '../../../../../../app.config'
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CURRENCIES } from '../../../../../../../assets/js/currencies';
import { PAYMENT_TERMS, DELIVERY_TERMS } from '../../../../../../../assets/js/paymentTerms.js'
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { DeleteHRMSSalaryFormModalComponent } from '../../../hrms/salary-element-entries-single-molecule/salary-element-entries-single-molecule.component';

@Component({
  selector: 'app-account-receivable-molecule',
  templateUrl: './account-receivable-molecule.component.html',
  styleUrls: ['./account-receivable-molecule.component.scss']
})
export class AccountReceivableMoleculeComponent implements OnInit {

  @Input() pageType: any
  @ViewChild('soForm', { static: false }) soForm;
  @ViewChild('msForm', { static: false }) msForm;
  @ViewChild('msFormTwo', { static: false }) msFormTwo;
  @ViewChild('collectionForm', { static: false }) collectionForm;
  @ViewChild('stepper', { static: false }) private myStepper: MatStepper;
  pipe = new DatePipe('en-US');
  salesOrderFormGroup: FormGroup;
  materialShpAndDelFormGroup: FormGroup
  materialShpAndDelFormGroupTwo: FormGroup
  collectionsFormGroup: FormGroup
  salesOrder: any
  stepDisabled: boolean = false
  actualDate: any
  formattedDate: any
  wareHouses: any
  filteredCurrencies: Observable<string[]>
  currency = new FormControl();
  filteredCustomers: Observable<string[]>
  customer = new FormControl();
  reviewExpanded: any = true
  draftedOrders: any
  supliers: any
  poSummary: any = {
    vatPercentage: 0,
    discountAmount: 0,
    subTotal: 0,
    amount: 0,
    vatAmount: 0,
    totalAmount: 0,
    subTotalInSAR: 0,
    amountInSAR: 0
  }
  displayedColumns: string[];
  displayedColumnsReview: string[];
  ELEMENT_DATA: PeriodicElement[] = []
  dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
  vatPercentages: any = appConfig.DropdownOptions.vatPercentages
  selection = new SelectionModel<PeriodicElement>(true, []);
  mrQty: any;
  msTypeAtPayment: any
  soTypeAtPayments: any
  customers: any
  itemCodes: any
  salesOrders: any
  filteredItemCodes: Observable<string[]>
  itemCode = new FormControl();
  currencies: any
  paymentTerms: any = DELIVERY_TERMS
  paymentTerms2: any = PAYMENT_TERMS
  salesOrderOptions: any = []
  closedSalesOrders: any = []
  defaultValueSO: any;
  availableBanks: any = appConfig.DropdownOptions.bankLists
  enums: any
  typesOfTransactions: any
  salesReps: any
  filteredItems: any
  validationMessages = {
    bankCode: [
      { type: 'required', message: 'Bank Code is required' },
      { type: 'pattern', message: 'Enter proper bank code' }]
  }
  assetCodes: any;
  warehouse = new FormControl();
  vatCodes: any = []
  currenciesRate: any = []
  observers: any[] = [];
  msCodes: any[] = []
  invoiceBookings: any = []
  invoiceTypes: any = []

  constructor(
    private _formBuilder: FormBuilder,
    private financeService: FinanceService,
    public dialog: MatDialog,
    private matSnack: MatSnackBar,
    private utilityService: CommonUtilityService) {
    this.currencies = CURRENCIES
    this.typesOfTransactions = TRANSACTION_TYPES
  }

  get supply() { return this.salesOrderFormGroup.controls; }
  get itemsArr() { return this.supply.itemsArray as FormArray; }

  get so() { return this.collectionsFormGroup.controls; }
  get amountArr() { return this.so.soArray as FormArray; }

  get mr() { return this.materialShpAndDelFormGroup.controls; }
  get fixedAssetArr() { return this.mr.fixedAssetArray as FormArray; }

  ngOnInit() {

    const isSalesOrder = this.pageType === 'salesReturn'

    this.getConversionRate();
    this.displayedColumns = ['select', 'S no', 'Item Code', 'Description', 'Quantity', 'Unit']// 'Price', 'Discount %', 'Sub Total', 'VAT %', 'Total Price'];
    this.displayedColumnsReview = ['S no', 'Item Code', 'Description', 'Quantity', 'Unit']// 'Price', 'Discount %', 'Sub Total', 'VAT %', 'Total Price'];

    this.financeService.getSupplierDetails('customermasterdatasetup').subscribe((res: any) => {
      if (res && res.supplierInfo && res.itemInfo) {
        this.customers = res.customerInfo
        this.itemCodes = res.itemInfo;
        this.wareHouses = res.warehouse;
        this.assetCodes = res.asset;
        this.salesReps = res.salesRep;
        this.vatCodes = res.financeVAT
        this.filteredCustomers = this.customer.valueChanges.pipe(startWith(''), map(value => this._filterCustomer(value)));
      }
    },
      err => {
        if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.showMsg('error', err.error.errors[0].message)
        } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      });

    if (!isSalesOrder) {
      this.financeService.getSalesOrder('sales-order', 'CREATED').subscribe((res: any) => {
        this.draftedOrders = res.salesOrderList;
      },
        err => {
          if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
            this.showMsg('error', err.error.errors[0].message)
          } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
        })

      this.financeService.getSalesOrder('sales-order').subscribe((res: any) => {
        this.salesOrderOptions = res.salesOrderList;
        this.defaultValueSO = res.salesOrderList.map(e => ({ salesOrder: e.salesOrder, item: e.item }));
      },
        err => {
          if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
            this.showMsg('error', err.error.errors[0].message)
          } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
        })
    }

    if (isSalesOrder) {
      this.financeService.getSalesOrder('sales-order', 'CLOSED').subscribe((res: any) => {
        this.closedSalesOrders = res.salesOrderList;
      },
        err => { })

      this.financeService.getSalesOrder('return-sales-order', 'APPROVED').subscribe((res: any) => {
        console.log(res)
        this.salesOrderOptions = res.salesOrderList;
      },
        err => { })
    }

    this.financeService.getFinanceData(isSalesOrder ? 'material-return' : 'material-shipped').subscribe((res: any) => {
      if (!!res && !!res.materialShippedList) this.msCodes = res.materialShippedList
    },
      err => {})

    this.financeService.getFinanceData('approved-invoices?orderType=ACCOUNTS_RECEIVABLE').subscribe((res: any) => {
      if (!!res && !!res.invoices && res.invoices.length > 0) {
        this.invoiceTypes = res.invoices
      }
    },
      err => {
      });

    let initialRow = this._formBuilder.group({
      quantity: ['', Validators.required],
      availableQuantity: [{ value: '', disabled: true }],
      itemDescription: [{ value: '', disabled: true }, Validators.required],
      price: [{ value: 0, disabled: isSalesOrder }, Validators.required],
      unit: [{ value: '', disabled: true }, [Validators.required]],
      totalprice: [{ value: '', disabled: true }, Validators.required],
      vatCode: [{ value: '', disabled: isSalesOrder }, Validators.required],
      vatPercentage: [{ value: '5', disabled: isSalesOrder }, Validators.required],
      discountPercentage: [{ value: '', disabled: isSalesOrder }, Validators.required],
      subTotal: [{ value: '', disabled: true }, [Validators.required]],
      tableItemCode: ['', Validators.required],
      subTotalInSAR: [''],
      totalpriceInSAR: [''],
      priceInSAR: [0],
    })

    if (this.pageType === 'accountReceivable') {
      this.salesOrderFormGroup = this._formBuilder.group({
        customer: ['', Validators.required],
        warehouse: ['', Validators.required],
        draftedOrder: [''],
        currency: ['', Validators.required],
        paymentTerm: ['', Validators.required],
        paymentTerm2: ['', Validators.required],
        estimatedDeliveryDate: [new Date(), Validators.required],
        creationDate: [new Date(), Validators.required],
        notes: [''],
        salesRep: ['', Validators.required],
        customerOrderReference: ['', Validators.required],
        itemsArray: new FormArray([initialRow])
      });
    } else {
      this.salesOrderFormGroup = this._formBuilder.group({
        customer: ['', Validators.required],
        warehouse: ['', Validators.required],
        draftedOrder: [''],
        currency: ['', Validators.required],
        paymentTerm2: ['', Validators.required],
        creationDate: [new Date(), Validators.required],
        notes: [''],
        salesRep: ['', Validators.required],
        customerOrderReference: ['', Validators.required],
        itemsArray: new FormArray([])
      });
    }

    this.materialShpAndDelFormGroup = this._formBuilder.group({
      soNumber: ['', Validators.required],
      customer: [{ value: '', disabled: true }, Validators.required],
      currency: [{ value: '', disabled: true }, Validators.required],
      amount: [{ value: '', disabled: true }, Validators.required],
      warehouse: [{ value: '', disabled: true }, Validators.required],
      shipmentDate: ['', Validators.required],
      fixedAssetArray: new FormArray([])
    });

    this.materialShpAndDelFormGroupTwo = this._formBuilder.group({
      referenceOrder: ['', Validators.required],
      vatAmount: [{ value: '', disabled: true }, Validators.required],
      invoiceAmount: ['', Validators.required],
      invoiceDate: ['', Validators.required],
      invoiceCurrency: [{ value: '', disabled: true }, Validators.required],
      deliveryNote: [{ value: '', disabled: true }, Validators.required],
      notes: ['']
    });

    this.collectionsFormGroup = this._formBuilder.group({
      transactionEntryDate: ['', Validators.required],
      bankName: ['', Validators.required],
      documentDate: ['', Validators.required],
      typeOfTransaction: ['', Validators.required],
      customer: ['', Validators.required],
      invoiceBookings: [[], Validators.required],
      currency: ['', Validators.required],
      grandtotalprice: ['', Validators.required],
      soArray: new FormArray([]),
      vatCode: ['', Validators.required],
      vatPercentage: ['', Validators.required],
      grossamt: ['', Validators.required],
      notes: [''],
    });

    this.filteredCurrencies = this.currency.valueChanges.pipe(startWith(''), map(value => this._filterCurrency(value)));
  }

  _filterCurrency = value => {
    const filterValue = value.toLowerCase();

    return this.currencies.filter(option => option.toLowerCase().includes(filterValue));
  }

  _filterCustomer = value => {
    const filterValue = value.toLowerCase();

    return this.customers.filter(option => option.name.toLowerCase().includes(filterValue));
  }

  ManageItemCodeControl(index: number) {

    var arrayControl = this.salesOrderFormGroup.get('itemsArray') as FormArray;

    this.filteredItemCodes = arrayControl.at(index).get('tableItemCode').valueChanges
      .pipe(startWith(''), map(value => this._filterItemCode(value)));

    this.observers.push(this.filteredItemCodes);

  }

  deleteDialog(i, row) {
    const dialogRef = this.dialog.open(DeleteHRMSSalaryFormModalComponent, {
      data: { index: i }
    });

    dialogRef.afterClosed().subscribe(result => {
      result && this.removeItem(i, row);
    });
  }

  setEnums(property) {
    const values = [];
    this.enums[property].map(each => {
      englishEnums[property].map(obj => {
        if (each.code === obj.code) {
          values.push(obj);
        }
      });
    });
    return values;
  }

  updateSelect = (formGroup, key, value, currency, data?: any) => {
    this[formGroup].patchValue({
      [key]: value
    });
    if(data && data.deliveryTerms && data.paymentTerms) {
      this[formGroup].patchValue({
        paymentTerm: data.deliveryTerms,
        paymentTerm2: data.paymentTerms
      });
    }
    if (key == 'currency') {
      this.updateConveRate(this.utilityService.splitCode(value))
    } else if (key == 'customer' && currency) {
      this[formGroup].patchValue({
        currency: currency
      });
      this.updateConveRate(this.utilityService.splitCode(currency))
    }
  }

  getConversionRate() {
    this.financeService.getSupplierDetails('currencies').subscribe((res: any) => {
      if (res && res.currencies) {
        this.currenciesRate = res.currencies;
      }
    },
      err => {
      });
  }

  updateSo = customer => {

    this.invoiceBookings = this.invoiceTypes.filter(po => po.company.bpCode === customer.bpCode)

    this.collectionsFormGroup.patchValue({ bankName: customer.bank, currency: customer.currency })
  }

  setCurrency = (e, currency) => {
    if (e.isUserInput) {
      this.salesOrderFormGroup.get('currency').setValue(currency.currency)
      if (this.pageType === 'accountReceivable') this.salesOrderFormGroup.get('paymentTerm').setValue(currency.paymentTerms)
      this.salesOrderFormGroup.get('paymentTerm2').setValue(currency.paymentTerms2)
    }
  }

  updateFixedAsset = (item: any) => {
    const fixedAsset = this.fixedAssetArr.getRawValue();
    if (Array.isArray(item) && item.length > 0) {
      item.map(each => {
        const duplicateAsset = fixedAsset.filter(e => e.itemCode == each.itemCode);
        if (duplicateAsset.length > 0) return;
        if (each.productType.toLowerCase() === "cost") {
          this.fixedAssetArr.push(this._formBuilder.group({
            itemCode: [{ value: each.itemCode, disabled: true }, Validators.required],
            assetCode: [null, Validators.required]
          }));
        }
      })
    }
  }

  updateDraftedOrder = order => {
    if (this.pageType === 'accountReceivable') {
      this.salesOrderFormGroup.patchValue({
        supplier: order.customer,
        warehouse: order.warehouse.split(" ")[0],
        currency: order.currency,
        customer: order.customer,
        paymentTerm: order.deliveryTerms,
        paymentTerm2: order.paymentTerm,
        estimatedDeliveryDate: new Date(order.estimatedDeliveryDate),
        creationDate: new Date(order.creationDate),
        customerOrderReference: order.customerOrderReference,
        salesRep: order.salesRep,
        notes: order.notes
      })
    } else {
      this.salesOrderFormGroup.patchValue({
        supplier: order.customer,
        warehouse: order.warehouse.split(" ")[0],
        currency: order.currency,
        customer: order.customer,
        paymentTerm2: order.paymentTerms,
        creationDate: new Date(order.creationDate),
        customerOrderReference: order.customerOrderReference,
        salesRep: order.salesRep,
        notes: order.notes
      })
    }

    this.getItemDetails(order.warehouse.split(' ')[0])

    this.itemsArr.clear()

    let convRate = this.currenciesRate.filter(e => e.isocode === this.utilityService.splitCode('SAR-Saudi riyal'))[0];

    const isAccountReceivable = this.pageType === 'accountReceivable'
    if (!!order && !!order.item && order.item.length > 0) {
      order.item.map(item => {
        this.itemsArr.push(this._formBuilder.group({
          quantity: [item.quantity, Validators.required],
          actualQtn: [item.quantity],
          availableQuantity: [{ value: item.availableQuantity, disabled: true }],
          itemDescription: [{ value: item.itemDescription, disabled: true }, [Validators.required]],
          price: [{ value: parseFloat(item.price).toFixed(2), disabled: !isAccountReceivable }, [Validators.required]],
          priceInSAR: [
            order.currency === 'SAR-Saudi riyal' ? parseFloat(item.price).toFixed(2) : (item.price / convRate.conversion).toFixed(2),
            Validators.required],
          unit: [{ value: item.unit, disabled: true }, [Validators.required]],
          tableItemCode: [{ value: item.itemCode, disabled: !isAccountReceivable }, Validators.required],
          totalprice: [{ value: item.itemTotal, disabled: true }, Validators.required],
          totalpriceInSAR: [{ value: Number(item.itemTotal).toFixed(2), disabled: true }, Validators.required],
          vatCode: [{ value: item.financeVAT.vatCode, disabled: !isAccountReceivable }, Validators.required],
          vatPercentage: [{ value: parseFloat(item.financeVAT.vatPercentage), disabled: !isAccountReceivable }, Validators.required],
          discountPercentage: [{ value: parseFloat(item.discountPercentage), disabled: !isAccountReceivable }, Validators.required],
          subTotal: [{ value: parseFloat(item.subTotal).toFixed(2), disabled: true }, [Validators.required]],
          subTotalInSAR: ['']
        }));
      })
      this.summaryCalculation()
    }

  }

  addItem = (i?: any, item?: any) => {

    const isSalesReturn = this.pageType === 'salesReturn'

    let newRow = this._formBuilder.group({
      quantity: ['', Validators.required],
      availableQuantity: [{ value: '', disabled: true }],
      itemDescription: [{ value: '', disabled: true }, Validators.required],
      price: [{ value: 0, disabled: isSalesReturn }, Validators.required],
      priceInSAR: [0],
      unit: [{ value: '', disabled: true }, [Validators.required]],
      totalprice: [{ value: '', disabled: true }, Validators.required],
      vatPercentage: [{ value: '', disabled: isSalesReturn }, Validators.required],
      vatCode: [{ value: '', disabled: isSalesReturn }, Validators.required],
      discountPercentage: [{ value: '', disabled: isSalesReturn }, Validators.required],
      subTotal: [{ value: '', disabled: true }, [Validators.required]],
      tableItemCode: [[], Validators.required],
      subTotalInSAR: [''],
      totalpriceInSAR: [''],
    });

    this.itemsArr.push(newRow);

    if (!!this.itemCodes) {
      const controls = <FormArray>this.salesOrderFormGroup.controls['itemsArray'];
      this.ManageItemCodeControl(controls.length - 1);
    }
  }

  _filterItemCode = value => {
    const filterValue = String(value).toLowerCase();

    return this.itemCodes.filter(option => option.item.description.toLowerCase().includes(filterValue));
  }

  removeItem = (i, item) => {
    this.itemsArr.removeAt(i);
    this.observers.splice(i, 1)
    this.summaryCalculation();
  }

  updateSODetails = (data) => {
    this.ELEMENT_DATA = [];
    this.selection.clear();
    let amount = (this.calculateAmount(data.item))
    this.ELEMENT_DATA = data.item.filter(item => Number(item.quantity) > 0);
    this.dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);

    const isSalesReturn = this.pageType === 'salesReturn'

   // if (!isSalesReturn) {
      this.materialShpAndDelFormGroup.patchValue({
        customer: data.customer + "-" + data.customerName,
        currency: data.currency,
        warehouse: data.warehouse.split(" ")[0],
        amount: amount.amount,
      });
    //}
    // } else {
    //   this.materialShpAndDelFormGroup.patchValue({
    //     customer: data.company.name,
    //     currency: data.company.currency,
    //     warehouse: '',
    //     amount: amount.invoiceAmount,
    //   });
    // }

    this.materialShpAndDelFormGroupTwo.patchValue({ invoiceCurrency: data.currency })

    if (!!data && !!data.item) {
      data.item.map((item, i) => {
        this.updateMrQty(item.remainingQuantity, item, i)
      })
    }
    this.masterToggle();
    this.updateFixedAsset(data.item);
    this.materialShpAndDelFormGroup.get('amount').setValue(amount.amount.toFixed(2))
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.itemCode + 1}`;
  }

  updateAmountMS(event, row) {
    if (event) {
      this.selection.toggle(row);

      let sumAmnt = this.calculateAmount(this.selection.selected)

      this.materialShpAndDelFormGroup.get('amount').setValue(sumAmnt.amount.toFixed(2));
      if (event.checked && row.productType.toLowerCase() === "cost") {
        this.fixedAssetArr.push(this._formBuilder.group({
          assetCode: [null, Validators.required],
          itemCode: [{ value: row.itemCode, disabled: true }, Validators.required],
        }))
      } else {
        const { fixedAssetArray } = this.materialShpAndDelFormGroup.getRawValue();
        fixedAssetArray.map((each: any, i: number) => {
          if (each.itemCode === row.itemCode) {
            this.fixedAssetArr.removeAt(i);
          }
        })
      }
    } else {
      return;
    }
  }

  updateQty = (ele, i: any) => {
    this.mrQty = ele.remainingQuantity;

    let defaultQty;

    if (this.defaultValueSO[this.materialShpAndDelFormGroup.get('soNumber').value] && this.defaultValueSO[this.materialShpAndDelFormGroup.get('soNumber').value][ele.itemCode]) {
      defaultQty = this.defaultValueSO[this.materialShpAndDelFormGroup.get('soNumber').value][ele.itemCode];
    } else {
      this.defaultValueSO = {
        [this.materialShpAndDelFormGroup.get('soNumber').value]: {
          [ele.itemCode]: this.mrQty
        }
      }
      defaultQty = this.mrQty
    }

    const dialogRef = this.dialog.open(UpdateMSQtyFormModalComponent, {
      data: { qty: this.mrQty, code: ele.itemCode, valideQtyLimit: defaultQty }
    });

    dialogRef.afterClosed().subscribe(result => {
      result && this.updateMrQty(result, ele, i);
    });
  }

  updateMrQty = (e, data, indexNum) => {
    const index = indexNum//this.ELEMENT_DATA.findIndex(e => e.itemCode === data.itemCode);
    let subTotal = (e * Number(this.ELEMENT_DATA[index].price)) * (1 - (Number(this.ELEMENT_DATA[index].discountPercentage) / 100))
    let itemTotal = subTotal * (1 + ((Number(this.ELEMENT_DATA[index].vatPercentage)) / 100))
    // let subTotal = (e * Number(this.ELEMENT_DATA[index].price)) - ((e * Number(this.ELEMENT_DATA[index].price)) * Number(this.ELEMENT_DATA[index].discountPercentage) / 100);
    // let itemTotal = (subTotal + (subTotal * ((Number(this.ELEMENT_DATA[index].vatPercentage)) / 100)));
    this.ELEMENT_DATA[index].remainingQuantity = String(e);
    this.ELEMENT_DATA[index].itemTotal = String(itemTotal);
    this.ELEMENT_DATA[index].subTotal = String(subTotal);
    this.dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
    this.dataSource.data.forEach(row => {
      if (row.itemCode == data.itemCode && this.checkboxLabel(data).indexOf("deselect row") > -1) {
        this.selection.select(row);
      }
    });

    setTimeout(() => {
      let sumAmnt = this.calculateAmount(this.selection.selected)
      let vat = Number(sumAmnt.vatPercentage) / this.selection.selected.length;

      let consolidatedAmt = Number(sumAmnt.subTotal) * (1 + (vat / 100));
      // this.materialReceivingFormGroup.get('amount').setValue(consolidatedAmt.toFixed(2));
      this.materialShpAndDelFormGroup.get('amount').setValue(consolidatedAmt.toFixed(2));
      this.materialShpAndDelFormGroupTwo.get('invoiceAmount').setValue(sumAmnt.amount.toFixed(2))
      this.materialShpAndDelFormGroupTwo.get('vatAmount').setValue(sumAmnt.vatAmount.toFixed(2))
    }, 2)




  }

  updateItemsPayAmount = (event, so) => {
    let rawData = this.collectionsFormGroup.getRawValue();
    if (event.isUserInput) {
      if (event.source.selected) {
        this.amountArr.push(this._formBuilder.group({
          invoiceNo: [{ value: so.invoiceNumber, disabled: true }, Validators.required],
          totalprice: [parseFloat(so.invoiceAmount), Validators.required],
          selectedMSNumber: [{ value: so.referenceOrderData.referenceOrder2, disabled: true }, Validators.required],
          materialShipped: [{ value: so.referenceOrder, disabled: true }, Validators.required],
        }));
        this.collectionsFormGroup.patchValue({ currency: this._filterCurrency(so.currency)[0] })
      } else {

        let index = rawData.soArray.findIndex((e: any) => e.selectedMSNumber === so.code);
        this.amountArr.removeAt(index);
      }
      rawData = this.collectionsFormGroup.getRawValue();
      let sumAmnt = rawData.soArray.reduce((acc, e: any) => {
        return acc += e.totalprice;
      }, 0);
      this.collectionsFormGroup.get('grossamt').setValue(sumAmnt);

      if (this.collectionsFormGroup.get('vatPercentage').value && this.collectionsFormGroup.get('vatPercentage').value !== '') {
        let val = this.collectionsFormGroup.get('vatPercentage').value;
        let subTotal = this.collectionsFormGroup.get('grossamt').value;
        let sumAmnt: any = (Number(subTotal) * (1 + val / 100)).toFixed(2);
        this.collectionsFormGroup.get('grandtotalprice').setValue(parseFloat(sumAmnt));
      } else {
        this.collectionsFormGroup.get('grandtotalprice').setValue(parseFloat(sumAmnt));
      }
    }
  }

  updateTotalPayAmount = (i) => {
    let rawData = this.collectionsFormGroup.getRawValue();
    let sumAmnt = rawData.soArray.reduce((acc, e: any) => {
      return acc += e.totalprice;
    }, 0);
    this.collectionsFormGroup.get('grossamt').setValue(sumAmnt);

    if (this.collectionsFormGroup.get('vatPercentage').value && this.collectionsFormGroup.get('vatPercentage').value !== '') {
      let val = this.collectionsFormGroup.get('vatPercentage').value;
      let subTotal = this.collectionsFormGroup.get('grossamt').value;
      let sumAmnt: any = (Number(subTotal) * (1 + val / 100)).toFixed(2);
      this.collectionsFormGroup.get('grandtotalprice').setValue(parseFloat(sumAmnt));
    }
  }

  calculateAmount = items => {
    let summary = {
      vatPercentage: 0,
      discountPercentage: 0,
      subTotal: 0,
      amount: 0,
      disPercent: 0,
      qty: 0,
      price: 0,
      vatPercent: 0,
      vatAmount: 0,
    }
    return items.reduce((acc, e: any) => {
      let qty = acc.qty += parseInt(e.quantity)
      let total = (acc.price += parseFloat(e.price))
      let disPercentage = acc.disPercent += parseFloat(e.discountPercentage)
      let subTotal = acc.subTotal += parseFloat(e.subTotal)
      let vatPercentage = acc.vatPercent += parseFloat(e.vatPercentage)
      let vatAmount = acc.vatAmount += (parseFloat(parseFloat(e.subTotal).toFixed(2)) * (parseFloat(e.vatPercentage) / 100))
      return {
        disPercent: acc.discountPercentage += (e.discountPercentage),
        vatPercent: vatPercentage,
        vatPercentage: vatPercentage,
        discountPercentage: disPercentage,
        qty: qty,
        price: (acc.price) += parseFloat(e.price),
        subTotal: parseFloat(subTotal),
        vatAmount: (vatAmount),
        discountAmount: (qty * total * disPercentage) / 100,
        amount: acc.amount += (Number(e.remainingQuantity) * Number(e.price))

      }
    }, summary);
  }

  summaryCalculation = () => {
    let summary = {
      vatPercentage: 0,
      discountPercentage: 0,
      subTotal: 0,
      subTotalInSAR: 0,
      amount: 0,
      amountInSAR: 0,
      disPercent: 0,
      qty: 0,
      price: 0,
      vatPercent: 0,
      vatAmount: 0,
      discountAmount: 0,
      totalAmount: 0
    }
    let rawData = this.salesOrderFormGroup.getRawValue();
    this.poSummary = rawData.itemsArray.reduce((acc, e: any) => {
      let qty = acc.qty += parseInt(e.quantity)
      let price = (acc.price += parseFloat(e.price))
      let disPercentage = acc.disPercent += parseFloat(e.discountPercentage)
      let subTotal = acc.subTotal += parseFloat(e.subTotal)
      let subTotalInSAR = acc.subTotalInSAR += parseFloat(e.subTotalInSAR === '' ? 0 : e.subTotalInSAR)
      let vatPercentage = acc.vatPercent += parseFloat(e.vatPercentage)
      let vatAmount = acc.vatAmount += (parseFloat(e.subTotal)) * ((parseFloat(e.vatPercentage) / 100))
      let discountAmount = acc.discountAmount += ((e.quantity * e.price) * (parseFloat(e.discountPercentage) / 100))
      let totalAmount = acc.totalAmount += (e.quantity * e.price)
      return {
        disPercent: acc.discountPercentage += (e.discountPercentage),
        vatPercent: vatPercentage,
        vatPercentage: vatPercentage,
        discountPercentage: disPercentage,
        qty: qty,
        price: price,
        subTotal: parseFloat(subTotal),
        subTotalInSAR: parseFloat(subTotalInSAR),
        vatAmount: (vatAmount),
        discountAmount: parseFloat(discountAmount),
        amount: acc.amount += parseFloat(e.totalprice),
        amountInSAR: acc.amountInSAR += Number(e.totalpriceInSAR),
        totalAmount: totalAmount

      }
    }, summary);
  }

  onStepChange = (e: any) => {
    if (e.selectedIndex === 3) {
      this.msTypeAtPayment = []
      this.financeService.getApprovedMaterialShipped().subscribe((res: any) => {
        this.soTypeAtPayments = res.materialShippedList
      },
        err => {

        })
    }
  }

  updateItemDetails = (event, index, itemData) => {
    let selectedItemInfo = this.itemCodes.filter(e => e.itemCode === itemData.itemCode)[0];
    let convRate = this.currenciesRate.filter(e => e.isocode === this.utilityService.splitCode(this.salesOrderFormGroup.value.currency))[0];

    this.itemsArr.at(index).patchValue({
      quantity: '',
      availableQuantity: selectedItemInfo.availableQuantity,
      itemDescription: selectedItemInfo.item.description,
      price: this.salesOrderFormGroup.value.currency === '' || this.salesOrderFormGroup.value.currency.includes('SAR') ?
        (selectedItemInfo.item.purchasePrice).toFixed(2) : (selectedItemInfo.item.purchasePrice / convRate.conversion).toFixed(2),
      priceInSAR: selectedItemInfo.item.purchasePrice.toFixed(2),
      unit: selectedItemInfo.item.inventoryUnit,
      totalprice: '',
      vatPercentage: '5',
      discountPercentage: '',
      subTotal: '',
      subTotalInSAR: '',
      totalpriceInSAR: ''
    });
  }

  updateVat = (i, vatPercentage) => {
    this.itemsArr.at(i).patchValue({ vatPercentage: vatPercentage })
    this.updateAmount(i)
  }

  checkPrice = (i) => {
    if (this.itemsArr.at(i).value.price != null && this.itemsArr.at(i).value.price <= 0) {
      this.itemsArr.at(i).patchValue({ price: 1 });
    }
  }

  getItemDetails = warehouseCode => {

    let uri = `stocklevels?warehouse=${warehouseCode}&itemCode=&itemGroup=&date`

    this.financeService.getInventoryData(uri).subscribe((res: any) => {
      if (!!res && !!res.stockLevels) {
        this.itemCodes = res.stockLevels
        const controls = <FormArray>this.salesOrderFormGroup.controls['itemsArray'];
        this.ManageItemCodeControl(controls.length - 1);
      }

    },
      err => {

      })
  }

  updateAmount = (i, percent?: any) => {

    let subTotal: any
    let grandTotal: any
    let subTotalInSAR: any
    let grandTotalInSAR: any

    if (this.itemsArr.at(i).value.quantity != null && this.itemsArr.at(i).value.quantity < 1) {
      this.itemsArr.at(i).patchValue({ quantity: 1 });
    }

    if (this.pageType === 'salesReturn' && this.itemsArr.at(i).value.quantity != null && this.itemsArr.at(i).value.quantity > this.itemsArr.at(i).value.actualQtn) {
      this.itemsArr.at(i).patchValue({ quantity: this.itemsArr.at(i).value.actualQtn });
    }

    if (!!this.itemsArr.at(i).get('availableQuantity').value && this.itemsArr.at(i).value.quantity > this.itemsArr.at(i).get('availableQuantity').value) {
      this.itemsArr.at(i).patchValue({ quantity: this.itemsArr.at(i).get('availableQuantity').value })
    }

    if (!this.utilityService.isNullOrUndefined(this.itemsArr.at(i).value.quantity) &&
      !this.utilityService.isUndefinedOrEmpty(this.itemsArr.at(i).value.quantity) &&
      !this.utilityService.isNullOrUndefined(this.itemsArr.at(i).value.price) &&
      !this.utilityService.isUndefinedOrEmpty(this.itemsArr.at(i).value.price) &&
      !this.utilityService.isUndefinedOrEmpty(this.itemsArr.at(i).value.discountPercentage) &&
      !this.utilityService.isNullOrUndefined(this.itemsArr.at(i).value.discountPercentage)) {
      subTotal = (this.itemsArr.at(i).value.quantity * this.itemsArr.at(i).value.price) * (1 - (this.itemsArr.at(i).value.discountPercentage / 100))
      grandTotal = subTotal * (1 + ((!!percent ? parseInt(percent) : Number(this.itemsArr.at(i).value.vatPercentage)) / 100))
      subTotalInSAR = (this.itemsArr.at(i).value.quantity * this.itemsArr.at(i).value.priceInSAR) * (1 - (this.itemsArr.at(i).value.discountPercentage / 100))
      grandTotalInSAR = subTotalInSAR * (1 + ((!!percent ? parseInt(percent) : Number(this.itemsArr.at(i).value.vatPercentage)) / 100))
      this.itemsArr.at(i).patchValue(
        {
          subTotal: (subTotal).toFixed(2),
          totalprice: grandTotal.toFixed(2),
          subTotalInSAR: (subTotalInSAR).toFixed(2),
          totalpriceInSAR: grandTotalInSAR.toFixed(2),
        }
      );
    }

    this.summaryCalculation()

  }

  onSubmitSo = action => {

    const { customer, itemsArray, currency, warehouse, draftedOrder, paymentTerm, paymentTerm2, estimatedDeliveryDate, creationDate, notes, customerOrderReference, salesRep } = this.salesOrderFormGroup.getRawValue();

    const itemList: any = [];

    itemsArray.forEach(item => {
      itemList.push({
        itemCode: item.tableItemCode,
        quantity: item.quantity,
        itemTotal: item.totalprice,
        price: item.price,
        discountPercentage: item.discountPercentage,
        financeVAT: {
          vatPercentage: parseFloat(item.vatPercentage),
          vatCode: item.vatCode
        },
        subTotal: item.subTotal
      })
    });

    const request = {
      warehouse: warehouse,
      customer: this.utilityService.splitCode(customer),
      currency: this.utilityService.splitCode(currency),
      paymentTerms: paymentTerm2,
      soTotal: this.poSummary.amount,
      item: itemList,
      creationDate: this.pipe.transform(creationDate, 'MM/dd/yyyy'),
      notes,
      salesRep: this.utilityService.splitCode(salesRep),
      customerOrderReference,
      totalAmountInSAR: this.poSummary.subTotalInSAR,
      totalTaxAmountInSAR: this.poSummary.amountInSAR
    };

    if (this.pageType === 'accountReceivable') {
      request['deliveryTerms'] = paymentTerm
      request['estimatedDeliveryDate'] = this.pipe.transform(estimatedDeliveryDate, 'MM/dd/yyyy')
    } else {
      request['salesOrder'] = draftedOrder
    }


    this.financeService.submitSalesOrder(this.pageType === 'salesReturn' ? 'return-sales-order' : 'sales-order', request, action).subscribe((res: any) => {
      this.soForm.resetForm()
      const pageType = this.pageType === 'salesReturn' ? 'return sales order' : 'sales order'
      this.showMsg('success', action === 'save' ? `${pageType} ${res.salesOrder} saved successfully` : `${pageType} ${res.salesOrder} submitted successfully`);
      this.salesOrderFormGroup.reset();
      while (this.itemsArr.length !== 0) {
        this.itemsArr.removeAt(0)
      }
    }, err => {
      if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.showMsg('error', err.error.errors[0].message)
      } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    });
  }

  displayItemId = itemId => {
    return !!itemId ? itemId : undefined
  }

  updateConveRate(curr) {
    let convRate = this.currenciesRate.filter(e => e.isocode === curr)[0];
    this.itemsArr.controls.forEach((e, i) => {
      let { tableItemCode } = this.itemsArr.at(i).value;
      if (tableItemCode && tableItemCode !== "") {
        let price = this.itemCodes.filter(e => e.item.itemCode === tableItemCode)[0].item.purchasePrice;
        this.itemsArr.at(i).patchValue({ price: !curr.includes("SAR") ? price / convRate.conversion : price });
        this.updateAmount(i)
      }
    })
  }

  updateInvoiceDetails = ms => {
    ms.item[0]['remainingQuantity'] = ms.item[0].quantity
    let amount = this.calculateAmount(ms.item)
    this.materialShpAndDelFormGroupTwo.get('invoiceAmount').setValue(Number(ms.invoiceAmount))
    this.materialShpAndDelFormGroupTwo.get('vatAmount').setValue(amount.vatAmount.toFixed(2))
    this.materialShpAndDelFormGroupTwo.get('invoiceCurrency').setValue(ms.currency)
    this.materialShpAndDelFormGroupTwo.patchValue({ deliveryNote: ms.deliveryNote });
  }

  submitMSInvoice = () => {
    const { invoiceAmount, invoiceDate, referenceOrder, invoiceCurrency, notes, deliveryNote } = this.materialShpAndDelFormGroupTwo.getRawValue()
    const invoice = this.pipe.transform(invoiceDate, 'MM/dd/yyyy');

    const request = {
      referenceOrder,
      invoiceAmount,
      invoiceDate: invoice,
      currency: invoiceCurrency.split('-')[0],
      notes,
      deliveryNote,
      orderType: this.pageType === 'accountReceivable' ? 'ACCOUNTS_RECEIVABLE' : 'RETURN_ORDER'
    };

    if(this.pageType === 'accountReceivable') {
      request['notes'] = notes
      request['deliveryNote'] = deliveryNote
    }


    this.financeService.submitInvoice('invoice-booking', request).subscribe((res: any) => {
      this.msFormTwo.resetForm()
      this.showMsg('success', `${res.invoiceNumber} has been submitted successfully`);
    },
      err => {
        if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.showMsg('error', err.error.errors[0].message)
        } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      })
  }

  submitMSForm = () => {

    const { soNumber, fixedAssetArray, currency, warehouse, shipmentDate, amount } = this.materialShpAndDelFormGroup.getRawValue()

    this.selection.selected.map(item => item.quantity = item.remainingQuantity);

    this.filteredItems = this.selection.selected.filter(item => item.remainingQuantity !== '0')
    if (this.filteredItems.length == 0) {
      this.showMsg('error', 'Selected item has no remaining quantity');
      return;
    }

    let result = this.filteredItems.map((e) => {
      if (fixedAssetArray.findIndex(k => k.itemCode === e.itemCode) > -1) {
        return {
          ...e,
          assetCode: fixedAssetArray[fixedAssetArray.findIndex(k => k.itemCode === e.itemCode)].assetCode
        }
      } else {
        return { ...e, assetCode: "" }
      }
    });

    const request = {
      materialShippedData: {
        salesOrder: soNumber,
        warehouse: warehouse,
        shipmentDate: this.pipe.transform(shipmentDate, 'MM/dd/yyyy'),
        currency: this.utilityService.splitCode(currency),
        item: result,
        invoiceAmount: amount
      }
    };

    const uri = this.pageType === 'accountReceivable' ? 'material-shipped' : 'material-return'

    this.financeService.submitMaterialShipped(uri, request).subscribe((res: any) => {
      this.msForm.resetForm()
      this.fixedAssetArr.reset()
      this.showMsg('success', `${res.code} has been submitted successfully`);

    },
      err => {
        if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.showMsg('error', err.error.errors[0].message)
        } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      })
  }

  submitPayment = () => {

    const { customer, documentDate, typeOfTransaction, transactionEntryDate, currency, bankName, invoiceBookings, grossamt, notes, vatPercentage, vatCode } = this.collectionsFormGroup.value;
    const { soArray, grandtotalprice } = this.collectionsFormGroup.getRawValue();
    const transDate = this.pipe.transform(transactionEntryDate, 'MM/dd/yyyy');
    const docDate = this.pipe.transform(documentDate, 'MM/dd/yyyy');

    const request = {
      PaymentsData: {
        company: customer,
        invoiceBookings,
        transactionEntryDate: transDate,
        documentDate: docDate,
        transactionType: typeOfTransaction,
        //amount: grandtotalprice,
        currency: this.utilityService.splitCode(currency),
        bank: bankName,
        "grossAmount": grossamt,
        "netAmount": grandtotalprice,
        "comment": notes,
        "vatPercentage": vatPercentage,
        "vatDetails": {
          "vatCode": this.utilityService.splitCode(vatCode)
        },
        orderAmountMap: soArray.map(e => {
          return {
            orderNumber: e.invoiceNo,
            amount: e.totalprice
          }
        }),
        paymentType: 'ACCOUNTS_RECEIVABLE'
      }
    }

    console.log(request);

    this.financeService.submitPayment(request).subscribe((res: any) => {
      this.collectionForm.resetForm()
      this.showMsg('success', (`the Collection ${res.code} has been submitted successfully`))
    },
      err => {
        if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.showMsg('error', err.error.errors[0].message)
        } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
      })


  }

  showMsg = (type, succMsg) => {
    Swal.fire({
      icon: type,
      text: succMsg,
      timer: 15000,
      showCloseButton: true,
      cancelButtonText:
        '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }

  updateAmountPayment(e) {
    const val = e.target.value;

    // this.paymentFormGroup.patchValue({ vatPercentage: val });
    let subTotal = this.collectionsFormGroup.get('grossamt').value;
    let sumAmnt: any = (Number(subTotal) * (1 + val / 100)).toFixed(2);
    this.collectionsFormGroup.get('grandtotalprice').setValue(parseFloat(sumAmnt));
  }

  updateAmountPaymentGross(e) {
    const subTotal = e.target.value;
    if (this.collectionsFormGroup.get('vatPercentage').value && this.collectionsFormGroup.get('vatPercentage').value !== '') {
      let val = this.collectionsFormGroup.get('vatPercentage').value;
      let sumAmnt: any = (Number(subTotal) * (1 + val / 100)).toFixed(2);
      this.collectionsFormGroup.get('grandtotalprice').setValue(parseFloat(sumAmnt));
    } else {
      this.collectionsFormGroup.get('grandtotalprice').setValue(parseFloat(subTotal));
    }
  }

  updateVatPayment(val) {
    this.collectionsFormGroup.patchValue({ vatPercentage: val });
    let subTotal = this.collectionsFormGroup.get('grossamt').value;
    let sumAmnt: any = (Number(subTotal) * (1 + val / 100)).toFixed(2);
    this.collectionsFormGroup.get('grandtotalprice').setValue(parseFloat(sumAmnt));
  }

  checkVat(type) {
    if (type == "AdvanceReceipts") {
      this.collectionsFormGroup.get("invoiceBookings").clearValidators();
      this.collectionsFormGroup.get("invoiceBookings").enable()
      this.collectionsFormGroup.get("vatCode").setValidators([Validators.required]);
      this.collectionsFormGroup.get("vatPercentage").setValidators([Validators.required]);
      this.collectionsFormGroup.get("notes").setValidators([Validators.required]);
      this.collectionsFormGroup.get("vatCode").enable();
      this.collectionsFormGroup.get("vatPercentage").enable();
      this.collectionsFormGroup.get("notes").enable();
    } else if (type == "UnallocatedReceipts") {
      this.collectionsFormGroup.get("invoiceBookings").clearValidators();
      this.collectionsFormGroup.get("invoiceBookings").enable()
      this.collectionsFormGroup.get("vatCode").clearValidators();
      this.collectionsFormGroup.get("vatPercentage").clearValidators();
      this.collectionsFormGroup.get("notes").clearValidators();
      this.collectionsFormGroup.get("vatCode").enable();
      this.collectionsFormGroup.get("vatPercentage").enable();
      this.collectionsFormGroup.get("notes").enable();

    } else {
      this.collectionsFormGroup.get("invoiceBookings").setValidators([Validators.required]);
      this.collectionsFormGroup.get("invoiceBookings").enable()
      this.collectionsFormGroup.get("vatCode").clearValidators();
      this.collectionsFormGroup.get("vatPercentage").clearValidators();
      this.collectionsFormGroup.get("vatCode").enable();
      this.collectionsFormGroup.get("vatPercentage").enable();
      this.collectionsFormGroup.get("notes").clearValidators();
      this.collectionsFormGroup.get("notes").enable();
    }
  }
}

export interface PeriodicElement {
  itemCode: number;
  itemDescription: string;
  quantity: string;
  price: string;
  itemTotal: string;
  unit: string;
  discountPercentage: string;
  subTotal: string;
  remainingQuantity: string,
  vatPercentage: string
}

@Component({
  selector: 'update-qty',
  templateUrl: 'update-qty.html',
  providers: [AccountReceivableMoleculeComponent]
})
export class UpdateMSQtyFormModalComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data, public dialog: MatDialog) { }
  onNoClick(): void {
    this.dialog.closeAll();
  }

  checkQty = (e) => {
    if (this.data.qty && this.data.qty > parseInt(this.data.valideQtyLimit) || isNaN(Number(this.data.qty))) {
      this.data.qty = this.data.valideQtyLimit;
    }
    if (this.data.qty !== "" && this.data.qty !== null && this.data.qty < 1) {
      this.data.qty = this.data.valideQtyLimit;
    }
  }

}