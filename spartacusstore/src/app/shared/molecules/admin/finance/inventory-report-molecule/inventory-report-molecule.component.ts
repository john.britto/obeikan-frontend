import { Component, OnInit, Output, EventEmitter, ViewChild, Inject } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { API_FAILURE_MSG, FinanceService } from '../../../../../services/finance.service'
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';
import { MatPaginator } from '@angular/material/paginator';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { CommonUtilityService } from '../../../../../services/common-utility-service'


@Component({
  selector: 'app-inventory-report-molecule',
  templateUrl: './inventory-report-molecule.component.html',
  styleUrls: ['./inventory-report-molecule.component.scss']
})
export class InventoryReportMoleculeComponent implements OnInit {

  @Output() scrollToPosition: EventEmitter<string> = new EventEmitter();
  pipe = new DatePipe('en-US');
  displayedColumns: string[];
  transactionDisplayColumns: string[]
  ELEMENT_DATA: PeriodicElement[]
  dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
  printDataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
  transactionDataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  filterFormGroup: FormGroup
  itemCodes: any = []
  wareHouses: any = []
  itemGroups: any = [];
  defaultData = []
  filteredItemCode: Observable<string[]>
  filteredWarehouse: Observable<string[]>
  filteredItemGroup: Observable<string[]>
  itemCode = new FormControl();
  warehouse = new FormControl();
  itemGroup = new FormControl();
  currenciesRate: any;

  constructor(
    private _formBuilder: FormBuilder,
    private financeService: FinanceService,
    private utilityService: CommonUtilityService,
    public dialog: MatDialog,) { }

  ngOnInit() {

    this.financeService.getInventoryData('stocklevels').subscribe((res: any) => {
      if (!!res && !!res.stockLevels) {
        this.defaultData = res.stockLevels;
        this.ELEMENT_DATA = this.restructureStockValues(res.stockLevels); 
        this.dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
        this.printDataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
        this.dataSource.sort = this.sort;
        this.displayedColumns = ['No', 'itemCode', 'itemDescription', 'itemGroup', 'availableQuantity', 'currency', 'price', 'maucValue', 'maucValueInSAR', 'wareHouseCode', 'wareHouseDesc', 'inventoryValue']
        this.dataSource.paginator = this.paginator;
        this.itemCodes = [...new Set(res.distinctItems)]
        this.itemGroups = [...new Set(res.distinctItemGroup)]
        this.wareHouses = [...new Set(res.distinctWarehouse)]
        this.scrollToPosition.emit();
        this.filteredItemCode = this.itemCode.valueChanges.pipe(startWith(''), map(value => this._filterItemCode(value)));
        this.filteredWarehouse = this.warehouse.valueChanges.pipe(startWith(''), map(value => this._filterWarehouse(value)));
        this.filteredItemGroup = this.itemGroup.valueChanges.pipe(startWith(''), map(value => this._filterItemGroup(value)));
      }
    },
      err => {
        this.showMsg('error', API_FAILURE_MSG)
        this.scrollToPosition.emit();
      });

    this.filterFormGroup = this._formBuilder.group({
      date: [''],
      warehouse: [''],
      itemCode: [''],
      itemGroup: ['']
    });

    this.financeService.getSupplierDetails('currencies').subscribe((res: any) => {
      if (res && res.currencies) {
        this.currenciesRate = res.currencies.filter(e => e.isocode === "SAR")[0].conversion;
      }
    },
      err => {
      });

  }

  _filterItemCode = value => {
    const filterValue = value.toLowerCase();

    return this.itemCodes.filter(option => {
      return this.utilityService.splitDesc(option).toLowerCase().includes(filterValue)
    } );
  }

  _filterWarehouse = value => {
    const filterValue = value.toLowerCase();

    return this.wareHouses.filter(option => this.utilityService.splitDesc(option).toLowerCase().includes(filterValue));
  }

  _filterItemGroup = value => {
    const filterValue = value.toLowerCase();

    return this.itemGroups.filter(option => option.toLowerCase().includes(filterValue));
  }

  unselectValue = key => this.filterFormGroup.patchValue({[key]: ''})

  onFilter = () => {

    const { date, warehouse, itemCode, itemGroup } = this.filterFormGroup.getRawValue()

    if (!!date || !!warehouse || !!itemCode || !!itemGroup) {

      let wareHouse = ''; let itemcode = '';

      // if(!!warehouse) wareHouse  = warehouse.substring(0, 3)
      // if(!!itemCode) itemcode  = itemCode.substring(0, 3)

      if(!!warehouse) wareHouse  = warehouse.split(" ")[0]
      if(!!itemCode) itemcode  = itemCode.split(" ")[0]

      const query = `?warehouse=${wareHouse}&itemCode=${itemcode}&itemGroup=${itemGroup}&date=${!!date ? this.pipe.transform(date, 'MM/dd/yyyy') : ''}`

      this.financeService.getInventoryData(`stocklevels${query}`).subscribe((res: any) => {
        if (!!res && !!res.stockLevels) {
          this.ELEMENT_DATA = this.restructureStockValues(res.stockLevels)
          this.dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
          this.printDataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
          this.dataSource.sort = this.sort;
          this.displayedColumns = ['No', 'itemCode', 'itemDescription', 'itemGroup', 'availableQuantity', 'currency', 'price', 'maucValue', 'maucValueInSAR', 'wareHouseCode', 'wareHouseDesc', 'inventoryValue']
          this.dataSource.paginator = this.paginator;
          this.itemCodes = [...new Set(res.distinctItems)]
          this.itemGroups = [...new Set(res.distinctItemGroup)]
          this.wareHouses = [...new Set(res.distinctWarehouse)]
          this.filteredItemCode = this.itemCode.valueChanges.pipe(startWith(''), map(value => this._filterItemCode(value)));
          this.filteredWarehouse = this.warehouse.valueChanges.pipe(startWith(''), map(value => this._filterWarehouse(value)));
          this.filteredItemGroup = this.itemGroup.valueChanges.pipe(startWith(''), map(value => this._filterItemGroup(value)));
        } else {
          this.ELEMENT_DATA = []
          this.dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
          this.printDataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
          this.dataSource.sort = this.sort;
          this.displayedColumns = ['No', 'itemCode', 'itemDescription', 'itemGroup', 'availableQuantity', 'currency', 'price', 'wareHouseCode', 'wareHouseDesc', 'inventoryValue']
          this.dataSource.paginator = this.paginator;
          this.itemCodes = [...new Set(res.distinctItems)]
          this.itemGroups = [...new Set(res.distinctItemGroup)]
          this.wareHouses = [...new Set(res.distinctWarehouse)]
          this.filteredItemCode = this.itemCode.valueChanges.pipe(startWith(''), map(value => this._filterItemCode(value)));
          this.filteredWarehouse = this.warehouse.valueChanges.pipe(startWith(''), map(value => this._filterWarehouse(value)));
          this.filteredItemGroup = this.itemGroup.valueChanges.pipe(startWith(''), map(value => this._filterItemGroup(value)));
        }
      },
        err => {
          this.showMsg('error', API_FAILURE_MSG)
        })
    }
  }

  showTransaction = transaction => {

    if (!!transaction) {
      this.financeService.getInventoryData(`stocklevelhistory/${transaction}`).subscribe((res: any) => {
        if (!!res && !!res.stockLevelHistoryEntries) {
          const dialogRef = this.dialog.open(TransactionHistoryModalComponent, {
            data: res.stockLevelHistoryEntries
          });
        } else {
          this.showMsg('error', 'No transactions to display')
        }
      })
    }
  }

  onPrint = () => {
    window.print()
  }

  showMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }

  updateSelect(formGroup, key, value) {
    this[formGroup].patchValue({
      [key]: value
    });
  }

  restructureStockValues = stocks => {
    let stockValues = [];

    stocks.map((stock => {
       stockValues.push({
        ...stock,
        currency: stock.item.currency,
        itemDescription: stock.item.description,
        itemGroup: stock.item.itemGroup,
        wareHouseCode: stock.warehouse.code,
        wareHouseDesc: stock.warehouse.description,
        maucValue: stock.maucValue.toFixed(2),
        maucValueInSAR: this.utilityService.splitCode(stock.item.currency) === "SAR" ? stock.maucValue.toFixed(2) : (stock.maucValue / this.currenciesRate).toFixed(2),
        price: stock.item.price
       })
    }))
    return stockValues
  }

}

export interface PeriodicElement {
  itemCode: string,
  reservedQty: string
}

@Component({
  selector: 'transaction-history-modal',
  templateUrl: 'transaction-history-modal.html',
  providers: [InventoryReportMoleculeComponent]
})
export class TransactionHistoryModalComponent {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  ELEMENT_DATA: PeriodicElement[]
  transactionDataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
  transactionDisplayColumns = ['No', 'updateDate', 'updateType', 'reservedQty', 'actualQty', 'comment']

  constructor(@Inject(MAT_DIALOG_DATA) public data, public dialog: MatDialog) {
    this.ELEMENT_DATA = data
    this.transactionDataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
    this.transactionDisplayColumns = ['No', 'updateDate', 'updateType', 'actualQty', 'reservedQty', 'comment']
    this.transactionDataSource.paginator = this.paginator;
  }
  onNoClick(): void {
    this.dialog.closeAll();
  }

  print = () => {
    window.print()
  }

}