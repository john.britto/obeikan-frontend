import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { FinanceService, VALIDATION_MESSAGES, assetGroups } from '../../../../../services/finance.service'
import { DatePipe } from '@angular/common';
import * as XLSX from 'xlsx';
import { CommonUtilityService } from '../../../../../services/common-utility-service';
import { appConfig } from '../../../../../app.config'
import Swal from 'sweetalert2';
import { CURRENCIES } from '../../../../../../assets/js/currencies';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-general-ledger-molecule',
  templateUrl: './general-ledger-molecule.component.html',
  styleUrls: ['./general-ledger-molecule.component.scss']
})
export class GeneralLedgerMoleculeComponent implements OnInit {

  @ViewChild('glForm', { static: false }) glForm;
  pipe = new DatePipe('en-US');
  glFormGroup: FormGroup;
  validationMessages = VALIDATION_MESSAGES;
  today = new Date();
  transactions: any = []
  supliers: any = []
  customers: any = []
  draftedGls: any = []
  ledgers: any = []
  dimensions: any = []
  types: any = appConfig.DropdownOptions.transactionTypes
  currencies: any = CURRENCIES
  selectedFiscalPeriod: any
  selectedTaxPeriod: any
  templateUrl: any
  doNotShowSumbit: boolean = false;
  isEnteredAmt: boolean = false;
  vatCodes: any = [];
  assets: any = [];
  showAsset: boolean = false;
  matchedAsset: any = {}
  setMandInvNumFalse: boolean = false;
  isMSI: boolean = false;

  constructor(
    private _formBuilder: FormBuilder,
    private financeService: FinanceService,
    private utilityService: CommonUtilityService) {
    this.today.setDate(this.today.getDate());
  }

  get supply() { return this.glFormGroup.controls; }
  get transactionsArr() { return this.supply.transactionArray as FormArray; }

  ngOnInit() {

    this.financeService.getGLData('transaction-type').subscribe((res: any) => {
      this.transactions = res.financialTransactionTypeList
    },
      err => {
      })

    this.financeService.getSupplierDetails('customermasterdatasetup').subscribe((res: any) => {
      this.supliers = (!!res && !!res.supplierInfo) ? res.supplierInfo : []
      this.customers = (!!res && !!res.customerInfo) ? res.customerInfo : []
      this.ledgers = (!!res && !!res.ledgerAccount) ? res.ledgerAccount : []
      this.dimensions = (!!res && !!res.dimension) ? res.dimension : []
      this.vatCodes = (!!res && !!res.financeVAT) ? res.financeVAT : []
      this.assets = (!!res && !!res.asset) ? res.asset : []
    },
      err => { }
    )

    this.financeService.getGLData('draft-financial-entry').subscribe((res: any) => {
      if (!!res && !!res.generalLedgerFinancialList) this.draftedGls = res.generalLedgerFinancialList
    },
      err => {

      })

    this.financeService.downloadGLTemplate().subscribe((res: any) => {
      if (!!res && !!res.contentSlots && !!res.contentSlots.contentSlot && !!res.contentSlots.contentSlot.components.component[0].media.url) {
        this.templateUrl = res.contentSlots.contentSlot.components.component[0].media.url
      }
    },
      err => {

      })

    this.glFormGroup = this._formBuilder.group({
      date: ['', Validators.required],
      fiscalMonth: [{ value: '', disabled: true }, [Validators.required]],
      fiscalYear: [{ value: '', disabled: true }, [Validators.required]],
      draftedGl: [''],
      taxMonth: [{ value: '', disabled: true }, Validators.required],
      taxYear: [{ value: '', disabled: true }, Validators.required],
      transaction: ['', Validators.required],
      description: [''],
      currency: ['', Validators.required],
      transactionArray: new FormArray([])
    });
  }

  getPeriodData = () => {
    const { date, draftedGl, description, transaction, narration, currency } = this.glFormGroup.getRawValue()

    this.financeService.getGLData('period', this.pipe.transform(date, 'MM/dd/yyyy')).subscribe((res: any) => {
      if (!!res.osanedPeriodList && res.osanedPeriodList.length > 0) {
        let fiscalPeriod
        let fiscalYear
        let taxPeriod
        let taxYear

        res.osanedPeriodList.map(period => {
          if (period.periodType === 'Fiscal') {
            this.selectedFiscalPeriod = period
            fiscalPeriod = Number(period.period)
            fiscalYear = Number(period.year)
          }
          if (period.periodType === 'Tax') {
            this.selectedTaxPeriod = period
            taxPeriod = Number(period.period)
            taxYear = Number(period.year)
          }
        })

        this.glFormGroup = this._formBuilder.group({
          date: [date, Validators.required],
          fiscalMonth: [{ value: fiscalPeriod, disabled: false }, [Validators.required]],
          fiscalYear: [{ value: fiscalYear, disabled: true }, [Validators.required]],
          taxMonth: [{ value: taxPeriod, disabled: false }, [Validators.required]],
          taxYear: [{ value: taxYear, disabled: true }, [Validators.required]],
          transaction: [transaction, Validators.required],
          draftedGl: [draftedGl],
          description: [description],
          narration: [narration],
          currency: [currency, Validators.required],
          transactionArray: this.transactionsArr
        });

      } else {
        this.resetGLForm()
      }
    },
      err => {
        this.resetGLForm()
      })
  }

  updateTransaction = (transaction) => {
    if (transaction.code === "FAM") {
      this.showAsset = true;

      if (this.transactionsArr.controls.length > 0) {
        this.transactionsArr.at(0).get('asset').setValidators([Validators.required]);
        this.transactionsArr.at(0).get('asset').enable()
      }

    } else {
      this.showAsset = false;

      if (this.transactionsArr.controls.length > 0) {
        this.transactionsArr.at(0).get('asset').clearValidators()
        this.transactionsArr.at(0).get('asset').enable()
      }
    }
    if (transaction.code === "JNL") {
      this.setMandInvNumFalse = true;
      setTimeout(() => {
        this.transactionsArr.controls.forEach((e, i) => {
          this.transactionsArr.at(i).get('invoiceNumber').clearValidators()
          this.transactionsArr.at(i).get('invoiceNumber').enable()
        })
      }, 2)
    } else {
      this.setMandInvNumFalse = false;
      setTimeout(() => {
        this.transactionsArr.controls.forEach((e, i) => {
          this.transactionsArr.at(i).get('invoiceNumber').setValidators([Validators.required]);
          this.transactionsArr.at(i).get('invoiceNumber').enable()
        })
      }, 2)
    }

    if (transaction.code === "MSI") {
      this.isMSI = true;
      this.transactionsArr.controls.forEach((e, i) => {
        this.transactionsArr.at(i).get('vatCode').setValue(this.transactionsArr.at(0).get('vatCode').value);
        this.transactionsArr.at(i).get('vatPercentage').setValue(this.transactionsArr.at(0).get('vatPercentage').value);
      })
    } else {
      this.isMSI = false;
      this.transactionsArr.controls.forEach((e, i) => {
        this.transactionsArr.at(i).get('vatCode').setValue(null);
        this.transactionsArr.at(i).get('vatPercentage').setValue(null);
      })

    }
    //this.glFormGroup.get('description').setValue(transaction.description)
  }

  updateDraftedGl = gl => {

    const { draftedGl } = this.glFormGroup.getRawValue()

    while (this.transactionsArr.length !== 0) {
      this.transactionsArr.removeAt(0)
    }

    if (!!gl.financialEntryList && gl.financialEntryList.length > 0) {
      for (let transaction of gl.financialEntryList) {

        let dimen1; let dimen2; let dimen3; let dimen4; let dimen5;

        for (let type of transaction.ledgerAccount.dimensionStatus) {
          if (type.key === 'Line_Of_Business') {
            if (type.value === 'Mandatory') dimen1 = { value: '', isValidationReq: true }
            else if (type.value === 'Optional') dimen1 = { value: '', isValidationReq: false }
          }
          if (type.key === 'Item_Group') {
            if (type.value === 'Mandatory') dimen2 = { value: '', isValidationReq: true }
            else if (type.value === 'Optional') dimen2 = { value: '', isValidationReq: false }
          }
          if (type.key === 'Project') {
            if (type.value === 'Mandatory') dimen3 = { value: '', isValidationReq: true }
            else if (type.value === 'Optional') dimen3 = { value: '', isValidationReq: false }
          }
          if (type.key === 'Department') {
            if (type.value === 'Mandatory') dimen4 = { value: '', isValidationReq: true }
            else if (type.value === 'Optional') dimen4 = { value: '', isValidationReq: false }
          }
          if (type.key === 'Employee_Miscellaneous') {
            if (type.value === 'Mandatory') dimen5 = { value: '', isValidationReq: true }
            else if (type.value === 'Optional') dimen5 = { value: '', isValidationReq: false }
          }
        }

        for (let dimension of transaction.dimensions) {
          if (dimension.key === 'Line_Of_Business' && dimen1) dimen1['value'] = dimension.value
          if (dimension.key === 'Item_Group' && dimen2) dimen2['value'] = dimension.value
          if (dimension.key === 'Project' && dimen3) dimen3['value'] = dimension.value
          if (dimension.key === 'Department' && dimen4) dimen4['value'] = dimension.value
          if (dimension.key === 'Employee_Miscellaneous' && dimen5) dimen5['value'] = dimension.value
        }

        let isCustomerAndSupplierEmpty = (!!transaction.customer && !!transaction.customer.bpCode) && (!!transaction.supplier && !!transaction.supplier.bpCode)

        let newRow = this._formBuilder.group({
          ledger: [transaction.ledgerAccount.accountNumber, Validators.required],
          customer: [(!!transaction.customer && !!transaction.customer.bpCode) ? transaction.customer.bpCode : ''],
          //(!!transaction.customer && !!transaction.customer.bpCode) || !isCustomerAndSupplierEmpty ? Validators.required : null],
          supplier: [(!!transaction.supplier && !!transaction.supplier.bpCode) ? transaction.supplier.bpCode : ''],
          //, (!!transaction.supplier && !!transaction.supplier.bpCode) ? Validators.required : null],
          dimen1: [{ value: (!!dimen1 && !!dimen1.value) ? dimen1.value : '', disabled: !dimen1 }],
          dimen2: [{ value: (!!dimen2 && !!dimen2.value) ? dimen2.value : '', disabled: !dimen2 }],
          dimen3: [{ value: (!!dimen3 && !!dimen3.value) ? dimen3.value : '', disabled: !dimen3 }],
          dimen4: [{ value: (!!dimen4 && !!dimen4.value) ? dimen4.value : '', disabled: !dimen4 }],
          dimen5: [{ value: (!!dimen5 && !!dimen5.value) ? dimen5.value : '', disabled: !dimen5 }],
          //currency: [transaction.currency, Validators.required],
          narration: [transaction.narration],
          amount: [Number(transaction.grossAmount), Validators.required],
          vatCode: [transaction.vatDetails.vatCode, Validators.required],
          vatPercentage: [Number(transaction.vatPercentage), Validators.required],
          grandtotalprice: [Number(transaction.amount), Validators.required],
          type: [transaction.type, Validators.required],
          invoiceNumber: [transaction.invoiceNumber, this.setMandInvNumFalse ? null : Validators.required],
          asset: ['']
        });

        if (transaction.type === "FAM") this.showAsset = true;

        if (!!dimen1 && !!dimen1.isValidationReq && dimen1.isValidationReq) newRow.get('dimen1').setValidators(Validators.required)
        if (!!dimen2 && !!dimen2.isValidationReq && dimen2.isValidationReq) newRow.get('dimen2').setValidators(Validators.required)
        if (!!dimen3 && !!dimen3.isValidationReq && dimen3.isValidationReq) newRow.get('dimen3').setValidators(Validators.required)
        if (!!dimen4 && !!dimen4.isValidationReq && dimen4.isValidationReq) newRow.get('dimen4').setValidators(Validators.required)
        if (!!dimen5 && !!dimen5.isValidationReq && dimen5.isValidationReq) newRow.get('dimen5').setValidators(Validators.required)

        this.transactionsArr.push(newRow);

        if (this.setMandInvNumFalse) {
          setTimeout(() => {
            this.transactionsArr.controls.forEach((e, i) => {
              this.transactionsArr.at(i).get('invoiceNumber').clearValidators()
              this.transactionsArr.at(i).get('invoiceNumber').enable()
            })
          }, 2)
        }
      }
      let date = (gl.financialEntryList[0].date)
      this.glFormGroup = this._formBuilder.group({
        date: [new Date(date), Validators.required],
        fiscalMonth: [{ value: Number(gl.financialEntryList[0].fiscalPeriod.period), disabled: false }, [Validators.required]],
        fiscalYear: [{ value: Number(gl.financialEntryList[0].fiscalPeriod.year), disabled: true }, [Validators.required]],
        taxMonth: [{ value: Number(gl.financialEntryList[0].taxPeriod.period), disabled: false }, [Validators.required]],
        taxYear: [{ value: Number(gl.financialEntryList[0].taxPeriod.year), disabled: true }, [Validators.required]],
        transaction: [gl.financialEntryList[0].transactionType.code, Validators.required],
        draftedGl: [draftedGl],
        description: [gl.financialEntryList[0].description],
        //narration: [gl.financialEntryList[0].narration],
        currency: [gl.financialEntryList[0].currency],
        transactionArray: this.transactionsArr
      });
      this.checkAmount();
    }
  }

  getLedgerDetails = i => {

    if (!this.utilityService.isUndefinedOrEmpty(this.transactionsArr.value[i].ledger)) {
      this.financeService.getGLData('ledger-account', this.transactionsArr.value[i].ledger).subscribe((res: any) => {

        if (!!res && !!res.dimensionStatus && res.dimensionStatus.length > 0) {
          res.dimensionStatus.map(dimension => {
            if (dimension.key === 'Item_Group') {
              this.setDimensions(i, dimension.value, 'dimen2')
            }
            if (dimension.key === 'Line_Of_Business') {
              this.setDimensions(i, dimension.value, 'dimen1')
            }
            if (dimension.key === 'Project') {
              this.setDimensions(i, dimension.value, 'dimen3')
            }
            if (dimension.key === 'Department') {
              this.setDimensions(i, dimension.value, 'dimen4')
            }
            if (dimension.key === 'Employee_Miscellaneous') {
              this.setDimensions(i, dimension.value, 'dimen5')
            }
          })
        }
      },
        err => {

        })
    }
  }

  setDimensions = (i, type, dimension) => {
    if (type === 'Mandatory') {
      this.transactionsArr.at(i).get([dimension]).setValidators([Validators.required]);
      this.transactionsArr.at(i).get([dimension]).enable()
    } else if (type === 'Optional') {
      this.transactionsArr.at(i).get([dimension]).enable()
    } else {
      this.transactionsArr.at(i).get([dimension]).setValue('');
      this.transactionsArr.at(i).get([dimension]).disable()
    }
  }

  updateRequiredField = (e, field, index) => {
    if (e.isUserInput) {
      if (field === "customer") {
        //this.transactionsArr.at(index).get("supplier").clearValidators();
        this.transactionsArr.at(index).get("supplier").setValue('');
        //this.transactionsArr.at(index).get("customer").setValidators([Validators.required]);
      }
      if (field === "supplier") {
        //this.transactionsArr.at(index).get("customer").clearValidators();
        this.transactionsArr.at(index).get("customer").setValue('');
        //this.transactionsArr.at(index).get("supplier").setValidators([Validators.required]);
      }
      //this.transactionsArr.at(index).get([field]).setValidators([Validators.required]);
    }
  }

  addTransaction = () => {

    let newRow = this._formBuilder.group({
      ledger: ['', Validators.required],
      customer: [''],
      supplier: [''],
      dimen1: [{ value: '', disabled: true }],
      dimen2: [{ value: '', disabled: true }],
      dimen3: [{ value: '', disabled: true }],
      dimen4: [{ value: '', disabled: true }],
      dimen5: [{ value: '', disabled: true }],
      //currency: ['', Validators.required],
      narration: [''],
      amount: ['', Validators.required],
      type: ['', Validators.required],
      vatCode: ['', Validators.required],
      vatPercentage: ['', Validators.required],
      grandtotalprice: ['', Validators.required],
      invoiceNumber: this.setMandInvNumFalse ? [''] : ['', Validators.required], //['', this.setMandInvNumFalse ? null : Validators.required],
      asset: ['']
    });

    this.transactionsArr.push(newRow);

    if (this.setMandInvNumFalse) {
      setTimeout(() => {
        this.transactionsArr.controls.forEach((e, i) => {
          this.transactionsArr.at(i).get('invoiceNumber').clearValidators()
          this.transactionsArr.at(i).get('invoiceNumber').enable()
        })
      }, 2);
    }

    if(this.isMSI) {
      this.transactionsArr.controls.forEach((e, i) => {
        this.transactionsArr.at(i).get('vatCode').setValue(this.transactionsArr.at(0).get('vatCode').value);
        this.transactionsArr.at(i).get('vatPercentage').setValue(this.transactionsArr.at(0).get('vatPercentage').value);
      })
    }

    if (this.showAsset) {
      this.transactionsArr.at(0).get('asset').setValidators([Validators.required]);
      this.transactionsArr.at(0).get('asset').enable()
    } else {
      this.transactionsArr.at(0).get('asset').clearValidators()
      this.transactionsArr.at(0).get('asset').enable()
    }
  }

  removeTransaction = i => {
    this.transactionsArr.removeAt(i);
    this.checkAmount()
  }

  onUnSelect = (i, key) => this.transactionsArr.at(i).patchValue({ [key]: '' })

  checkLimit = (e, key) => {

    if (key === 'fiscalMonth' || key === 'taxMonth') {
      this.glFormGroup.patchValue({
        [key]: Number(e.target.value) < 14 ? Number(e.target.value) : ''
      })
    }
    if (key === 'fiscalYear' || key === 'taxYear') {
      this.glFormGroup.patchValue({
        [key]: (e.target.value).length > 4 ? e.target.value.slice(0, -(e.target.value.length - 4)) : (e.target.value)
      })
    }
  }

  resetGLForm = () => {

    const { date, transaction, draftedGl, description, narration, currency } = this.glFormGroup.getRawValue()

    this.glFormGroup = this._formBuilder.group({
      date: [date, Validators.required],
      fiscalMonth: [{ value: '', disabled: true }, [Validators.required]],
      fiscalYear: [{ value: '', disabled: true }, [Validators.required]],
      taxMonth: [{ value: '', disabled: true }, [Validators.required]],
      taxYear: [{ value: '', disabled: true }, [Validators.required]],
      transaction: [transaction, Validators.required],
      draftedGl: [draftedGl],
      description: [description],
      //narration: [narration],
      currency: [currency, Validators.required],
      transactionArray: this.transactionsArr
    });
  }

  submitGL = action => {

    const { date, fiscalMonth, fiscalYear, taxMonth, taxYear, description, narration, draftedGl, currency } = this.glFormGroup.getRawValue()
    if (!!fiscalMonth && !!fiscalYear && !!taxMonth && !!taxYear) {
      const transactionType = this.glFormGroup.getRawValue().transaction;
      let items = []
      let financialEntries = {}

      this.transactionsArr.getRawValue().map((transaction, i) => {
        let dimensions = [
          !!transaction.dimen1 ? {
            key: 'Line_Of_Business',
            value: transaction.dimen1,
          } : null,
          !!transaction.dimen4 ? {
            key: 'Department',
            value: transaction.dimen4,
          } : null,
          !!transaction.dimen2 ? {
            key: 'Item_Group',
            value: transaction.dimen2,
          } : null,
          !!transaction.dimen5 ? {
            key: 'Employee_Miscellaneous',
            value: transaction.dimen5,
          } : null,
          !!transaction.dimen3 ? {
            key: 'Project',
            value: transaction.dimen3,
          } : null
        ]
        dimensions = dimensions.filter(item => {
          return item !== null
        })
        const request = {
          date: this.pipe.transform(date, 'MM/dd/yyyy'),
          currency: this.utilityService.splitCode(currency),
          amount: transaction.grandtotalprice,
          "grossAmount": transaction.amount,
          "vatPercentage": transaction.vatPercentage,
          "vatDetails": {
            "vatCode": this.utilityService.splitCode(transaction.vatCode)
          },
          "invoiceNumber": transaction.invoiceNumber,

          dimensions: dimensions,
          type: transaction.type,
          description: description,
          narration: transaction.narration,
          transactionType: {
            code: transactionType
          },
          ledgerAccount: {
            accountNumber: transaction.ledger
          },
          fiscalPeriod: {
            periodType: 'Fiscal',
            period: fiscalMonth,
            year: fiscalYear
          },
          taxPeriod: {
            periodType: 'Tax',
            period: taxMonth,
            year: taxYear
          },
          customer: {
            bpCode: transaction.customer
          },
          supplier: {
            bpCode: transaction.supplier
          }
        }
        if (this.matchedAsset[i]) {
          request["ledgerNumber"] = this.matchedAsset[i].code
          request["ledgerDescription"] = this.matchedAsset[i].description
        }
        items.push(request)
      });
      financialEntries = { financialEntries: items }

      if (!!draftedGl) financialEntries['generalLedgerCode'] = draftedGl.code;
      this.financeService.submitGL(financialEntries, action).subscribe((res: any) => {
        this.showMsg('success', action === 'save' ? `General Ledger ${res.generalLedgerCode} has been saved successfully` : `General Ledger ${res.generalLedgerCode} has been submitted successfully`)
        this.glForm.resetForm()
      },
        err => {
          if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
            this.showMsg('error', err.error.errors[0].message)
          } else this.showMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
        })
    }
  }

  checkAmount() {
    let transactions = this.transactionsArr.getRawValue();
    let summedValues = transactions.reduce((acc, each) => {
      if (each.type == "Credit") {
        return { ...acc, credit: acc.credit += Number(each["grandtotalprice"]) }
      } else {
        return { ...acc, debit: acc.debit += Number(each["grandtotalprice"]) }
      }
    }, {
        credit: 0,
        debit: 0
      });

    this.doNotShowSumbit = summedValues.credit !== summedValues.debit;
    this.isEnteredAmt = this.transactionsArr.value.every(e => e.type !== "");
  }

  showMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true,
      cancelButtonText:
        '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }

  onDownloadTemplate = e => {
    e.preventDefault();
    if (this.utilityService.isBrowser()) {
      window.open(environment.hostName + this.templateUrl, '_blank')
    }
  }

  onUploadFile = (event: any) => {
    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    const file = event.target.files[0];

    reader.onload = () => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: 'binary' });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        initial[name] = XLSX.utils.sheet_to_json(sheet);
        return initial;
      }, {});
      !!jsonData.Sheet1 ? this.addUploadedTransaction(jsonData.Sheet1) : '';
    };
    reader.readAsBinaryString(file);
  }
  resetGL = () => this.glForm.markAsUntouched();

  checkAssets(i, asset) {
    let matchedAsset = assetGroups.filter(e => e.description.toUpperCase() === asset.group.toUpperCase())
    if (matchedAsset.length > 0) {
      this.matchedAsset[i] = matchedAsset[0];
      this.ledgers.push({ accountNumber: matchedAsset[0].code, description: matchedAsset[0].description });
      this.transactionsArr.at(i).patchValue({
        ledger: String(matchedAsset[0].code)
      });
      this.getLedgerDetails(i);
    }
  }

  addUploadedTransaction = transactions => {

    while (this.transactionsArr.length !== 0) {
      this.transactionsArr.removeAt(0)
    }

    this.glFormGroup.patchValue({
      //currency: transactions[0].currency
    })

    for (let transaction of transactions) {
      let ledgerAcc;
      if (transaction.ledgerAccount) ledgerAcc = String(transaction.ledgerAccount);
      let invalidLedger = this.ledgers.findIndex(e => e.accountNumber == ledgerAcc);
      if (!transaction.ledgerAccount || invalidLedger == -1) this.showMsg('error', 'Invalid ledger Account present at row: ' + (transactions.indexOf(transaction) + 1) + '.')
      let newRow = this._formBuilder.group({
        ledger: [ledgerAcc, Validators.required],
        customer: [''],
        supplier: [''],
        dimen1: [{ value: transaction.dimension1, disabled: !transaction.dimension1 }],
        dimen2: [{ value: transaction.dimension2, disabled: !transaction.dimension2 }],
        dimen3: [{ value: transaction.dimension3, disabled: !transaction.dimension3 }],
        dimen4: [{ value: transaction.dimension4, disabled: !transaction.dimension4 }],
        dimen5: [{ value: transaction.dimension5, disabled: !transaction.dimension5 }],
        //currency: [transaction.currency, Validators.required],
        narration: [transaction.narration],
        amount: [transaction["Gross Amount"], Validators.required],
        vatCode: [transaction["VAT Code"], Validators.required],
        vatPercentage: [transaction["VAT %"], Validators.required],
        grandtotalprice: [transaction["Net amount"], Validators.required],
        type: [transaction.type, Validators.required],
        asset: [''],
        invoiceNumber: [transaction.invoiceNumber, this.setMandInvNumFalse ? null : Validators.required],
      });
      if (transaction.type === "FAM") this.showAsset = true;
      this.transactionsArr.push(newRow);
      this.getLedgerDetails(transactions.indexOf(transaction))

      if (this.setMandInvNumFalse) {
        setTimeout(() => {
          this.transactionsArr.controls.forEach((e, i) => {
            this.transactionsArr.at(i).get('invoiceNumber').clearValidators()
            this.transactionsArr.at(i).get('invoiceNumber').enable()
          })
        }, 2)
      }
    }
    this.checkAmount();
    if (this.doNotShowSumbit) {
      this.showMsg('error', 'Error - Unbalanced JV');
      this.transactionsArr.reset();
      while (this.transactionsArr.length !== 0) {
        this.transactionsArr.removeAt(0)
      }
    }
  }

  updateAmount = (i, percent?: any) => {

    let subTotal: any
    let grandTotal: any

    subTotal = this.transactionsArr.at(i).value.amount;

    let value = percent ? percent : this.transactionsArr.at(i).value.vatPercentage;
    grandTotal = (Number(subTotal) * (1 + value / 100))
    // grandTotal = subTotal * (1 + ((!!percent ? parseInt(percent) : Number(this.transactionsArr.at(i).value.vatPercentage)) / 100))

    this.transactionsArr.at(i).patchValue(
      {
        //subTotal: (subTotal).toFixed(2),
        grandtotalprice: grandTotal.toFixed(2),

      }
    );

    this.checkAmount()
  }

  updateVat = (i, vatPercentage) => {
    this.transactionsArr.at(i).patchValue({ vatPercentage: vatPercentage })
    this.updateAmount(i);
    if(this.isMSI) {
      this.transactionsArr.controls.forEach((e, i) => {
        this.transactionsArr.at(i).get('vatCode').setValue(this.transactionsArr.at(0).get('vatCode').value);
        this.transactionsArr.at(i).get('vatPercentage').setValue(this.transactionsArr.at(0).get('vatPercentage').value);
      })
    }
  }

}
