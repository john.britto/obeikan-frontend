import { Component, OnInit, ViewChild, Inject, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { API_FAILURE_MSG, FinanceService, TRANSACTION_TYPES } from '../../../../../services/finance.service'
import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { MatTableDataSource } from '@angular/material/table';
import { appConfig } from '../../../.././../app.config'
import { SelectionModel } from '@angular/cdk/collections';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { englishEnums } from '../../../../../app.enums';
import { CommonUtilityService } from '../../../../../services/common-utility-service'
import { CURRENCIES } from '../../../../../../assets/js/currencies';
import { PAYMENT_TERMS } from '../../../../../../assets/js/paymentTerms.js'
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { DeleteHRMSSalaryFormModalComponent } from '../../hrms/salary-element-entries-single-molecule/salary-element-entries-single-molecule.component';

@Component({
  selector: 'app-finance-form-molecule',
  templateUrl: './finance-form-molecule.component.html',
  styleUrls: ['./finance-form-molecule.component.scss']
})
export class FinanceFormMoleculeComponent implements OnInit {

  @Output() scrollToPosition: EventEmitter<string> = new EventEmitter();
  @ViewChild('poForm', { static: false }) poForm;
  @ViewChild('mrForm', { static: false }) mrForm;
  @ViewChild('mrTwoForm', { static: false }) mrTwoForm;
  @ViewChild('paymentForm', { static: false }) paymentForm;
  @ViewChild('stepper', { static: false }) private myStepper: MatStepper;
  pipe = new DatePipe('en-US');
  isLinear = false;
  POCreationFormGroup: FormGroup;
  poSummary: any = {
    vatPercentage: 0,
    discountAmount: 0,
    subTotal: 0,
    amount: 0,
    vatAmount: 0,
    totalAmount: 0,
    subTotalInSAR: 0,
    amountInSAR: 0
  }
  materialReceivingFormGroup: FormGroup;
  materialReceivingFormGroupTwo: FormGroup;
  paymentFormGroup: FormGroup;
  supliers: any
  supliers2: any
  wareHouses: any
  itemCodes: any
  currencies: any
  filteredCurrencies: Observable<string[]>
  currency = new FormControl();
  filteredSuppliers: Observable<string[]>
  filteredSuppliers2: Observable<string[]>
  supplier = new FormControl();
  filteredItemCodes: Observable<string[]>
  itemCode = new FormControl();
  paymentTerms: any = PAYMENT_TERMS
  draftedOrders: any
  poNumbers: any
  reviewExpanded: any = true
  isOptional: any = false
  reviewDate: any
  typesOfTransactions: any
  poTypeAtPayment: any;
  availableBanks: any = appConfig.DropdownOptions.bankLists
  ELEMENT_DATA: PeriodicElement[] = []
  dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
  displayedColumns: string[];
  displayedColumnsReview: string[];
  stepDisabled: boolean = false;
  vatPercentages: any = appConfig.DropdownOptions.vatPercentages
  selection = new SelectionModel<PeriodicElement>(true, []);
  mrQty: any;
  valideQtyLimit: any;
  enums: any;
  defaultValuePO: any;
  filteredItems: any
  assetCodes: any;
  warehouse = new FormControl();
  vatCodes: any = []
  currenciesRate: any = [];
  observers: any[] = [];
  mrCodes: any[] = []
  invoiceBookings: any = []
  invoiceTypes: any = []
  supplier2 = new FormControl();
  poCodes: any = [];
  invoiceMrList: any;
  showPoNum: boolean = false;

  constructor(
    private _formBuilder: FormBuilder,
    private financeService: FinanceService,
    public dialog: MatDialog,
    private matSnack: MatSnackBar,
    private utilityService: CommonUtilityService) {
    this.currencies = CURRENCIES
    this.typesOfTransactions = TRANSACTION_TYPES
  }

  get supply() { return this.POCreationFormGroup.controls; }
  get itemsArr() { return this.supply.itemsArray as FormArray; }

  get po() { return this.paymentFormGroup.controls; }
  get amountArr() { return this.po.poArray as FormArray; }

  get mr() { return this.materialReceivingFormGroup.controls; }
  get fixedAssetArr() { return this.mr.fixedAssetArray as FormArray; }

  ngOnInit() {
    this.getConversionRate();
    this.displayedColumns = ['select', 'S no', 'Item Code', 'Description', 'Quantity', 'Unit']
    this.displayedColumnsReview = ['S no', 'Item Code', 'Description', 'Quantity', 'Unit']
    this.financeService.getSupplierDetails('customermasterdatasetup').subscribe((res: any) => {
      if (res && res.supplierInfo && res.itemInfo) {
        this.supliers = res.supplierInfo;
        this.supliers2 = res.supplierInfo;
        this.wareHouses = res.warehouse;
        this.assetCodes = res.asset;
        this.vatCodes = res.financeVAT
        this.scrollToPosition.emit();
        this.filteredSuppliers = this.supplier.valueChanges.pipe(startWith(''), map(value => this._filterSupplier(value)));
        this.filteredSuppliers2 = this.supplier2.valueChanges.pipe(startWith(''), map(value => this._filterSupplier2(value)));

      }
    },
      err => {
        this.scrollToPosition.emit();
      });

    this.financeService.getPurchaseOrder().subscribe((res: any) => {
      if (res && res.purchaseOrderList && res.purchaseOrderList.length > 0) {
        this.poNumbers = res.purchaseOrderList;
        this.defaultValuePO = res.purchaseOrderList.map(e => ({ purchaseOrder: e.purchaseOrder, item: e.item }));
      }
      this.scrollToPosition.emit();
    });

    this.financeService.getPurchaseOrder('CREATED').subscribe((res: any) => {
      if (res && res.purchaseOrderList && res.purchaseOrderList.length > 0) {
        this.draftedOrders = res.purchaseOrderList;
      }
      this.scrollToPosition.emit();
    });

    // this.financeService.getFinanceData('material-received').subscribe((res: any) => {
    //   if(!!res && !!res.materialReceived) this.mrCodes = res.materialReceived
    // },
    // err => {
      
    // })

    let initialRow = this._formBuilder.group({
      quantity: ['', Validators.required],
      availableQuantity: [{ value: '', disabled: true }],
      itemDescription: [{ value: '', disabled: true }, Validators.required],
      price: [0, Validators.required],
      priceInSAR: [0],
      unit: [{ value: '', disabled: true }, [Validators.required]],
      totalprice: [{ value: '', disabled: true }, Validators.required],
      totalpriceInSAR: [''],
      vatCode: ['', Validators.required],
      vatPercentage: ['5', Validators.required],
      discountPercentage: ['', Validators.required],
      subTotal: [{ value: '', disabled: true }, [Validators.required]],
      subTotalInSAR: [''],
      itemCode: ['', Validators.required]
    })

    this.POCreationFormGroup = this._formBuilder.group({
      supplier: ['', Validators.required],
      draftedOrder: [''],
      warehouse: ['', Validators.required],
      currency: ['', Validators.required],
      amount: [{ value: '', disabled: true }, Validators.required],
      itemsArray: new FormArray([initialRow]),
      paymentTerm: ['', Validators.required],
      estimatedDeliveryDate: [new Date(), Validators.required],
      creationDate: [new Date(), Validators.required],
      filterCurrency: [''],
      notes: [''],
    });

    this.materialReceivingFormGroup = this._formBuilder.group({
      poNumber: ['', Validators.required],
      supplier: [{ value: '', disabled: true }, Validators.required],
      warehouse: [{ value: '', disabled: true }, Validators.required],
      currency: [{ value: '', disabled: true }, Validators.required],
      amount: [{ value: '', disabled: true }, Validators.required],
      packingSlipNumber: ['', Validators.required],
      //vatAmount: [''],

      fixedAssetArray: new FormArray([])
    });

    this.materialReceivingFormGroupTwo = this._formBuilder.group({
      supplier: ['', Validators.required],
      referenceOrder: ['', Validators.required],
      vatAmount: [{ value: '', disabled: true }, Validators.required],
      invoiceNumber: ['', Validators.required],
      setAmount: [''],
      invoiceAmount: ['', Validators.required],
      invoiceDate: ['', Validators.required],
      invoiceCurrency: [{ value: '', disabled: true }, Validators.required],
      notes: [''],
      pOrder: ['', Validators.required],
    });

    this.paymentFormGroup = this._formBuilder.group({
      transactionEntryDate: ['', Validators.required],
      bankName: ['', Validators.required],
      documentDate: ['', Validators.required],
      typeOfTransaction: ['', Validators.required],
      supplier: ['', Validators.required],
      invoiceBookings: [[], Validators.required],
      currency: ['', Validators.required],
      // grandtotalprice: [{ value: '', disabled: true }, Validators.required],
      grandtotalprice: ['', Validators.required],
      documentNumber: ['', Validators.required],
      poArray: new FormArray([]),
      vatCode: [''],
      vatPercentage: [''],
      grossamt: ['', Validators.required],
      notes: [''],
      poNumber: ['']
    });

    this.financeService.getFinanceData('approved-invoices?orderType=ACCOUNTS_PAYABLE').subscribe((res: any) => {
      if (!!res && !!res.invoices && res.invoices.length > 0) {
        this.invoiceTypes = res.invoices
      }
    },
      err => {
      });

    this.filteredCurrencies = this.currency.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filterCurrency(value))
      );

    this.getItemDetails(null)
  }

  getConversionRate() {
    this.financeService.getSupplierDetails('currencies').subscribe((res: any) => {
      if (res && res.currencies) {
        this.currenciesRate = res.currencies;
      }
    },
      err => {
      });
  }

  displayItemId = itemId => {
    return !!itemId ? itemId : undefined
  }

  ManageItemCodeControl(index: number) {

    var arrayControl = this.POCreationFormGroup.get('itemsArray') as FormArray;

    this.filteredItemCodes = arrayControl.at(index).get('itemCode').valueChanges
      .pipe(startWith(''), map(value => this._filterItemCode(value)));

    this.observers.push(this.filteredItemCodes);
  }

  deleteDialog(i, row) {
    const dialogRef = this.dialog.open(DeleteHRMSSalaryFormModalComponent, {
      data: { index: i }
    });

    dialogRef.afterClosed().subscribe(result => {
      result && this.removeItem(i, row);
    });
  }

  checkPrice = (i) => {
    if (this.itemsArr.at(i).value.price != null && this.itemsArr.at(i).value.price <= 0) {
      this.itemsArr.at(i).patchValue({ price: 1 });
    }
  }

  updateAmount = (i, percent?: any) => {

    let subTotal: any
    let grandTotal: any
    let subTotalInSAR: any
    let grandTotalInSAR: any

    if (this.itemsArr.at(i).value.quantity != null && this.itemsArr.at(i).value.quantity < 1) {
      this.itemsArr.at(i).patchValue({ quantity: 1 });
    }

    const currency = this.POCreationFormGroup.getRawValue().currency
    let convRate = this.currenciesRate.filter(e => e.isocode === this.utilityService.splitCode(this.POCreationFormGroup.value.currency))[0];
    this.itemsArr.at(i).value.priceInSAR = currency === 'SAR-Saudi riyal' ? parseFloat(this.itemsArr.at(i).value.price).toFixed(2) : (this.itemsArr.at(i).value.price / convRate.conversion).toFixed(2)
    
    if (!this.utilityService.isNullOrUndefined(this.itemsArr.at(i).value.quantity) &&
      !this.utilityService.isUndefinedOrEmpty(this.itemsArr.at(i).value.quantity) &&
      !this.utilityService.isNullOrUndefined(this.itemsArr.at(i).value.price) &&
      !this.utilityService.isUndefinedOrEmpty(this.itemsArr.at(i).value.price) &&
      !this.utilityService.isUndefinedOrEmpty(this.itemsArr.at(i).value.discountPercentage) &&
      !this.utilityService.isNullOrUndefined(this.itemsArr.at(i).value.discountPercentage)) {
      subTotal = (this.itemsArr.at(i).value.quantity * this.itemsArr.at(i).value.price) * (1 - (this.itemsArr.at(i).value.discountPercentage / 100))
      grandTotal = subTotal * (1 + ((!!percent ? parseInt(percent) : Number(this.itemsArr.at(i).value.vatPercentage)) / 100))
      // subTotalInSAR = (this.itemsArr.at(i).value.quantity * this.itemsArr.at(i).value.priceInSAR) * (1 - (this.itemsArr.at(i).value.discountPercentage / 100))
      subTotalInSAR = subTotal * convRate.conversion
      grandTotalInSAR = grandTotal * convRate.conversion

      this.itemsArr.at(i).patchValue(
        {
          subTotal: (subTotal).toFixed(2),
          totalprice: grandTotal.toFixed(2),
          subTotalInSAR: (subTotalInSAR).toFixed(2),
          totalpriceInSAR: grandTotalInSAR.toFixed(2),

        }
      );
    }

    this.summaryCalculation()

  }

  setCurrency = (e, supplier) => {
    if (e.isUserInput) {
      this.POCreationFormGroup.get('currency').setValue(supplier.currency)
      this.POCreationFormGroup.get('paymentTerm').setValue(supplier.paymentTerms)
    }
  }

  calculateAmount = items => {
    let summary = {
      vatPercentage: 0,
      discountPercentage: 0,
      subTotal: 0,
      amount: 0,
      disPercent: 0,
      qty: 0,
      price: 0,
      vatPercent: 0,
      vatAmount: 0
    }
    return items.reduce((acc, e: any) => {
      let qty = acc.qty += parseInt(e.quantity)
      let total = (acc.price += parseFloat(e.price))
      let disPercentage = acc.disPercent += parseFloat(e.discountPercentage)
      let subTotal = acc.subTotal += parseFloat(e.subTotal)
      let vatPercentage = acc.vatPercent += parseFloat(e.vatPercentage)
      let vatAmount = acc.vatAmount += (parseFloat(parseFloat(e.subTotal).toFixed(2)) * (parseFloat(e.vatPercentage) / 100))
      return {
        disPercent: acc.discountPercentage += (e.discountPercentage),
        vatPercent: vatPercentage,
        vatPercentage: vatPercentage,
        discountPercentage: disPercentage,
        qty: qty,
        price: (acc.price) += parseFloat(e.price),
        subTotal: parseFloat(subTotal),
        vatAmount: parseFloat(vatAmount),
        discountAmount: (qty * total * disPercentage) / 100,
        amount: acc.amount += (Number(e.remainingQuantity) * Number(e.price))
      }
    }, summary);
  }

  summaryCalculation = () => {
    let summary = {
      vatPercentage: 0,
      discountPercentage: 0,
      subTotal: 0,
      subTotalInSAR: 0,
      amount: 0,
      amountInSAR: 0,
      disPercent: 0,
      qty: 0,
      price: 0,
      vatPercent: 0,
      vatAmount: 0,
      discountAmount: 0,
      totalAmount: 0
    }
    let rawData = this.POCreationFormGroup.getRawValue();
    this.poSummary = rawData.itemsArray.reduce((acc, e: any) => {
      let qty = acc.qty += parseInt(e.quantity)
      let price = (acc.price += parseFloat(e.price))
      let disPercentage = acc.disPercent += parseInt(e.discountPercentage)
      let subTotal = acc.subTotal += parseFloat(e.subTotal)
      let subTotalInSAR = acc.subTotalInSAR += parseFloat(e.subTotalInSAR)
      let vatPercentage = acc.vatPercent += parseInt(e.vatPercentage)
      let vatAmount = acc.vatAmount += (parseFloat(e.subTotal)) * ((parseFloat(e.vatPercentage) / 100))
      let discountAmount = acc.discountAmount += ((e.quantity * e.price) * (parseFloat(e.discountPercentage) / 100))
      let totalAmount = acc.totalAmount += (e.quantity * e.price)
      return {
        disPercent: acc.discountPercentage += (e.discountPercentage),
        vatPercent: vatPercentage,
        vatPercentage: vatPercentage,
        discountPercentage: disPercentage,
        qty: qty,
        price: price,
        subTotal: parseFloat(subTotal),
        subTotalInSAR: parseFloat(subTotalInSAR),
        vatAmount: parseFloat(vatAmount),
        discountAmount: parseFloat(discountAmount),
        amount: acc.amount += Number(e.totalprice),
        amountInSAR: acc.amountInSAR += Number(e.totalpriceInSAR),
        totalAmount: totalAmount

      }
    }, summary);
  }

  getItemDetails = warehouseCode => {

    // let uri = `stocklevels?warehouse=${warehouseCode}&itemCode=&itemGroup=&date`
    let uri = `stocklevels`;
    this.financeService.getInventoryData(uri).subscribe((res: any) => {
      if (!!res && !!res.stockLevels) {
        this.itemCodes = res.stockLevels
        const controls = <FormArray>this.POCreationFormGroup.controls['itemsArray'];
        this.ManageItemCodeControl(controls.length - 1);
      }
    },
      err => {

      })
  }
  addItem = () => {
    let newRow = this._formBuilder.group({
      quantity: ['', Validators.required],
      availableQuantity: [{ value: '', disabled: true }],
      itemDescription: [{ value: '', disabled: true }, Validators.required],
      price: [0, Validators.required],
      priceInSAR: [0],
      unit: [{ value: '', disabled: true }, [Validators.required]],
      totalprice: [{ value: '', disabled: true }, Validators.required],
      vatPercentage: ['', Validators.required],
      vatCode: ['', Validators.required],
      discountPercentage: ['', Validators.required],
      subTotal: [{ value: '', disabled: true }, [Validators.required]],
      subTotalInSAR: [''],
      totalpriceInSAR: [''],
      itemCode: ['', Validators.required],
    });

    this.itemsArr.push(newRow);

    if (!!this.itemCodes) {
      const controls = <FormArray>this.POCreationFormGroup.controls['itemsArray'];
      this.ManageItemCodeControl(controls.length - 1);
    }
  }

  removeItem = (i, item) => {
    this.itemsArr.removeAt(i);
    this.observers.splice(i, 1)
    this.summaryCalculation();
  }

  updateItemDetails = (event, index, itemData) => {
    let selectedItemInfo = this.itemCodes.filter(e => e.itemCode === itemData.itemCode)[0];
    let convRate = this.currenciesRate.filter(e => e.isocode === this.utilityService.splitCode(this.POCreationFormGroup.value.currency))[0];

    this.itemsArr.at(index).patchValue({
      quantity: '',
      itemDescription: selectedItemInfo.item.description,
      availableQuantity: selectedItemInfo.availableQuantity,
      price: this.POCreationFormGroup.value.currency === '' || this.POCreationFormGroup.value.currency.includes('SAR') ?
        (selectedItemInfo.item.purchasePrice).toFixed(2) : (selectedItemInfo.item.purchasePrice / convRate.conversion).toFixed(2),
      priceInSAR: selectedItemInfo.item.purchasePrice.toFixed(2),
      unit: selectedItemInfo.item.inventoryUnit,
      totalprice: '',
      vatPercentage: '5',
      discountPercentage: '',
      subTotal: '',
      subTotalInSAR: '',
      totalpriceInSAR: ''
    });
  }

  updateVat = (i, vatPercentage) => {
    this.itemsArr.at(i).patchValue({ vatPercentage: vatPercentage })
    this.updateAmount(i)
  }

  updateDraftedOrder = order => {
    this.POCreationFormGroup.patchValue({
      supplier: order.supplier,
      warehouse: order.warehouse.split(' ')[0],
      currency: order.currency,
      paymentTerm: order.paymentTerms,
      estimatedDeliveryDate: new Date(order.estimatedDeliveryDate),
      creationDate: new Date(order.creationDate),
      notes: order.notes
    });

    this.getItemDetails(order.warehouse.split(' ')[0])

    while (this.itemsArr.length !== 0) {
      this.itemsArr.removeAt(0)
    }

    let convRate = this.currenciesRate.filter(e => e.isocode === this.utilityService.splitCode('SAR-Saudi riyal'))[0];

    if (!!order && !!order.item && order.item.length > 0) {
      order.item.map(item => {
        this.itemsArr.push(this._formBuilder.group({
          quantity: [item.quantity, Validators.required],
          availableQuantity: [{ value: item.availableQuantity, disabled: true }],
          itemDescription: [{ value: item.itemDescription, disabled: true }, Validators.required],
          price: [parseFloat(item.price).toFixed(2), Validators.required],
          priceInSAR: [
            order.currency === 'SAR-Saudi riyal' ? parseFloat(item.price).toFixed(2) : (item.price / convRate.conversion).toFixed(2),
            Validators.required], unit: [{ value: item.unit, disabled: true }, [Validators.required]],
          totalprice: [{ value: Number(item.itemTotal).toFixed(2), disabled: true }, Validators.required],
          totalpriceInSAR: [{ value: Number(item.itemTotal).toFixed(2), disabled: true }, Validators.required],
          vatPercentage: [parseFloat(item.financeVAT.vatPercentage), Validators.required],
          vatCode: [item.financeVAT.vatCode, Validators.required],
          discountPercentage: [parseFloat(item.discountPercentage), Validators.required],
          subTotal: [{ value: parseFloat(item.subTotal).toFixed(2), disabled: true }, [Validators.required]],
          subTotalInSAR: [{ value: parseFloat(item.subTotal).toFixed(2), disabled: true }, [Validators.required]],
          itemCode: [item.itemCode, Validators.required]
        }));
      })
    }
    this.summaryCalculation()
  }

  updateFixedAsset = (item: any) => {
    const fixedAsset = this.fixedAssetArr.getRawValue();
    if (Array.isArray(item) && item.length > 0) {
      item.map(each => {
        const duplicateAsset = fixedAsset.filter(e => e.itemCode == each.itemCode);
        if (duplicateAsset.length > 0) return;
        if (each.productType.toLowerCase() === 'cost') {
          this.fixedAssetArr.push(this._formBuilder.group({
            itemCode: [{ value: each.itemCode, disabled: true }, Validators.required],
            assetCode: [null, Validators.required]
          }));
        }
      })
    }
  }

  updatePODetails = (data) => {
    this.ELEMENT_DATA = [];
    this.selection.clear();
    let amount = this.calculateAmount(data.item)
    this.ELEMENT_DATA = data.item.filter(item => Number(item.quantity) > 0);
    this.dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
    this.materialReceivingFormGroup.patchValue({
      supplier: data.supplier,
      currency: data.currency,
      warehouse: data.warehouse,
      amount: amount,
      packingSlipNumber: data.packingSlipNumber
    });

    this.materialReceivingFormGroupTwo.patchValue({ invoiceCurrency: data.currency })

    if (!!data && !!data.item) {
      data.item.map((item, i) => {
        this.updateMrQty(item.remainingQuantity, item, i)
      })
    }

    this.masterToggle();

    this.updateFixedAsset(data.item);
    this.materialReceivingFormGroup.get('amount').setValue(amount.amount.toFixed(2))
  }

  updateInvoiceDetails = (event, mr) => {
    if (event.isUserInput) {
      if (event.source.selected) {
        mr.item[0]['remainingQuantity'] = mr.item[0].quantity
        let amount = this.calculateAmount(mr.item);
        this.materialReceivingFormGroupTwo.get('setAmount').setValue(Number(this.materialReceivingFormGroupTwo.get('setAmount').value) + Number(mr.invoiceAmount))
        this.materialReceivingFormGroupTwo.get('invoiceAmount').setValue(Number(this.materialReceivingFormGroupTwo.get('invoiceAmount').value) + Number(mr.invoiceAmount))
        this.materialReceivingFormGroupTwo.get('vatAmount').setValue((Number(this.materialReceivingFormGroupTwo.get('vatAmount').value) + Number(amount.vatAmount)).toFixed(2))
        this.materialReceivingFormGroupTwo.get('invoiceCurrency').setValue(mr.currency)
      } else {
        mr.item[0]['remainingQuantity'] = mr.item[0].quantity
        let amount = this.calculateAmount(mr.item);
        this.materialReceivingFormGroupTwo.get('setAmount').setValue(Number(this.materialReceivingFormGroupTwo.get('setAmount').value) - Number(mr.invoiceAmount))
        this.materialReceivingFormGroupTwo.get('invoiceAmount').setValue(Number(this.materialReceivingFormGroupTwo.get('invoiceAmount').value) - Number(mr.invoiceAmount))
        this.materialReceivingFormGroupTwo.get('vatAmount').setValue((Number(this.materialReceivingFormGroupTwo.get('vatAmount').value) - Number(amount.vatAmount)).toFixed(2))
        this.materialReceivingFormGroupTwo.get('invoiceCurrency').setValue(mr.currency)
      }
    }


  }

  submitInvoice = action => {
    const submitData = this.POCreationFormGroup.getRawValue();

    const { supplier, itemsArray, currency, warehouse, draftedOrder, paymentTerm, estimatedDeliveryDate, notes, creationDate } = submitData;

    const itemList: any = [];

    for (let k = 0; k < itemsArray.length; k++) {
      itemList.push({
        itemCode: itemsArray[k].itemCode,
        quantity: itemsArray[k].quantity,
        itemTotal: itemsArray[k].totalprice,
        price: itemsArray[k].price,
        itemDescription: itemsArray[k].itemDescription,
        unit: itemsArray[k].unit,
        financeVAT: {
          vatPercentage: parseFloat(itemsArray[k].vatPercentage),
          vatCode: itemsArray[k].vatCode
        },
        discountPercentage: itemsArray[k].discountPercentage,
        subTotal: itemsArray[k].subTotal
      })
    }

    const request = {
      purchaseOrderData: {
        supplier: this.utilityService.splitCode(supplier),
        warehouse: warehouse,
        currency: this.utilityService.splitCode(currency),
        paymentTerms: paymentTerm,
        estimatedDeliveryDate: this.pipe.transform(estimatedDeliveryDate, 'MM/dd/yyyy'),
        creationDate: this.pipe.transform(creationDate, 'MM/dd/yyyy'),
        poTotal: this.poSummary.amount,
        item: itemList,
        notes,
        totalAmountInSAR: this.poSummary.subTotalInSAR,
        totalTaxAmountInSAR: this.poSummary.amountInSAR
      }
    };

    if (!!draftedOrder) request.purchaseOrderData['purchaseOrder'] = draftedOrder
    console.log(request)
    this.financeService.submitPurchaseOrder(request, action).subscribe((res: any) => {
      this.poForm.resetForm()
      this.showSuccessMsg(action === 'save' ? `Purchase Order ${res.purchaseOrder} saved successfully` : `Purchase Order ${res.purchaseOrder} submitted successfully`);
      this.POCreationFormGroup.reset();
      while (this.itemsArr.length !== 0) {
        this.itemsArr.removeAt(0)
      }
    }, err => {
      if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.showErrorMsg(err.error.errors[0].message)
      } else this.showErrorMsg(API_FAILURE_MSG)
    });
  }

  submitMSInvoice = () => {
    const { invoiceAmount, invoiceDate, referenceOrder, invoiceCurrency, notes, invoiceNumber } = this.materialReceivingFormGroupTwo.getRawValue()
    const invoice = this.pipe.transform(invoiceDate, 'MM/dd/yyyy');

    const mrList = referenceOrder.map(e => {
      return {code: e};
    })

    const request = {
        materialRecivedList: mrList,
        referenceOrder: "",
        invoiceAmount,
        invoiceDate: invoice,
        currency: invoiceCurrency.split('-')[0],
        invoiceNumber,
        notes,
        orderType: 'ACCOUNTS_PAYABLE'
    };

    this.financeService.submitInvoice('invoice-booking', request).subscribe((res: any) => {
      this.mrTwoForm.resetForm()
      this.showSuccessMsg(`${res.invoiceNumber} has been submitted successfully`);
    },
      err => {
        if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.showErrorMsg(err.error.errors[0].message)
        } else this.showErrorMsg(API_FAILURE_MSG)
      })
  }

  submitMSForm = () => {

    const { poNumber, fixedAssetArray, currency, warehouse, packingSlipNumber, amount } = this.materialReceivingFormGroup.getRawValue()

    this.selection.selected.map(item => item.quantity = item.remainingQuantity);

    this.filteredItems = this.selection.selected.filter(item => item.remainingQuantity !== '0')
    if (this.filteredItems.length == 0) {
      this.showErrorMsg('Selected item has no remaining quantity');
      return;
    }

    let result = this.filteredItems.map((e) => {
      if (fixedAssetArray.findIndex(k => k.itemCode === e.itemCode) > -1) {
        return {
          ...e,
          assetCode: fixedAssetArray[fixedAssetArray.findIndex(k => k.itemCode === e.itemCode)].assetCode
        }
      } else {
        return { ...e, assetCode: '' }
      }
    });

    const request = {
      materialReceivedData: {
        purchaseOrder: poNumber,
        warehouse: warehouse,
        packingSlipNumber,
        currency: this.utilityService.splitCode(currency),
        item: result,
        invoiceAmount: amount
      }
    }; console.log(request)
    this.financeService.submitMaterialReceived(request).subscribe((res: any) => {
      this.mrForm.resetForm()
      this.showSuccessMsg(`${res.code} has been submitted successfully`);
      this.financeService.getFinanceData('approved-invoices?orderType=ACCOUNTS_PAYABLE').subscribe((res: any) => {
        if (res && res.invoices.length > 0) {
          this.invoiceTypes = res.invoices
        }
      },
        err => {
          if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
            this.showErrorMsg(err.error.errors[0].message)
          } else this.showErrorMsg(API_FAILURE_MSG)
        })
    },
      err => {
        if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.showErrorMsg(err.error.errors[0].message)
        } else this.showErrorMsg(API_FAILURE_MSG)
      })
  }

  setEnums(property) {
    const values = [];
    this.enums[property].map(each => {
      englishEnums[property].map(obj => {
        if (each.code === obj.code) {
          values.push(obj);
        }
      });
    });
    return values;
  }

  removeRowPay(i, po) {
    this.amountArr.removeAt(i);
    let rawData = this.paymentFormGroup.getRawValue();
    let index = rawData.poArray.findIndex((e: any) => e.invoiceNo === po.invoiceNumber);
    if(index > -1) {
      this.removeRowPay(index, po)
    }
  }

  updateItemsPayAmount = (event, po) => {
    if (event.isUserInput) {
      if (event.source.selected) {
        po.materialRecivedList.map(e => {
          this.amountArr.push(this._formBuilder.group({
            invoiceNo: [{ value: po.invoiceNumber, disabled: true }, Validators.required],
            totalprice: [{ value:Number(e.invoiceAmount).toFixed(2), disabled: true }, Validators.required],
            selectedPoNumber: [{ value: e.purchaseOrder, disabled: true }, Validators.required],
            materialReceived: [{ value: e.code, disabled: true }, Validators.required],
          }));
        })
        this.paymentFormGroup.patchValue({ currency: this._filterCurrency(po.currency)[0] })
      } else { 
        let rawData = this.paymentFormGroup.getRawValue();
        let index = rawData.poArray.findIndex((e: any) => e.invoiceNo === po.invoiceNumber);
        this.removeRowPay(index, po);
      }

      let rawData = this.paymentFormGroup.getRawValue();
      let sumAmnt = rawData.poArray.reduce((acc, e: any) => {
        return acc += Number(e.totalprice);
      }, 0);
      this.paymentFormGroup.get('grossamt').setValue(parseFloat(sumAmnt));

      if(this.paymentFormGroup.get('vatPercentage').value && this.paymentFormGroup.get('vatPercentage').value !== '') {
        let val = this.paymentFormGroup.get('vatPercentage').value;
        let subTotal = this.paymentFormGroup.get('grossamt').value;
        let sumAmnt: any = (Number(subTotal) * (1 + val / 100)).toFixed(2);
        this.paymentFormGroup.get('grandtotalprice').setValue(parseFloat(sumAmnt));
      } else {
        this.paymentFormGroup.get('grandtotalprice').setValue(parseFloat(sumAmnt));
      }
    }
  }

  updateTotalPayAmount = (i) => {
    let rawData = this.paymentFormGroup.getRawValue();
    let sumAmnt = rawData.poArray.reduce((acc, e: any) => {
      return acc += Number(e.totalprice);
    }, 0);
    this.paymentFormGroup.get('grossamt').setValue(parseFloat(sumAmnt));

    if(this.paymentFormGroup.get('vatPercentage').value && this.paymentFormGroup.get('vatPercentage').value !== '') {
      let val = this.paymentFormGroup.get('vatPercentage').value;
      let subTotal = this.paymentFormGroup.get('grossamt').value;
      let sumAmnt: any = (Number(subTotal) * (1 + val / 100)).toFixed(2);
      this.paymentFormGroup.get('grandtotalprice').setValue(parseFloat(sumAmnt));
    }
  }

  updateMrQty = (e, data, indexNum) => {
    const index = indexNum//this.ELEMENT_DATA.findIndex(each => each.itemCode === data.itemCode);
    let subTotal = (e * Number(this.ELEMENT_DATA[index].price)) * (1 - (Number(this.ELEMENT_DATA[index].discountPercentage) / 100))
    let itemTotal = subTotal * (1 + ((Number(this.ELEMENT_DATA[index].vatPercentage)) / 100))
    //let subTotal = (e * Number(this.ELEMENT_DATA[index].price)) - ((e * Number(this.ELEMENT_DATA[index].price)) * Number(this.ELEMENT_DATA[index].discountPercentage) / 100);
    //let itemTotal = (subTotal + (subTotal * ((Number(this.ELEMENT_DATA[index].vatPercentage)) / 100)));
    this.ELEMENT_DATA[index].remainingQuantity = String(e);
    this.ELEMENT_DATA[index].itemTotal = String(itemTotal);
    this.ELEMENT_DATA[index].subTotal = String(subTotal);
    this.dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
    for (let k = 0; k < this.dataSource.data.length; k++) {
      if (this.dataSource.data[k].itemCode == data.itemCode && this.checkboxLabel(data).indexOf('deselect row') > -1) {
        this.selection.select(this.dataSource.data[k]);
      }
    }

    setTimeout(() => {
      let calculatedAmount = this.calculateAmount(this.selection.selected);
      let vat = Number(calculatedAmount.vatPercentage) / this.selection.selected.length;
      let consolidatedAmt = Number(calculatedAmount.subTotal) * (1 + (vat / 100));
      //this.materialReceivingFormGroup.get('amount').setValue(calculatedAmount.amount.toFixed(2));
      this.materialReceivingFormGroup.get('amount').setValue(consolidatedAmt.toFixed(2));
    }, 2)

  }

  updateQty = (ele, i: any) => {
    this.mrQty = ele.remainingQuantity;

    let defaultQty;

    if (this.defaultValuePO[this.materialReceivingFormGroup.get('poNumber').value] && this.defaultValuePO[this.materialReceivingFormGroup.get('poNumber').value][ele.itemCode]) {
      defaultQty = this.defaultValuePO[this.materialReceivingFormGroup.get('poNumber').value][ele.itemCode];
    } else {
      this.defaultValuePO = {
        [this.materialReceivingFormGroup.get('poNumber').value]: {
          [ele.itemCode]: this.mrQty
        }
      }
      defaultQty = this.mrQty
    }

    const dialogRef = this.dialog.open(UpdateQtyFormModalComponent, {
      data: { qty: this.mrQty, code: ele.itemCode, valideQtyLimit: defaultQty }
    });

    dialogRef.afterClosed().subscribe(result => {
      result && this.updateMrQty(result, ele, i);
    });
  }

  updateAmountMR(event, row) {
    if (event) {
      this.selection.toggle(row);

      let calculatedAmount = this.calculateAmount(this.selection.selected)

      this.materialReceivingFormGroup.get('amount').setValue(calculatedAmount.amount.toFixed(2));
      if (event.checked && row.productType.toLowerCase() === 'cost') {
        this.fixedAssetArr.push(this._formBuilder.group({
          assetCode: [null, Validators.required],
          itemCode: [{ value: row.itemCode, disabled: true }, Validators.required],
        }))
      } else {
        const { fixedAssetArray } = this.materialReceivingFormGroup.getRawValue();
        fixedAssetArray.map((each: any, i: number) => {
          if (each.itemCode === row.itemCode) {
            this.fixedAssetArr.removeAt(i);
          }
        })
      }
    } else {
      return;
    }
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.itemCode + 1}`;
  }

  updatePo = supplier => {

    this.invoiceBookings = this.invoiceTypes.filter(po => po.company.bpCode === supplier.bpCode)
    // this.invoiceBookings =  this.invoiceTypes
    this.paymentFormGroup.patchValue({ bankName: supplier.bank, currency: supplier.currency })
  }

  submitPayment = () => {

    const { supplier, documentDate, typeOfTransaction, transactionEntryDate, currency, bankName, documentNumber, invoiceBookings,
     grossamt, notes, vatPercentage, vatCode, poNumber } = this.paymentFormGroup.value;
    const { poArray, grandtotalprice } = this.paymentFormGroup.getRawValue();
    const transDate = this.pipe.transform(transactionEntryDate, 'MM/dd/yyyy');
    const docDate = this.pipe.transform(documentDate, 'MM/dd/yyyy');

    const request = {
      PaymentsData: {
        company: supplier,
        invoiceBookings,
        transactionEntryDate: transDate,
        documentDate: docDate,
        documentNumber,
        transactionType: typeOfTransaction,
        //amount: grandtotalprice,
        currency: this.utilityService.splitCode(currency),
        bank: bankName,
        "grossAmount" : grossamt,
        "netAmount" : grandtotalprice,
        "comment" : notes,
        "vatPercentage": vatPercentage,
        "vatDetails" : {
          "vatCode" : this.utilityService.splitCode(vatCode)
        },
        "purchaseOrderNumber" : poNumber,
        orderAmountMap: poArray.map(e => {
          return {
            orderNumber: e.invoiceNo,
            amount: e.totalprice
          }
        }),
        paymentType: 'ACCOUNTS_PAYABLE'
      }
    }

    console.log(request);

    this.financeService.submitPayment(request).subscribe((res: any) => {
      this.paymentForm.resetForm()
      this.showSuccessMsg(`the Payment ${res.code} has been submitted successfully`)
    },
      err => {
        if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.showErrorMsg(err.error.errors[0].message)
        } else this.showErrorMsg(API_FAILURE_MSG)
      })


  }

  showErrorMsg = (errMsg) => {
    Swal.fire({
      icon: 'error',
      text: errMsg,
      timer: 15000,
      showCloseButton: true,
      cancelButtonText:
        '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
  showSuccessMsg = (succMsg) => {
    Swal.fire({
      icon: 'success',
      text: succMsg,
      timer: 15000,
      showCloseButton: true,
      cancelButtonText:
        '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }

  _filterCurrency = value => {
    const filterValue = value.toLowerCase();

    return this.currencies.filter(option => option.toLowerCase().includes(filterValue));
  }

  _filterSupplier = value => {
    const filterValue = value.toLowerCase();

    return this.supliers.filter(option => option.name.toLowerCase().includes(filterValue));
  }

  _filterSupplier2 = value => {
    const filterValue = value.toLowerCase();

    return this.supliers2.filter(option => option.name.toLowerCase().includes(filterValue));
  }

  _filterItemCode = value => {
    const filterValue = String(value).toLowerCase();

    return this.itemCodes.filter(option => option.item.description.toLowerCase().includes(filterValue));
  }

  updateSelect = (formGroup, key, value, currency, data?: any) => {
    this[formGroup].patchValue({
      [key]: value
    });
    if(data && data.paymentTerms) {
      this[formGroup].patchValue({
        paymentTerm: data.paymentTerms
      });
    }
    if (key == 'currency') {
      this.updateConveRate(this.utilityService.splitCode(value))
    } else if (key == 'supplier' && currency) {
      this[formGroup].patchValue({
        currency: currency
      });
      this.updateConveRate(this.utilityService.splitCode(currency))
    }
  }

  updateSelectAt = (formGroup, key, value, i) => {
    this[formGroup].at(i).patchValue(
      { [key]: value }
    );
  }

  updateConveRate(curr) {
    let convRate = this.currenciesRate.filter(e => e.isocode === curr)[0];
    this.itemsArr.controls.forEach((e, i) => {
      let { itemCode } = this.itemsArr.at(i).value;
      if (itemCode && itemCode !== '') {
        let price = this.itemCodes.filter(e => e.item.itemCode === itemCode)[0].item.purchasePrice;
        this.itemsArr.at(i).patchValue({ price: !curr.includes("SAR") ? price / convRate.conversion : price });
        this.updateAmount(i)
      }
    });

  }

  checkVat(type) {
    if(type == "AdvancePayment") {
      this.paymentFormGroup.get("invoiceBookings").clearValidators();
      this.paymentFormGroup.get("invoiceBookings").enable()
      this.showPoNum = false;
      this.paymentFormGroup.get("poNumber").clearValidators();
      this.paymentFormGroup.get("poNumber").enable();
      this.paymentFormGroup.get("vatCode").setValidators([Validators.required]);
      this.paymentFormGroup.get("vatPercentage").setValidators([Validators.required]);
      this.paymentFormGroup.get("vatCode").enable();
      this.paymentFormGroup.get("vatPercentage").enable();
    } else if(type == "UnallocatedPayment") {
      this.paymentFormGroup.get("invoiceBookings").clearValidators();
      this.paymentFormGroup.get("invoiceBookings").enable()
      this.paymentFormGroup.get("vatCode").clearValidators();
      this.paymentFormGroup.get("vatPercentage").clearValidators();
      this.paymentFormGroup.get("vatCode").enable();
      this.paymentFormGroup.get("vatPercentage").enable();
      this.showPoNum = true;
      this.paymentFormGroup.get("poNumber").setValidators([Validators.required]);
      this.paymentFormGroup.get("poNumber").enable();
    } else {
      this.paymentFormGroup.get("invoiceBookings").setValidators([Validators.required]);
      this.paymentFormGroup.get("invoiceBookings").enable()
      this.paymentFormGroup.get("vatCode").clearValidators();
      this.paymentFormGroup.get("vatPercentage").clearValidators();
      this.paymentFormGroup.get("vatCode").enable();
      this.paymentFormGroup.get("vatPercentage").enable();
      this.showPoNum = false;
      this.paymentFormGroup.get("poNumber").clearValidators();
      this.paymentFormGroup.get("poNumber").enable();
    }
  }

  updateAmountPayment(e) {
    const val = e.target.value;

    // this.paymentFormGroup.patchValue({ vatPercentage: val });
    let subTotal = this.paymentFormGroup.get('grossamt').value;
    let sumAmnt: any = (Number(subTotal) * (1 + val / 100)).toFixed(2);
    this.paymentFormGroup.get('grandtotalprice').setValue(parseFloat(sumAmnt));
  }

  updateAmountPaymentGross(e) {
    const subTotal = e.target.value;
    if(this.paymentFormGroup.get('vatPercentage').value && this.paymentFormGroup.get('vatPercentage').value !== '') {
      let val = this.paymentFormGroup.get('vatPercentage').value;
      let sumAmnt: any = (Number(subTotal) * (1 + val / 100)).toFixed(2);
      this.paymentFormGroup.get('grandtotalprice').setValue(parseFloat(sumAmnt));
    } else {
      this.paymentFormGroup.get('grandtotalprice').setValue(parseFloat(subTotal));
    }
  }

  updateVatPayment(val) {
    this.paymentFormGroup.patchValue({ vatPercentage: val });
    let subTotal = this.paymentFormGroup.get('grossamt').value;
    let sumAmnt: any = (Number(subTotal) * (1 + val / 100)).toFixed(2);
    this.paymentFormGroup.get('grandtotalprice').setValue(parseFloat(sumAmnt));
  }

  updateMRFilter(po) {
    if(Array.isArray(this.invoiceMrList)) {
      let val = this.invoiceMrList.map(e => {
        if(e.purchaseOrder.purchaseOrder == po) {
          this.mrCodes = e.materialReceivedList;
        }
      });
    }
  }

  updatePOFilter(formGroup, key, value) {
    this[formGroup].patchValue({
      [key]: value
    });

    this.financeService.getMR(this.utilityService.splitCode(value)).subscribe((res: any) => {
        this.invoiceMrList = res.purchaseOrderList;
        if(res && res.purchaseOrderList && res.purchaseOrderList[0]) {
          this.poCodes = res.purchaseOrderList.map(e => {
            return e.purchaseOrder.purchaseOrder
          });
        }
    },
    err => {
    });
  }
}

export interface PeriodicElement {
  //currency: string;
  itemCode: number;
  itemDescription: string;
  quantity: string;
  price: string;
  itemTotal: string;
  unit: string;
  discountPercentage: string,
  subTotal: string
  vatPercentage: string,
  remainingQuantity: string

}

@Component({
  selector: 'update-qty',
  templateUrl: 'update-qty.html',
  providers: [FinanceFormMoleculeComponent]
})
export class UpdateQtyFormModalComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data, public dialog: MatDialog) { }
  onNoClick(): void {
    this.dialog.closeAll();
  }

  checkQty = (e) => {
    if (this.data.qty && this.data.qty > parseInt(this.data.valideQtyLimit) || isNaN(Number(this.data.qty))) {
      this.data.qty = this.data.valideQtyLimit;
    }
    if (this.data.qty !== "" && this.data.qty !== null && this.data.qty < 1) {
      this.data.qty = this.data.valideQtyLimit;
    }
  }

}