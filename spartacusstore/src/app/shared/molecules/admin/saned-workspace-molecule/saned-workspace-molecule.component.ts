import { Component, OnInit } from '@angular/core';
import { appConfig } from 'src/app/app.config';

@Component({
  selector: 'app-saned-workspace-molecule',
  templateUrl: './saned-workspace-molecule.component.html',
  styleUrls: ['./saned-workspace-molecule.component.scss']
})
export class SanedWorkspaceMoleculeComponent implements OnInit {

  sanedWorkspaceLinks: any = appConfig.sanedWorkspace
  constructor() { }

  ngOnInit() {
  }

}
