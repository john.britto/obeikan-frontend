import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdminService } from '../../../../../services/admin.service';
import {PayRollService} from "../../../../../services/payroll.service";
import Swal from 'sweetalert2';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-salary-element-entries-single-molecule',
  templateUrl: './salary-element-entries-single-molecule.component.html',
  styleUrls: ['./salary-element-entries-single-molecule.component.scss']
})
export class SalaryElementEntriesSingleMoleculeComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  pipe = new DatePipe('en-US');
  employees: any = []
  seesFormGroup: FormGroup;
  employeeNumber: any
  effectiveDate: any
  ELEMENT_DATA: PeriodicElement[] = []
  dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
  displayedColumns = ['S no', 'entryType', 'classification', 'recurring', 'startDate', 'endDate', 'pk', 'status', 'action'];
  editData: any = {}
  xpandStatus: boolean = false;

  constructor( private _formBuilder: FormBuilder, private adminService: AdminService, private payrollService: PayRollService, private dialog: MatDialog) { }

  ngOnInit() {

    this.adminService.getMasterData().subscribe((res: any) => {
      if(!!res && !!res.hrEmployeeMasterData) this.employees = res.hrEmployeeMasterData
    },
    err => {

    })

    this.seesFormGroup = this._formBuilder.group({
      employeeName: ['', Validators.required],
      employeeNo: [{value: '', disabled: true}, [Validators.required]],
      jobTitle: [{value: '', disabled: true}, [Validators.required]],
      department: [{value: '', disabled: true}, [Validators.required]],
      date: ['', Validators.required]
    });
  }

  updateEmployee = employee => {
    this.seesFormGroup.patchValue({
      employeeNo: employee.employeeNumber,
      jobTitle: employee.jobTilte,
      department: employee.department
    });
    this.seesFormGroup.get("date").setValue(null);
    this.employeeNumber = employee.employeeNumber
  }

  getDetails = () => {
    const{ date, employeeNo } = this.seesFormGroup.getRawValue()
    if(!employeeNo) return this.showErrorMsg("please select a Employee");
    this.effectiveDate = date;
    this.payrollService.getPayroll(employeeNo, this.pipe.transform(this.effectiveDate, 'MM/dd/yyyy')).subscribe((res: any) => {
      let payRoll = res.payrollEntries ? res.payrollEntries : [];
      let ot = res.otEntries ? res.otEntries : [];

      this.ELEMENT_DATA = [ ...payRoll, ...ot].filter(Boolean);
      this.dataSource = new MatTableDataSource<PeriodicElement>(this.ELEMENT_DATA);
      this.dataSource.paginator = this.paginator;
      this.xpandStatus = true;
    },
    err => {

    })
  }

  editRow = (row, tabGroup) => {
    let editDataUpdate: any = {};
    if(row.classification === "Earnings") {
      if(row.recurring) {
        editDataUpdate.earningRecurring = row;
        tabGroup.selectedIndex = 2;
      } else {
        editDataUpdate.earningOnce = row;
        tabGroup.selectedIndex = 0;
      }
    } else if (row.classification === "Deductions") {
      if(row.recurring) {
        editDataUpdate.deductionRecurring = row;
        tabGroup.selectedIndex = 3;
      } else {
        editDataUpdate.deductionOnce = row;
        tabGroup.selectedIndex = 1;
      }
    } else if(row.classification === "OT/ST") {
      editDataUpdate.ot = row;
      tabGroup.selectedIndex = 4;
    }
    this.editData = editDataUpdate;
  }

  deleteRow = (id, row) => {
    let queryParam = '';
    if(row.classification === "OT/ST") {
      queryParam = `payrollEntryPK=&overTimeEntryPK=${id}`
    } else {
      queryParam = `payrollEntryPK=${id}&overTimeEntryPK=`
    }
    this.adminService.deletePayroll(`delete-payrollentry/${this.employeeNumber}?${queryParam}`).subscribe((res: any) => {
      this.showSuccessMsg("Payroll Entry Deleted Successfully");
      this.getDetails();
    },
      err => {
        this.showErrorMsg("Something Went Wrong!!")
      })
  }

  deleteDialog(row) {
    const dialogRef = this.dialog.open(DeleteHRMSSalaryFormModalComponent, {
      data: { pk: row.pk }
    });

    dialogRef.afterClosed().subscribe(result => {
      result && this.deleteRow(result.pk, row);
    });
  }

  showErrorMsg(msg) {
    Swal.fire({
      icon: "error",
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }

  showSuccessMsg(msg) {
    Swal.fire({
      icon: "success",
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}

export interface PeriodicElement {
  //currency: string;
  classification: string;
  entryType: string;
  recurring: string;
  startDate: string;
  endDate: string;
  pk: string;
}

@Component({
  selector: 'app-delete-hrms-modal-salary',
  templateUrl: 'delete-hrms-modal-salary.html',
  providers: [SalaryElementEntriesSingleMoleculeComponent]
})
export class DeleteHRMSSalaryFormModalComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data, public dialog: MatDialog) { }
  onNoClick(): void {
    this.dialog.closeAll();
  }
}