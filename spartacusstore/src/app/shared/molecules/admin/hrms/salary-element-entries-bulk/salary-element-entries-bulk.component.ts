import { Component, OnInit } from '@angular/core';
import * as XLSX from 'xlsx';
import Swal from 'sweetalert2';
import { environment } from '../../../../../../environments/environment';
import { CommonUtilityService } from '../../../../../services/common-utility-service';
import { AdminService } from '../../../../../services/admin.service'
import { appConfig } from 'src/app/app.config';

@Component({
  selector: 'app-salary-element-entries-bulk',
  templateUrl: './salary-element-entries-bulk.component.html',
  styleUrls: ['./salary-element-entries-bulk.component.scss']
})
export class SalaryElementEntriesBulkComponent implements OnInit {

  downloadxl: any

  constructor(private utilService: CommonUtilityService, private adminService: AdminService) { }

  ngOnInit() {

    this.adminService.downloadFile().subscribe((res: any) => {
      if(!!res && !!res.contentSlots && !!res.contentSlots.contentSlot) {
        let contents = []
        res.contentSlots.contentSlot.forEach(content => {
          if(content.slotId === 'HRServicePayrollProcessSlot') {
            contents = content.components.component
          }
        });

        if(contents.length > 0) {
          contents.forEach(comp => {
            if(comp.uid === 'HRServicesPayrollProcessExcelComponent') {
              this.downloadxl = comp.media.url
            }
          })
        }
      }
    },
    err => {

    })

  }

  downloadFile = e => {
    e.preventDefault();
    const downloadUrl = environment.hostName + this.downloadxl;
    if (this.utilService.isBrowser() && !!this.downloadxl) {
      window.open(downloadUrl, '_self')
    }
  }

  uploadFile = (event: any) => {
    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    const file = event.target.files[0];
    reader.onload = () => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: 'binary' });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        initial[name] = XLSX.utils.sheet_to_json(sheet);
        return initial;
      }, {});
      jsonData ? this.uploadTemplate(jsonData, file) : '';
    };
    reader.readAsBinaryString(file);
  }

  uploadTemplate= (jsonData, file) => {
    !!jsonData.oneTimeDeductions ? jsonData.oneTimeDeductions.splice(0, 2): null
    !!jsonData.oneTimeEarnings ? jsonData.oneTimeEarnings.splice(0, 2): null
    !!jsonData.overTimeEntries ? jsonData.overTimeEntries.splice(0, 2): null
    !!jsonData.recurringDeductions ? jsonData.recurringDeductions.splice(0, 2): null
    !!jsonData.recurringEarnings ? jsonData.recurringEarnings.splice(0, 2): null
    
    this.adminService.uploadTemplate(jsonData, file).subscribe((res: any) => {
      if(res) {
        this.displayMsg('success', 'Form has been uploaded successfully')
      }
    },
    err => {
      if(!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
        this.displayMsg('error', err.error.errors[0].message)
      } else this.displayMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
    })
  }

  displayMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}
