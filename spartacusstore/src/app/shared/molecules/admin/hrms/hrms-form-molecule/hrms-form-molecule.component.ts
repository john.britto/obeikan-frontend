import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import {
  FormBuilder,
  Validators
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CommonUtilityService } from '../../../../../services/common-utility-service';
import { englishEnums } from '../../../../../app.enums';
import { DatePipe } from '@angular/common';
import {
  AdminService,
  VALIDATION_MESSAGES
} from '../../../../../services/admin.service';
import Swal from 'sweetalert2';
import { appConfig } from '../../../../../app.config'
@Component({
  selector: 'app-hrms-form-molecule',
  templateUrl: './hrms-form-molecule.component.html',
  styleUrls: ['./hrms-form-molecule.component.scss'],
})

export class HrmsFormMoleculeComponent implements OnInit, OnChanges {
  @Input() enums: any;
  @Input() empList: any;
  @Input() employeeList: any;
  @Input() isModel: any
  @Output() refreshEmpList = new EventEmitter();
  @Output() refreshEmpListEdit = new EventEmitter();
  @Input() data: any;
  pipe = new DatePipe('en-US');

  genders: any;
  nationalities: any = appConfig.nationalities;
  maritalStatus: any;
  contractStatuses: any;
  banks: any;
  location: any = [];
  religions: any;
  jobTitles: any;
  grades: any = [];
  supervisorIds: any = [];
  medicalInsurences: any = []
  countryCodes: any = []
  yesOrNoOtions: any = appConfig.DropdownOptions.yesOrNoOtions
  departments: any = [];
  divisions: any = [];
  personalInfo: any;
  contactInfo: any;
  employmentInfo: any;
  cfd: any
  contractDetails: any;
  bankDetails: any;
  allowanceDetailsForm: any
  validationMessages = VALIDATION_MESSAGES;
  superVisorList: any[] = [];
  showGradeField: boolean = false;
  showDeptField: boolean = false;
  showDivField: boolean = false;
  showLocField: boolean = false;
  showJobTitleField: boolean = false;
  showOtherMIField: boolean = false
  showOtherCountryCode: boolean = false

  editEmployee: any = false;
  formData = {
    firstName: null,
    lastName: null,
    fatherName: null,
    gender: null,
    iqamaNo: null,
    nationality: null,
    dateOfBirth: null,
    townOfBirth: null,
    maritalStatus: null,
    religion: null,
    employeeNumber: null,
    joiningDate: null,
    professionOnIqama: null,
    jobTilte: null,
    grade: null,
    department: null,
    division: null,
    supervisorEmpId: null,
    location: null,
    basicSalary: null,
    contractStatus: null,
    mobileNumber: null,
    emailAddress: null,
    bankName: null,
    bankCode: null,
    iban: null,
    housing: null,
    transportation: null,
    overTime: null,
    medicalInsurance: null,
    countryCode: null
  };
  minDate: any 
  maxDate: any
  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private matSnack: MatSnackBar,
    private utilService: CommonUtilityService
  ) {
    this.maxDate = new Date();
    
    let month = this.maxDate.getMonth() === 0 ? 11 : this.maxDate.getMonth() - 1
    let year = this.maxDate.getMonth() === 0 ? this.maxDate.getFullYear() - 1 : this.maxDate.getFullYear()
    this.minDate = new Date(year, month, 1)
  }
  ngOnChanges() {
    if (this.empList && Array.isArray(this.empList)) {
      this.empList.map((each) => {
        this.superVisorList.push(each.employeeNumber)
      })
    }

    this.editEmployee = false;
    this.grades = []; this.supervisorIds = []; this.departments = []; this.divisions = []; this.jobTitles = []; this.medicalInsurences = []
    if (this.enums) {
      this.maritalStatus = this.utilService.setEnums(this.enums, 'osanedMaritalStatus');
      this.religions = this.utilService.setEnums(this.enums, 'osanedReligions');
      this.contractStatuses = this.utilService.setEnums(this.enums, 'osanedContractStatus');
      this.banks = this.utilService.setEnums(this.enums, 'osanedBanks');
      //this.location = this.utilService.setEnums(this.enums, 'osanedLocations');
      this.genders = this.utilService.setEnums(this.enums, 'osanedGender');
    }
    if (this.employeeList && this.employeeList.length > 0) {
      this.employeeList.map(emp => {
        this.grades.push(emp.grade);
        this.supervisorIds.push(emp.employeeNumber);
        this.departments.push(emp.department);
        this.divisions.push(emp.division);
        this.jobTitles.push(emp.jobTilte);
        this.location.push(emp.location);
        this.medicalInsurences.push(emp.medicalInsurance)
      });
      const uniqueGrade = new Set(this.grades);
      this.grades = [...uniqueGrade];
      this.grades.push("Others");
      const uniqueMI = new Set(this.medicalInsurences);
      this.medicalInsurences = [...uniqueMI];
      this.medicalInsurences.push("Others");
      const uniqueJobTiles = new Set(this.jobTitles);
      this.jobTitles = [...uniqueJobTiles];
      this.jobTitles.push("Others");
      const uniqueSId = new Set(this.supervisorIds);
      this.supervisorIds = [...uniqueSId].filter(Boolean);
      const uniqueDept = new Set(this.departments);
      this.departments = [...uniqueDept];
      const uniqueDiv = new Set(this.divisions);
      this.departments.push("Others");
      this.divisions = [...uniqueDiv];
      this.divisions.push("Others");
      const uniqueLoc = new Set(this.location);
      this.location = [...uniqueLoc];
      this.location.push("Others");
    }
    if (this.data) {
      this.editEmployee = true;
      this.formData = this.data;
      this.ngOnInit();
    }
  }

  updateOthersField(e, type, value) {
    if (e.isUserInput) {
      if (type === 'grade') {
        if (value === "Others") {
          this.showGradeField = true;
          this.employmentInfo.get('othersgrade').setValidators([Validators.required]);
        } else {
          this.showGradeField = false;
          this.employmentInfo.get('othersgrade').clearValidators();
        }
      }
      if (type === 'dept') {
        if (value === "Others") {
          this.showDeptField = true;
          this.employmentInfo.get('othersdept').setValidators([Validators.required]);
        } else {
          this.showDeptField = false;
          this.employmentInfo.get('othersdept').clearValidators();
        }
      }
      if (type === 'division') {
        if (value === "Others") {
          this.showDivField = true;
          this.employmentInfo.get('othersdiv').setValidators([Validators.required]);
        } else {
          this.showDivField = false;
          this.employmentInfo.get('othersdiv').clearValidators();
        }
      }
      if (type === 'location') {
        if (value === "Others") {
          this.showLocField = true;
          this.employmentInfo.get('othersloc').setValidators([Validators.required]);
        } else {
          this.showLocField = false;
          this.employmentInfo.get('othersloc').clearValidators();
        }
      }
      if (type === 'jobTitle') {
        if (value === "Others") {
          this.showJobTitleField = true;
          this.employmentInfo.get('othersjobTitles').setValidators([Validators.required]);
        } else {
          this.showJobTitleField = false;
          this.employmentInfo.get('othersjobTitles').clearValidators();
        }
      }
      if (type === 'medicalInsurence') {
        if (value === "Others") {
          this.showOtherMIField = true;
          this.allowanceDetailsForm.get('otherMI').setValidators([Validators.required]);
        } else {
          this.showOtherMIField = false;
          this.allowanceDetailsForm.get('otherMI').clearValidators();
        }
      }
      if (type === 'countryCode') {
        if (value === "Others") {
          this.showOtherCountryCode = true;
          this.contactInfo.get('otherCountryCode').setValidators([Validators.required, Validators.pattern(/^[0-9]{2,3}$/)]);
          this.contactInfo.get('countryCode').clearValidators();
        } else {
          this.showOtherCountryCode = false;
          this.contactInfo.get('otherCountryCode').clearValidators();
          this.contactInfo.get('countryCode').setValidators([Validators.required, Validators.pattern(/^[0-9]{2,3}$/)]);
        }
        this.contactInfo.get('countryCode').updateValueAndValidity()
        this.contactInfo.get('otherCountryCode').updateValueAndValidity()
      }

    }
  }

  setEnums(property) {
    const values = [];
    this.enums[property].map(each => {
      englishEnums[property].map(obj => {
        if (each.code === obj.code) {
          values.push(obj);
        }
      });
    });
    return values;
  }

  ngOnInit() {

    let allCountryCodes = ( !this.utilService.isNullOrUndefined(this.employeeList) && this.employeeList.length > 0) ? this.employeeList.filter(emp => { return !this.utilService.isNullOrUndefined(emp.countryCode) }) : []
    
    if(allCountryCodes.length > 0) {
      allCountryCodes.filter(code => {
        if(!this.countryCodes.includes(code.countryCode)) {
          this.countryCodes.push(code.countryCode)
          return true
        } else return false
      })
    }

    const MOBILE_PATTERN = '^((\\+[0-9]{2}-?)|0)?[0-9]{9}$';

    this.personalInfo = this.fb.group({
      firstName: [this.formData.firstName, Validators.compose([Validators.required])],
      lastName: [this.formData.lastName, Validators.compose([Validators.required])],
      Fathername: [this.formData.fatherName, Validators.compose([Validators.required])],
      gender: [this.formData.gender, Validators.compose([Validators.required])],
      IqamaNo: [this.formData.iqamaNo, Validators.compose([Validators.required])],
      DOB: [this.formData.dateOfBirth ? new Date(this.formData.dateOfBirth) : null,
      Validators.compose([Validators.required])],
      town: [this.formData.townOfBirth, Validators.compose([Validators.required])],
      religion: [this.formData.religion, Validators.compose([Validators.required])],
      maritalStatus: [this.formData.maritalStatus, Validators.compose([Validators.required])],
      nationality: [this.formData.nationality, Validators.compose([Validators.required])]
    });

    this.contactInfo = this.isModel ? this.fb.group({
      phoneNo: [this.formData.mobileNumber, Validators.compose([Validators.required, Validators.pattern(/^[0-9]{11,12}$/)])],
      email: [this.formData.emailAddress, Validators.compose([Validators.required, Validators.email])]
    }) : 
    this.fb.group({
      countryCode: [this.formData.countryCode, Validators.compose([Validators.required, Validators.pattern(/^[0-9]{2,3}$/)])],
      otherCountryCode: [''],
      phoneNo: [this.formData.mobileNumber, Validators.compose([Validators.required, Validators.pattern(/^[0-9]{9,10}$/)])],
      email: [this.formData.emailAddress, Validators.compose([Validators.required, Validators.email])]
    });

    this.employmentInfo = this.fb.group({
      empNo: [{ value: this.formData.employeeNumber, disabled: this.formData.employeeNumber ? true : false }],
      joiningDate: [this.formData.joiningDate && this.editEmployee ? new Date(this.formData.joiningDate) : '',
      Validators.compose([Validators.required])],

      jobTitle: [this.formData.jobTilte, Validators.compose([Validators.required])],
      grade: [this.formData.grade, Validators.compose([Validators.required])],
      department: [this.formData.department, Validators.compose([Validators.required])],
      division: [this.formData.division, Validators.compose([Validators.required])],
      supEmpId: [this.formData.supervisorEmpId, Validators.compose([Validators.required])],
      location: [this.formData.location, Validators.compose([Validators.required])],
      iqamaProfession: [this.formData.professionOnIqama, Validators.compose([Validators.required])],
      othersgrade: [''],
      othersdept: [''],
      othersdiv: [''],
      othersloc: [''],
      othersjobTitles: ['']

    });

    if (this.editEmployee) {
      this.cfd = this.fb.group({
        changeEffectiveDate: ['', Validators.compose([Validators.required])]
      });
    }
    this.contractDetails = this.fb.group({
      basicSalary: [this.formData.basicSalary, Validators.compose([Validators.required])],
      contractStatus: [this.formData.contractStatus, Validators.compose([Validators.required])]
    });

    this.bankDetails = this.fb.group({
      bankName: [this.formData.bankName, Validators.compose([Validators.required])],
      bankCode: [this.formData.bankCode,
      Validators.compose([Validators.required, Validators.pattern(this.utilService.ALPHANUMERIC_PATTERN)])],
      IBAN: [this.formData.iban, Validators.compose([Validators.required, Validators.pattern(this.utilService.ALPHANUMERIC_PATTERN)])]
    });

    this.allowanceDetailsForm = this.fb.group({
      housingPercentage: [parseFloat(this.formData.housing), Validators.compose([Validators.required])],
      transportationPercentage: [parseFloat(this.formData.transportation), Validators.compose([Validators.required])],
      overtime: [(!!this.formData.overTime ? (this.formData.overTime === 'true' ? 'YES' : 'NO') : ''), Validators.compose([Validators.required])],
      medicalInsurence: [this.formData.medicalInsurance, Validators.compose([Validators.required])],
      otherMI: ['']
    })
  }

  changeAccordian() {

  }

  onSubmitForm = (action?: any) => {

    const { firstName, lastName, Fathername, gender, IqamaNo, nationality, DOB, town, maritalStatus, religion } = this.personalInfo.value;

    const { empNo, joiningDate, iqamaProfession, jobTitle, grade, department, division, supEmpId, location, othersgrade, othersdept, othersdiv, othersloc, othersjobTitles } = this.employmentInfo.value;

    const { basicSalary, contractStatus } = this.contractDetails.value;

    const { phoneNo, email } = this.contactInfo.value;

    const { bankName, bankCode, IBAN } = this.bankDetails.value;

    const { housingPercentage, transportationPercentage, overtime, medicalInsurence, otherMI } = this.allowanceDetailsForm.value

    if (
      this.contactInfo.valid &&
      this.employmentInfo.valid &&
      this.personalInfo.valid &&
      this.bankDetails.valid &&
      this.contractDetails.valid) {
      const jd = this.pipe.transform(joiningDate, 'MM/dd/yyyy');
      const db = this.pipe.transform(DOB, 'MM/dd/yyyy');
      let data = {
        firstName,
        lastName,
        fatherName: Fathername,
        gender,
        iqamaNo: IqamaNo,
        nationality,
        dateOfBirth: db,
        townOfBirth: town,
        maritalStatus,
        religion,
        employeeNumber: this.formData.employeeNumber ? this.formData.employeeNumber + '' : empNo + '',
        joiningDate: jd,
        professionOnIqama: iqamaProfession,
        jobTilte: jobTitle === 'Others' ? othersjobTitles : jobTitle,
        grade: grade === "Others" ? othersgrade : grade,
        department: department === "Others" ? othersdept : department,
        division: division === "Others" ? othersdiv : division,
        supervisorEmpId: this.editEmployee ? supEmpId + '' : !!supEmpId.split('-')[1] ? supEmpId.split('-')[1] : supEmpId,
        location: location === "Others" ? othersloc : location,
        basicSalary: basicSalary + '',
        contractStatus,
        mobileNumber: phoneNo + '',
        emailAddress: email,
        bankName,
        bankCode,
        iban: IBAN,
        housing: housingPercentage,
        transportation: transportationPercentage,
        overTime: overtime,
        medicalInsurance: medicalInsurence === 'Others' ? otherMI : medicalInsurence,
      }


      let request: any;
      if (this.editEmployee) {
        request = data;
        request.action = action
        request.changeEffectiveDate = this.pipe.transform(this.cfd.value.changeEffectiveDate, 'MM/dd/yyyy');
      } else {
        data['countryCode'] = this.contactInfo.get('countryCode').value === 'Others' ? this.contactInfo.get('otherCountryCode').value : this.contactInfo.get('countryCode').value
        request = {
          hrEmployeeMasterData:
            [data],
        };
      }
      this.adminService.submitForm(request, this.editEmployee).subscribe((res: any) => {
        if (this.editEmployee) {
          this.dialogMsg('success', appConfig.apiResponseMessages.genericSuccessMsg);
          this.closeModel(true)
          this.ngOnInit();
        } else {
          this.dialogMsg('success', appConfig.apiResponseMessages.genericSuccessMsg);
          this.resetForm()
          this.refreshEmpList.emit(true);
        }
      },
        err => {
          if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
            this.dialogMsg('error', err.error.errors[0].message)
          } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
        });

    }
  }

  resetForm = () => {
    this.personalInfo.reset()
    this.contactInfo.reset()
    this.employmentInfo.reset()
    this.contractDetails.reset()
    this.bankDetails.reset()
    this.allowanceDetailsForm.reset()
  }

  closeModel = action => {
    this.refreshEmpListEdit.emit(action);
  }

  dialogMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }
}
