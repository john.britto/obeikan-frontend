import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { AdminService } from '../../../../services/admin.service';
import { DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import { iframeUrls } from '../../../organisms/reports-organism/reports.config'
import { LoginService } from '../../../../services/login.service';

@Component({
  selector: 'app-admin-chart-molecule',
  templateUrl: './admin-chart-molecule.component.html',
  styleUrls: ['./admin-chart-molecule.component.scss']
})
export class AdminChartMoleculeComponent implements OnInit {

  salesUrl: SafeResourceUrl;
  payableUrl: SafeResourceUrl
  receivableUrl: SafeResourceUrl
  inventoryUrl: SafeResourceUrl
  lastMonthSalary: SafeResourceUrl
  lastMonthOnvertime: SafeResourceUrl
  lastMonthHousingTravelExp: SafeResourceUrl
  variableExpenses: SafeResourceUrl
  noOfEmployees: SafeResourceUrl
  saudization: SafeResourceUrl
  empOnDuty: SafeResourceUrl
  customerRole: any
  powerBIUrl: any

  constructor(
    private adminService: AdminService,
    private router: Router,
    private sanitizer: DomSanitizer,
    private loginService: LoginService) { 
      router.events.subscribe((val) => {
        if(val instanceof NavigationEnd) {
          let url = val.url
  
          switch(url) {
            case '/admin/hrms':
              this.customerRole = 'HR'
              return
            case '/admin/finance':
              this.customerRole = 'Finance'
              return
              case '/admin/my-saned':
              this.customerRole = 'MySaned'
              return
            default:
              this.customerRole = ''
              return
          }
        }
    });
    }

  ngOnInit() {

    this.powerBIUrl = this.loginService.powerBiData

    if(!!this.powerBIUrl && !!this.powerBIUrl.entry) {
      this.powerBIUrl.entry.forEach(entry => {
        if(entry.key === 'salesTile') this.salesUrl = this.sanitizer.bypassSecurityTrustResourceUrl(entry.value);
        if(entry.key === 'payableTile') this.payableUrl = this.sanitizer.bypassSecurityTrustResourceUrl(entry.value);
        if(entry.key === 'ReceivableTile') this.receivableUrl = this.sanitizer.bypassSecurityTrustResourceUrl(entry.value);
        if(entry.key === 'InventoryTile') this.inventoryUrl = this.sanitizer.bypassSecurityTrustResourceUrl(entry.value);
        if(entry.key === 'lastMonthSalaryTile') this.lastMonthSalary = this.sanitizer.bypassSecurityTrustResourceUrl(entry.value);
        if(entry.key === 'lastMonthOnvertimeTile') this.lastMonthOnvertime = this.sanitizer.bypassSecurityTrustResourceUrl(entry.value);
        if(entry.key === 'housingTravelExpTile') this.lastMonthHousingTravelExp = this.sanitizer.bypassSecurityTrustResourceUrl(entry.value);
        if(entry.key === 'VariableExpensesTile') this.variableExpenses = this.sanitizer.bypassSecurityTrustResourceUrl(entry.value);
        if(entry.key === 'noOfEmployeesTile') this.noOfEmployees = this.sanitizer.bypassSecurityTrustResourceUrl(entry.value);
        if(entry.key === 'saudizationTile') this.saudization = this.sanitizer.bypassSecurityTrustResourceUrl(entry.value);
        if(entry.key === 'empOnDutyTile') this.empOnDuty = this.sanitizer.bypassSecurityTrustResourceUrl(entry.value);
      });
    }
  }

  openIframe = key => {
    this.adminService.iframeKey = key
    this.router.navigate(['/admin/my-reports'])
  }

}
