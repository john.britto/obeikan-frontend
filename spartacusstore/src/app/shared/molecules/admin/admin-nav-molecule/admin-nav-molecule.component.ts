import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ADMIN_NAV_LINKS, AdminService } from '../../../../services/admin.service'
import { CommonUtilityService } from '../../../../services/common-utility-service';
import { Router, ActivatedRoute } from '@angular/router';
import { appConfig } from '../../../../app.config'

@Component({
  selector: 'app-admin-nav-molecule',
  templateUrl: './admin-nav-molecule.component.html',
  styleUrls: ['./admin-nav-molecule.component.scss']
})
export class AdminNavMoleculeComponent implements OnInit {

  @Input() userType: any
  adminNavLinks: any;
  navClickValue: string = ADMIN_NAV_LINKS[0].routerLink;
  hrNavReports: any = appConfig.DropdownOptions.hrReports
  compassReports: any = appConfig.DropdownOptions.compassReports
  financeNavReports: any = appConfig.DropdownOptions.financeReports
  isFinanceUser: boolean = false
  isHrUser: boolean = false

  constructor(private utilityService: CommonUtilityService,
    private router: Router,
    private route: ActivatedRoute,
    private adminService: AdminService) {
    this.adminNavLinks = ADMIN_NAV_LINKS;
  }

  ngOnInit() {
    this.userType = JSON.parse(this.utilityService.getCookie('customerRoles'))
    const customerRoles = this.userType

    this.userType = this.userType.map(user => user.toUpperCase())
    this.navClickValue = this.router.url.split('/')[2];
    this.adminNavLinks = ADMIN_NAV_LINKS.map((each: any) => {
      if(each.name === 'company' && customerRoles.includes('SuperAdmin')) {
        return {
          ...each,
          active: true
        }
      }
      if (!this.userType.includes(each.name.toUpperCase()) && !appConfig.commonUserAccess.includes(each.name)) {
        return {
          ...each,
          active: false
        }
      } else {
        return { ...each }
      }
    });

    this.setActiveNav(this.navClickValue)

    const isHr = customerRoles.some(role=> appConfig.DropdownOptions.customerRoles.includes(role))

    if (isHr) {
      this.adminNavLinks[0].active = true
    }

    if (!!customerRoles &&
      customerRoles.length === 1 &&
      customerRoles.includes('HREmployee')) {
      this.adminNavLinks[0].active = false
    }

    const userType = this.adminService.userType

  if (!!userType) {
      let filteredLinks = this.adminNavLinks.map((each: any) => {
        if (each.name === 'Finance' && userType === 'Finance') {
          return {
            ...each,
            active: true
          }
        } else if (each.name === 'Finance' && userType === 'HR') {
          return {
            ...each,
            active: false
          }
        } else {
          return {
            ...each
          }
        }
      });
      this.adminNavLinks = filteredLinks
    }
    this.isHrUser = (customerRoles.includes('HR') || customerRoles.includes('GeneralManager')) ? true : false
    this.isFinanceUser = (customerRoles.includes('Finance') || customerRoles.includes('FinanceManager')) ? true : false
  }

  navClick(nav: any) {
    if (!nav.active) return;
    if (this.userType === "HR" && nav.name !== "Hr") return;
    this.navClickValue = nav.routerLink

    for (let i = 0; i < this.adminNavLinks.length; i++) {
      if(this.adminNavLinks[i].name === nav.name) this.adminNavLinks[i].url = this.adminNavLinks[i].hoverImg
      else this.adminNavLinks[i].url = this.adminNavLinks[i].imgUrl
    }

    this.setActiveNav(nav.routerLink)
  }

  setActiveNav = path => {
    this.adminNavLinks = this.adminNavLinks.map((nav: any) => {
      if(nav.routerLink === path) {
        return {
          ...nav,
          isActiveLink: true,
          url: nav.hoverImg
        } 
      } else {
        return {
          ...nav,
          isActiveLink: false,
          url: nav.imgUrl
        }
      }
    })
  }

  openIframe = key => {
    this.adminService.iframeKey = key
    this.router.navigate(['/admin/my-reports'])
  }
  
}
