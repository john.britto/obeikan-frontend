import { Component, OnInit, Input } from '@angular/core';
import { AdminService } from '../../../../services/admin.service'
import { SuperAdminService } from '../../../../services/super-admin.service'
import { Router, NavigationEnd  } from '@angular/router';
import { CommonUtilityService } from '../../../../services/common-utility-service';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-admin-header-molecule',
  templateUrl: './admin-header-molecule.component.html',
  styleUrls: ['./admin-header-molecule.component.scss']
})
export class AdminHeaderMoleculeComponent implements OnInit {

  @Input() userType: any
  @Input() pageTitle: any
  @Input() pageName: any
  hrImage: any = '../../assets/images/icons/HR_Management_icon.png'
  financeImage: any = '../../assets/images/icons/Finance-icon.png'
  defaultImage: any = '../../assets/images/icons/Finance-icon.png'
  dpImage: any = '../../assets/images/hrms.png'
  isSuperAdmin: any = false;
  uName: any;
  companyPic: any
  hostname: string = environment.hostName
  profilePic: any

  constructor(
    private adminService: AdminService,
    private route: Router,
    private utilService: CommonUtilityService,
    private superAdminService: SuperAdminService) { }

  ngOnInit() {
    this.isSuperAdmin = this.utilService.getCookie('isSuperAdmin') && JSON.parse(this.utilService.getCookie('isSuperAdmin'));
    this.uName = this.utilService.getLocalStorage('displayName');

    this.adminService.getEmpDetails('employee-info').subscribe((res: any) => {
      if(!!res && !!res.profilePicture && !!res.profilePicture.url) this.profilePic = res.profilePicture.url
    },
    err => {
    })

    if(this.isSuperAdmin) {
      this.adminService.getCompanyDetails('company').subscribe((res: any) => {
        if(!!res && !!res.profilePicture && !!res.profilePicture.url) this.companyPic = res.profilePicture.url
      },
      err => {
      })
    }
  }

  onLogout = e => {
    e.preventDefault()

    this.superAdminService.setSuperAdminRoles([])
    this.adminService.onLogout()
  }

  navigateToFCM = () => {
    this.route.navigate(['/superAdmin'])
  }
}
