import { Component, OnInit, Inject, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RegisterUserService } from '../../../services/register-user.service';
import { LoginService } from '../../../services/login.service';
import { CommonUtilityService } from '../../../services/common-utility-service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { appConfig } from 'src/app/app.config';

@Component({
  selector: 'app-register-molecule',
  templateUrl: './register-molecule.component.html',
  styleUrls: ['./register-molecule.component.scss']
})
export class RegisterMoleculeComponent implements OnInit, AfterViewInit {

  @ViewChild('firstName', { static: false }) firstNameElement: ElementRef;
  registerForm: FormGroup;
  userContext: any;
  termsAndConditions: any = false;
  validationMessages = {
    firstName: [{ type: 'required', message: 'First name is required' }],
    lastName: [{ type: 'required', message: 'Last name is required' }],
    email: [
      { type: 'required', message: 'Email is required' },
      { type: 'email', message: 'Enter a valid email' }
    ],
    prefix: [
      { type: 'required', message: 'Mobile code is required' },
      { type: 'pattern', message: 'Enter 3 digit code' }
    ],
    mobile: [
      { type: 'required', message: 'Mobile number is required' },
      { type: 'pattern', message: 'Please enter a valid mobile number' }
    ],
    company: [{ type: 'required', message: 'Company name is required' },
    { type: 'pattern', message: 'Maximum 10 Characters allowed' }],
    vatNumber: [{ type: 'required', message: 'Vat Number is required' }],
    commercial: [{ type: 'required', message: 'Commercial registration is required' },
    { type: 'pattern', message: 'Maximum 10 Characters allowed' }],
    confirmCommercial: [
      { type: 'required', message: 'confirm Commercial registration is required' },
      { type: 'notEquivalent', message: 'Commercial registration does not match' },
      { type: 'pattern', message: 'Maximum 10 Characters allowed' }
    ]

  };
  constructor(private fb: FormBuilder, private regUserService: RegisterUserService, private router: Router,
    private matSnack: MatSnackBar, private utilService: CommonUtilityService, private loginService: LoginService,
    public dialog: MatDialog) {
    this.loginService.userContext.subscribe(context => {
      this.userContext = context;
    });
  }

  ngAfterViewInit() {
    this.firstNameElement.nativeElement.focus();
  }

  ngOnInit() {

    this.registerForm = this.fb.group({
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      email: [null, Validators.compose([
        Validators.required,
        Validators.email
      ])],
      prefix: [null, Validators.compose([
        Validators.required,
        Validators.pattern(this.utilService.PREFIX_PATTERN)
      ])],
      mobile: [null, Validators.compose([
        Validators.required,
        Validators.pattern(this.utilService.MOBILE_PATTERN)
      ])],
      company: [null, Validators.compose([
        Validators.required
      ])],
      vatNumber: [null, Validators.required],
      commercial: [null, Validators.compose([
        Validators.required,
        Validators.pattern(this.utilService.MAX_TEN_PATTERN)

      ])],
      confirmCommercial: [null, Validators.compose([
        Validators.required,
        Validators.pattern(this.utilService.MAX_TEN_PATTERN)

      ])],
    }, { validator: this.pwdConfirming('commercial', 'confirmCommercial') });
  }
  pwdConfirming(key: string, confirmationKey: string) {
    return (group: FormGroup) => {
      const input = group.controls[key];
      const confirmationInput = group.controls[confirmationKey];
      return confirmationInput.setErrors(
        input.value !== confirmationInput.value ? { notEquivalent: true } : null
      );
    };
  }
  setTerms(e) {
    this.termsAndConditions = e.checked;
  }
  registerUser(user) {
    this.dialog.closeAll()
    user.mobile = user.prefix + user.mobile;
    const { firstName, lastName, email, mobile, company, commercial } = user;

    if (firstName && lastName && email && mobile && company && commercial) {
      const token = this.utilService.getCookie('AuthToken');
      if (token) {
        this.dialog.open(DialogDataExampleDialog, {
          data: '',
          disableClose: true
        });
        this.regUserService.registerUserDetails(token, user).subscribe((result: any) => {
          this.dialog.closeAll()
          this.dialogMsg('success', 'User Registerd successfully !')
          this.router.navigate(['/login']);
        }, (err) => {
          this.dialog.closeAll()
          if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
            this.dialogMsg('error', err.error.errors[0].message)
          } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
        });
      }
    }
  }

  dialogMsg = (type, msg) => {
    Swal.fire({
      icon: type,
      text: msg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }

}

@Component({
  selector: 'dialog-data-example-dialog',
  templateUrl: 'dialog-data-example-dialog.html',
})
export class DialogDataExampleDialog {
  constructor(@Inject(MAT_DIALOG_DATA) public data: '') { }
}
