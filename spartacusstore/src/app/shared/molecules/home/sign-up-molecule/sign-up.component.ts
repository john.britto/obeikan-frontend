import { Component, OnInit, Input } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { CommonUtilityService } from '../../../../services/common-utility-service';
@Component({
  selector: 'app-cx-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
})
export class SignUpComponent implements OnInit {

  @Input() sectionThreeVideoUrl: any
  @Input() sectionThreeBanner: any
  hostName: string = environment.hostName;
  isPlaying: boolean = false

  constructor(private utilityService: CommonUtilityService) {}

  ngOnInit() {}

  onControlVideo = () => {
    if(this.utilityService.isBrowser) {
      let video = <HTMLVideoElement> document.getElementById('home-video')
      if(this.isPlaying){
        this.isPlaying = !this.isPlaying
         video.pause()
        }
      else {
        this.isPlaying = !this.isPlaying
        video.play()
      }
    }
  }
}
