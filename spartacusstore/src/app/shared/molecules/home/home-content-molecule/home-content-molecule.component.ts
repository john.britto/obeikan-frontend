import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-home-content-molecule',
  templateUrl: './home-content-molecule.component.html',
  styleUrls: ['./home-content-molecule.component.scss']
})
export class HomeContentMoleculeComponent implements OnInit {

  @Input() sectionThreeParaContent: any
  
  constructor() { }

  ngOnInit() {
  }

}
