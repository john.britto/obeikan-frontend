import { Component, OnInit, Input, HostListener } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { CommonUtilityService } from '../../../../services/common-utility-service';
import { appConfig } from 'src/app/app.config';

@Component({
  selector: 'app-banner-molecule',
  templateUrl: './banner-molecule.component.html',
  styleUrls: ['./banner-molecule.component.scss']
})
export class BannerMoleculeComponent implements OnInit {

  @Input() sectionOneBannerContent: any
  @Input() sectionOneDesktopBannerContent: any
  hostName: string = environment.hostName;
  isDesktop: boolean

  constructor(private utilityService: CommonUtilityService) { }

  ngOnInit() {
    if (this.utilityService.isBrowser()) {
      this.isDesktop = window.innerWidth > appConfig.mobileScreenWidth
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (this.utilityService.isBrowser()) {
      this.isDesktop = window.innerWidth > appConfig.mobileScreenWidth
    }
  }

}
