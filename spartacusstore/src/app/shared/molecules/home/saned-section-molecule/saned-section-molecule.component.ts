import { Component, Input, SimpleChanges, OnChanges, ViewChild, ElementRef } from '@angular/core';
import { forkJoin } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { CommonUtilityService } from '../../../../services/common-utility-service';
import { EmitterService } from '../../../../services/emitter.service';

@Component({
  selector: 'app-saned-section-molecule',
  templateUrl: './saned-section-molecule.component.html',
  styleUrls: ['./saned-section-molecule.component.scss']
})
export class SanedSectionMoleculeComponent implements OnChanges {

  @ViewChild('sanedPlans', { static: false }) sanedPlans: ElementRef;
  @Input() sanedPlansContent: any
  @Input() sanedPlanInfoContent: any
  hostName: string = environment.hostName;
  productDetails: any

  constructor(
    private utilityService: CommonUtilityService,
    private emitter: EmitterService) {
    this.emitter.scrollToSaned.subscribe((data) => {
      this.sanedPlans.nativeElement.scrollIntoView({behavior: 'smooth'});
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.sanedPlansContent.currentValue && this.productDetails === undefined) {
      this.getProductDetails()
    }
  }

  getProductDetails = () => {

    let products = [] 

    if(!!this.sanedPlansContent && this.sanedPlansContent.length > 0) {
      this.sanedPlansContent.forEach(item => {
        let itemName = item.content === "HRMS" ? "HRMS_CORE" : item.content;
        products.push(this.utilityService.getRequest(environment.pdpEndPoint + itemName, ''))
      });
    }
    forkJoin([...products]).subscribe((res: any) => {
      if(!!res) this.productDetails = res;
    });
  }
}
