import { Component, Input, OnInit, HostListener } from '@angular/core';
import { environment } from '../../../../../environments/environment'
import { CommonUtilityService } from '../../../../services/common-utility-service';
import { appConfig } from '../../../../app.config';

@Component({
  selector: 'cx-carousal-molecule',
  templateUrl: './carousal-molecule.component.html',
  styleUrls: ['./carousal-molecule.component.scss']
})

export class CarousalMoleculeComponent implements OnInit {

  @Input() carousalData: any
  @Input() carousalDesktopImages: any
  hostName: string = environment.hostName;
  isDesktop: boolean = true

  constructor(private utilityService: CommonUtilityService) { }

  ngOnInit() {
    if (this.utilityService.isBrowser()) {
      this.isDesktop = window.innerWidth > appConfig.mobileScreenWidth
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (this.utilityService.isBrowser()) {
      this.isDesktop = window.innerWidth > appConfig.mobileScreenWidth
    }
  }
}
