import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonUtilityService } from '../../../services/common-utility-service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-about-us-molecule',
  templateUrl: './about-us-molecule.component.html',
  styleUrls: ['./about-us-molecule.component.scss']
})
export class AboutUsMoleculeComponent implements OnInit {
  aboutUsContent: any;
  constructor(private utilityService: CommonUtilityService) { }

  ngOnInit() {
    const aboutUsUrl = environment.aboutUS;
    this.utilityService.getRequest(aboutUsUrl, '').subscribe(data => {
      const response = JSON.parse(JSON.stringify(data));
      if (
        response &&
        response.contentSlots &&
        response.contentSlots.contentSlot
      ) {
        this.aboutUsContent = response.contentSlots.contentSlot.components.component[0];
      }
    });
  }

}
