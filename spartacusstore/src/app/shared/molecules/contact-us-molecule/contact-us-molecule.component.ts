import { Component, OnInit } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import Swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { CommonUtilityService } from '../../../services/common-utility-service';
import { appConfig } from '../../../app.config';
@Component({
  selector: 'app-contact-us-molecule',
  templateUrl: './contact-us-molecule.component.html',
  styleUrls: ['./contact-us-molecule.component.scss']
})
export class ContactUsMoleculeComponent implements OnInit {

  contactUsContent: any
  userDetailsForm: FormGroup;
  validationMessages = {
    name: [{ type: 'required', message: 'Full name is required' }],
    email: [
      { type: 'required', message: 'Email is required' },
      { type: 'email', message: 'Enter a valid email' }
    ],
    message: [{ type: 'required', message: 'Please enter some message' }],
  };

  constructor(
    private fb: FormBuilder,
    private utilityService: CommonUtilityService) { }

  ngOnInit() {

    const aboutUsUrl = environment.contactUS;
    this.utilityService.getRequest(aboutUsUrl, '').subscribe(data => {
      const response = JSON.parse(JSON.stringify(data));
      
     if (
        response &&
        response.contentSlots &&
        response.contentSlots.contentSlot
      ) {
        this.contactUsContent = response.contentSlots.contentSlot.components.component[0];
      }
    });


    // user details form validations
    this.userDetailsForm = this.fb.group({
      name: [null, Validators.required],
      email: [null, Validators.compose([
        Validators.required,
        Validators.email
      ])],
      message: [null, Validators.required]
    });
  }
  onSubmitUserDetails(userMsg) {

    const { name, email, message } = this.userDetailsForm.getRawValue()
      const uri = `${environment.contactUsPostApi}name=${name}&email=${email}&message=${message}`

      let httpHeaders = new HttpHeaders()
            .set(
                'Authorization',
                `Bearer ${this.utilityService.getCookie('AuthToken')}`
            )
            .set('Content-Type', 'application/json');

        let options = {
            headers: httpHeaders,
        };

      this.utilityService.postRequest(uri, '', options).subscribe((res) => {
        if(!!res) this.dialogMsg('success', appConfig.apiResponseMessages.genericSuccessMsg)
       },
       err => {
        if (!!err && !!err.error && !!err.error.errors && !!err.error.errors[0] && !!err.error.errors[0].message) {
          this.dialogMsg('error', err.error.errors[0].message)
        } else this.dialogMsg('error', appConfig.apiResponseMessages.GenericErrorMsg)
       });
    }

    dialogMsg = (type, msg) => {
      Swal.fire({
        icon: type,
        text: msg,
        timer: 15000,
        showCloseButton: true, 
        cancelButtonText:
        '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
        showConfirmButton: false,
        background: 'black',
        toast: true
      })
    }
}
