import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';
import { ServicesModule } from '../../services/services.module';
import { DirectivesModule } from '../directives/directives.module';
import { MatStepperModule, MatInputModule, MatDialogModule, MatFormFieldModule, MatSortModule, MatPaginatorModule, MatButtonModule, MatAutocompleteModule, MatTableModule } from '@angular/material';
import { PipesModule } from '../pipes/pipes.module';
import { HeaderLinksMoleculeComponent } from './common/header-links-molecule/header-links-molecule.component';
import { HeaderNavMoleculeComponent } from './common/header-nav-molecule/header-nav-molecule.component';
import { CarousalMoleculeComponent } from './home/carousal-molecule/carousal-molecule.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LogoMoleculeComponent } from './logo-molecule/logo-molecule.component';
import { ContactUsMoleculeComponent } from './contact-us-molecule/contact-us-molecule.component';
import { ClientLogoComponent } from './home/client-logo-molecule/client-logo.component';
import { SanedSectionMoleculeComponent } from './home/saned-section-molecule/saned-section-molecule.component';
import { RegisterMoleculeComponent } from './register-molecule/register-molecule.component';
import { SignUpComponent } from './home/sign-up-molecule/sign-up.component';
import { FooterComponent } from './common/footer-molecule/footer.component';
import { LoginFormMoleculeComponent } from './login-form-molecule/login-form-molecule.component';
import { AppRoutingModule } from '../../app-routing.module';
import { PaymentMoleculeComponent } from './purchase-path/payment-molecule/payment-molecule.component';
import { OrderSummaryMoleculeComponent } from './purchase-path/order-summary-molecule/order-summary-molecule.component';
import { ReviewMoleculeComponent } from './purchase-path/review-molecule/review-molecule.component';
import { CartSummaryComponent } from './purchase-path/cart-summary/cart-summary.component';
import { SubsSummaryComponent } from './purchase-path/subs-molecule/subs-summary.component';
import { CheckoutBreadcrumbMoleculeComponent } from './purchase-path/checkout-breadcrumb-molecule/checkout-breadcrumb-molecule.component'
import { CartMoleculeComponent } from './purchase-path/cart-molecule/cart-molecule.component';
import { PageHeaderMoleculeComponent } from './common/page-header-molecule/page-header-molecule.component';
import { CartCouponMoleculeComponent } from './purchase-path/cart-coupon-molecule/cart-coupon-molecule.component';
import { LoaderMoleculeComponent } from './common/loader-molecule/loader-molecule.component';
import { OtpLoginMolecule } from './login-form-molecule/login-form-molecule.component';
import { AboutUsMoleculeComponent } from './about-us-molecule/about-us-molecule.component';
import { SuperAdminLandingPageMoleculeComponent } from './super-admin/super-admin-landing-page-molecule/super-admin-landing-page-molecule.component';
import { SuperAdminHRMSMoleculeComponent } from './super-admin/super-admin-hrmsmolecule/super-admin-hrmsmolecule.component';
import { SuperAdminFinanceMoleculeComponent } from './super-admin/super-admin-finance-molecule/super-admin-finance-molecule.component';
import { AdminNavMoleculeComponent } from './admin/admin-nav-molecule/admin-nav-molecule.component';
import { AdminHeaderMoleculeComponent } from './admin/admin-header-molecule/admin-header-molecule.component';
import { HrmsFormMoleculeComponent } from './admin/hrms/hrms-form-molecule/hrms-form-molecule.component';
import { AdminNavMobileMoleculeComponent } from './admin/admin-nav-mobile-molecule/admin-nav-mobile-molecule.component';
import { AdminUploadMoleculeComponent } from './admin/admin-upload-molecule/admin-upload-molecule.component';
import { AdminChartMoleculeComponent } from './admin/admin-chart-molecule/admin-chart-molecule.component';
import { SuperAdminFinanceFormMoleculeComponent } from './super-admin/super-admin-finance-form-molecule/super-admin-finance-form-molecule.component';
import { FinanceManagerFormMoleculeComponent } from './finance-manager-form-molecule/finance-manager-form-molecule.component';
import { FinanceFormMoleculeComponent } from './admin/finance/finance-form-molecule/finance-form-molecule.component';
import { ApprovedDashboardMoleculeComponent } from './profile/approved-dashboard-molecule/approved-dashboard-molecule.component';
import { MaterialReceivedMoleculeComponent } from './profile/approved-dashboard-molecule/material-received-molecule/material-received-molecule.component'
import { ProfilePaymentMoleculeComponent } from './profile/approved-dashboard-molecule/payment-molecule/payment-molecule.component'
import { PoMoleculeComponent } from './profile/approved-dashboard-molecule/po-molecule/po-molecule.component'
import { SoMoleculeComponent } from './profile/approved-dashboard-molecule/so-molecule/so-molecule.component';
import { TableMoleculeComponent } from './profile/approved-dashboard-molecule/table-molecule/table-molecule.component';
import { OperationHistoryMoleculeComponent } from './profile/operation-history-molecule/operation-history-molecule.component';
import { ApproverListMoleculeComponent } from './approver-list-molecule/approver-list-molecule.component';
import { ApproverDetailsMoleculeComponent } from './approver-details-molecule/approver-details-molecule.component'
import { DialogDataExampleDialog } from './register-molecule/register-molecule.component';
import { AccountReceivableMoleculeComponent } from './admin/finance/admin-finance/account-receivable-molecule/account-receivable-molecule.component';
import { UpdateQtyFormModalComponent } from "./admin/finance/finance-form-molecule/finance-form-molecule.component";
import { UpdateMSQtyFormModalComponent } from './admin/finance/admin-finance/account-receivable-molecule/account-receivable-molecule.component';
import { MaterialShippedMoleculeComponent } from './profile/material-shipped-molecule/material-shipped-molecule.component';
import { FinancialEntryMoleculeComponent } from './profile/financial-entry-molecule/financial-entry-molecule.component';
import { PaymentEntryMoleculeComponent } from './profile/financial-entry-molecule/payment-entry-molecule/payment-entry-molecule.component';
import { FinancialEntryTableMoleculeComponent } from './profile/financial-entry-molecule/financial-entry-table-molecule/financial-entry-table-molecule.component'
import { BusinessTripComponent } from './admin/my-sanad-molecules/business-trip/business-trip.component';
import { AllServicesComponent } from './admin/my-sanad-molecules/all-services/all-services.component';
import { ExpenseClaimComponent } from './admin/my-sanad-molecules/expense-claim/expense-claim.component';
import { LeaveManagementComponent } from './admin/my-sanad-molecules/leave-management/leave-management.component';
import { LeaveCancelationComponent } from './admin/my-sanad-molecules/leave-cancelation/leave-cancelation.component';
import { LeaveTicketRequestComponent } from './admin/my-sanad-molecules/leave-ticket-request/leave-ticket-request.component';
import { ClearanceComponent } from './admin/my-sanad-molecules/clearance/clearance.component';
import { HousingAdvanceComponent } from './admin/my-sanad-molecules/housing-advance/housing-advance.component';
import { BusinessTripInternationalComponent } from './admin/my-sanad-molecules/business-trip-international/business-trip-international.component';
import { ExitReentryVisaComponent } from './admin/my-sanad-molecules/exit-reentry-visa/exit-reentry-visa.component';
import { ResignationComponent } from './admin/my-sanad-molecules/resignation/resignation.component';
import { CashAdvanceComponent } from './admin/my-sanad-molecules/cash-advance/cash-advance.component';
import { CompanyLettersComponent } from './admin/my-sanad-molecules/company-letters/company-letters.component';
import { OverTimeComponent } from './admin/my-sanad-molecules/over-time/over-time.component';
import { TimeCorrectionComponent } from './admin/my-sanad-molecules/time-correction/time-correction.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { OtShortHrsComponent } from './admin/my-sanad-molecules/ot-short-hrs/ot-short-hrs.component';
import { RecurringLoanDeductionsComponent } from './admin/my-sanad-molecules/recurring-loan-deductions/recurring-loan-deductions.component';
import { OneTimeDeductionsComponent } from './admin/my-sanad-molecules/one-time-deductions/one-time-deductions.component';
import { OneTimeEarningsComponent } from './admin/my-sanad-molecules/one-time-earnings/one-time-earnings.component';
import { RecurringEarningsComponent } from './admin/my-sanad-molecules/recurring-earnings/recurring-earnings.component';
import { PayrollActionComponent } from './admin/my-sanad-molecules/payroll-action/payroll-action.component';
import { InventoryReportMoleculeComponent } from './admin/finance/inventory-report-molecule/inventory-report-molecule.component';
import { GeneralLedgerMoleculeComponent } from './admin/finance/general-ledger-molecule/general-ledger-molecule.component';
import { TransactionHistoryModalComponent } from './admin/finance/inventory-report-molecule/inventory-report-molecule.component';
import { SalaryElementEntriesSingleMoleculeComponent } from './admin/hrms/salary-element-entries-single-molecule/salary-element-entries-single-molecule.component';
import { SalaryElementEntriesBulkComponent } from './admin/hrms/salary-element-entries-bulk/salary-element-entries-bulk.component'
import { DeleteHRMSSalaryFormModalComponent } from './admin/hrms/salary-element-entries-single-molecule/salary-element-entries-single-molecule.component';
import { SanedWorkspaceMoleculeComponent } from './admin/saned-workspace-molecule/saned-workspace-molecule.component';
import { InfoContentMoleculeComponent } from './common/info-content-molecule/info-content-molecule.component';
import { BannerMoleculeComponent } from './home/banner-molecule/banner-molecule.component';
import { HomeContentMoleculeComponent } from './home/home-content-molecule/home-content-molecule.component';
import { ReasonUpdateFormModalComponent } from './profile/approved-dashboard-molecule/table-molecule/table-molecule.component';
import { HosuingAllowanceMoleculeComponent } from './admin/my-sanad-molecules/hosuing-allowance-molecule/hosuing-allowance-molecule.component';
import { WorkCalenderMoleculeComponent } from './admin/my-sanad-molecules/work-calender-molecule/work-calender-molecule.component';
import { CashAdvanceMoleculeComponent } from './admin/my-sanad-molecules/cash-advance-molecule/cash-advance-molecule.component';
import { OvertimeMoleculeComponent } from './admin/my-sanad-molecules/overtime-molecule/overtime-molecule.component';
import { TimesheetPolicyMoleculeComponent } from './admin/my-sanad-molecules/timesheet-policy-molecule/timesheet-policy-molecule.component';
import { MyProfileMoleculeComponent } from './my-profile-molecule/my-profile-molecule.component';
import { CompanyMoleculeComponent } from './company-molecule/company-molecule.component';

const components = [
  HeaderLinksMoleculeComponent,
  HeaderNavMoleculeComponent,
  CarousalMoleculeComponent,
  LogoMoleculeComponent,
  ContactUsMoleculeComponent,
  ClientLogoComponent,
  SanedSectionMoleculeComponent,
  RegisterMoleculeComponent,
  SignUpComponent,
  FooterComponent,
  LoginFormMoleculeComponent,
  PaymentMoleculeComponent,
  OrderSummaryMoleculeComponent,
  ReviewMoleculeComponent,
  CartSummaryComponent,
  CheckoutBreadcrumbMoleculeComponent,
  CartMoleculeComponent,
  PageHeaderMoleculeComponent,
  CartCouponMoleculeComponent,
  LoaderMoleculeComponent,
  OtpLoginMolecule,
  AboutUsMoleculeComponent,
  SuperAdminLandingPageMoleculeComponent,
  SuperAdminHRMSMoleculeComponent,
  SuperAdminFinanceMoleculeComponent,
  SuperAdminFinanceFormMoleculeComponent,
  AdminNavMoleculeComponent,
  AdminHeaderMoleculeComponent,
  HrmsFormMoleculeComponent,
  AdminNavMobileMoleculeComponent,
  AdminUploadMoleculeComponent,
  AdminChartMoleculeComponent,
  FinanceManagerFormMoleculeComponent,
  FinanceFormMoleculeComponent,
  ApprovedDashboardMoleculeComponent,
  MaterialReceivedMoleculeComponent,
  ProfilePaymentMoleculeComponent,
  PoMoleculeComponent,
  SoMoleculeComponent,
  TableMoleculeComponent,
  OperationHistoryMoleculeComponent,
  ApproverListMoleculeComponent,
  ApproverDetailsMoleculeComponent,
  DialogDataExampleDialog,
  AccountReceivableMoleculeComponent,
  UpdateQtyFormModalComponent,
  UpdateMSQtyFormModalComponent,
  MaterialShippedMoleculeComponent,
  FinancialEntryMoleculeComponent,
  PaymentEntryMoleculeComponent,
  FinancialEntryTableMoleculeComponent,
  BusinessTripComponent,
  AllServicesComponent,
  ExpenseClaimComponent,
  LeaveManagementComponent,
  LeaveCancelationComponent,
  LeaveTicketRequestComponent,
  ClearanceComponent,
  HousingAdvanceComponent,
  BusinessTripInternationalComponent,
  ExitReentryVisaComponent,
  ResignationComponent,
  CashAdvanceComponent,
  CompanyLettersComponent,
  OverTimeComponent,
  TimeCorrectionComponent,
  OtShortHrsComponent,
  RecurringLoanDeductionsComponent,
  OneTimeDeductionsComponent,
  OneTimeEarningsComponent,
  RecurringEarningsComponent,
  InventoryReportMoleculeComponent,
  GeneralLedgerMoleculeComponent,
  TransactionHistoryModalComponent,
  PayrollActionComponent,
  SalaryElementEntriesSingleMoleculeComponent,
  SalaryElementEntriesBulkComponent,
  DeleteHRMSSalaryFormModalComponent,
  SanedWorkspaceMoleculeComponent,
  InfoContentMoleculeComponent,
  MyProfileMoleculeComponent,
  BannerMoleculeComponent,
  HomeContentMoleculeComponent,
  ReasonUpdateFormModalComponent,
  SanedWorkspaceMoleculeComponent,
  HosuingAllowanceMoleculeComponent,
  WorkCalenderMoleculeComponent,
  CashAdvanceMoleculeComponent,
  OvertimeMoleculeComponent,
  CompanyMoleculeComponent,
  TimesheetPolicyMoleculeComponent,
  SubsSummaryComponent
];

@NgModule({
  imports: [
    ServicesModule,
    DirectivesModule,
    PipesModule,
    NgbModule,
    MatDialogModule,
    CommonModule,
    RouterModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    AppRoutingModule,
    MatStepperModule,
    MatInputModule,
    MatButtonModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    NgxMaterialTimepickerModule
  ],
  exports: [...components, RouterModule],
  declarations: [...components],
  entryComponents: [OtpLoginMolecule, DialogDataExampleDialog, UpdateQtyFormModalComponent, UpdateMSQtyFormModalComponent, UpdateMSQtyFormModalComponent,
    TransactionHistoryModalComponent, DeleteHRMSSalaryFormModalComponent, ReasonUpdateFormModalComponent],
  providers: [],
})

export class MoleculesModule { }
