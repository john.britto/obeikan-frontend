import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import {
  CheckoutService,
  CARD_TYPES,
  MONTH_LIST,
  ACCEPT_TERMS_MSG,
} from '../../../../services/checkout.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute , Router} from '@angular/router';
import { CommonUtilityService } from '../../../../services/common-utility-service';
import { EmitterService } from '../../../../services/emitter.service';
import Swal from 'sweetalert2';
import { LoginService } from '../../../../services/login.service'

@Component({
  selector: 'app-payment-molecule',
  templateUrl: './payment-molecule.component.html',
  styleUrls: ['./payment-molecule.component.scss'],
})
export class PaymentMoleculeComponent implements OnInit {
  paymentDetails: any;
  defaultPayment: boolean;
  paymentForm: FormGroup;
  checkoutBreadcrumb: any;
  termsAndConditions: any = false;
  validationMessages = {
    cardType: [{ type: 'required', message: 'Please select any option' }],
    name: [
      { type: 'required', message: 'name is required' },
      { type: 'name', message: 'Enter a valid name' },
    ],
    cardNumber: [
      { type: 'required', message: 'cardNumber is required' },
      { type: 'pattern', message: 'Please enter a valid card number' },
    ],
    cvvNumber: [
      { type: 'required', message: 'cvvNumber is required' },
      { type: 'pattern', message: 'Please enter a valid cvv number' },
    ],
    message: [{ type: 'required', message: 'Please enter some message' }],
    expMonth: [{ type: 'required', message: 'Please select any option' }],
    expYear: [{ type: 'required', message: 'Please select any option' }],
  };

  selectedValue: string;
  cardType: any = CARD_TYPES;
  setAsDefault: any = false;
  expMonth: any = MONTH_LIST;
  expYear: any;

  constructor(
    private fb: FormBuilder,
    private checkoutService: CheckoutService,
    private matSnack: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    private utilityService: CommonUtilityService,
    private emitter: EmitterService,
    private loginService: LoginService
  ) {
    this.checkoutService.paymentContext.subscribe(context => {
      if (this.paymentForm !== undefined && this.paymentForm.valid) {
        this.checkoutService.setPaymentDetails = this.paymentForm;
        this.paymentDetails = context;
        this.defaultPayment = this.checkoutService.getDefaultPayment;
        this.paymentDetails = this.checkoutService.getPaymentDetails;
      }
    });

    this.paymentDetails = this.checkoutService.getPaymentDetails;
    this.defaultPayment = this.checkoutService.getDefaultPayment;
  }

  ngOnInit() {
    let year = new Date().getFullYear();
    let range = [];

    for (let i = 0; i < 10; i++) {
      range.push(parseInt(String(year + i)));
      this.expYear = range;
    }

    const CARD_PATTERN = '^[0-9]{16}$';
    const CVV_PATTERN = '^[0-9]{3}$';

    this.paymentForm = this.fb.group({
      cardType: [null, Validators.compose([Validators.required])],
      name: [null, Validators.compose([Validators.required])],
      cardNumber: [
        null,
        Validators.compose([
          Validators.required,
          Validators.pattern(CARD_PATTERN),
        ]),
      ],
      cvvNumber: [
        null,
        Validators.compose([
          Validators.required,
          Validators.pattern(CVV_PATTERN),
        ]),
      ],
      expMonth: [null, Validators.compose([Validators.required])],
      expYear: [null, Validators.compose([Validators.required])],
    });
  }

  setTerms(e) {
    this.termsAndConditions = e.checked;
  }

  onSubmitPaymentDetails(data) {
    if (this.termsAndConditions) {
      this.checkoutService.placeOrder(this.termsAndConditions).subscribe(
        (res: any) => {
          this.checkoutService.orderDetails = res
          this.utilityService.setCookie('orderId', res.code);
          const order = [];
          order.push(res.code);
          if (this.utilityService.getCookie('isSuperAdmin')) {
              if (this.utilityService.getCookie('superAdminOrders')) {
                  const codes = JSON.parse(this.utilityService.getCookie('superAdminOrders'));
                  codes.push(order[0]);
                  this.utilityService.setCookie('superAdminOrders', JSON.stringify(codes));
              } else {
                this.utilityService.setCookie('superAdminOrders', JSON.stringify(order));
              }
              this.emitter.superAdminSource.next(true);
          } else {
            const authToken = this.utilityService.getCookie('AuthToken')
            const userId = this.utilityService.getLocalStorage('displayUid')
            this.loginService.fetchOrderDetails(authToken, userId).subscribe((res: any) => {
              if (res && res.pagination && res.pagination.totalResults === 0) {
                this.router.navigateByUrl('/order-confirmation');
              } else {
                this.utilityService.setCookie('isSuperAdmin', true);
                const code = [];
                res.orders.map(order => code.push(order.code));
                this.utilityService.setCookie('superAdminOrders', JSON.stringify(code));
                this.emitter.superAdminSource.next(true);
                this.router.navigateByUrl('/order-confirmation');
              }
            },
              err => {
                this.router.navigateByUrl('/order-confirmation');              })
          }
          this.router.navigateByUrl('/order-confirmation');
        },
        err => {}
      );
    } else {
      Swal.fire({
        icon: 'info',
        text: ACCEPT_TERMS_MSG,
        timer: 15000,
        showCloseButton: true, 
        cancelButtonText:
        '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
        showConfirmButton: false,
        background: 'black',
        toast: true
      })
    }
  }
}
