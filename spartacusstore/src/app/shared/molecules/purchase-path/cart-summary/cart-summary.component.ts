import { Component, OnInit, Input  } from '@angular/core';
import { environment } from '../../../../../environments/environment'
import { MatSnackBar } from '@angular/material/snack-bar';
import { CartService, CART_UPDATE_FAILURE_MSG, UPDATE_CART } from '../../../../services/cart.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-cart-summary',
  templateUrl: './cart-summary.component.html',
  styleUrls: ['./cart-summary.component.scss']
})
export class CartSummaryComponent implements OnInit {

  @Input() cartItems: any;
  @Input() code: any;
  itemQtn: any = 1;
  hostName: any = environment.hostName;
  cartContext: any;
  productType: any;
  constructor(private cartService: CartService, private matSnack: MatSnackBar) { }

  ngOnInit() {
    this.cartItems = this.cartItems.map((each: any) => {
      let hrmsPrice = 'SAR ' + (each.totalPrice.value / each.quantity).toFixed(2);
      return {
          price: (each.product.code !== "HRMS") ? 'SAR ' + 
          each.product.hrmsPriceData.priceDataList[0].value.toFixed(2) : 
          hrmsPrice,
          ...each
      }
    });
  }

  updateQtn = (type, index, entry) => {
    this.cartItems[index].quantity = this.cartItems[index].quantity + parseInt(type)
    this.updateCart(this.cartItems[index].quantity, entry);
  }

  updateInput = (e, index, entry) => {
    if(parseInt(e) >= 1 && parseInt(e) <= 99) {
      this.cartItems[index].quantity = parseInt(e)
    } else {
      this.cartItems[index].quantity = 99;
    }
    this.updateCart(this.cartItems[index].quantity, entry)
  }

  updateCart = (qtn, entry) => {
    this.cartService.updateCart(qtn, entry).subscribe( (res: any) => {
      this.cartContext = UPDATE_CART;
      this.cartService.changeCartContext(this.cartContext)
    },
    err => {
      Swal.fire({
        icon: 'success',
        text: CART_UPDATE_FAILURE_MSG,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
        showConfirmButton: false,
        background: 'black',
        toast: true
      })
    });
  }

  deleteCart = entry => {
    this.cartService.deleteCart(entry).subscribe((res: any) => {
      this.cartContext = UPDATE_CART;
      this.cartService.changeCartContext(this.cartContext)
    },
    err => {
      Swal.fire({
        icon: 'success',
        text: CART_UPDATE_FAILURE_MSG,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
        showConfirmButton: false,
        background: 'black',
        toast: true
      })
    });
  }

}
