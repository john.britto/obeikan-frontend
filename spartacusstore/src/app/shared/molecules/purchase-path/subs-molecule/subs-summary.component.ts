import { Component, OnInit, Input  } from '@angular/core';
import { environment } from '../../../../../environments/environment'
import { MatSnackBar } from '@angular/material/snack-bar';
import { CartService, SUBS_UPDATE_FAILURE_MSG, UPDATE_CART } from '../../../../services/cart.service';
import Swal from 'sweetalert2';
import { CommonUtilityService } from '../../../../services/common-utility-service';
import { PdpService } from '../../../../services/pdp.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-subs-summary',
  templateUrl: './subs-summary.component.html',
  styleUrls: ['./subs-summary.component.scss']
})
export class SubsSummaryComponent implements OnInit {

  @Input() subsItems: any;
  tableData: any = [];
  itemQtn: any = 1;
  hostName: any = environment.hostName;
  cartContext: any;
  productType: any;
  month: any = { HRMS_CORE: 1, Finance_CORE: 1}
  constructor(private cartService: CartService, private router: Router, private matSnack: MatSnackBar, private pdpService: PdpService, private utilityService: CommonUtilityService) { }

  ngOnInit() {
    this.subsItems.map((each: any) => {
    //   this.cartContext = UPDATE_CART;
    //   this.cartService.changeCartContext(this.cartContext)
      
      this.utilityService.getRequest(environment.pdpEndPoint + each.productCode, '').subscribe( (res: any) => {
        this.tableData.push(res);
      }, err => {
        Swal.fire({
          icon: 'error',
          text: SUBS_UPDATE_FAILURE_MSG,
        timer: 15000,
        showCloseButton: true, 
        cancelButtonText:
        '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
          showConfirmButton: false,
          background: 'black',
          toast: true
        })
      })
    })

    // this.subsItems = this.subsItems.map((each: any) => {
    //   let hrmsPrice = 'SAR ' + (each.totalPrice.value / each.quantity).toFixed(2);
    //   return {
    //       price: (each.product.code !== "HRMS") ? 'SAR ' + 
    //       each.product.hrmsPriceData.priceDataList[0].value.toFixed(2) : 
    //       hrmsPrice,
    //       ...each
    //   }
    // });
  }

  checkout(item) {
    const userId = this.utilityService.getLocalStorage('displayUid');
    const url = environment.hostName + environment.cartAPI + userId + '/carts';
    this.pdpService.createCart(url).subscribe((res: any) => {
      const result = JSON.parse(JSON.stringify(res));
      if (result && result.code) {
        this.addToCart(item, url, result.code);
      }
    }, (err) => {
      Swal.fire({
        icon: 'error',
        text: "something went wrong",
        timer: 2000,
        showConfirmButton: false,
        background: 'black',
        toast: true
      })
    });
  }

  addToCart(item, url, cartId) {
    let qty;
    // Add product to the cart
    //this.isHrms && this.addSubscription();
    url = url + '/' + cartId + '/entries';
    qty = item.quantity;
    const prodCode = item.productCode;
    this.pdpService.addToCartRenewal(prodCode, url, qty, Number(this.month[item.productCode])).subscribe((res: any) => {
      const result = JSON.parse(JSON.stringify(res));
      if (result.statusCode === 'success') {
        this.utilityService.setCookie('cartId', cartId);
        this.router.navigateByUrl('/checkout');
      }
    }, (err) => {
      Swal.fire({
        icon: 'error',
        text: "something went wrong",
        timer: 15000,
        showCloseButton: true,
        cancelButtonText:
          '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
        showConfirmButton: false,
        background: 'black',
        toast: true
      })
    });
  }

  getInfo(code, key) {
    const getValue = (object, keys) => keys.split('.').reduce((o, k) => (o || {})[k], object);
    return getValue(this.tableData.filter(e => e.code === code)[0],key)
  }

  updateQtn = (type, index, entry) => {
    this.subsItems[index].quantity = this.subsItems[index].quantity + parseInt(type)
    this.updateCart(this.subsItems[index].quantity, entry);
  }

  updateInput = (e, index, entry) => {
    if(parseInt(e) >= 1 && parseInt(e) <= 12) {
      this.month = {
        ...this.month,
        [entry]: e
      }
    } else if(e !=="") {
      this.month = {
        ...this.month,
        [entry]: "12"
      }
    }
  }

  updateCart = (qtn, entry) => {
    this.cartService.updateCart(qtn, entry).subscribe( (res: any) => {
      this.cartContext = UPDATE_CART;
      this.cartService.changeCartContext(this.cartContext)
    },
    err => {
      Swal.fire({
        icon: 'error',
        text: SUBS_UPDATE_FAILURE_MSG,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
        showConfirmButton: false,
        background: 'black',
        toast: true
      })
    });
  }

  deleteCart = entry => {
    this.cartService.deleteCart(entry).subscribe((res: any) => {
      this.cartContext = UPDATE_CART;
      this.cartService.changeCartContext(this.cartContext)
    },
    err => {
      Swal.fire({
        icon: 'error',
        text: SUBS_UPDATE_FAILURE_MSG,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
        showConfirmButton: false,
        background: 'black',
        toast: true
      })
    });
  }

}
