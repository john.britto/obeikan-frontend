import { Component, OnInit, Inject, ElementRef, ViewChild, AfterViewInit, ViewEncapsulation } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { PdpService } from '../../../services/pdp.service'
import {
  LoginService,
  INVALID_CREDENTIAL,
  INVALID_OTP,
  EMAIL_LINK_EXPIRED,
  USER_NOT_FOUND
} from '../../../services/login.service';
import { CommonUtilityService } from '../../../services/common-utility-service';
import { EmitterService } from '../../../services/emitter.service';
import Swal from 'sweetalert2';
import { appConfig } from '../../../app.config'


@Component({
  selector: 'app-login-form-molecule',
  templateUrl: './login-form-molecule.component.html',
  styleUrls: ['./login-form-molecule.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class LoginFormMoleculeComponent implements OnInit, AfterViewInit {

  @ViewChild('email', { static: false }) emailElement: ElementRef;
  @ViewChild('password', { static: false }) passwordElement: ElementRef;
  @ViewChild('otp', { static: false }) otpElement: ElementRef;
  userContext: any;
  loginError: boolean = false;
  userLoginForm: FormGroup;
  userOTPForm: FormGroup;
  userEmailForm: FormGroup;
  returnUrl: any;
  validationMessages = {
    email: [
      { type: 'required', message: 'Email is required' },
      { type: 'email', message: 'Enter a valid email' },
    ],
    password: [{ type: 'required', message: 'Password is required' }],
    otp: [{ type: 'required', message: 'Otp is required' }],
    message: [{ type: 'required', message: 'Please enter some message' }],
  };
  userName: any = "";
  userToken: any;
  buttonType: string;
  otp: any;
  loginForm: FormGroup = this.userEmailForm;
  isLoggedIn: boolean = false;
  prePopulateEmail: string = '';
  isPassword: boolean = false
  isSendingOTPInProgress: boolean = false
  OTPLinkText: any = 'Resend OTP'
  isLoaded: boolean = false

  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private router: Router, private route: ActivatedRoute,
    private utilityService: CommonUtilityService,
    private pdpService: PdpService,
    private matSnack: MatSnackBar,
    public dialog: MatDialog,
    private emitter: EmitterService,
    private el: ElementRef
  ) {
    this.loginService.userContext.subscribe(context => {
      this.userContext = context;
    });

    if(this.utilityService.getCookie('isAuthenticated')) {
      this.router.navigate(['/'])
    }
  }

  ngAfterViewInit() {
    this.emailElement.nativeElement.focus();
  }

  ngAfterViewChecked() {
    if (this.passwordElement && this.buttonType === 'password' && !this.isLoaded) {
      this.isLoaded = true
      setTimeout(() => {
        this.passwordElement.nativeElement.focus();
      }, 100);
    }
    if (this.otpElement && this.buttonType === 'otp') this.otpElement.nativeElement.focus();
  }

  ngOnInit() {

    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
    this.userLoginForm = this.fb.group({
      password: [null, Validators.compose([Validators.required])],
    });

    this.userOTPForm = this.fb.group({
      otp: [null, Validators.compose([Validators.required])],
    });
    this.userEmailForm = this.fb.group({
      email: [
        null,
        Validators.compose([Validators.required, Validators.email]),
      ]
    });

    this.loginForm = this.userEmailForm;
    this.route.queryParams.subscribe(params => {
      setTimeout(() => {
        this.loginForm.get('email').setValue(params.emailId);
      }, 0);
    });

  }

  onSubmitLoginDetails(type: any) {
    this.isPassword = true
    const userData = this.loginForm.value
    if (!type) {

      if (!!userData && !!userData.email) {
        this.loginService.fetchToken(userData).subscribe((res: any) => {
          if (!!res && res.access_token) {
            const token = res.access_token
            const userId = userData.email
            this.userToken = token;
            this.fetchCustomerDetails(token, userId)
          } else {
            this.showErrorMsg(INVALID_CREDENTIAL)
          }
        },
          err => {
            this.showErrorMsg(INVALID_CREDENTIAL)
          });
      }
    } else if (type === "password") {
      userData.email = this.userName
      userData.password = this.userLoginForm.value.password
      this.loginService.userLogin(userData).subscribe((res: any) => {
        this.isLoggedIn = true;
        this.fetchCustomerDetails(this.userToken, this.userName)
      },
        err => {
          if (err && err.error && err.error.error === 'invalid_grant') {
            this.showErrorMsg(INVALID_CREDENTIAL)
          }
        })
    } else if (type === 'otp') {
      userData.otp = this.userOTPForm.value.otp
      this.loginService.activateProfile(this.userToken, this.userName, userData.otp).subscribe((res: any) => {
        if (res.active) {

          this.fetchCustomerDetails(this.userToken, this.userName)

        } else {

          this.showErrorMsg(EMAIL_LINK_EXPIRED)

        }
      },
        err => {

          if (err && err.error && err.error.errors[0].message === EMAIL_LINK_EXPIRED) {

            this.showErrorMsg(EMAIL_LINK_EXPIRED)

          } else if (err && err.error && err.error.errors[0].message === 'Invalid OTP') {

            this.showErrorMsg(INVALID_OTP)

          } else {

            this.showErrorMsg(INVALID_OTP)

          }

        })
    }
  }

  showErrorMsg = (errMsg) => {
    Swal.fire({
      icon: 'error',
      text: errMsg,
      timer: 15000,
      showCloseButton: true, 
      cancelButtonText:
      '<i class="fa  fa-thumbs-down" style="text-align:right"></i>',
      showConfirmButton: false,
      background: 'black',
      toast: true
    })
  }

  openOtpDialog(token, userId): void {
    const dialogRef = this.dialog.open(OtpLoginMolecule, {
      width: '250px',
      data: { otp: '' }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.otp = result;

      this.loginService.activateProfile(token, userId, this.otp).subscribe((res: any) => {
        if (res.active) {

          this.fetchCustomerDetails(token, userId)

        } else {

          this.showErrorMsg(EMAIL_LINK_EXPIRED)

        }
      },
        err => {

          if (err && err.error && err.error.errors[0].message === EMAIL_LINK_EXPIRED) {

            this.showErrorMsg(EMAIL_LINK_EXPIRED)

          } else if (err && err.error && err.error.errors[0].message === 'Invalid OTP') {

            this.showErrorMsg(INVALID_OTP)

          } else {

            this.showErrorMsg(INVALID_OTP)

          }

        })
    });
  }

  fetchCustomerDetails = (token, userId) => {

    this.loginService.fetchUserDetails(token, userId).subscribe((res: any) => {
      if (res.active) {
        this.userName = userId;
        this.loginForm = this.userLoginForm;
        this.buttonType = 'password';
      } else {
        this.userName = userId;
        this.buttonType = 'otp';
        this.loginForm = this.userOTPForm;
      }

      if (this.isLoggedIn) {
        this.userContext.displayUID = res.displayUid;
        this.userContext.displayName = res.name;
        this.userContext.isAuthenticated = true;

        this.loginService.powerBiData = !!res.powerBiUrls ? res.powerBiUrls : null
        this.utilityService.setLocalStorage('displayName', this.userContext.displayName);
        this.utilityService.setLocalStorage('displayUid', this.userContext.displayUID);
        this.utilityService.setLocalStorage('accessbility', JSON.stringify(res.accessbility));
        this.utilityService.setCookie('isAuthenticated', true);
        this.utilityService.getAuth();

        if (res.customerRole && Array.isArray(res.customerRole.customerRoleList)) {
          this.loginService.customerRoles = res.customerRole.customerRoleList
          this.utilityService.setCookie('customerRoles', JSON.stringify(res.customerRole.customerRoleList));
          this.loginService.changeUserContext(this.userContext);

          res.customerRole.customerRoleList.map((role: any) => {
            switch (role) {
              case 'SuperAdmin':
                this.utilityService.removeCookie('adminType')
                this.fetchOrderDetails(token, userId)
                return;
              case 'VerificationUser':
                this.utilityService.setCookie('adminType', role);
                this.router.navigate(['/verification-admin'])
                return;
              case ('Finance' || 'FinanceManager'):
                this.utilityService.setCookie('adminType', role);
                if (res.superAdminInfo && res.superAdminInfo.superAdminInfoList[0]) {
                  this.utilityService.setCookie('financeSuperUser', res.superAdminInfo.superAdminInfoList[0].emailId)
                }
                this.router.navigate(['/admin/finance'])
                return;
              case 'HR':
              case 'GeneralManager':
                this.utilityService.setCookie('adminType', role);
                this.router.navigate(['/admin/hrms'])
                return;
              case 'HREmployee':
                this.utilityService.setCookie('adminType', role);
                this.router.navigate(['/admin/my-saned'])
                return;
              default:
                return this.router.navigate(['/']);
            }
          });
        } else {
          this.loginService.changeUserContext(this.userContext);
          this.router.navigate(['/'])

        }
      }

    },
      err => {
        this.showErrorMsg(USER_NOT_FOUND)
      })
  }

  fetchOrderDetails = (token, userId) => {
    this.loginService.fetchOrderDetails(token, userId).subscribe((res: any) => {
      if (res && res.pagination && res.pagination.totalResults === 0) {

        // should redirect to pdp
        this.router.navigateByUrl(this.returnUrl);
      } else {
        this.userContext.isSuperUser = true
        this.loginService.changeUserContext(this.userContext);
        this.utilityService.setCookie('isSuperAdmin', true);
        const code = [];
        res.orders.map(order => code.push(order.code));
        this.utilityService.setCookie('superAdminOrders', JSON.stringify(code));
        this.emitter.superAdminSource.next(true);
        // should redirect to saned service
        this.router.navigateByUrl(this.returnUrl);
      }
    },
      err => {
        this.router.navigateByUrl(this.returnUrl);
      })
  }

  fetchCartId = userId => {
    const url = environment.hostName + environment.cartAPI + userId + '/carts';

    this.pdpService.checkCartExist(url).subscribe((data: any) => {

      const response = JSON.parse(JSON.stringify(data))
      if (response && response.carts && response.carts.length > 0 && response.carts[0].code) {
        this.utilityService.setCookie('cartId', response.carts[0].code)
      }
      this.router.navigateByUrl(this.returnUrl);
    },
      err => {
        this.router.navigateByUrl(this.returnUrl);
      })
  }

  resendOTP = e => {

    e.preventDefault()

    if (e.target.text === 'Resend OTP') {
      this.isSendingOTPInProgress = true

      this.loginService.resendOTP(this.userToken, this.userName).subscribe((res: any) => {
        this.isSendingOTPInProgress = false
        this.OTPLinkText = 'OTP Sent'

        setTimeout(() => {
          this.OTPLinkText = 'Resend OTP'
        }, 1500);
      },
        err => {
          this.isSendingOTPInProgress = false
          this.OTPLinkText = 'Resend OTP'
          this.showErrorMsg(appConfig.apiResponseMessages.GenericErrorMsg)
        })
    }
  }

}

@Component({
  selector: 'otp-login-molecule',
  templateUrl: './otp-login-molecule.component.html',
})
export class OtpLoginMolecule {

  constructor(
    public dialogRef: MatDialogRef<OtpLoginMolecule>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

interface DialogData {
  otp: any;
}
