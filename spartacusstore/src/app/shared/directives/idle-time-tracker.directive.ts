
import { Directive, HostListener, OnInit, OnDestroy, PLATFORM_ID, Inject, Output, EventEmitter } from '@angular/core';

import { environment } from './../../../environments/environment';
import { appConfig } from '../../app.config';
import { LoginService } from '../../services/login.service';
import { CommonUtilityService } from '../../services/common-utility-service'

@Directive({
    selector: '[appIdleTimeTracker]'
})
export class IdleTimeTrackerDirective implements OnDestroy, OnInit {
    @Output() idleTimeHandler = new EventEmitter<boolean>();
    lastActivityOn: number;
    public hostName: any;
    public timerId: any;

    constructor(
        private loginService: LoginService,
        private utilityService: CommonUtilityService) {
    }

    ngOnDestroy() {
        this.utilityService.removeCookie('isAuthenticated')
        clearInterval(this.timerId);
    }

    ngOnInit() {
        this.trackIdleTime();

        //idle timer auto logout on session timeout
        if (this.utilityService.isBrowser()) {
            this.timerId = setInterval(() => {
                if (this.utilityService.getCookie('isAuthenticated')) {
                    if (this.lastActivityOn !== undefined && this.lastActivityOn > 0) {
                        const inactiveTime = Date.now() - this.lastActivityOn;
                        if (inactiveTime > (appConfig.sessionTimeoutInterval * 60 * 1000)) {
                            this.idleTimeHandler.emit(true);
                            this.loginService.emitIdleTracker(true);
                            this.utilityService.removeCookie('isAuthenticated')
                            this.lastActivityOn = 0;
                        }
                    }
                }
            }, 5000);
        }
    }

    // Listens to the key press event
    @HostListener('keypress') onKeypress() {
        this.trackIdleTime();
    }
    // Listens to the mouse over event
    @HostListener('mouseover') onMouseOver() {
        this.trackIdleTime();
    }
    // Listens to the mouse enter event
    @HostListener('mouseenter') onMouseEnter() {
        this.trackIdleTime();
    }
    // Listens to the mouse move event
    @HostListener('mousemove') onMouseMove() {
        this.trackIdleTime();
    }
    // Listens to the mouse down event
    @HostListener('mousedown') onMouseDown() {
        this.trackIdleTime();
    }
    // Listens to the mouse up event
    @HostListener('mouseup') onMouseUp() {
        this.trackIdleTime();
    }
    // Listens to the mouse leave event
    @HostListener('mouseleave') onMouseLeave() {
        this.trackIdleTime();
    }
    // Listens to the mouse out event
    @HostListener('mouseout') onMouseOut() {
        this.trackIdleTime();
    }
    // Listens to the click event
    @HostListener('click') onClick() {
        this.trackIdleTime();
    }
    // Listens to the double click event
    @HostListener('dblclick') onDblClick() {
        this.trackIdleTime();
    }
    // Listens to the scroll event
    @HostListener('scroll') onScroll() {
        this.trackIdleTime();
    }

    // Listens to the touch event on mobile device
    @HostListener('touchmove') onTouchMove() {
        this.trackIdleTime();
    }

    // function to keep track on signed in user to get the idle time to sign out after specific inactive time
    trackIdleTime() {
        this.lastActivityOn = Date.now();
    }
}
