import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IdleTimeTrackerDirective } from './idle-time-tracker.directive';


@NgModule({
    imports: [
        CommonModule
     ],
    exports: [ IdleTimeTrackerDirective ],
    declarations: [ IdleTimeTrackerDirective ],
    providers: [],
})
export class DirectivesModule { }
