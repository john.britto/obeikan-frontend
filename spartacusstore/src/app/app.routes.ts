import { Routes } from '@angular/router';

import { HOME_ROUTES } from './routes/home.routes'
import { HRMS_ROUTES } from './routes/hrms.routes';
import { FINANCE_ROUTES } from './routes/finance.routes';
import { FINANCE_MANAGER_ROUTES } from './routes/finance-manager.routes';
import { VERIFICATION_ADMIM_ROUTES } from './routes/verification-admin.routes';
import { AuthGuard } from './services/auth-guard.service';
import { NotFoundOrganismComponent } from './shared/organisms/not-found-organism/not-found-organism.component';
import { HomeTemplateComponent } from './templates/home-template/home-template.component';
import { HrmsTemplateComponent } from './templates/hrms-template/hrms-template.component';
import { FinanceTemplateComponent } from './templates/finance-template/finance-template.component';
import { VerificationAdminTemplateComponent } from './templates/verification-admin-template/verification-admin-template.component';
import { FinanceManagerTemplateComponent } from './templates/finance-manager-template/finance-manager-template.component';

export const routes: Routes = [{
  path: '',
  component: HomeTemplateComponent,
  children: HOME_ROUTES
},
{
  path: 'admin',
  component: HrmsTemplateComponent,
  children: HRMS_ROUTES,
  canActivate: [AuthGuard]
},
{
  path: 'finance',
  component: FinanceTemplateComponent,
  children: FINANCE_ROUTES
},
{
  path: 'verification-admin',
  component: VerificationAdminTemplateComponent,
  children: VERIFICATION_ADMIM_ROUTES
},
{
  path: 'financeManager',
  component: FinanceManagerTemplateComponent,
  children: FINANCE_MANAGER_ROUTES
},
{
  path: '**',
  redirectTo: '404',
  data: [{
    pageName: 'Not Found',
  }]
},
{
  path: '404',
  component: NotFoundOrganismComponent,
  pathMatch: 'full',
  data: [{
    pageName: 'Not Found',
  }]
}
];
