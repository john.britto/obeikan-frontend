import { Component, OnInit } from '@angular/core';
import { CommonUtilityService } from './services/common-utility-service'
import { LoginService } from './services/login.service'
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { AdminService } from './services/admin.service'



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit  {
  userContext: any
  subscription: Subscription;

  constructor(
    private utilityService: CommonUtilityService,
    private loginService: LoginService,
    private router: Router,
    private adminService: AdminService) {
    this.utilityService.getAuth();
    this.loginService.userContext.subscribe(context => {
      this.userContext = context;
    });
  }

  ngOnInit() {
    this.subscription = this.loginService.idleTrackerListener().subscribe((res: any) => {
      if(res) {
        this.adminService.onLogout()
      }
    })
    if(this.utilityService.getCookie('isAuthenticated')) {
      this.userContext.displayName = this.utilityService.getLocalStorage('displayName')
      this.userContext.displayUID = this.utilityService.getLocalStorage('displayUid')
      this.userContext.isAuthenticated = this.utilityService.getCookie('isAuthenticated')

      this.loginService.changeUserContext(this.userContext)
    } else this.loginService.removeLocalStorage()

    this.router.events.subscribe(event => {
      // Scroll to top if accessing a page, not via browser history stack
      if (event instanceof NavigationEnd) {
        if(this.utilityService.isBrowser()) {
          window.scrollTo(0, 0);
        }
      }
    });
  }
}
