import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServicesModule } from '../services/services.module';
import { SharedModule } from '../shared/shared.module';
import { HomeTemplateComponent } from './home-template/home-template.component';
import { HrmsTemplateComponent } from './hrms-template/hrms-template.component';
import { FinanceTemplateComponent } from './finance-template/finance-template.component';
import { VerificationAdminTemplateComponent } from './verification-admin-template/verification-admin-template.component';
import { FinanceManagerTemplateComponent } from './finance-manager-template/finance-manager-template.component'


const components = [
    HomeTemplateComponent,
    HrmsTemplateComponent,
    FinanceTemplateComponent,
    VerificationAdminTemplateComponent
]

@NgModule({
    imports: [
        CommonModule,
        ServicesModule,
        SharedModule
    ],
    exports: [
        ...components
    ],
    declarations: [
        ...components,
        FinanceManagerTemplateComponent,
    ],
    providers: [],
})
export class TemplateModule { }
