import { Component, OnInit } from '@angular/core';
import { CommonUtilityService } from '../../services/common-utility-service';
import { Router, NavigationEnd  } from '@angular/router';

@Component({
  selector: 'app-home-template',
  templateUrl: './home-template.component.html',
  styleUrls: ['./home-template.component.scss']
})
export class HomeTemplateComponent implements OnInit {

  userType: any
  isFloatingHeader: boolean = false
  routePath: any
  previousUrl: string;
  currentUrl: string;

  isBlackNav: boolean = false
  blackNavPages: string[] = ['register', 'login','about-saned', 'contactus', 'cart','superadmin', 'checkout']
  floatingHeaderPage: string[] = ['pdp', 'register', '', 'login', 'about-saned', 'contactus', 'cart', 'superadmin', 'checkout']
  isConfirmationScreen: boolean = false

  constructor(
    private utilityService: CommonUtilityService,
    private router: Router
  ) { 

    this.currentUrl = this.router.url;

    router.events.subscribe((val) => {
      if(val instanceof NavigationEnd) {
        this.previousUrl = this.currentUrl;
        this.currentUrl = val.url;

        if(this.previousUrl === '/order-confirmation' && this.currentUrl === '/checkout/payment') this.router.navigate(['/'])

        this.routePath = val.url.split('/')[1].toLocaleLowerCase()
        this.routePath = this.routePath.split('?')[0]

        this.isBlackNav = this.blackNavPages.includes(this.routePath)
        this.isFloatingHeader = this.floatingHeaderPage.includes(this.routePath)

        if(this.routePath === 'order-confirmation') this.isConfirmationScreen = true
        else this.isConfirmationScreen = false
      }
  });

    this.userType = this.utilityService.getCookie('adminType')

    if(!!this.userType) {
      this.router.navigate(['/admin/hrms'])
    }
  }

  ngOnInit() {
  }

}
