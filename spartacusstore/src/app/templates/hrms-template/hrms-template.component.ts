import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CommonUtilityService } from '../../services/common-utility-service';
import { Router, NavigationEnd } from '@angular/router';
import { AdminService } from '../../services/admin.service'

@Component({
  selector: 'app-hrms-template',
  templateUrl: './hrms-template.component.html',
  styleUrls: ['./hrms-template.component.scss']
})
export class HrmsTemplateComponent implements OnInit {

  userType: any
  pageName: any
  pageTitle: any
  adminPageHeaderContext: any

  constructor(
    private utilityService: CommonUtilityService,
    private router: Router,
    private adminService: AdminService) {

    router.events.subscribe((val) => {
      if(val instanceof NavigationEnd) {
        let url = val.url

        switch(url) {
          case '/admin/hrms':
            this.pageName = 'HR'
            this.pageTitle = null
            return
          case '/admin/finance':
            this.pageName = 'Finance'
            this.pageTitle = null
            return
          case '/admin/myprofile':
            this.pageName = 'My Profile'
            this.pageTitle = 'Update Profile'
            return
            case '/admin/my-profile':
            this.pageName = 'My Profile'
            this.pageTitle = 'Update Profile Page'
            return
            case '/admin/company':
            this.pageName = 'Company'
            this.pageTitle = 'Update Company Page'
            return
            case '/admin/my-reports':
            this.pageName = 'Reports'
            this.pageTitle = 'My Reports'
            return
            case '/admin/my-saned':
            this.pageName = 'My Saned'
            this.pageTitle = ''
            return
            case '/admin/saned-workspace':
            this.pageName = 'Saned Workspace'
            this.pageTitle = ''
            return
          default:
            this.pageName = 'My Profile'
            this.pageTitle = ''
            return
        }
      }
  });

  this.adminService.adminPageHeaderContext.subscribe(context => {
    if(!!context) {
      this.pageTitle = context
    }
  })

   }

  ngOnInit() {
  }
}
