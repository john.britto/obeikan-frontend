import { NgModule } from '@angular/core';
import {MatFormFieldModule, MatIconModule, MatDividerModule,
        MatButtonModule, MatInputModule, MatRippleModule,
        MatCardModule, MatSelectModule,   MatDatepickerModule,
        MatNativeDateModule, MatCheckboxModule, MatSnackBarModule, MatRadioModule,
        MatProgressSpinnerModule, MatGridListModule, MatExpansionModule, MatListModule, MatDialogModule,
        MatSortModule,MatTooltipModule,MatTabsModule, DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
import { MatMomentDateModule, MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';const modules = [
    MatButtonModule,
    MatInputModule,
    MatRippleModule,
    MatIconModule,
    MatDividerModule,
    MatCardModule,
    MatSelectModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatRadioModule,
    MatProgressSpinnerModule,
    MatGridListModule,
    MatExpansionModule,
    MatListModule,
    MatTooltipModule,
    MatTabsModule,
    MatMomentDateModule,
    MatDialogModule
];
export const MY_FORMATS = {
    parse: {
      dateInput: 'LL',
    },
    display: {
      dateInput: 'DD-MMM-YYYY',
      monthYearLabel: 'MMM YYYY',
      dateA11yLabel: 'LL',
      monthYearA11yLabel: 'MMMM YYYY',
    },
  };
@NgModule({
imports: [...modules],
exports: [...modules],
providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ]
})
export class MaterialModule {}

