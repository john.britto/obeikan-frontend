export const englishEnums = {
   osanedBanks: [{
      code: 'Al Rajhi Bank',
      name: 'RJHI'
   }, {
      code: 'ARAB NATIONAL BANK',
      name: 'ANB'
   }, {
      code: 'Bank Al Bilad',
      name: 'ALBI'
   }, {
      code: 'Bank Al Jazira',
      name: 'BJZ'
   }, {
      code: 'BANQUE SAUDI FRANSI',
      name: 'BSFR'
   }, {
      code: 'EMIRATES BANK',
      name: 'EBIL'
   }, {
      code: 'INMA BANK',
      name: 'INMA'
   }, {
      code: 'Riyadh Bank',
      name: 'RYD'
   }, {
      code: 'Samba Bank',
      name: 'SMBA'
   }, {
      code: 'SAUDI AMERICAN BANK',
      name: 'SAM'
   }, {
      code: 'SAUDI BRITISH BANK',
      name: 'SAB'
   }, {
      code: 'Saudi Holandi Bank',
      name: 'SHB'
   }, {
      code: 'Saudi Investment Bank',
      name: 'SIB'
   }, {
      code: 'THE NATIONAL COMMERCIAL BANK',
      name: 'NCB'
   }],
   osanedContractStatus: [{
      code: 'Family',
      name: 'Family'
   }, {
      code: 'Single',
      name: 'Single'
   }],
   osanedCountries: [{
      code: 'Algerian',
      name: 'Algerian'
   }, {
      code: 'American',
      name: 'American'
   }, {
      code: 'Australian',
      name: 'Australian'
   }, {
      code: 'Austrian',
      name: 'Austrian'
   }, {
      code: 'Bahraini',
      name: 'Bahraini'
   }, {
      code: 'Bangladeshi',
      name: 'Bangladeshi'
   }, {
      code: 'Belgian',
      name: 'Belgian'
   }, {
      code: 'British',
      name: 'British'
   }, {
      code: 'Canadian',
      name: 'Canadian'
   }, {
      code: 'Chinese',
      name: 'Chinese'
   }, {
      code: 'Danish',
      name: 'Danish'
   }, {
      code: 'Dutch',
      name: 'Dutch'
   }, {
      code: 'Egyptian',
      name: 'Egyptian'
   }, {
      code: 'Eritrean',
      name: 'Eritrean'
   }, {
      code: 'Ethiopian',
      name: 'Ethiopian'
   }, {
      code: 'Filipino',
      name: 'Filipino'
   }, {
      code: 'Finnish',
      name: 'Finnish'
   }, {
      code: 'French',
      name: 'French'
   }, {
      code: 'German',
      name: 'German'
   }, {
      code: 'Greek',
      name: 'Greek'
   }, {
      code: 'Holland',
      name: 'Holland'
   }, {
      code: 'Indian',
      name: 'Indian'
   }, {
      code: 'Indonesian',
      name: 'Indonesian'
   }, {
      code: 'Irish',
      name: 'Irish'
   }, {
      code: 'Italian',
      name: 'Italian'
   }, {
      code: 'Jordanian',
      name: 'Jordanian'
   }, {
      code: 'Kenya',
      name: 'Kenya'
   }, {
      code: 'Kuwaiti',
      name: 'Kuwaiti'
   }, {
      code: 'Lebanese',
      name: 'Lebanese'
   }, {
      code: 'Luxembourg',
      name: 'Luxembourg'
   }, {
      code: 'Malaysian',
      name: 'Malaysian'
   }, {
      code: 'Mali',
      name: 'Mali'
   }, {
      code: 'Malian',
      name: 'Malian'
   }, {
      code: 'Mauritian',
      name: 'Mauritian'
   }, {
      code: 'Mauritius',
      name: 'Mauritius'
   }, {
      code: 'Moroccan',
      name: 'Moroccan'
   }, {
      code: 'Nepalese',
      name: 'Nepalese'
   }, {
      code: 'Nigerian',
      name: 'Nigerian'
   }, {
      code: 'Norwegian',
      name: 'Norwegian'
   }, {
      code: 'Pakistani',
      name: 'Pakistani'
   }, {
      code: 'Palestinian',
      name: 'Palestinian'
   }, {
      code: 'Polish',
      name: 'Polish'
   }, {
      code: 'Portuguese',
      name: 'Portuguese'
   }, {
      code: 'Romanian',
      name: 'Romanian'
   }, {
      code: 'Russian',
      name: 'Russian'
   }, {
      code: 'Saudi Arabian',
      name: 'Saudi Arabian'
   }, {
      code: 'Singaporian',
      name: 'Singaporian'
   }, {
      code: 'Somalian',
      name: 'Somalian'
   }, {
      code: 'South African',
      name: 'South African'
   }, {
      code: 'Spanish',
      name: 'Spanish'
   }, {
      code: 'Sri Lankan',
      name: 'Sri Lankan'
   }, {
      code: 'Sudanese',
      name: 'Sudanese'
   }, {
      code: 'Swedish',
      name: 'Swedish'
   }, {
      code: 'Swiss',
      name: 'Swiss'
   }, {
      code: 'Syrian',
      name: 'Syrian'
   }, {
      code: 'Tunisian',
      name: 'Tunisian'
   }, {
      code: 'Turkish',
      name: 'Turkish'
   }, {
      code: 'U.K',
      name: 'U.K'
   }, {
      code: 'United Arab Emirates',
      name: 'United Arab Emirates'
   }, {
      code: 'Yemeni',
      name: 'Yemeni'
   }],
   osanedGender: [{
      code: 'FEMALE',
      name: 'Female'
   }, {
      code: 'MALE',
      name: 'Male'
   }],
   osanedJobTitles: [{
      code: 'ACCOUNT PAYABLE JUNIOR ACCOUNTANT',
      name: 'ACCOUNT PAYABLE JUNIOR ACCOUNTANT'
   }, {
      code: 'ADMIN SUPERVISOR',
      name: 'ADMIN SUPERVISOR'
   }, {
      code: 'APPLICATION DEVELOPER',
      name: 'APPLICATION DEVELOPER'
   }, {
      code: 'APPLICATION SUPPORT CONSULTANT',
      name: 'APPLICATION SUPPORT CONSULTANT'
   }, {
      code: 'ARABIC LANGUAGE EDITOR',
      name: 'ARABIC LANGUAGE EDITOR'
   }, {
      code: 'ASSISTANT MANAGER SECURITY',
      name: 'ASSISTANT MANAGER SECURITY'
   }, {
      code: 'AUTOMATION SENIOR ENGINEER',
      name: 'AUTOMATION SENIOR ENGINEER'
   }, {
      code: 'BUYER',
      name: 'BUYER'
   }, {
      code: 'CAPA COORDINATOR',
      name: 'CAPA COORDINATOR'
   }, {
      code: 'CHAIRMAN',
      name: 'CHAIRMAN'
   }, {
      code: 'CHIEF EXECUTIVE OFFICER',
      name: 'CHIEF EXECUTIVE OFFICER'
   }, {
      code: 'CHIEF INTERNAL AUDITOR',
      name: 'CHIEF INTERNAL AUDITOR'
   }, {
      code: 'CLERK',
      name: 'CLERK'
   }, {
      code: 'COPYRIGHT OFFICER',
      name: 'COPYRIGHT OFFICER'
   }, {
      code: 'DATA SCIENTIST',
      name: 'DATA SCIENTIST'
   }, {
      code: 'DIRECTOR OF IOT',
      name: 'DIRECTOR OF IOT'
   }, {
      code: 'EMPLOYEE RELATIONS SPECIALIST',
      name: 'EMPLOYEE RELATIONS SPECIALIST'
   }, {
      code: 'FINANCIAL ACCOUNTANT',
      name: 'FINANCIAL ACCOUNTANT'
   }, {
      code: 'FIRE INSPECTOR',
      name: 'FIRE INSPECTOR'
   }, {
      code: 'GENERAL LEDGER ACCOUNTANT',
      name: 'GENERAL LEDGER ACCOUNTANT'
   }, {
      code: 'GROUP BUSINESS EXCELLENCE DIRECTOR',
      name: 'GROUP BUSINESS EXCELLENCE DIRECTOR'
   }, {
      code: 'GROUP BUSINESS EXCELLENCE MANAGER',
      name: 'GROUP BUSINESS EXCELLENCE MANAGER'
   }, {
      code: 'GROUP DATA & BI MANAGER',
      name: 'GROUP DATA & BI MANAGER'
   }, {
      code: 'GROUP FINANCIAL CONTROLLER',
      name: 'GROUP FINANCIAL CONTROLLER'
   }, {
      code: 'GROUP IT MANAGER',
      name: 'GROUP IT MANAGER'
   }, {
      code: 'GROUP PMO MANAGER',
      name: 'GROUP PMO MANAGER'
   }, {
      code: 'GS SUPERVISOR',
      name: 'GS SUPERVISOR'
   }, {
      code: 'HEAD OF EMPLOYEE CAPABILITY BUILDING',
      name: 'HEAD OF EMPLOYEE CAPABILITY BUILDING'
   }, {
      code: 'HR OPERATIONS SUPERVISOR',
      name: 'HR OPERATIONS SUPERVISOR'
   }, {
      code: 'HSSE MANAGER',
      name: 'HSSE MANAGER'
   }, {
      code: 'INFRASTRUCTURE MANAGER',
      name: 'INFRASTRUCTURE MANAGER'
   }, {
      code: 'IOT BUSINESS CONSULTANT',
      name: 'IOT BUSINESS CONSULTANT'
   }, {
      code: 'IP TELEPHONE ADMINISTRATOR',
      name: 'IP TELEPHONE ADMINISTRATOR'
   }, {
      code: 'MAINTENANCE PLANNING SUPERVISOR',
      name: 'MAINTENANCE PLANNING SUPERVISOR'
   }, {
      code: 'MAINTENANCE SUPERVISOR',
      name: 'MAINTENANCE SUPERVISOR'
   }, {
      code: 'MANAGER - INTERNAL AUDIT',
      name: 'MANAGER - INTERNAL AUDIT'
   }, {
      code: 'MECHANICAL MANAGER',
      name: 'MECHANICAL MANAGER'
   }, {
      code: 'OFFICER',
      name: 'OFFICER'
   }, {
      code: 'OPERATION MANAGER',
      name: 'OPERATION MANAGER'
   }, {
      code: 'Operator',
      name: 'Operator'
   }, {
      code: 'PAYABLES TEAM LEADER',
      name: 'PAYABLES TEAM LEADER'
   }, {
      code: 'PMO MANAGER',
      name: 'PMO MANAGE'
   }, {
      code: 'PREPRESS MANAGER',
      name: 'PREPRESS MANAGER'
   }, {
      code: 'PROGRAM DIRECTOR',
      name: 'PROGRAM DIRECTOR'
   }, {
      code: 'PROJECT COORDINATOR',
      name: 'PROJECT COORDINATOR'
   }, {
      code: 'PROJECT MANAGER',
      name: 'PROJECT MANAGER'
   }, {
      code: 'PROJECTS JUNIOR ACCOUNTANT',
      name: 'PROJECTS JUNIOR ACCOUNTANT'
   }, {
      code: 'PUBLISHING TECHNICAL MANAGER',
      name: 'PUBLISHING TECHNICAL MANAGER'
   }, {
      code: 'QA TECHNICIAN',
      name: 'QA TECHNICIAN'
   }, {
      code: 'QC ENGINEER',
      name: 'QC ENGINEER'
   }, {
      code: 'QC OFFICER',
      name: 'QC OFFICER'
   }, {
      code: 'QC SUPERINTENDENT',
      name: 'QC SUPERINTENDENT'
   }, {
      code: 'REAL ESTATE',
      name: 'REAL ESTATE'
   }, {
      code: 'Receiver',
      name: 'Receiver'
   }, {
      code: 'SALESMAN',
      name: 'SALESMAN'
   }, {
      code: 'SCIENCE EDITOR',
      name: 'SCIENCE EDITOR'
   }, {
      code: 'SECRETARY',
      name: 'SECRETARY'
   }, {
      code: 'SENIOR HR BUSINESS PARTNER',
      name: 'SENIOR HR BUSINESS PARTNER'
   }, {
      code: 'SENIOR HUMAN RESOURCES SUPERVISOR',
      name: 'SENIOR HUMAN RESOURCES SUPERVISOR'
   }, {
      code: 'SENIOR INTERNAL AUDITOR',
      name: 'SENIOR INTERNAL AUDITOR'
   }, {
      code: 'SENIOR LANGUAGE EDITOR',
      name: 'SENIOR LANGUAGE EDITOR'
   }, {
      code: 'SENIOR OPERATOR',
      name: 'SENIOR OPERATOR'
   }, {
      code: 'STEEL DETAILER ENGINEER',
      name: 'STEEL DETAILER ENGINEER'
   }, {
      code: 'TALENT ACQUISITION SUPERVISOR',
      name: 'TALENT ACQUISITION SUPERVISOR'
   }, {
      code: 'Tool Maker',
      name: 'Tool Maker'
   }, {
      code: 'TREASURY SENIOR ANALYST',
      name: 'TREASURY SENIOR ANALYST'
   }, {
      code: 'UTILITIES MANAGER',
      name: 'UTILITIES MANAGER'
   }, {
      code: 'UTILITIES SUPERINTENDENT',
      name: 'UTILITIES SUPERINTENDENT'
   }],
   osanedLocations: [{
      code: '142',
      name: 'Riyadh'
   }, {
      code: '182',
      name: 'Turkey'
   }, {
      code: '183',
      name: 'Dubai'
   }, {
      code: '184',
      name: 'China'
   }, {
      code: '185',
      name: 'CBOB Dubai Intl Office'
   }, {
      code: '186',
      name: 'Pakistan'
   }, {
      code: '187',
      name: 'Syria'
   }, {
      code: '188',
      name: 'Tunisia'
   }, {
      code: '189',
      name: 'Thailand'
   }, {
      code: '190',
      name: 'Lebanon'
   }, {
      code: '191',
      name: 'Brazil'
   }, {
      code: '192',
      name: 'Cairo'
   }, {
      code: '193',
      name: 'Iraq'
   }, {
      code: '194',
      name: 'Kuwait'
   }, {
      code: '195',
      name: 'Yanbu'
   }, {
      code: '196',
      name: 'Iran'
   }, {
      code: '197',
      name: 'South Africa'
   }, {
      code: '198',
      name: 'CBOB Dubai Office'
   }, {
      code: '199',
      name: 'Egypt'
   }, {
      code: '200',
      name: 'Nigeria'
   }, {
      code: '201',
      name: 'Romania'
   }, {
      code: '202',
      name: 'Libya'
   }, {
      code: '203',
      name: 'Zimbabwe'
   }, {
      code: '204',
      name: 'Germany'
   }, {
      code: '205',
      name: 'Saudi Arab'
   }, {
      code: '206',
      name: 'Algeria'
   }],
   osanedMaritalStatus: [{
      code: 'Divorced',
      name: 'Divorced'
   }, {
      code: 'Domestic Partner',
      name: 'Domestic Partner'
   }, {
      code: 'Legally Separated',
      name: 'Legally Separated'
   }, {
      code: 'Living Together',
      name: 'Living Together'
   }, {
      code: 'Married',
      name: 'Married'
   }, {
      code: 'Single',
      name: 'Single'
   }, {
      code: 'Widowed',
      name: 'Widowed'
   }, {
      code: 'Widowed With Surviving Pension',
      name: 'Widowed With Surviving Pension'
   }],
   osanedReligions: [
      {
         code: 'Islam',
         name: 'Islam'
      },
      {
         code: 'Baptist',
         name: 'Baptist'
      }, {
         code: 'Buddhist',
         name: 'Buddhist'
      }, {
         code: 'Christian',
         name: 'Christian'
      }, {
         code: 'Christian Reformed',
         name: 'Christian Reformed'
      }, {
         code: 'Hindu',
         name: 'Hindu'
      }, {
         code: 'Jewish',
         name: 'Jewish'
      }, {
         code: 'Non Muslim',
         name: 'Non Muslim'
      }, {
         code: 'Others',
         name: 'Others'
      }]
};
