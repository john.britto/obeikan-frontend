import { environmentConst } from '../environments/env.constant'

export const appConfig = {
    apiResponseMessages: {
        GenericErrorMsg: 'Something went wrong.Please try again after some time',
        genericSuccessMsg: 'Form has been submitted successfully'
    },
    LoaderExceptionalApis: {
        resendOTP: environmentConst.endpoints.login.resendOTP
    },
    sessionTimeoutInterval: 30,
    pattern: {
        wareHouseCodeMaxlimit: '^[a-zA-Z0-9]{1,6}$',
        wareHouseDescMaxlimit: '^[ A-Za-z0-9_@./#&+-]{1,35}$',
        datePattern: '^(1[0-2]|0[1-9])/(3[01]|[12][0-9]|0[1-9])/[0-9]{4}$',
        monthMaxlimit: '^(1[0-2]|[1-9])$',
        yearMaxlimit: `^19[5-9]|20[0-4]|3050$`
    },
    commonUserAccess: ['my-reports', 'myprofile','my-profile','company', 'HREmployee', 'admin', 'SanedWorkspace', 'saned-compass'],
    DropdownOptions: {
        vatPercentages: ['0%', '5%'],
        calenderDays: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
        inventoryUnit: [
            { code: 'BOX', name: 'Box' },
            { code: 'CAN', name: ' Can' },
            { code: 'CM', name: ' centimeter' },
            { code: 'CM3', name: ' Cubic Centimeter' },
            { code: 'CTN', name: ' Carton' },
            { code: 'DAY', name: ' Day' },
            { code: 'DRM', name: ' DRUM' },
            { code: 'EAC', name: ' each' },
            { code: 'GAL', name: ' Gallon' },
            { code: 'GRM', name: ' Gram' },
            { code: 'HRS', name: ' Hours' },
            { code: 'KG', name: ' kilogram ' },
            { code: 'KM', name: ' Kilo Meter' },
            { code: 'LBS', name: ' Pound' },
            { code: 'LTR', name: ' Liter' },
            { code: 'MTR', name: ' Meter' },
            { code: 'MIN', name: ' Minutes' },
            { code: 'ML', name: ' Milliter' },
            { code: 'MM', name: ' Millimeter ' },
            { code: 'MON', name: ' Month' },
            { code: 'NOS', name: ' Number' },
            { code: 'PAL', name: ' Pallet' },
            { code: 'PCS', name: ' Pieces' },
            { code: 'PKS', name: ' packs' },
            { code: 'ROL', name: ' Roll' },
            { code: 'SEC', name: ' Second' },
            { code: 'SET', name: ' Set' },
            { code: 'SHT', name: ' Sheet' },
            { code: 'Pair', name: 'Pair'},
            { code: 'Pack', name: 'Packet'},
            { code: 'BOK', name: 'Book'},
            { code: 'PKS', name: 'packs'},
            { code: 'ROL', name: 'Roll'},
            { code: 'SEC', name: 'Second'},
            { code: 'SET', name: 'Set'},
            // { code: 'SHT', name: 'Sheet'},
            { code: 'SQM', name: 'Square Meter'},
            { code: 'ROLL', name: 'Roll'}
        ],
        bankLists: [
            'SHB-Saudi Holandi Bank',
            'ALBI-Bank Al Bilad',
            'ANB-ARAB NATIONAL BANK',
            'BJZ-Bank Al Jazira',
            'BSFR-BANQUE SAUDI FRANSI',
            'EBIL-EMIRATES BANK',
            'INMA-INMA BANK',
            'NCB-THE NATIONAL COMMERCIAL BANK',
            'RYD-Riyadh Bank',
            'RJHI-Al Rajhi Bank',
            'SAB-SAUDI BRITISH BANK',
            'SAM-SAUDI AMERICAN BANK',
            'SIB-Saudi Investment Bank',
            'SMBA-Samba Bank',
            'ALN-ALINMA BANK',
            'AUE-Ahli United Bank',
            'BLD-BANK AL BILAD',
            'BNP-BNP PARIBAS',
            'GIB-Gulf International Bank',
            'MSR-Misr Bank',
            'SFB-Saudi Fransi Bank'
        ],
        currencies: ['SAR', 'SGD', 'INR'],
        yesOrNoOtions: ['YES', 'NO'],
        transactionTypes: ['Credit', 'Debit'],
        financialGroup: ['001-Domestic/Local', '002-Foreign', '003-Inter-company'],
        accountTypes: ['Balance_Sheet', 'Profit_or_Loss', 'Intercompany'],
        ledgerStatus: ['FREE', 'Block_Manual', 'Block_All'],
        dimensionStatus: ['Not used', 'Optional', 'Mandatory'],
        dimensionTypes: ['Line_Of_Business', 'Item_Group', 'Project', 'Department', 'Employee_Miscellaneous'],
        otstTypes: ['Overtime Weekdays', 'Overtime Weekends', 'Shorttime'],
        deductionTypes: ['Shorttime', 'Absence Deduction', 'Air Ticket Difference Deduction', 'Canteen Deduction', 'Chamber of Commerce Deduction', 'Computer or Laptop Deduction', 'Courier Charges', 'DHL Deductions', 'Electricity and Water Bill', 'EOS Additional Deduction', 'Exit Rentry Deduction', 'Food Allowance Adjustment', 'Food Canteen Deduction', 'Guarantor Deduction', 'Hotel Deduction', 'IQAMA Renewal Deduction', 'Medical Card Lost Penalty', 'Mobile Expenses', 'Notice Period Deduction', 'OGC Accommodation Deduction', 'Other Deduction', 'Passport Lost Deduction', 'Penalty Deduction', 'Previous Month Balance Deduction', 'Release Salary Leave Settlement Deduction', 'Salary Balancing Deduction', 'Temporary Advance to Employees Recovery', 'Traffic Penalty One Time', 'Transportation Advance Recovery', 'Travel Deduction', 'Visa Processing Deduction'],
        earningTypes: ['Absence Deduction Return', 'Absent Deduction Reimbursment', 'Acting Delegation', 'Advance Salary Next Month', 'Air Ticket Difference Payment', 'Air Ticket Encashment', 'Basic Salary Arrears', 'Bonus', 'Boofiya Expenses', 'Business Trip Allowance', 'Business Trip Lumpsum', 'Call In', 'Canteen Deduction Reimbursement', 'Car Insurance Payment', 'Car Loan Over Deduction', 'Car Maint Fuel Expenses', 'Car Maint Fuel Expenses', 'Car Maintenance', 'Cash Advance Over Deduction', 'Cash Advance Payment', 'Certification Fee', 'Commission', 'E Wakala Payment', 'Eid Days Payment', 'Electricity Bill', 'End Of Service Award', 'EOS Additional Payment', 'EOS Next Month Salary', 'ERT Allowance One Time', 'ERT Arrears', 'Exit Reentry Payment', 'Expense Claim', 'Family Visa Payment', 'Food Allowance One Time', 'Food Allowance with Advance Salary', 'Fuel Expense', 'Guest Entertainment', 'Hotel Bills', 'Housing Allowance Advance', 'Housing Allowance Arrears', 'Incentives', 'Iqama Work Permit Payment', 'Laundry_Parking Payment', 'Leave Advance', 'Leave Encashment', 'Leave Encashment EOS', 'Marriage Allowance', 'Maternity Expenses', 'Medical Expense Claim', 'Mobile Allowance Arrears', 'Mobile Allowance One Time', 'Mobile Person', 'National Day Payment', 'Notice Period Payment', 'Nurse Allowance', 'Off Day Overtime Payment', 'Office Supply', 'OGC Accommodation Deduction Reimbursement', 'On Call Payment', 'Other Payment', 'Overtime Adjustments', 'Overtime Arrears', 'Personal Car Loan', 'Previous Month Balance Earning', 'Release Salary', 'Release Salary Leave Settlement', 'Relocation Allowance', 'Remote Area Allowance Arrears', 'Reward And Recognition Recommendation', 'Salary Adjustment Payment', 'Salary Balancing Earning', 'Samsung Over Deduction Payment', 'Security Allowance Arrears', 'Shorttime Deduction Repayment', 'Special Allowance Arrears', 'Special Allowance One Time', 'Special Bonus', 'Taxi Fare', 'Temporary Advance to Employees', 'Traffic Penalty Repayment', 'Training Material', 'Transportation Allowance Advance', 'Transportation Allowance Arrears', 'Travel Expense', 'Utility Reimbursement', 'Vacation Difference', 'Vacation Salary', 'Visa Processing Payment', 'Working Meals'],
        recurringTypes: ['Conveyance Allowance', 'Telephone Recurring', 'Emergency Response Teams Allowance', 'Overseas Assistance Allowance', 'Industrial Security Allowance', 'Food Allowance', 'Personal Car Allowance', 'Acting Allowance', 'Remote Area Allowance', 'Security Allowance', 'Special Allowance Recurring', 'Fuel Expense Recurring', 'Nature Of Work Allowance Recurring', 'Mobile Allowance Recurring'],
        recurringLoanTypes: ['Purchase Fund', 'Other Loans', 'Iqama Loan Deduction', 'Consumer Product Bank Loans', 'Advance Pay Recovery', 'Housing Advance Recovery', 'Recruitment Charges', 'Car Accident Charges', 'Car Loan Recovery', 'Cash Advance Deduction', 'Samsung Loan Deduction', 'Medical Insurance Deduction', 'United Arab Emirates (UAE)', 'Bookstore Deduction', 'Medical Card Upgrade', 'Traffic Panelty', 'Advance Pay Adjustment'],
        customerRoles: ['CEO', 'HRAdmin', 'GeneralManager'],
        approverTypes: [
            {
                name: 'Supervisor',
                code: 'SUP'
            },
            {
                name: 'Nominee',
                code: 'SE'
            }
        ],
        financeApproverTypes: [
            {
                name: 'Role Title',
                code: 'RT'
            },
            {
                name: 'Specific Employee',
                code: 'SE'
            }
        ],
        vatCodes: [
            {
                vatCode: 'EG0',
                vatDescription: 'Exempt Goods',
                vatPercentage: 0
            },
            {
                vatCode: 'ES0',
                vatDescription: 'Exempt Service',
                vatPercentage: 0
            },
            {
                vatCode: 'F00',
                vatDescription: 'Foreign Bank Transaction',
                vatPercentage: 0
            },
            {
                vatCode: 'G05',
                vatDescription: 'Standard Rate Goods',
                vatPercentage: 5
            },
            {
                vatCode: 'GG0',
                vatDescription: 'Zero rate Goods Import',
                vatPercentage: 0
            },
            {
                vatCode: 'GS0',
                vatDescription: 'Zero rate Service Import ',
                vatPercentage: 0
            },
            {
                vatCode: 'LG5',
                vatDescription: 'Local Bank Transaction ',
                vatPercentage: 5
            },
            {
                vatCode: 'OB0',
                vatDescription: '100% Intercomp- Bank Trans',
                vatPercentage: 0
            },
            {
                vatCode: 'OG0',
                vatDescription: '100% Intercompany Goods ',
                vatPercentage: 0
            },
            {
                vatCode: 'OS0',
                vatDescription: '100% Intercompany Service ',
                vatPercentage: 0
            },
            {
                vatCode: 'R05',
                vatDescription: 'Reverse Charge ',
                vatPercentage: 5
            },
            {
                vatCode: 'S05',
                vatDescription: 'Standard Rate Service',
                vatPercentage: 5
            },
            {
                vatCode: 'TAS',
                vatDescription: 'Tax Statement',
                vatPercentage: 0
            },
            {
                vatCode: 'UN',
                vatDescription: 'Unregistered supplier VAT',
                vatPercentage: 0
            },
            {
                vatCode: 'ZERO',
                vatDescription: 'Zero Tax',
                vatPercentage: 0
            },
            {
                vatCode: 'ZG0',
                vatDescription: 'Zero rate Goods Export',
                vatPercentage: 0
            },
            {
                vatCode: 'ZGD',
                vatDescription: 'Zero rate Goods Domestic',
                vatPercentage: 0
            },
            {
                vatCode: 'ZS0',
                vatDescription: 'Zero rate Service Export',
                vatPercentage: 0
            },
            {
                vatCode: 'ZSD',
                vatDescription: 'Zero rate Service Domestic',
                vatPercentage: 0
            },
            {
                vatCode: 'G15',
                vatDescription: 'Standard Rate Goods',
                vatPercentage: 15
            },
            {
                vatCode: 'LG15',
                vatDescription: 'Local Bank Transfer',
                vatPercentage: 15
            },
            {
                vatCode: 'R15',
                vatDescription: 'Reverse Charge',
                vatPercentage: 15
            },
            {
                vatCode: 'S15',
                vatDescription: 'Standard Rate Service',
                vatPercentage: 15
            }
        ],
        accessibilities: ['Accounts Payable', 'Accounts Receivable', 'General Ledger', 'Inventory Report', 'Return Order'],
        typeOneArray: ['Grade', 'Employee'],
        typeTwoArray: ['Percentage of Salary', 'Fixed Amount'],
        leaveTypes: ['Annual_Leave', 'Compassionate Leave', 'Examination Leave', 'Hajj Leave', 'Leave With Advance', 'Marriage Leave', 'Maternity Leave', 'Paternity Leave', 'Sick Leave', 'Unpaid Leave'],
        hrReports: [
            {
                name: 'Employee Dashboard',
                imageUrl: '',
                hoverImage: '',
                url: '',
                key: 'hrms'
            },
            {
                name: 'Payroll Dashboard',
                imageUrl: '',
                hoverImage: '',
                url: '',
                key: 'hrPayrollRegister'
            }
        ],
        financeReports: [
            {
                name: 'Sales Overview',
                imageUrl: '',
                hoverImage: '',
                url: '',
                key: 'sales'
            },
            {
                name: 'Payable Overview',
                imageUrl: '',
                hoverImage: '',
                url: '',
                key: 'payable'
            },
            {
                name: 'Receivable Overview',
                imageUrl: '',
                hoverImage: '',
                url: '',
                key: 'recievable'
            },
            {
                name: 'Inventory Overview',
                imageUrl: '',
                hoverImage: '',
                url: '',
                key: 'inventoryOverview'
            },
            {
                name: 'Financial Statement',
                imageUrl: '',
                hoverImage: '',
                url: '',
                key: 'finance'
            },

            {
                name: 'Collection',
                imageUrl: '',
                hoverImage: '',
                url: '',
                key: 'collection'
            },
            {
                name: 'Expenses Analysis',
                imageUrl: '',
                hoverImage: '',
                url: '',
                key: 'expense'
            },
            {
                name: 'ITBR',
                imageUrl: '',
                hoverImage: '',
                url: '',
                key: 'itbr'
            },
            {
                name: 'Account payable Reports',
                imageUrl: '',
                hoverImage: '',
                url: '',
                key: 'APReport'
            },
            {
                name: 'Account Receivable Reports',
                imageUrl: '',
                hoverImage: '',
                url: '',
                key: 'ARReport'
            },
            {
                name: 'Fixed Asset Reports',
                imageUrl: '',
                hoverImage: '',
                url: '',
                key: 'fixedasset'
            },
            {
                name: 'Expenses Analysis',
                imageUrl: '',
                hoverImage: '',
                url: '',
                key: 'expense'
            },
            {
                name: 'VAT Reports',
                imageUrl: '',
                hoverImage: '',
                url: '',
                key: 'VATReports'
            },
            {
                name: 'Sales Reports',
                imageUrl: '',
                hoverImage: '',
                url: '',
                key: 'soReport'
            },
            {
                name: 'Purchase Reports',
                imageUrl: '',
                hoverImage: '',
                url: '',
                key: 'poReport'
            },
            {
                name: 'Inventory Reports',
                imageUrl: '',
                hoverImage: '',
                url: '',
                key: 'inventoryReport'
            },
            {
                name: 'Ledger Transaction Details',
                imageUrl: '',
                hoverImage: '',
                url: '',
                key: 'ledgerTransactionDetails'
            },

        ],
        compassReports: [
            {
                name: 'Sales',
                imageUrl: '',
                hoverImage: '',
                url: '',
                key: 'sales'
            },
            {
                name: 'Production',
                imageUrl: '',
                hoverImage: '',
                url: '',
                key: 'compassReportOne'
            },
            {
                name: 'Supply Chain',
                imageUrl: '',
                hoverImage: '',
                url: '',
                key: 'compassReportTwo'
            },
            {
                name: 'Finance',
                imageUrl: '',
                hoverImage: '',
                url: '',
                key: 'compassReportThree'
            },
            {
                name: 'HR',
                imageUrl: '',
                hoverImage: '',
                url: '',
                key: 'compassReportFour'
            },
        ]
    },
    nationalities: [
        'Algerian',
        'American',
        'Austrian',
        'Australian',
        'Belgian',
        'Bangladeshi',
        'Bahraini',
        'Canadian',
        'Chinese',
        'German',
        'Danish',
        'Egyptian',
        'Eritrean',
        'Spanish',
        'Ethiopian',
        'Finnish',
        'Filipino',
        'French',
        'British',
        'Greek',
        'Holland',
        'Indonesian',
        'Irish',
        'Indian',
        'Italian',
        'Jordanian',
        'Kenya',
        'Kuwaiti',
        'Lebanese',
        'Luxembourg',
        'Mali',
        'Malaysian',
        'Malian',
        'Moroccan',
        'Mauritian',
        'Mauritius',
        'Nigerian',
        'Dutch',
        'Norwegian',
        'Nepalese',
        'Pakistani',
        'Polish',
        'Palestinian',
        'Portuguese',
        'Romanian',
        'Russian',
        'Saudi Arabian',
        'Swedish',
        'South African',
        'Singaporian',
        'Somalian',
        'Sri Lankan',
        'Sudanese',
        'Swiss',
        'Syrian',
        'Turkish',
        'Tunisian',
        'U.K',
        'Yemeni',
        'United Arab Emirates'
    ],
    hrmsNavLists: {
        mainTiles: [
            {
                'name': 'Master Data Setup',
                'caseName': 'MasterdataSetup',
                'imgUrl': '../../assets/images/icons/hr/master_data_setup.png',
                'HoverImgUrl': '../../assets/images/icn_masterdataicn_masterdata_black.png',
                'url': '../../assets/images/icn_masterdata.png',
                'active': false,
            },
            {
                'name': 'HR Policies Setup',
                'caseName': 'HRPoliciesSetup',
                'imgUrl': '../../assets/images/icons/hr/hr_policy_setup.png',
                'HoverImgUrl': '../../assets/images/icn_hrpoliciessetupicn_hrpoliciessetup_blacksmall.png',
                'url': '../../assets/images/icn_hrpoliciessetup.png',
                'active': false,
            },
            {
                'name': 'Payroll Process',
                'caseName': 'PayrollProcess',
                'imgUrl': '../../assets/images/icons/hr/payroll_process.png',
                'HoverImgUrl': '../../assets/images/icn_payrollprocessicn_payrollprocess_black.png',
                'url': '../../assets/images/icn_payrollprocess.png',
                'active': false,
            },
            // {
            //     'name': 'Recruitment',
            //     'caseName': 'Recruitment',
            //     'imgUrl': '../../assets/images/icn_recruitment.png',
            //     'HoverImgUrl': '../../assets/images/icn_recruitmenticn_recruitment_black.png',
            //     'url': '../../assets/images/icn_recruitment.png',
            //     'active': false,
            // }
        ],
        hrPoliciesSubTiles: [
            {
                'name': 'Housing Allowance Management',
                'caseName': 'HousingAllowanceManagement',
                'imgUrl': '../../assets/images/icons/hr/housing_allowance_management_blue.png',
                'HoverImgUrl': '../../assets/images/icons/hr/housing_allowance_management.png',
                'url': '../../assets/images/icn_masterdata.png',
                'active': false,
            },
            {
                'name': 'Transportation Allowance Mgmt',
                'caseName': 'TransportationAllowanceManagement',
                'imgUrl': '../../assets/images/icons/hr/travel_allowance_management.png',
                'HoverImgUrl': '../../assets/images/icons/hr/travel_allowance_management_white.png',
                'url': '../../assets/images/icn_hrpoliciessetup.png',
                'active': false,
            },
            {
                'name': 'Cash Advance Policy',
                'caseName': 'cashAdvancePolicy',
                'imgUrl': '../../assets/images/icons/hr/salary_elements.png',
                'HoverImgUrl': '../../assets/images/icons/hr/salary_elements_white.png',
                'url': '../../assets/images/icn_hrpoliciessetup.png',
                'active': false,
            },
            {
                'name': 'Work Calender Policies',
                'caseName': 'workcalenderpolicies',
                'imgUrl': '../../assets/images/icons/hr/salary_elements.png',
                'HoverImgUrl': '../../assets/images/icons/hr/salary_elements_white.png',
                'url': '../../assets/images/icn_hrpoliciessetup.png',
                'active': false,
            },
            {
                'name': 'Medical Insurance',
                'caseName': 'MedicalInsurance',
                'imgUrl': '../../assets/images/icons/hr/medical_insurance.png',
                'HoverImgUrl': '../../assets/images/icons/hr/medical_insurance_white.png',
                'url': '../../assets/images/icn_payrollprocess.png',
                'active': false,
            },
            {
                'name': 'Per Diem Calculation',
                'caseName': 'PerDiemCalculation',
                'imgUrl': '../../assets/images/icons/hr/per_diem_calculation.png',
                'HoverImgUrl': '../../assets/images/icons/hr/per_diem_calculation_white.png',
                'url': '../../assets/images/icn_recruitment.png',
                'active': false,
            },
            {
                'name': 'Time Sheet Calculations',
                'caseName': 'TimeSheetCalculations',
                'imgUrl': '../../assets/images/icons/hr/time_management.png',
                'HoverImgUrl': '../../assets/images/icons/hr/time_management_white.png',
                'url': '../../assets/images/icn_recruitment.png',
                'active': false,
            },
            {
                'name': 'Overtime Calculations',
                'caseName': 'OvertimeCalculations',
                'imgUrl': '../../assets/images/icons/hr/overtime_calculation.png',
                'HoverImgUrl': '../../assets/images/icons/hr/overtime_calculation_white.png',
                'url': '../../assets/images/icn_recruitment.png',
                'active': false,
            },
            {
                'name': 'Timesheet Policy',
                'caseName': 'timesheetPolicy',
                'imgUrl': '../../assets/images/icons/hr/leave_request.png',
                'HoverImgUrl': '../../assets/images/icons/hr/leave_request_white.png',
                'url': '../../assets/images/icn_recruitment.png',
                'active': false,
            },
            {
                'name': 'Leave Entitlement',
                'caseName': 'LeaveEntitlement',
                'imgUrl': '../../assets/images/icons/hr/leave_entitlement.png',
                'HoverImgUrl': '../../assets/images/icons/hr/leave_entitlement_white.png',
                'url': '../../assets/images/icn_recruitment.png',
                'active': false,
            },
            {
                'name': 'Other',
                'caseName': 'Other',
                'imgUrl': '../../assets/images/icons/hr/other_request.png',
                'HoverImgUrl': '../../assets/images/icons/hr/other_request_white.png',
                'url': '../../assets/images/icn_recruitment.png',
                'active': false,
            }
        ],
        payrollSubTabs: [
            {
                'name': 'Run Payroll',
                'caseName': 'RunPayroll',
                'imgUrl': '../../assets/images/icons/hr/run_payroll.png',
                'HoverImgUrl': '../../assets/images/icons/hr/run_payroll_white.png',
                'url': '../../assets/images/icn_masterdata.png',
                'active': false,
            },
            {
                'name': 'Salary Element Entries - Single',
                'caseName': 'SalaryElementEntriesSingle',
                'imgUrl': '../../assets/images/icons/hr/salary_element_single.png',
                'HoverImgUrl': '../../assets/images/icons/hr/salary_element_single_white.png',
                'url': '../../assets/images/icn_hrpoliciessetup.png',
                'active': false,
            },
            {
                'name': 'Salary Element Entries - Bulk',
                'caseName': 'SalaryElementEntriesBulk',
                'imgUrl': '../../assets/images/icons/hr/salary_element_bulk.png',
                'HoverImgUrl': '../../assets/images/icons/hr/salary_element_bulk_white.png',
                'url': '../../assets/images/icn_masterdata.png',
                'active': false,
            }
        ],
        payrollDeductionTiles: [
            {
                'name': 'Recurring Loan Deductions',
                'caseName': 'RecurringLoanDeductions',
                'imgUrl': '../../assets/images/icons/hr/per_diem_calculation.png',
                'HoverImgUrl': '../../assets/images/icons/hr/per_diem_calculation_white.png',
                'url': '../../assets/images/icn_masterdata.png',
                'active': false,
            },
            {
                'name': 'One Time Deductions',
                'caseName': 'OneTimeDeductions',
                'imgUrl': '../../assets/images/icons/hr/overtime_calculation.png',
                'HoverImgUrl': '../../assets/images/icons/hr/overtime_calculation_white.png',
                'url': '../../assets/images/icn_hrpoliciessetup.png',
                'active': false,
            },
        ],
        payrollEarningsTiles: [
            {
                'name': 'One Time Earnings',
                'caseName': 'OneTimeEarnings',
                'imgUrl': '../../assets/images/icons/hr/per_diem_calculation.png',
                'HoverImgUrl': '../../assets/images/icons/hr/per_diem_calculation_white.png',
                'url': '../../assets/images/icn_masterdata.png',
                'active': false,
            },
            {
                'name': 'Recurring Earnings',
                'caseName': 'RecurringEarnings',
                'imgUrl': '../../assets/images/icons/hr/overtime_calculation.png',
                'HoverImgUrl': '../../assets/images/icons/hr/overtime_calculation_white.png',
                'url': '../../assets/images/icn_hrpoliciessetup.png',
                'active': false,
            }
        ],
    },

    approverDashboardPageType: [
        {
            code: 'AP',
            name: 'Account Payable'
        },
        {
            code: 'AR',
            name: 'Account Receivable'
        },
        {
            code: 'GL',
            name: 'General Ledger'
        },
        {
            code: 'SR',
            name: 'Sales Return'
        }
    ],
    approverDashboardHRPageType: [
        {
            code: 'SE',
            name: 'Workflow Requests'
        },
        {
            code: 'PR',
            name: 'Payroll Entries'
        },

        {
            code: 'RP',
            name: 'Run Payroll'
        }
    ],
    sanedWorkspace: [
        {
            name: 'Calender',
            description: 'Schedule and share meeting and event times, and automatically get reminders. ',
            img: '../assets/svg/calender.svg',
            url: 'https://outlook.office.com/owa/'
        },
        {
            name: 'Delve',
            description: 'Get personal insights and relevant information based on who you work with and what you work on. ',
            img: '../assets/svg/delve.svg',
            url: 'https://aqtllc-my.sharepoint.com/_layouts/15/me.aspx?origin=shell'
        },
        {
            name: 'Dynamics 365',
            description: 'Break down the silos between your business processes and applications with Microsoft Dynamics 365. ',
            img: '../assets/svg/calender.svg',
            url: 'https://home.dynamics.com/?autolaunch=true'
        },
        {
            name: 'Excel',
            description: 'Discover and connect to data, model and analyze it, and visualize insights. ',
            img: '../assets/svg/excel.svg',
            url: 'https://www.office.com/launch/excel'
        },
        {
            name: 'Forms',
            description: 'Create surveys, quizzes, and polls and easily see results in real time. ',
            img: '../assets/svg/forms.svg',
            url: 'https://forms.office.com/Pages/DesignPage.aspx'
        },
        {
            name: 'Excel',
            description: 'Discover and connect to data, model and analyze it, and visualize insights. ',
            img: '../assets/svg/excel.svg',
            url: 'https://www.office.com/launch/excel'
        },
        {
            name: 'OneDrive',
            description: 'Store, access, and share your files in one place. ',
            img: '../assets/svg/onedrive.svg',
            url: 'https://aqtllc-my.sharepoint.com/'
        },
        {
            name: 'OneNote',
            description: 'Capture and organize your notes across all your devices. ',
            img: '../assets/svg/onenote.svg',
            url: 'https://www.office.com/launch/onenote'
        },
        {
            name: 'Outlook',
            description: 'Business-class email through a rich and familiar Outlook experience. ',
            img: '../assets/svg/outlook.svg',
            url: 'https://outlook.office.com/owa/'
        },
        {
            name: 'Planner',
            description: 'Create plans, organize and assign tasks, share files, and get progress updates. ',
            img: '../assets/svg/planner.svg',
            url: 'https://tasks.office.com'
        },
        {
            name: 'PowerPoint',
            description: 'Design professional presentations. ',
            img: '../assets/svg/point.svg',
            url: 'https://www.office.com/launch/powerpoint'
        },
        {
            name: 'SharePoint',
            description: 'Share and manage content, knowledge, and applications to empower teamwork. ',
            img: '../assets/svg/sharepoint.svg',
            url: 'https://aqtllc.sharepoint.com/_layouts/15/sharepoint.aspx'
        },
        {
            name: 'Stream',
            description: 'Share videos of classes, meetings, presentations, and training sessions. ',
            img: '../assets/svg/stream.svg',
            url: 'https://web.microsoftstream.com/'
        },
        {
            name: 'Sway',
            description: 'Create and share interactive reports, presentations, and personal stories. ',
            img: '../assets/svg/sway.svg',
            url: 'https://www.office.com/launch/sway'
        },
        {
            name: 'Teams',
            description: 'The customizable, chat-based team workspace in Office 365. ',
            img: '../assets/svg/team.svg',
            url: 'https://aka.ms/mstfw'
        },
        {
            name: 'To Do',
            description: 'Manage, prioritize, and complete the most important things you need to achieve every day. ',
            img: '../assets/svg/todo.svg',
            url: 'https://to-do.office.com/tasks'
        },
        {
            name: 'WhiteBoard',
            description: 'Ideate and collaborate on a freeform canvas designed for pen, touch and keyboard. ',
            img: '../assets/svg/board.svg',
            url: 'https://whiteboard.microsoft.com/'
        },
        {
            name: 'Word',
            description: 'Bring out your best writing. ',
            img: '../assets/svg/word.svg',
            url: 'https://www.office.com/launch/word'
        },
        {
            name: 'Yammer',
            description: 'Connect with coworkers and classmates, share information, and organize around projects. ',
            img: '../assets/svg/yammer.svg',
            url: 'https://www.yammer.com/office365'
        }
    ],
    monthArr: [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
    ],
    mobileScreenWidth: 768
}