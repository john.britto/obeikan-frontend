import { HomeOrganismComponent } from '../shared/organisms/home-organism/home-organism.component';
import { HomeResolverService } from '../services/home-resolver.service';
import { CartResolverService } from '../services/cart-resolver.service';
import { RegisterMoleculeComponent } from '../shared/molecules/register-molecule/register-molecule.component';
import { LoginOrganismComponent } from '../shared/organisms/login-organism/login-organism.component';
import { CheckoutOrganismComponent } from '../shared/organisms/checkout-organism/checkout-organism.component';
import { PaymentMoleculeComponent } from '../shared/molecules/purchase-path/payment-molecule/payment-molecule.component';
import { ReviewMoleculeComponent } from '../shared/molecules/purchase-path/review-molecule/review-molecule.component';
import { PdpOrganismComponent } from '../shared/organisms/pdp-organism/pdp-organism.component';
import { CartOrganismComponent } from '../shared/organisms/cart-organism/cart-organism.component'
import { OrderConfirmationOrganismComponent } from '../shared/organisms/order-confirmation-organism/order-confirmation-organism.component';
import { ContactUsMoleculeComponent } from '../shared/molecules/contact-us-molecule/contact-us-molecule.component';
import { AboutUsMoleculeComponent } from '../shared/molecules/about-us-molecule/about-us-molecule.component';
import { AuthGuard } from '../services/auth-guard.service';
import { SuperAdminOrganismComponent } from '../shared/organisms/super-admin-organism/super-admin-organism.component';
import { SuperAdminHRMSMoleculeComponent } from '../shared/molecules/super-admin/super-admin-hrmsmolecule/super-admin-hrmsmolecule.component';
import { SuperAdminFinanceMoleculeComponent } from '../shared/molecules/super-admin/super-admin-finance-molecule/super-admin-finance-molecule.component';
import { ApprovalDashboardOrgnismComponent } from '../shared/organisms/approval-dashboard-orgnism/approval-dashboard-orgnism.component'
import { Component } from '@angular/core';
import { RenewalComponent } from '../shared/organisms/renewal-organism/renewal-organism.component';
import { ProductOrganismComponent } from '../shared/organisms/product-organism/product-organism.component'

export const HOME_ROUTES = [{
    path: '',
    component: HomeOrganismComponent,
    data: [{
      pageName: 'Home Page',
    }],
    resolve: {
      homeData: HomeResolverService
    }
  },
  {
    path: 'register',
    component: RegisterMoleculeComponent
  },
  {
    path: 'login',
    component: LoginOrganismComponent,
  },
  { path: 'checkout', component: CheckoutOrganismComponent,
  canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'payment', pathMatch: 'full' },
      { path: 'payment', component: PaymentMoleculeComponent },
      { path: 'review', component: ReviewMoleculeComponent }
    ]
  },
  {
    path: 'Pdp',
    Component: ProductOrganismComponent,
    children: [
      { path: '', redirectTo: 'hrms', pathMatch: 'full' },
      { path: 'HRMS',
      component: PdpOrganismComponent,
      data: [{
        pageName: 'HRMS',
      }]
    },
      {
        path: 'Finance',
        component: PdpOrganismComponent,
        data: [{
          pageName: 'Finance',
        }]
      }
    ]
  },
  {
    path: 'renewal',
    component: RenewalComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'cart',
    component: CartOrganismComponent,
    canActivate: [AuthGuard],
    resolve: {
      cartData: CartResolverService
    }
  },
  {
    path: 'products',
    component: LoginOrganismComponent,
    pathMatch: 'full',
    data: [{
      pageName: 'Products Listing',
    }]
  },
  {
    path: 'about-saned',
    component: AboutUsMoleculeComponent,
    data: [{
      pageName: 'About Saned Page',
    }],
  },
  {
    path: 'order-confirmation',
    component: OrderConfirmationOrganismComponent,
    canActivate: [AuthGuard],
    data: [{
      pageName: 'Contact US Page',
    }],
  },
  {
    path: 'contactUs',
    component: ContactUsMoleculeComponent,
    data: [{
      pageName: 'Order Confirmation Page',
    }], 
  },
  {
    path: 'approval-dashboard',
    component: ApprovalDashboardOrgnismComponent,
    canActivate: [AuthGuard],
    data: [{
      pageName: 'Approval Dashboard Page',
    }], 
  },
  {
    path: 'superAdmin',
    component: SuperAdminOrganismComponent,
    canActivate: [AuthGuard],
    data: [{
      pageName: 'Super Admin Page',
    }], 
    children: [
      { path: '', redirectTo: 'superAdmin', pathMatch: 'full' },
      { path: 'hrms', component: SuperAdminHRMSMoleculeComponent },
      { path: 'finance', component: SuperAdminFinanceMoleculeComponent }
    ]
  },]