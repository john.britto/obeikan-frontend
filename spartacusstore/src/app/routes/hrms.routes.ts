import { AdminOrganismComponent } from '../shared/organisms/admin-organism/admin-organism.component';
import { FinanceOrganismComponent } from '../shared/organisms/finance-organism/finance-organism.component'
import { ProfileOrganismComponent } from '../shared/organisms/profile-organism/profile-organism.component'
import { ReportsOrganismComponent } from '../shared/organisms/reports-organism/reports-organism.component'
import { MySanadOrganismComponent } from '../shared/organisms/saned/my-sanad-organism/my-sanad-organism.component'
import { SanedWorkspaceMoleculeComponent } from '../shared/molecules/admin/saned-workspace-molecule/saned-workspace-molecule.component'
import { MyProfileOrganismComponent } from '../shared/organisms/my-profile-organism/my-profile-organism.component'
import { CompanyOrganismComponent } from '../shared/organisms/company-organism/company-organism.component';

export const HRMS_ROUTES = [
  { path: '', redirectTo: 'hrms', pathMatch: 'full' },
  {
    path: 'hrms',
    component: AdminOrganismComponent,
    data: [{
      pageName: 'hrms Page',
    }],
  },
  {
    path: 'finance',
    component: FinanceOrganismComponent,
    data: [{
      pageName: 'Finance Page',
    }],
  },
  {
    path: 'myprofile',
    component: ProfileOrganismComponent,
    data: [{
      pageName: 'My profile Page',
    }],
  },
  {
    path: 'my-profile',
    component: MyProfileOrganismComponent,
    data: [{
      pageName: 'My-Profile Page',
    }],
  },
  {
    path: 'company',
    component: CompanyOrganismComponent,
    data: [{
      pageName: 'Company Page',
    }],
  },
  {
    path: 'my-reports',
    component: ReportsOrganismComponent,
    data: [{
      pageName: 'my_report',
    }],
  },
  {
    path: 'saned-compass',
    component: ReportsOrganismComponent,
    data: [{
      pageName: 'saned_compass',
    }],
  },
  {
    path: 'my-saned',
    component: MySanadOrganismComponent,
  },
  {
    path: 'saned-workspace',
    component: SanedWorkspaceMoleculeComponent,
  },
]