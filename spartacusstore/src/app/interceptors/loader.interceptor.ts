import { Injectable } from "@angular/core";
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs";
import { finalize } from "rxjs/operators";
import { LoaderService } from '../services/loader.service';
import { environmentConst } from '../../environments/env.constant'
@Injectable()
export class LoaderInterceptor implements HttpInterceptor {
    constructor(public loaderService: LoaderService) { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if ((req.url.indexOf(environmentConst.endpoints.login.resendOTP) !== -1) || (typeof req.body === 'string' && (req.body.indexOf('commercialRegistration') !== -1))) {
            this.loaderService.hide();
        } else {
            this.loaderService.show()
        }
        return next.handle(req).pipe(
            finalize(() => this.loaderService.hide())
        );
    }
}