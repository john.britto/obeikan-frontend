import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { CommonUtilityService } from '../services/common-utility-service';
import { environment } from '../../environments/environment';
import { LoginService } from './login.service'
import { EmitterService } from './emitter.service'

@Injectable({
    providedIn: 'root',
})
@Injectable()
export class FinanceService {

    userContext: any

    constructor(
        private utilityService: CommonUtilityService,
        private loginService: LoginService,
        private emitter: EmitterService,
        private router: Router) {

        this.loginService.userContext.subscribe(context => {
            this.userContext = context;
        });
    }

    submitForm = request => {
        // const request = new HttpParams()
        // .set('cartId', this.utilityService.getCookie('cartId'))



        let httpHeaders = new HttpHeaders()
            .set(
                'Authorization',
                `Bearer ${this.utilityService.getCookie('AuthToken')}`
            )
            .set('Content-Type', 'application/json');

        let options = {
            headers: httpHeaders,
        };

        let url = `${environment.superAdmin.hrCreateUserAPI + this.utilityService.getLocalStorage('displayUid')}/masterdatasetup`;

        return this.utilityService.postRequest(url, request, options);
    }

    downloadFile = () => {

        let httpHeaders = new HttpHeaders().set(
            'Authorization',
            `Bearer ${this.utilityService.getCookie('AuthToken')}`
        );

        let options = {
            headers: httpHeaders,
        };

        let url = environment.adminDownloadApi

        return this.utilityService.getRequest(url, options)
    }

    getConversionRate = uri => {

        let httpHeaders = new HttpHeaders().set(
            'Authorization',
            `Bearer ${this.utilityService.getCookie('AuthToken')}`
        );

        let options = {
            headers: httpHeaders,
        };

        let uid = this.utilityService.getCookie('financeSuperUser') ? this.utilityService.getCookie('financeSuperUser') : this.utilityService.getLocalStorage('displayUid');
        const url = `${environment.superAdmin.conversionAPI}${uid}/${uri}`;

        return this.utilityService.getRequest(url, options)

    }

    getSupplierDetails = uri => {

        let httpHeaders = new HttpHeaders().set(
            'Authorization',
            `Bearer ${this.utilityService.getCookie('AuthToken')}`
        );

        let options = {
            headers: httpHeaders,
        };

        let uid = this.utilityService.getCookie('financeSuperUser') ? this.utilityService.getCookie('financeSuperUser') : this.utilityService.getLocalStorage('displayUid');
        const url = `${environment.superAdmin.financeGetCustomerAPI}${uid}/${uri}`;

        return this.utilityService.getRequest(url, options)

    }

    getPurchaseOrder = (type?: any) => {

        let httpHeaders = new HttpHeaders().set(
            'Authorization',
            `Bearer ${this.utilityService.getCookie('AuthToken')}`
        );

        let options = {
            headers: httpHeaders,
        };

        let url = `${environment.superAdmin.financeGetCustomerAPI}${this.utilityService.getLocalStorage('displayUid')}/purchase-order`

        url += type ? '?status=' + type : ''

        return this.utilityService.getRequest(url, options)
    }

    getApprovedMaterialShipped = () => {
        let httpHeaders = new HttpHeaders().set(
            'Authorization',
            `Bearer ${this.utilityService.getCookie('AuthToken')}`
        );

        let options = {
            headers: httpHeaders,
        };

        const url = `${environment.superAdmin.financeGetCustomerAPI}${this.utilityService.getLocalStorage('displayUid')}/material-shipped`

        return this.utilityService.getRequest(url, options)
    }

    submitPurchaseOrder = (request, action) => {

        var formData: any = new FormData();
        formData.set("attachment", '')
        formData.set("purchaseOrderData", new Blob([JSON.stringify(request.purchaseOrderData)], { type: "application/json" }))


        let httpHeaders = new HttpHeaders()
            .set(
                'Authorization',
                `Bearer ${this.utilityService.getCookie('AuthToken')}`
            )

        let options = {
            headers: httpHeaders,
        };

        const url = `${environment.superAdmin.financeGetCustomerAPI}${this.utilityService.getLocalStorage('displayUid')}/purchase-order?action=${action}`

        return this.utilityService.postRequest(url, formData, options);
    }

    submitSalesOrder = (uri, request, action) => {
        var formData: any = new FormData();
        formData.set("attachment", '')
        formData.set("salesOrderData", new Blob([JSON.stringify(request)], { type: "application/json" }))


        let httpHeaders = new HttpHeaders()
            .set(
                'Authorization',
                `Bearer ${this.utilityService.getCookie('AuthToken')}`
            )

        let options = {
            headers: httpHeaders,
        };

        const url = `${environment.superAdmin.financeGetCustomerAPI}${this.utilityService.getLocalStorage('displayUid')}/${uri}?action=${action}`

        return this.utilityService.postRequest(url, formData, options);
    }

    removeOrder = (code, endpoint) => {
        let httpHeaders = new HttpHeaders()
            .set(
                'Authorization',
                `Bearer ${this.utilityService.getCookie('AuthToken')}`
            )

        let options = {
            headers: httpHeaders,
        };

        const url = `${environment.superAdmin.financeGetCustomerAPI}${this.utilityService.getLocalStorage('displayUid')}/${endpoint}/${code}`;

        return this.utilityService.postRequest(url, undefined, options);
    }

    submitMaterialReceived = request => {

        var formData: any = new FormData();
        formData.set("attachment", '')
        formData.set("materialReceivedData", new Blob([JSON.stringify(request.materialReceivedData)], { type: "application/json" }))


        let httpHeaders = new HttpHeaders()
            .set(
                'Authorization',
                `Bearer ${this.utilityService.getCookie('AuthToken')}`
            )

        let options = {
            headers: httpHeaders,
        };

        const url = `${environment.superAdmin.financeGetCustomerAPI}${this.utilityService.getLocalStorage('displayUid')}/material-received`

        return this.utilityService.postRequest(url, formData, options);
    }

    getMR = code => {
        let httpHeaders = new HttpHeaders().set(
            'Authorization',
            `Bearer ${this.utilityService.getCookie('AuthToken')}`
        );

        let options = {
            headers: httpHeaders,
        };

        let url = `${environment.superAdmin.financeGetCustomerAPI}${this.utilityService.getLocalStorage('displayUid')}/material-received`

        url += code ? '/supplierCode/' + code : '';

        return this.utilityService.getRequest(url, options)
    }

    submitMaterialShipped = (uri, request) => {

        var formData: any = new FormData();
        formData.set("attachment", '')
        formData.set("materialShippedData", new Blob([JSON.stringify(request.materialShippedData)], { type: "application/json" }))


        let httpHeaders = new HttpHeaders()
            .set(
                'Authorization',
                `Bearer ${this.utilityService.getCookie('AuthToken')}`
            )

        let options = {
            headers: httpHeaders,
        };

        const url = `${environment.superAdmin.financeGetCustomerAPI}${this.utilityService.getLocalStorage('displayUid')}/${uri}`

        return this.utilityService.postRequest(url, formData, options);
    }

    submitPayment = request => {

        var formData: any = new FormData();
        formData.set("attachment", '')
        formData.set("PaymentsData", new Blob([JSON.stringify(request.PaymentsData)], { type: "application/json" }))


        let httpHeaders = new HttpHeaders()
            .set(
                'Authorization',
                `Bearer ${this.utilityService.getCookie('AuthToken')}`
            )

        let options = {
            headers: httpHeaders,
        };

        const url = `${environment.superAdmin.financeGetCustomerAPI}${this.utilityService.getLocalStorage('displayUid')}/payments`

        return this.utilityService.postRequest(url, formData, options);
    }

    getApprovedsupplier = () => {

        let httpHeaders = new HttpHeaders().set(
            'Authorization',
            `Bearer ${this.utilityService.getCookie('AuthToken')}`
        );

        let options = {
            headers: httpHeaders,
        };

        const url = `${environment.superAdmin.financeGetCustomerAPI}${this.utilityService.getLocalStorage('displayUid')}/approvedsuppliers`

        return this.utilityService.getRequest(url, options)
    }

    getApprovedpurchaseOrder = supplier => {

        let httpHeaders = new HttpHeaders().set(
            'Authorization',
            `Bearer ${this.utilityService.getCookie('AuthToken')}`
        );

        let options = {
            headers: httpHeaders,
        };

        const url = `${environment.superAdmin.financeGetCustomerAPI}${this.utilityService.getLocalStorage('displayUid')}/approvedpurchaseorder/${supplier}`

        return this.utilityService.getRequest(url, options)
    }

    getSalesOrder = (uri, action?: any) => {

        let httpHeaders = new HttpHeaders().set(
            'Authorization',
            `Bearer ${this.utilityService.getCookie('AuthToken')}`
        );

        let options = {
            headers: httpHeaders,
        };

        let url = `${environment.superAdmin.financeGetCustomerAPI}${this.utilityService.getLocalStorage('displayUid')}/${uri}`

        url += action ? '?status=' + action : ''

        return this.utilityService.getRequest(url, options)
    }

    getGLData = (uri, request?: any) => {



        let httpHeaders = new HttpHeaders().set(
            'Authorization',
            `Bearer ${this.utilityService.getCookie('AuthToken')}`
        );

        let options = {
            headers: httpHeaders,
        };

        let url = `${environment.superAdmin.financialEntryAPI}${this.utilityService.getLocalStorage('displayUid')}/${uri}`

        if (uri === 'period') url += request ? '?date=' + request : ''
        if (uri === 'ledger-account') url += request ? '?code=' + request : ''

        return this.utilityService.getRequest(url, options)
    }

    submitGL = (request, action) => {
        var formData: any = new FormData();
        formData.set("attachment", '')
        formData.set("financialEntries", new Blob([JSON.stringify(request)], { type: "application/json" }))


        let httpHeaders = new HttpHeaders()
            .set(
                'Authorization',
                `Bearer ${this.utilityService.getCookie('AuthToken')}`
            )

        let options = {
            headers: httpHeaders,
        };

        const url = `${environment.superAdmin.financialEntryAPI}${this.utilityService.getLocalStorage('displayUid')}/financial-entry?action=${action}`

        return this.utilityService.postRequest(url, request, options);
    }

    downloadGLTemplate = () => {
        let httpHeaders = new HttpHeaders().set(
            'Authorization',
            `Bearer ${this.utilityService.getCookie('AuthToken')}`
        );

        let options = {
            headers: httpHeaders,
        };

        let url = environment.glDownloadApi

        return this.utilityService.getRequest(url, options)
    }

    getInventoryData = uri => {
         
        let httpHeaders = new HttpHeaders().set(
            'Authorization',
            `Bearer ${this.utilityService.getCookie('AuthToken')}`
        );

        let options = {
            headers: httpHeaders,
        };

        let url = `${environment.inventoryApi}${this.utilityService.getLocalStorage('displayUid')}/${uri}`

        return this.utilityService.getRequest(url, options)
    }

    getFinanceData = uri => {
         
        let httpHeaders = new HttpHeaders().set(
            'Authorization',
            `Bearer ${this.utilityService.getCookie('AuthToken')}`
        );

        let options = {
            headers: httpHeaders,
        };

        let url = `${environment.superAdmin.financeGetCustomerAPI}${this.utilityService.getLocalStorage('displayUid')}/${uri}`

        return this.utilityService.getRequest(url, options)
    }

    submitInvoice = (uri, request) => {
        
        var formData: any = new FormData();
        formData.set("attachment", '')
        formData.set("invoiceBookingData", new Blob([JSON.stringify(request)], { type: "application/json" }))

        let httpHeaders = new HttpHeaders()
            .set(
                'Authorization',
                `Bearer ${this.utilityService.getCookie('AuthToken')}`
            )

        let options = {
            headers: httpHeaders,
        };

        const url = `${environment.superAdmin.financeGetCustomerAPI}${this.utilityService.getLocalStorage('displayUid')}/${uri}`

        return this.utilityService.postRequest(url, formData, options);
    }

    onLogout = () => {

        this.userContext.displayUID = ''
        this.userContext.displayName = ''
        this.userContext.isAuthenticated = false;

        this.loginService.removeLocalStorage()
        this.loginService.changeUserContext(this.userContext);
        this.emitter.superAdminSource.next(true);
        this.router.navigateByUrl('/');
    }

}

export const ADMIN_NAV_LINKS = [
    {
        'name': 'hrms',
        'imgUrl': '../../assets/images/finance.png',
        'active': true,
        'routerLink': 'hrms'
    },
    {
        'name': 'Finance',
        'imgUrl': '../../assets/images/hr.png',
        'active': true,
        'routerLink': 'finance'
    },
    {
        'name': 'admin',
        'imgUrl': '../../assets/images/finance.png',
        'active': false,
        'routerLink': 'hrms'
    },
    {
        'name': 'admin',
        'imgUrl': '../../assets/images/finance.png',
        'active': false,
        'routerLink': 'finance'
    }
]

export const FINANCE_NAV_LINKS = [
    {
        'name': 'Accounts Payable',
        'imgUrl': '../../assets/images/icn_masterdata.png',
        'HoverImgUrl': '../../assets/images/icn_masterdataicn_masterdata_black.png',
        'url': '../../assets/images/icons/finance/account_payable.png',
        'active': false,
    },
    {
        'name': 'Accounts Receivable',
        'imgUrl': '../../assets/images/icn_hrpoliciessetup.png',
        'HoverImgUrl': '../../assets/images/icn_hrpoliciessetupicn_hrpoliciessetup_blacksmall.png',
        'url': '../../assets/images/icons/finance/account_receivable.png',
        'active': false,
    },
    {
        'name': 'General Ledger',
        'imgUrl': '../../assets/images/icn_payrollprocess.png',
        'HoverImgUrl': '../../assets/images/icn_payrollprocessicn_payrollprocess_black.png',
        'url': '../../assets/images/icons/finance/general_ledger.png',
        'active': false,
    },
    {
        'name': 'Inventory Report',
        'imgUrl': '../../assets/images/icn_recruitment.png',
        'HoverImgUrl': '../../assets/images/icn_recruitmenticn_recruitment_black.png',
        'url': '../../assets/images/icons/finance/inventory_report.png',
        'active': false,
    },
    {
        'name': 'Sales Return',
        'imgUrl': '../../assets/images/icn_hrpoliciessetup.png',
        'HoverImgUrl': '../../assets/images/icn_hrpoliciessetupicn_hrpoliciessetup_blacksmall.png',
        'url': '../../assets/images/icons/finance/sales.png',
        'active': false,
    }
]

export const REPORTS_FILTER = [
    'Report 1',
    'Report 2'
]

export const SUPPLIERS = {
    "purchaseOrderList": [
        {
            "amount": "75.00000000",
            "item": "103",
            "itemDescription": "Item details1",
            "price": "15.00000000",
            "purchaseOrder": "00005000",
            "quantity": "5",
            "supplier": "203"
        },
        {
            "amount": "75.00000000",
            "item": "104",
            "itemDescription": "Item details2",
            "price": "15.00000000",
            "purchaseOrder": "00005002",
            "quantity": "5",
            "supplier": "204"
        }
    ]
}

export const ITEM_CODES = [
    '001',
    '002',
    '003'
]

export const CURRENCY = [
    'SAR',
    'SGD',
    'INR'
]

export const MARITAL_STATUS = [
    'Married',
    'Single',
    'Divorced'
]

export const TRANSACTION_TYPES = [
    'ReceiptsTransaction',
    'PaymentTransaction',
    'UnallocatedPayment',
    'UnallocatedReceipts',
    'AdvancePayment',
    'AdvanceReceipts'
]

export const RELIGIOUS = [
    'Hindu',
    'Muslim',
    'Christian'
]

export const assetGroups = [
    {
      "code": 1261101,
      "description": "Building"
    },
    {
      "code": 1261102,
      "description": "Plant & Machinery"
    },
    {
      "code": 1261103,
      "description": "Equipments"
    },
    {
      "code": 1261104,
      "description": "Furniture"
    },
    {
      "code": 1261106,
      "description": "Vehicles"
    },
    {
      "code": 1261107,
      "description": "Computer Hardware & Software"
    },
    {
      "code": 1261111,
      "description": "Tools"
    },
    {
      "code": 1261112,
      "description": "Mobile"
    },
    {
      "code": 1261113,
      "description": "Leasehold Improvements"
    },
    {
      "code": 1261121,
      "description": "Right to use Assets-Building"
    },
    {
        "code": 1261101,
        "description": "BUILDING"
      },
      {
        "code": 1261107,
        "description": "COMPUTERS & SOFTWARES"
      },
      {
        "code": 1261111,
        "description": "TOOLS & EQUIPMENT"
      },
      {
        "code": 1261104,
        "description": "FURNITURE & FIXTURES"
      },
      {
        "code": 1261106,
        "description": "VEHICLE"
      }
  ]

export const VALIDATION_MESSAGES = {
    fullName: [
        { type: 'required', message: 'Full name is require' },
        { type: 'name', message: 'Enter a valid name' }
    ],

    Fathername: [
        { type: 'required', message: 'Father name is required' },
        { type: 'name', message: 'Enter a valid name' },
    ],
    gender: [
        { type: 'required', message: 'Gender is required' }],
    IqamaNo: [{ type: 'required', message: 'IqamaNo is required' }],

    DOB: [{ type: 'required', message: 'Date of Birth is required' }],
    religion: [{ type: 'required', message: 'Religion is required' }],
    maritalStatus: [{ type: 'required', message: 'Marital Status is required' }],
    phoneNo: [{ type: 'required', message: 'Phone Number is required' }],
    email: [
        { type: 'required', message: 'Email is Required' },
        { type: 'email', message: 'Please enter valid email id' }
    ],
    nationality: [{ type: 'required', message: 'Nationality is required' }],
    empNo: [{ type: 'required', message: 'Employee Number is required' }],
    joiningDate: [{ type: 'required', message: 'Joining Date is required' }],
    jobTitle: [{ type: 'required', message: 'Job Title is required' }],
    grade: [{ type: 'required', message: 'Grade is required' }],
    department: [{ type: 'required', message: 'Department is required' }],
    division: [{ type: 'required', message: 'Division is required' }],
    supEmpId: [{ type: 'required', message: 'Supervisor Employee Id is required' }],
    location: [{ type: 'required', message: 'Location is required' }],
    iqamaProfession: [{ type: 'required', message: 'Profession Of Iqama is required' }],
    basicSalary: [{ type: 'required', message: 'Basic Salary is required' }],
    contractStatus: [{ type: 'required', message: 'Contract Status is required' }],
    bankName: [{ type: 'required', message: 'Bank Name is required' }],
    bankCode: [{ type: 'required', message: 'Bank Code is required' }],
    IBAN: [{ type: 'required', message: 'IBAN Number is required' }],

};

export const API_FAILURE_MSG = 'Unexpected error. Please try again'

export const PO_CREATION_SUCCESS_MSG = 'PO Created successfully'

export const PAYMENT_SUCCESS_MSG = 'Form  submitted successfully' 