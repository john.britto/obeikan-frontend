import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { CommonUtilityService } from '../services/common-utility-service';
import { environment } from '../../environments/environment';
import { LoginService } from './login.service'
import { EmitterService } from './emitter.service'

@Injectable({
    providedIn: 'root',
})
@Injectable()
export class MyProfileService {

    userContext: any

    constructor(
        private utilityService: CommonUtilityService,
        private loginService: LoginService,
        private emitter: EmitterService,
        private router: Router) {


    }

    getReviewOrders = pageType => {

        let httpHeaders = new HttpHeaders().set(
            'Authorization',
            `Bearer ${this.utilityService.getCookie('AuthToken')}`
        );

        let options = {
            headers: httpHeaders,
        };

        const url = `${environment.superAdmin.financeGetCustomerAPI}${this.utilityService.getLocalStorage('displayUid')}/review-orders?orderType=${pageType}`

        return this.utilityService.getRequest(url, options)

    }

    getMaterialEntries = (pageType, uri) => {

        let httpHeaders = new HttpHeaders().set(
            'Authorization',
            `Bearer ${this.utilityService.getCookie('AuthToken')}`
        );

        let options = {
            headers: httpHeaders,
        };

        const url = `${environment.superAdmin.financeGetCustomerAPI}${this.utilityService.getLocalStorage('displayUid')}/finance/${uri}?orderType=${pageType}`

        return this.utilityService.getRequest(url, options)

    }

    getApprovalOrders = type => {

        let httpHeaders = new HttpHeaders().set(
            'Authorization',
            `Bearer ${this.utilityService.getCookie('AuthToken')}`
        );

        let options = {
            headers: httpHeaders,
        };

        const url = `${environment.superAdmin.financeGetCustomerAPI}${this.utilityService.getLocalStorage('displayUid')}/approval-orders?orderType=${type}`

        return this.utilityService.getRequest(url, options)

    }

    getPoDetail = (code, type) => {

        let httpHeaders = new HttpHeaders().set(
            'Authorization',
            `Bearer ${this.utilityService.getCookie('AuthToken')}`
        );

        let options = {
            headers: httpHeaders,
        };

        const url = `${environment.superAdmin.financeGetCustomerAPI}${this.utilityService.getLocalStorage('displayUid')}/${type}/${code}`

        return this.utilityService.getRequest(url, options)

    }

    updateStatus = (code, type, status) => {

        let httpHeaders = new HttpHeaders()
            .set(
                'Authorization',
                `Bearer ${this.utilityService.getCookie('AuthToken')}`
            )
            .set('Content-Type', 'application/json');

        let options = {
            headers: httpHeaders,
        };

        let url = `${environment.superAdmin.financeGetCustomerAPI + this.utilityService.getLocalStorage('displayUid')}/${type}/${code}?action=${status}`;

        return this.utilityService.postRequest(url, '', options);
    }

}


export const SUCCESS_MSG = 'Your request has been '