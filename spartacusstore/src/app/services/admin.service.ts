import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { CommonUtilityService } from '../services/common-utility-service';
import { environment } from '../../environments/environment';
import { LoginService } from './login.service'
import { EmitterService } from './emitter.service'

@Injectable({
    providedIn: 'root',
})
@Injectable()
export class AdminService {

    public refreshDashboard = new Subject();
    userContext: any
    _iFrameKey: any
    public _customerRoles: any
    _userType: any

    private adminPageHeaderContextData = new BehaviorSubject<adminPageHeaderContext>(
        new adminPageHeaderContext(null));
    adminPageHeaderContext = this.adminPageHeaderContextData.asObservable();

    constructor(
        private utilityService: CommonUtilityService,
        private loginService: LoginService,
        private emitter: EmitterService,
        private router: Router) {

        this.loginService.userContext.subscribe(context => {
            this.userContext = context;
        });
    }

    get customerRoles(): any {
        return this._customerRoles;
    }

    set customerRoles(roles: any) {
        this._customerRoles = roles;
    }

    get userType(): any {
        return this._userType;
    }

    set userType(roles: any) {
        this._userType = roles;
    }

    getHeader = () => {
        let httpHeaders = new HttpHeaders()
            .set(
                'Authorization',
                `Bearer ${this.utilityService.getCookie('AuthToken')}`
            )
            .set('Content-Type', 'application/json');

        return {
            headers: httpHeaders,
        };
    }

    changeAdminPageHeaderContext = (data: adminPageHeaderContext) => {
        this.adminPageHeaderContextData.next(data);
    }

    submitForm = (request, editEmployee) => {

        let url = `${environment.superAdmin.hrCreateUserAPI + this.utilityService.getLocalStorage('displayUid')}/masterdatasetup`;
        if (editEmployee) return this.utilityService.putRequest(url + '?submode=' + request.action, request, this.getHeader());
        return this.utilityService.postRequest(url, request, this.getHeader());
    }

    getMasterData = () => {

        let url = `${environment.superAdmin.hrCreateUserAPI}${this.utilityService.getLocalStorage('displayUid')}/masterdatasetup`

        return this.utilityService.getRequest(url, this.getHeader())

    }

    getLookupData = uri => {

        let url = `${environment.lookupApi}${this.utilityService.getLocalStorage('displayUid')}/${uri}`

        return this.utilityService.getRequest(url, this.getHeader())

    }

    getPayrollData = request => {

        let url = `${environment.payrollApi}${this.utilityService.getLocalStorage('displayUid')}/payrollentry-type/?${request}`

        return this.utilityService.getRequest(url, this.getHeader())
    }

    getPayrollEntries = () => {

        let url = `${environment.payrollApi}${this.utilityService.getLocalStorage('displayUid')}/getApprovalEntries`

        return this.utilityService.getRequest(url, this.getHeader())
    }

    getPayrollEntry = (pk, uri, reason) => {
        let reasonField = reason ? `&${reason}` : '';
        let url = `${environment.payrollApi}${this.utilityService.getLocalStorage('displayUid')}/${uri}/${pk}${reasonField}`
        return this.utilityService.getRequest(url, this.getHeader())
    }

    getPayrollDetails = uri => {

        let url = `${environment.payrollApi}${this.utilityService.getLocalStorage('displayUid')}/${uri}`

        return this.utilityService.getRequest(url, this.getHeader())
    }

    submitPayroll = (payload, uri) => {

        let url = `${environment.payrollApi}${this.utilityService.getLocalStorage('displayUid')}/${uri}`
        return this.utilityService.postRequest(url, payload, this.getHeader());
    }

    submitPayrollAction = uri => {

        let url = `${environment.payrollApi}${this.utilityService.getLocalStorage('displayUid')}/${uri}`

        return this.utilityService.postRequest(url, '', this.getHeader());
    }

    deletePayroll = (uri) => {

        let url = `${environment.payrollApi}${this.utilityService.getLocalStorage('displayUid')}/${uri}`;
        return this.utilityService.deleteRequest(url, this.getHeader());
    }

    getRunPayroll = uri => {

        let url = `${environment.payrollApi}${this.utilityService.getLocalStorage('displayUid')}/${uri}`

        return this.utilityService.getRequest(url, this.getHeader())
    }

    getHrRequest = uri => {

        let url = `${environment.superAdmin.hrRequestAPI}${this.utilityService.getLocalStorage('displayUid')}/${uri}`

        return this.utilityService.getRequest(url, this.getHeader())
    }

    getEmpDetails = uri => {

        let url = `${environment.superAdmin.hrCreateUserAPI}${this.utilityService.getLocalStorage('displayUid')}/${uri}`

        return this.utilityService.getRequest(url, this.getHeader())
    }

    postHrRequest = (uri, request) => {

        let url = `${environment.superAdmin.hrRequestAPI}${this.utilityService.getLocalStorage('displayUid')}/${uri}`
        return this.utilityService.postRequest(url, request, this.getHeader());
    }

    postHrLeaveDays = (uri, request) => {
        let url = `${environment.superAdmin.hrRequestAPI}${this.utilityService.getLocalStorage('displayUid')}/${uri}`
        return this.utilityService.postRequest(url, request, this.getHeader());
    }

    postPolicyRequest = (uri, request) => {

        let url = `${environment.policyApi}${this.utilityService.getLocalStorage('displayUid')}/${uri}`
        return this.utilityService.postRequest(url, request, this.getHeader());
    }

    getPolicyRequest = uri => {

        let url = `${environment.policyApi}${this.utilityService.getLocalStorage('displayUid')}/${uri}`

        return this.utilityService.getRequest(url, this.getHeader())
    }

    postHRRequest = (uri, request) => {

        let url = `${environment.superAdmin.hrRequestAPI}${this.utilityService.getLocalStorage('displayUid')}/${uri}/${request}`
        return this.utilityService.postRequest(url, '', this.getHeader());
    }

    updateRunPayroll = uri => {

        let url = `${environment.superAdmin.hrCreateUserAPI}${this.utilityService.getLocalStorage('displayUid')}/${uri}`
        return this.utilityService.postRequest(url, '', this.getHeader());
    }

    updateCancelEntries = uri => {

        let url = `${environment.superAdmin.hrRequestAPI}${this.utilityService.getLocalStorage('displayUid')}/${uri}`
        return this.utilityService.getRequest(url, this.getHeader());
    }

    getCompanyDetails = uri => {

        let url = `${environment.companyAPI}${this.utilityService.getLocalStorage('displayUid')}/${uri}`
        return this.utilityService.getRequest(url, this.getHeader());
    }

    updateCompanyRequest = (uri, request) => {

        let url = `${environment.companyAPI}${this.utilityService.getLocalStorage('displayUid')}/${uri}`

        return this.utilityService.putRequest(url, request, this.getHeader());

    }

    putRPRequest = (uri, request) => {

        let url = `${environment.superAdmin.hrRequestAPI}${this.utilityService.getLocalStorage('displayUid')}/${uri}`

        return this.utilityService.putRequest(url, request, this.getHeader());

    }

    hrPutRequest = (uri, request) => {

        let url = `${environment.superAdmin.hrCreateUserAPI}${this.utilityService.getLocalStorage('displayUid')}/${uri}`

        return this.utilityService.putRequest(url, request, this.getHeader());

    }

    uploadDpRequest = (uri, file) => {

        const authToken = `Bearer ${this.utilityService.getCookie('AuthToken')}`
        const httpHeaders = new HttpHeaders({
            'Authorization': authToken
        });

        var formData: any = new FormData();
        formData.set('profilePic', new Blob([file]))

        let url = `${environment.superAdmin.hrCreateUserAPI}${this.utilityService.getLocalStorage('displayUid')}/${uri}`

        return this.utilityService.postRequest(url, formData, { headers: httpHeaders });

    }

    uploadCompanyDpRequest = (uri, file) => {

        const authToken = `Bearer ${this.utilityService.getCookie('AuthToken')}`
        const httpHeaders = new HttpHeaders({
            'Authorization': authToken
        });

        var formData: any = new FormData();
        if(uri === 'company/profile-pic') formData.set('profilePic', new Blob([file]))
        if(uri === 'company/template-upload') {
            if(!!file.invoice)formData.set('invoiceTemplate', new Blob([file.invoice]))
            if(!!file.statement)formData.set('statementTemplate', new Blob([file.statement]))
            if(!!file.report)formData.set('reportTemplate', new Blob([file.report]))
        }

        let url = `${environment.companyAPI}${this.utilityService.getLocalStorage('displayUid')}/${uri}`

        return this.utilityService.postRequest(url, formData,{ headers: httpHeaders });

    }

    downloadFile = () => {

        let url = environment.adminDownloadApi

        return this.utilityService.getRequest(url, this.getHeader())
    }

    uploadTemplate = (request, file) => {

        var formData: any = new FormData();
        formData.set("attachment", file)
        formData.set("", new Blob([JSON.stringify(request)], { type: "application/json" }))

        let url = `${environment.payrollApi + this.utilityService.getLocalStorage('displayUid')}/create-bulkpayrollentries`;

        return this.utilityService.postRequest(url, request, this.getHeader());
    }

    isAuthorizedUser = (pageType, userRoles?: any) => {
        let roles = JSON.parse(this.utilityService.getCookie('customerRoles'));

        switch (pageType) {
            case 'hrms':
                return (roles.includes('HR') || roles.includes('GeneralManager')) || roles.includes('HREmployee') ? true : this.router.navigate(['/admin/myprofile'])
            case 'finance':
                return roles.includes('Finance') || roles.includes('FinanceManager') || (userRoles && userRoles.includes('Finance_CORE')) ? true : this.router.navigate(['/admin/myprofile'])
            case 'hrEmployee':
                return roles.includes('HREmployee') ? true : this.router.navigate(['/admin/myprofile'])
            default:
                return true
        }
    }

    setCode = (process, approvals) => {
        let code;
        approvals.forEach(level => {
            if (level.name === process) code = level.code
        })
        return code
    }

    onLogout = () => {

        this.userContext.displayUID = ''
        this.userContext.displayName = ''
        this.userContext.isAuthenticated = false;

        this.loginService.removeLocalStorage()

        // localStorage.removeItem('customerRoles')
        this.loginService.changeUserContext(this.userContext);
        this.emitter.superAdminSource.next(true);
        this.router.navigateByUrl('/');
    }

    get iframeKey() {
        return this._iFrameKey;
    }

    set iframeKey(roles) {
        this._iFrameKey = roles;
    }

    resetPassword = (oldPwd, newPwd) => {
        const authToken = `Bearer ${this.utilityService.getCookie('AuthToken')}`
        const httpHeaders = new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded;',
            'Authorization': authToken
        });

        const body = {
            oldPassword : oldPwd,
            newPassword : newPwd
          };
          const request: HttpParams = this.utilityService.buildQueryParams(body);

        let url = `${environment.companyAPI}${this.utilityService.getLocalStorage('displayUid')}/password`

        return this.utilityService.putRequest(url, request.toString(), { headers: httpHeaders });
    }
}

export class adminPageHeaderContext {
    constructor(public pageTitle: string) {
        this.pageTitle = pageTitle
    }
}

export const ADMIN_NAV_LINKS = [
    {
        'name': 'Hr',
        'imgUrl': '../../assets/images/icons/menu/hr_mgnt_grey.png',
        'hoverImg': '../../assets/images/icons/menu/hr_mgnt_green.png',
        'url': '../../assets/images/icons/menu/hr_mgnt_grey.png',
        'active': true,
        'routerLink': 'hrms',
        'fullName': 'HR Management',
        'isActiveLink': false
    },
    {
        'name': 'Finance',
        'imgUrl': '../../assets/images/icons/menu/finance_grey.png',
        'hoverImg': '../../assets/images/icons/menu/finance_green.png',
        'url': '../../assets/images/icons/menu/finance_grey.png',
        'active': true,
        'routerLink': 'finance',
        'fullName': 'Finance',
        'isActiveLink': false
    },
    {
        'name': 'admin',
        'imgUrl': '../../assets/images/icons/menu/my_history_grey.png',
        'hoverImg': '../../assets/images/icons/menu/my_history_green.png',
        'url': '../../assets/images/icons/menu/my_history_grey.png',
        'active': true,
        'routerLink': 'myprofile',
        'fullName': 'My History',
        'isActiveLink': false
    },
    {
        'name': 'my-profile',
        'imgUrl': '../../assets/images/icons/menu/my_sanad_grey.png',
        'hoverImg': '../../assets/images/icons/menu/my_sanad_green.png',
        'url': '../../assets/images/icons/menu/my_sanad_grey.png',
        'active': true,
        'routerLink': 'my-profile',
        'fullName': 'My Profile'
    },
    {
        'name': 'company',
        'imgUrl': '../../assets/images/icons/menu/my_history_grey.png',
        'hoverImg': '../../assets/images/icons/menu/my_history_green.png',
        'url': '../../assets/images/icons/menu/my_history_grey.png',
        'active': false,
        'routerLink': 'company',
        'fullName': 'My Company'
    },
    {
        'name': 'supplyChain',
        'imgUrl': '../../assets/images/icons/menu/my_history_grey.png',
        'hoverImg': '../../assets/images/icons/menu/my_history_green.png',
        'url': '../../assets/images/icons/menu/my_history_grey.png',
        'active': false,
        'routerLink': 'supplyChain',
        'fullName': 'Supply Chain',
        'isActiveLink': false
    },
    {
        'name': 'manufacturing',
        'imgUrl': '../../assets/images/icons/menu/my_history_grey.png',
        'hoverImg': '../../assets/images/icons/menu/my_history_green.png',
        'url': '../../assets/images/icons/menu/my_history_grey.png',
        'active': false,
        'routerLink': 'manufacturing',
        'fullName': 'Manufacturing',
        'isActiveLink': false
    },
    {
        'name': 'my-reports',
        'imgUrl': '../../assets/images/icons/menu/my_report_grey.png',
        'hoverImg': '../../assets/images/icons/menu/my_report_green.png',
        'url': '../../assets/images/icons/menu/my_report_grey.png',
        'active': true,
        'routerLink': 'my-reports',
        'fullName': 'My Reports',
        'isActiveLink': false
    },
    {
        'name': 'saned-compass',
        'imgUrl': '../../assets/images/icons/menu/my_report_grey.png',
        'hoverImg': '../../assets/images/icons/menu/my_report_green.png',
        'url': '../../assets/images/icons/menu/my_report_grey.png',
        'active': true,
        'routerLink': 'saned-compass',
        'fullName': 'saned compass'
    },
    {
        'name': 'HREmployee',
        'imgUrl': '../../assets/images/icons/menu/my_sanad_grey.png',
        'hoverImg': '../../assets/images/icons/menu/my_sanad_green.png',
        'url': '../../assets/images/icons/menu/my_sanad_grey.png',
        'active': true,
        'routerLink': 'my-saned',
        'fullName': 'My Saned',
        'isActiveLink': false
    },
    {
        'name': 'SanedWorkspace',
        'imgUrl': '../../assets/images/icons/menu/saned_workspace_grey.png',
        'hoverImg': '../../assets/images/icons/menu/saned_workspace_green.png',
        'url': '../../assets/images/icons/menu/saned_workspace_grey.png',
        'active': true,
        'routerLink': 'saned-workspace',
        'fullName': 'Saned Workspace',
        'isActiveLink': false
    }
]

export const SETUP_NAV_LINKS = [
    {
        'name': 'Master data Setup',
        'caseName': 'MasterdataSetup',
        'imgUrl': '../../assets/images/icn_masterdata.png',
        'HoverImgUrl': '../../assets/images/icn_masterdataicn_masterdata_black.png',
        'url': '../../assets/images/icn_masterdata.png',
        'active': false,
    },
    {
        'name': 'HR Policies Setup',
        'caseName': 'HRPoliciesSetup',
        'imgUrl': '../../assets/images/icn_hrpoliciessetup.png',
        'HoverImgUrl': '../../assets/images/icn_hrpoliciessetupicn_hrpoliciessetup_blacksmall.png',
        'url': '../../assets/images/icn_hrpoliciessetup.png',
        'active': false,
    },
    {
        'name': 'Payroll Process',
        'caseName': 'PayrollProcess',
        'imgUrl': '../../assets/images/icn_payrollprocess.png',
        'HoverImgUrl': '../../assets/images/icn_payrollprocessicn_payrollprocess_black.png',
        'url': '../../assets/images/icn_payrollprocess.png',
        'active': false,
    },
    {
        'name': 'Recruitment',
        'caseName': 'Recruitment',
        'imgUrl': '../../assets/images/icn_recruitment.png',
        'HoverImgUrl': '../../assets/images/icn_recruitmenticn_recruitment_black.png',
        'url': '../../assets/images/icn_recruitment.png',
        'active': false,
    }
]

export const REPORTS_FILTER = [
    'Report 1',
    'Report 2'
]

export const GENDERS = [
    'Male',
    'Female'
]

export const COUNTRIES = [
    'India',
    'Dubai',
    'Saudi Arabia'
]

export const MARITAL_STATUS = [
    'Married',
    'Single',
    'Divorced'
]

export const RELIGIOUS = [
    'Hindu',
    'Muslim',
    'Christian'
]

export const VALIDATION_MESSAGES = {

    firstName: [
        { type: 'required', message: 'First name is required' },
        { type: 'name', message: 'Enter a valid first name' }
    ],

    lastName: [
        { type: 'required', message: 'Last name is required' },
        { type: 'name', message: 'Enter a valid last name' }
    ],

    Fathername: [
        { type: 'required', message: 'Father name is required' },
        { type: 'name', message: 'Enter a valid name' },
    ],
    gender: [
        { type: 'required', message: 'Gender is required' }],
    IqamaNo: [{ type: 'required', message: 'ID is required' }],

    DOB: [{ type: 'required', message: 'Date of Birth is required' }],
    religion: [{ type: 'required', message: 'Religion is required' }],
    maritalStatus: [{ type: 'required', message: 'Marital Status is required' }],
    phoneNo: [{ type: 'required', message: 'Phone Number is required' },
    { type: 'pattern', message: 'Please enter a valid mobile number' }],
    countryCode: [{ type: 'required', message: 'Country Code is required' },
    { type: 'pattern', message: 'Please enter a valid Country Code' }],
    otherCountryCode: [{ type: 'required', message: 'Country Code is required' },
    { type: 'pattern', message: 'Please enter a valid Country Code' }],
    email: [
        { type: 'required', message: 'Email is Required' },
        { type: 'email', message: 'Please enter valid email id' }
    ],
    nationality: [{ type: 'required', message: 'Nationality is required' }],
    empNo: [{ type: 'required', message: 'Employee Number is required' }],
    joiningDate: [{ type: 'required', message: 'Joining Date is required' }],
    changeEffectiveDate: [{ type: 'required', message: 'Change Effective Date is required' }],
    jobTitle: [{ type: 'required', message: 'Job Title is required' }],
    grade: [{ type: 'required', message: 'Grade is required' }],
    town: [{ type: 'required', message: 'Town of birth is required' }],
    department: [{ type: 'required', message: 'Department is required' }],
    division: [{ type: 'required', message: 'Division is required' }],
    supEmpId: [{ type: 'required', message: 'Supervisor Employee Id is required' }],
    location: [{ type: 'required', message: 'Location is required' }],
    iqamaProfession: [{ type: 'required', message: 'Profession Of Iqama is required' }],
    basicSalary: [{ type: 'required', message: 'Basic Salary is required' }],
    contractStatus: [{ type: 'required', message: 'Contract Status is required' }],
    housingPercentage: [{ type: 'required', message: 'Housing Percentage is required' }],
    transportationPercentage: [{ type: 'required', message: 'Transportation Percentage is required' }],
    overtime: [{ type: 'required', message: 'Overtime is required' }],
    medicalInsurence: [{ type: 'required', message: 'Medical Insurance is required' }],
    bankName: [{ type: 'required', message: 'Bank Nmae is required' }],
    bankCode: [
        { type: 'required', message: 'Bank Code is required' },
        { type: 'pattern', message: 'Enter Proper Bank Code' }
    ],
    IBAN: [{ type: 'required', message: 'IBAN Number is required' },
    { type: 'pattern', message: 'Enter Proper IBAN' }],

};