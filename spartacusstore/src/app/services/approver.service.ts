import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CommonUtilityService } from '../services/common-utility-service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
@Injectable()

export class ApproverService {
    
    constructor(
        private http: HttpClient,
        private utilityService: CommonUtilityService
      ) {}

      getApprovalList = () => {

        let httpHeaders = new HttpHeaders().set(
            'Authorization',
            `Bearer ${this.utilityService.getCookie('AuthToken')}`
        );

        let options = {
            headers: httpHeaders,
        };

        const url = `${environment.superAdmin.financeGetCustomerAPI}${this.utilityService.getLocalStorage('displayUid')}/finance/master-data-approval`

        return this.utilityService.getRequest(url, options)

    }

    updateStatus = (code, status) => {

        let httpHeaders = new HttpHeaders()
            .set(
                'Authorization',
                `Bearer ${this.utilityService.getCookie('AuthToken')}`
            )
            .set('Content-Type', 'application/json');

        let options = {
            headers: httpHeaders,
        };

        let url = `${environment.superAdmin.financeGetCustomerAPI + this.utilityService.getLocalStorage('displayUid')}/finance/master-data//${code}?action=${status}`;

        return this.utilityService.postRequest(url, '', options);
    }
}


export const SUCCESS_MSG = 'Your request has been '

export const ATTACHMENT_URL ='/medias/hrServicesMasterDataSetupTemplate.xlsx?context=bWFzdGVyfGltYWdlc3w4NzY0fGFwcGxpY2F0aW9uL3ZuZC5vcGVueG1sZm9ybWF0cy1vZmZpY2Vkb2N1bWVudC5zcHJlYWRzaGVldG1sLnNoZWV0fGhmMS9oZTQvODc5NzY2ODAxNjE1OC9oclNlcnZpY2VzTWFzdGVyRGF0YVNldHVwVGVtcGxhdGUueGxzeHxkYWEzMjY5ZjViNzA3NzlmOTAwMmFkZjE1ZmMwNWNmYTM0NGE1M2ZmYjI5YTZlNDUxNjBkMDNjZThmOGQxNzA3&attachment=true'

