import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
@Injectable()
export class LoaderService {
    isLoading = new Subject<boolean>();
    isInProgress = false
    show() {
        this.isInProgress = true
        this.isLoading.next(true);
    }
    hide() {
        this.isInProgress = false
        this.isLoading.next(false);
    }
}
