import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { CommonUtilityService } from '../services/common-utility-service';
import { environment } from '../../environments/environment';
import { LoginService } from './login.service'
import { EmitterService } from './emitter.service'

@Injectable({
    providedIn: 'root',
})
@Injectable()
export class PayRollService {

    userContext: any

    constructor(
        private utilityService: CommonUtilityService,
        private loginService: LoginService,
        private emitter: EmitterService,
        private router: Router) {


    }

    getPayroll = (empNo, effDate) => {

        let httpHeaders = new HttpHeaders().set(
            'Authorization',
            `Bearer ${this.utilityService.getCookie('AuthToken')}`
        );

        let options = {
            headers: httpHeaders,
        };

        const url = `${environment.payrollApi}${this.utilityService.getLocalStorage('displayUid')}/payrollentries/${empNo}?effectiveDate=${effDate}`

        return this.utilityService.getRequest(url, options)

    }

}

export const SUCCESS_MSG = 'Your request has been '