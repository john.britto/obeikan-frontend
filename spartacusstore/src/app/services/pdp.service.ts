import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders , HttpParams} from '@angular/common/http';
import { CommonUtilityService } from '../services/common-utility-service';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ThrowStmt } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class PdpService {

  constructor(private http: HttpClient, private utilService: CommonUtilityService) { }
  checkCartExist(url) {
    const token = this.utilService.getCookie('AuthToken');
    const authToken = 'Bearer ' + token;
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded;',
      Authorization: authToken
    });
    return this.http.get(url, {headers: httpHeaders});
   }

   createCart(url) {
    const token = this.utilService.getCookie('AuthToken');
    const authToken = 'Bearer ' + token;
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded;',
      Authorization: authToken
    });
    return this.http.post(url, '', {headers: httpHeaders});
   }

   addToCart(code, url, qty) {
    const token = this.utilService.getCookie('AuthToken');
    const authToken = 'Bearer ' + token;
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded;',
       Authorization: authToken
    });
    const request: HttpParams  = this.utilService.buildQueryParams({code, qty});

    return this.http.post(url, request.toString(), {headers: httpHeaders});
   }

   addToCartRenewal(code, url, qty, months) {
    const token = this.utilService.getCookie('AuthToken');
    const authToken = 'Bearer ' + token;
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded;',
       Authorization: authToken
    });
    const request: HttpParams  = this.utilService.buildQueryParams({code, qty, months, renew: true});

    return this.http.post(url, request.toString(), {headers: httpHeaders});
   }

   subscription(url, priceRange) {
    const token = this.utilService.getCookie('AuthToken');
    const authToken = 'Bearer ' + token;
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded;',
       Authorization: authToken
    });
    const request: HttpParams  = this.utilService.buildQueryParams({priceRange});

    return this.http.post(url, request.toString(), {headers: httpHeaders});
   }
}

