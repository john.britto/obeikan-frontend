import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router  } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { CommonUtilityService } from '../services/common-utility-service';
import { environment } from '../../environments/environment';
import { LoginService } from './login.service'
import { EmitterService } from './emitter.service'
import { appConfig } from 'src/app/app.config';

@Injectable({
  providedIn: 'root'
})

export class SuperAdminService {

  private superAdminRoles = new Subject<string[]>();

  constructor( private utilityService: CommonUtilityService,
    private loginService: LoginService,
    private emitter: EmitterService,
    private router: Router) { }
  
  setSuperAdminRoles(roles: string[]) {
      this.superAdminRoles.next(roles);
  }

  getSuperAdminRole(): Observable<any> {
      return this.superAdminRoles.asObservable();
  }

  submitForm = request => {

    let httpHeaders = new HttpHeaders()
    .set(
    'Authorization',
    `Bearer ${this.utilityService.getCookie('AuthToken')}`
    )
    .set('Content-Type', 'application/json');

    let options = {
    headers: httpHeaders,
    };

    let url = `${environment.superAdmin.financeGetCustomerAPI + this.utilityService.getLocalStorage('displayUid')}/customermasterdatasetup`;

    return this.utilityService.postRequest(url, request, options);
}

submitFinanceMasterFormUpload = request => {
  let httpHeaders = new HttpHeaders()
  .set(
  'Authorization',
  `Bearer ${this.utilityService.getCookie('AuthToken')}`
  )

  let options = {
  headers: httpHeaders,
  };

  let url = `${environment.superAdmin.financeGetCustomerAPI + this.utilityService.getLocalStorage('displayUid')}/customermasterdatasetup`;

  return this.utilityService.postRequest(url, request, options);
}

submitFinanceManagerForm = (request, id) => {

  let httpHeaders = new HttpHeaders()
  .set(
  'Authorization',
  `Bearer ${this.utilityService.getCookie('AuthToken')}`
  )
  .set('Content-Type', 'application/json');

  let options = {
  headers: httpHeaders,
  };

  let url = `${environment.superAdmin.financeGetCustomerAPI + id}/finance-openbalance`;

  return this.utilityService.postRequest(url, request, options);
}

  validateUploadedTemplate = json => {

    let datePattern =  new RegExp(appConfig.pattern.datePattern)
    let errors: String[] = []

    let isCustomerValidationFailed = false
    if(json.customerInfo.length > 0) {
      let bpCode = [];
      for(let i = 0; i < json.customerInfo.length; i++) {
        if(!bpCode.includes(json.customerInfo[i].bpCode)) {
          bpCode.push(json.customerInfo[i].bpCode)
        } else {
          let error = 'Duplicate bpCode found in CustomerInfo'
          errors.push(error)
          isCustomerValidationFailed = true
          break
        }
      }
    }

    let isSupplierValidationFailed = false
    if(json.supplierInfo.length > 0) {
      let bpCode = [];
      for(let i = 0; i < json.supplierInfo.length; i++) {
        if(!bpCode.includes(json.supplierInfo[i].bpCode)) {
          bpCode.push(json.supplierInfo[i].bpCode)
        } else {
          let error = 'Duplicate bpCode found in SupplierInfo'
          errors.push(error)
          isSupplierValidationFailed = true
          break
        }
      }
    }

    let isItemInfoValidationFailed = false
    if(json.itemInfo.length > 0) {
      let itemCode = []
      for(let i = 0; i < json.itemInfo.length; i++) {
        if(!itemCode.includes(json.itemInfo[i].itemCode) && !isNaN(json.itemInfo[i].purchasePrice) && !isNaN(json.itemInfo[i].salesPrice)) {
          itemCode.push(json.itemInfo[i].itemCode)
        } else {
          console.log('itemInfo', i+2)
          let error = '';
          if(itemCode.includes(json.itemInfo[i].itemCode)) error = 'Duplicate itemId'
          if(isNaN(json.itemInfo[i].purchasePrice)) error += !!error ? ' and invalid purchase price' : 'invalid purchase price'
          if(isNaN(json.itemInfo[i].salesPrice)) error += !!error ? ' and invalid sales price' : 'invalid sales price'
          error += ' found in ItemInfo'
          errors.push(error)
          isItemInfoValidationFailed = true
          break
        }
      }
    }

    let isWarehouseValidationFailed = false
    if(json.warehouse.length > 0) {
      let code = [];
      for(let i = 0; i < json.warehouse.length; i++) {
        if(!code.includes(json.warehouse[i].code)) {
          code.push(json.warehouse[i].code)
        } else {
          let error = 'Duplicate Code found in Warehouse'
          errors.push(error)
          isWarehouseValidationFailed = true
          break
        }
      }
    }

    let isAssetValidationFailed = false
    if(json.asset.length > 0) {
      let assetCode = []
      for(let i = 0; i < json.asset.length; i++) {
        if(!assetCode.includes(json.asset[i].assetCode) && datePattern.test(json.asset[i].serviceDate) && datePattern.test(json.asset[i].purchaseDate)) {
          assetCode.push(json.asset[i].assetCode)
        } else {
          console.log('assets', i+2)
          let error = '';
          if(assetCode.includes(json.asset[i].assetCode)) error = 'Duplicate assetId'
          if(!datePattern.test(json.asset[i].serviceDate)) error += !!error ? ' and invalid Service Date' : 'invalid service date'
          if(!datePattern.test(json.asset[i].purchaseDate)) error += !!error ? ' and invalid Purchase Date' : 'invalid Purchase date'
          error += ' found in Assets'
          errors.push(error)
          isAssetValidationFailed = true
          break
        }
      }
    }

    let isLedgerValidationFailed = false
    if(json.ledgerAccount.length > 0) {
      let accountNumber = [];
      for(let i = 0; i < json.ledgerAccount.length; i++) {
        if(!accountNumber.includes(json.ledgerAccount[i].accountNumber)) {
          accountNumber.push(json.ledgerAccount[i].accountNumber)
        } else {
          let error = 'Duplicate Account number found in LedgerAccount'
          errors.push(error)
          isLedgerValidationFailed = true
          break
        }
      }
    }

    let isPeriodValidationFailed = false
    if(json.period.length > 0) {
      for(let i = 0; i < json.period.length; i++) {
        if(!datePattern.test(json.period[i].startDate)) {
          let error = 'Invalid Start date found in Period'
          errors.push(error)
          isPeriodValidationFailed = true
          break
        }
      }
    }

    let isDimensionValidationFailed = false
    if(json.dimension.length > 0) {
      let code = [];
      for(let i = 0; i < json.dimension.length; i++) {
        if(!code.includes(json.dimension[i].code)) {
          code.push(json.dimension[i].code)
        } else {
          let error = 'Duplicate Code found in Dimension'
          errors.push(error)
          isDimensionValidationFailed = true
          break
        }
      }
    }

    let isOBValidationFailed = false
    if(json.openingBalanceInfo.length > 0) {
      let uid = [];
      for(let i = 0; i < json.openingBalanceInfo.length; i++) {
        if(!uid.includes(json.openingBalanceInfo[i].uid) && datePattern.test(json.openingBalanceInfo[i].date) && !isNaN(json.openingBalanceInfo[i].amount)) {
          uid.push(json.openingBalanceInfo[i].uid)
        } else {
          let error = ''
          if(uid.includes(json.openingBalanceInfo[i].uid)) error = 'Duplicate Uid'
          if(!datePattern.test(json.openingBalanceInfo[i].date)) error += !!error ? ' and invalid Date' : 'invalid date'
          if(isNaN(json.openingBalanceInfo[i].amount)) error += !!error ? ' and invalid Amount' : 'invalid Amount'
          error += ' found in Opening Balance'
          isOBValidationFailed = true
          break
        }
      }
    }

    let isBPValidationFailed = false
    if(json.bpTransUploadInfo.length > 0) {
      let documentNumber = [];
      for(let i = 0; i < json.bpTransUploadInfo.length; i++) {
        if(!documentNumber.includes(json.bpTransUploadInfo[i].documentNumber) && datePattern.test(json.bpTransUploadInfo[i].documentDate)) {
          documentNumber.push(json.bpTransUploadInfo[i].documentNumber)
        } else {
          let error = ''
          if(documentNumber.includes(json.bpTransUploadInfo[i].documentNumber)) error = 'Duplicate Dcument Number'
          if(!datePattern.test(json.bpTransUploadInfo[i].documentDate)) error += !!error ? ' and invalid Document Date' : 'invalid Document date'
          error += ' found in BP Trans Upload'
          isBPValidationFailed = true
          break
        }
      }
    }

    let isItemUploadValidationFailed = false
    if(json.itemUploadInfo.length > 0) {
      let itemCode = [];
      for(let i = 0; i < json.itemUploadInfo.length; i++) {
        if(!itemCode.includes(json.itemUploadInfo[i].itemCode) && !isNaN(json.itemUploadInfo[i].purchasePrice) && !isNaN(json.itemUploadInfo[i].salesPrice )) {
          itemCode.push(json.itemUploadInfo[i].itemCode)
        } else {
          let error = ''
          if(itemCode.includes(json.itemUploadInfo[i].itemCode)) error = 'Duplicate itemCode'
          if(isNaN(json.itemUploadInfo[i].purchasePrice)) error += !!error ? ' and invalid purchase Price' : 'invalid purchase Price'
          if(isNaN(json.itemUploadInfo[i].salesPrice)) error += !!error ? ' and invalid sales Price' : 'invalid sales Price'
          error += ' found in Item Upload'
          isItemUploadValidationFailed = true
          break
        }
      }
    }
    return {
      isCustomerValidationFailed,
      isItemUploadValidationFailed,
      isItemInfoValidationFailed,
      isBPValidationFailed,
      isDimensionValidationFailed,
      isPeriodValidationFailed,
      isLedgerValidationFailed,
      isAssetValidationFailed,
      isWarehouseValidationFailed,
      isSupplierValidationFailed,
      isOBValidationFailed,
      errors
    }
  }
}
export const VALIDATION_MESSAGES = {
  bpCode: [{ type: 'required', message: 'BP code is required' }], 
  accountNumber: [{ type: 'required', message: 'Account Number is required' }], 
  searchKey: [{ type: 'required', message: 'Search Key is required' }], 
  subLevel: [{ type: 'required', message: 'Sublevel is required' }], 
  accountType: [{ type: 'required', message: 'Account Type is required' }], 
  type: [{ type: 'required', message: 'Type is required' }], 
  bank: [{ type: 'required', message: 'Bank is required' }], 
  status: [{ type: 'required', message: 'Status is required' }],
  parentLedger: [{ type: 'required', message: 'Parent Ledger is required' }], 
  dimension1: [{ type: 'required', message: 'Dimension one is required' }],
  dimension2: [{ type: 'required', message: 'Dimension two is required' }],
  dimension3: [{ type: 'required', message: 'Dimension three is required' }],
  dimension4: [{ type: 'required', message: 'Dimension four is required' }],
  dimension5: [{ type: 'required', message: 'Dimension five is required' }], 
  dimensionType: [{ type: 'required', message: 'Dimension Type is required' }], 
  name: [{ type: 'required', message: 'Name is required' }],
  address: [{ type: 'required', message: 'Address is required' }],
  country: [{ type: 'required', message: 'Country is required' }],
  city: [{ type: 'required', message: 'City is required' }],
  streetName: [{ type: 'required', message: 'Street Name is required' }],
  paymentTerm: [{ type: 'required', message: 'Terms of Payment is required' }],
  DeliveryTerm: [{ type: 'required', message: 'Terms of Deliveries is required' }],
  PaymentTerm: [{ type: 'required', message: 'Terms of Payment is required' }],
  vatNumber: [{ type: 'required', message: 'Vat number is required' }],
  currency: [{ type: 'required', message: 'Currency is required' }],
  financialGroup: [{ type: 'required', message: 'Financial group is required' }],

  itemCode: [{ type: 'required', message: 'Item code is required' }],
  description: [{ type: 'required', message: 'Description is required' },
  { type: 'pattern', message: 'Maximum 35 characters allowed' }],
  itemType: [{ type: 'required', message: 'Item type is required' }],
  itemGroup: [{ type: 'required', message: 'Item group is required' }],
  unitSet: [{ type: 'required', message: 'Unit set is required' }],
  inventoryUnit: [{ type: 'required', message: 'Inventory unit is required' }],
  price: [{ type: 'required', message: 'Price is required' }],
  purchasePrice: [{ type: 'required', message: 'Purchase Price is required' }],
  salesPrice: [{ type: 'required', message: 'Sales Price is required' }],
  code: [{ type: 'required', message: 'Code is required' },
  { type: 'pattern', message: 'Maximum 5 characters allowed' }],
  warehouse: [{ type: 'required', message: 'Warehouse is required' }],
  quantity: [{ type: 'required', message: 'Quantity is required' }],

  assetCode: [{ type: 'required', message: 'Code is required' }],
  assetName: [{ type: 'required', message: 'Name is required' }],
  assetExtension: [{ type: 'required', message: 'Ext is required' }],
  group: [{ type: 'required', message: 'Group is required' }],
  category: [{ type: 'required', message: 'category is required' }],
  assetdescription: [{ type: 'required', message: 'desc is required' }],
  transactionCost: [{ type: 'required', message: 'cost is required' }],
  transactionCurrency: [{ type: 'required', message: 'currency is required' }],

  periodType: [{ type: 'required', message: 'Period Type is required' }],
  year: [{ type: 'required', message: 'Period Type is required' }],
  period: [{ type: 'required', message: 'Period Type is required' }],
  startDate: [{ type: 'required', message: 'Period Type is required' }],
  descriptionPeriod: [{ type: 'required', message: 'Period Type is required' }],
  correctionPeriod: [{ type: 'required', message: 'Period Type is required' }],
  status1: [{ type: 'required', message: 'AP is required' }],
  status2: [{ type: 'required', message: 'AR is required' }],
  status3: [{ type: 'required', message: 'CMG is required' }],
  status4: [{ type: 'required', message: 'INT is required' }],
  status5: [{ type: 'required', message: 'GLD is required' }],

  key: [{ type: 'required', message: 'Code is required' }],
  value: [{ type: 'required', message: 'Description is required' }],
  assetLifeYear: [{ type: 'required', message: 'Year is required' }, { type: 'pattern', message: 'Please Enter valid Year in number.' }],
  assetLifeMonth: [{ type: 'required', message: 'Month is required' }, { type: 'pattern', message: 'Please Enter valid Month in number.' }],
};

export const assetCategory = [
  "A1100-AIR COOLERS",
  "A1200-AIR CONDITIONERS",
  "A1500-AIR CONDITIONERS",
  "B1100-BUILDINGS",
  "C1100-COMPUTERS & LAPTOPS",
  "C1300-LAPTOPS",
  "C1400-COMPUTER PRINTERS",
  "C1600-NETWORK",
  "C1700-ERP EQUIPMENT",
  "C1800-COMPUTER SERVERS",
  "C1900-COMPUTER PRINTERS",
  "C2000-COMPUTER HARDWARE AND SOFTWARE",
  "E1100-EQUIPMENTS",
  "E1200-TELEPHONE & MOBILES",
  "E1400-EQUIPMENTS",
  "E1600-TOOLS",
  "E1700-TOOLS",
  "F1100-OFFICE FURNITURE",
  "H1100-KITCHEN ITEMS",
  "H1300-TELEVISION & OTHERS",
  "H1400-WASHING MACHINE",
  "H1500-REFRIGERATOR",
  "H1600-REFRIGERATOR",
  "L1100-LEASE VEHILCE",
  "L1300-LEASED TRUCKS",
  "V1100-CAR",
  "V1200-BUS & TRAILER",
  "V1300-MOTOR CYCLE",
  "V1400-LEASED CAR",
  "V1500-LEASED TRUCKS"
]
