import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router  } from '@angular/router';
import { CommonUtilityService } from '../services/common-utility-service';
import { environment } from '../../environments/environment';
import { EmitterService } from './emitter.service'

@Injectable({
  providedIn: 'root'
})
export class VerifyAdminService {

  constructor(
    private utilityService: CommonUtilityService,
    private emitter: EmitterService,
    private router: Router) { }

    submitForm = (url, request) => {

      let httpHeaders = new HttpHeaders()
      .set(
      'Authorization',
      `Bearer ${this.utilityService.getCookie('AuthToken')}`
      )
      .set('Content-Type', 'application/json');

      let options = {
      headers: httpHeaders,
      };

      return this.utilityService.putRequest(url, request, options);

    }

}
