import { CanActivate } from "@angular/router";
import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot } from '@angular/router';
import { CommonUtilityService } from './common-utility-service'

@Injectable()

export class AuthGuard implements CanActivate {

    constructor(
        private utilityService: CommonUtilityService,
        private router: Router
    ) { }

    canActivate(activatedRoute: ActivatedRouteSnapshot) {
        if (this.utilityService.getCookie('isAuthenticated')) {
            switch (activatedRoute.url[0].path) {
                case 'approval-dashboard':
                    const approverRoles = ['ITApprover', 'FinanceApprover']
                    // const customerRoles: any = JSON.parse(localStorage.getItem('customerRoles'))
                    const customerRoles: any = this.utilityService.getCookie('customerRoles') !== undefined ? JSON.parse(this.utilityService.getCookie('customerRoles')) : []
                    return customerRoles.some(role => approverRoles.includes(role)) ? true : this.router.navigate(['/'])
                default:
                    return true;
            }
        }
        this.router.navigate(['/'])
    }
}