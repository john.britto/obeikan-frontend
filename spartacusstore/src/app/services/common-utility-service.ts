import { Injectable, ElementRef, Renderer2, PLATFORM_ID, Inject } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie';
import { of } from 'rxjs';
import { TransferState, makeStateKey } from '@angular/platform-browser';
import { environment } from '../../environments/environment';
import { englishEnums } from '../app.enums';

/**
 * Utility service for the application
 * Common utility functions that can be used throughout the application 
 * @author Mohamed Omar Farook
 */
@Injectable()
export class CommonUtilityService {

    transferedState: any;
    MOBILE_PATTERN = '^((\\+[0-9]{2}-?)|0)?[0-9]{8,10}$';
    PREFIX_PATTERN = '^[0-9]{3}$';
    ALPHANUMERIC_PATTERN = '^[a-zA-Z0-9]+$'
    MAX_TEN_PATTERN = '^[0-9]{1,10}$'

    constructor(
        @Inject(PLATFORM_ID) private plateformId: object,
        private http: HttpClient,
        private state: TransferState,
        private cookieService: CookieService,
        private router: Router) { }

    /**
     * @param input object to be validated
     * @returns true if object is undefined or empty, otherwise false
     */
    isUndefinedOrEmpty(input: any) {
        if (undefined !== input && '' !== input) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param input object to be validated
     * @returns true if object is undefined or null, otherwise false
     */
    isNullOrUndefined(input: any) {
        if (undefined !== input && null !== input) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Checks if application is running in browser
     */
    isBrowser(): boolean {
        if (isPlatformBrowser(this.plateformId)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param Endpoint url
     * @returns Get request response from server
     */
    getRequest(url: any, header) {

        let api = this.getApi(url)

        return this.http.get(api, header)
    }

    /**
     * @param Endpoint url
     * @returns Post request response from server
     */
    postRequest(url, request, options) {
        let api = this.getApi(url)

        return this.http.post(api, request, options)
    }

    postRequestWithCustomHostName(url, request, options) {
        let api = url

        return this.http.post(api, request, options)
    }

    /**
     * @param Endpoint url
     * @returns Put request response from server
     */
    putRequest(url, request, options) {
        let api = this.getApi(url)

        return this.http.put(api, request, options)
    }

    /**
     * @param Endpoint url
     * @returns patch request response from server
     */
    patchRequest(url, request, options) {
        let api = this.getApi(url)

        return this.http.patch(api, request, options)
    }

    /**
     * @param Endpoint url
     * @returns delete request response from server
     */
    deleteRequest(url, options) {
        let api = this.getApi(url)

        return this.http.delete(api, options)
    }

    /**
     * @param url
     * @returns generated endpoint url
     */
    getApi(url) {
        if (url === '../../assets/mockApi/home.json') {
            return url
        }
        return environment.hostName + url
    }

    getAuth() {
        const httpHeaders = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded;' });
        const url = environment.hostName + environment.oAuthAPI;
        const request = new HttpParams()
            .set('client_id', 'trusted_client')
            .set('client_secret', 'secret')
            .set('grant_type', 'client_credentials')
            .set('scope', 'extended');
        return this.http.post(url, request.toString(), { headers: httpHeaders }).subscribe((res: any) => {
            if (res && res.access_token) {
                this.setCookie('AuthToken', res.access_token);
            }
        },
            err => {
                this.router.navigate(['/404'])
            });
    }

    setCookie(key: any, value: any) {
        if (this.isBrowser()) {
            this.cookieService.put(
                key,
                value,
                {
                    path: '/',
                    domain: this.isBrowser() ? window.location.hostname : ''
                })
        }
    }

    setLocalStorage = (key: any, value: any) => {
        if (this.isBrowser()) {
            localStorage.setItem(key, JSON.stringify(value))
        }
    }

    getLocalStorage = (key: any) => {
        if (this.isBrowser()) {
            return JSON.parse(localStorage.getItem(key))
        }
    }

    removeLocalStorage = (key: any) => {
        if (this.isBrowser()) {
            localStorage.removeItem(key)
        }
    }

    checkLoggedInOrRedirect() {
        if (this.getCookie('isAuthenticated')) {
            // logged in so return true
            return true;
        }
        // not logged in so redirect to login page with the return url and return false
        const routeState = this.router.routerState.snapshot;
        this.router.navigate(['login'], { queryParams: { returnUrl: routeState.url } });
        return false;
    }

    getCookie(key) {
        if (this.isBrowser()) {
            return this.cookieService.get(key);
        }
        return
    }

    removeCookie(key: any) {
        if (this.isBrowser()) {
            this.cookieService.remove(
                key,
                {
                    path: '/',
                    domain: this.isBrowser() ? window.location.hostname : ''
                })
        }
    }
    buildQueryParams(source: Object): HttpParams {
        let target: HttpParams = new HttpParams();
        Object.keys(source).forEach((key: string) => {
            const value: string | number | boolean | Date = source[key];
            if ((typeof value !== 'undefined') && (value !== null)) {
                target = target.append(key, value.toString());
            }
        });
        return target;
    }
    getEnum() {
        const url = environment.enumAPI;
        const httpHeaders = new HttpHeaders({
            'Content-Type': 'application/xml'
        });
        const options = {
            headers: httpHeaders
        };
        return this.getRequest(url, options);
    }

    setEnums(enums, property) {
        const values = [];
        enums[property].map(each => {
            englishEnums[property].map(obj => {
                if (each.code === obj.code) {
                    values.push(obj);
                }
            });
        });
        return values;
    }

    /**
 * Set state value to transfer from seerver to client
 * @url key value to save it in server
  */
    setStateTransfer(url: string, header?: any) {
        const STATE = makeStateKey(url);
        this.transferedState = this.state.get(STATE, null as any);
        if (this.isBrowser() && this.transferedState) {
            return of(this.transferedState);
        } else {
            let api = this.getApi(url)
            return this.http.get<any>(api, header).pipe(tap(response => {
                this.state.set(STATE, response);
            }))
        }
    }

    splitCode = value => value.split('-')[0]
    splitDesc = value => value.split('-')[1]

    isUAEMobileFormat = number => new RegExp('^((\\+[0-9]{2}-?)|0)?[0-9]{9}$').test(number)

}

