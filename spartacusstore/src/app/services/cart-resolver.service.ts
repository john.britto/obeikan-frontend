import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Resolve } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { CommonUtilityService } from './common-utility-service';
import { environment } from '../../environments/environment';
import { HttpHeaders } from '@angular/common/http';
import { PdpService } from './pdp.service';

@Injectable()
export class CartResolverService implements Resolve<any> {
  constructor(
    private utilityService: CommonUtilityService,
    private pdpService: PdpService,
    private router: Router) { }

  resolve() {

      const httpHeaders = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.utilityService.getCookie('AuthToken')}`);
        
    const url = `${ environment.hostName}${environment.cartAPI}${this.utilityService.getLocalStorage('displayUid')}/carts`; 

    this.pdpService.checkCartExist(url).subscribe((data: any) => {

      const response = JSON.parse(JSON.stringify(data));

      if (response && response.carts && response.carts.length > 0 && response.carts[0].code) {

        const cartId = response.carts[0].code
        
        const cartApi = `${environment.cartAPI + this.utilityService.getLocalStorage('displayUid')}/carts/${cartId}`;

        const httpHeaders = new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Authorization', `Bearer${this.utilityService.getCookie('token')}`);

        const options = {
          headers: httpHeaders
        };

        return this.utilityService.getRequest(cartApi, options).pipe(
          catchError((err: any) => {
           this.router.navigate(['/404'])
            return err
          })
        );
      } else {
        return false;
      }
    },
    err => {
      return false;
    })
  }
}
