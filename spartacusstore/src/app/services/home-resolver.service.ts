import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Resolve } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { CommonUtilityService } from './common-utility-service';
import { environment } from '../../environments/environment'; 


@Injectable()
export class HomeResolverService implements Resolve<any> {
  constructor(
    private utilityService: CommonUtilityService,
    private router: Router) {}

  resolve() {
    return this.utilityService.getRequest(environment.homePageEndPoint, '').pipe(
      catchError((err: any) => {
       this.router.navigate(['/404'])
        return err
      })
    );
  }
}