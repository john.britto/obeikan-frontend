import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { CommonUtilityService } from '../services/common-utility-service';
import { environment } from '../../environments/environment';
import { environmentConst } from '../../environments/env.constant'
import { map, catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root',
})
@Injectable()

export class LoginService {

  private idleTimeTracker = new Subject<any>();

  private userContextData = new BehaviorSubject<UserContext>(
    new UserContext('', '', false, false));
  userContext = this.userContextData.asObservable();

  // _customerRoles: any = JSON.parse(localStorage.getItem('customerRoles'));
  _customerRoles: any = []
  _powerBiData: any

  constructor(
    private http: HttpClient,
    private utilityService: CommonUtilityService
  ) {
    if (this.utilityService.getCookie('customerRoles') !== undefined) {
      this._customerRoles = JSON.parse(this.utilityService.getCookie('customerRoles'))
    }
  }

  changeUserContext(data: UserContext) {
    this.userContextData.next(data);
  }

  emitIdleTracker(emitterData) {
    this.idleTimeTracker.next(emitterData);
  }

  idleTrackerListener(): Observable<any> {
    return this.idleTimeTracker.asObservable();
  }

  fetchToken(req) {

    const request = new HttpParams()
      .set('client_id', 'trusted_client')
      .set('client_secret', 'secret')
      .set('grant_type', 'client_credentials')
      .set('scope', 'extended')
      .set('username', req.email)
    //.set('password', req.password);

    let httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded')

    let options = {
      headers: httpHeaders
    };
    let tokenUrl = environment.tokenEndpoint;

    return this.utilityService.postRequest(tokenUrl, request, options)
  }

  userLogin(req) {

    const request = new HttpParams()
      .set('client_id', 'trusted_client')
      .set('client_secret', 'secret')
      .set('grant_type', 'password')
      .set('scope', 'extended')
      .set('username', req.email)
      .set('password', req.password);

    let httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded')

    let options = {
      headers: httpHeaders
    };
    let tokenUrl = environment.tokenEndpoint

    return this.utilityService.postRequest(tokenUrl, request, options)
  }

  fetchUserDetails = (token, username) => {

    let httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer${token}`)

    let options = {
      headers: httpHeaders
    };
    let loginUrl = environment.loginEndpoint + username + '?'

    return this.utilityService.getRequest(loginUrl, options)
  }

  activateProfile = (token, username, otp) => {

    let httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', `Bearer${token}`)

    let options = {
      headers: httpHeaders
    };

    const request = new HttpParams()
      .set('otp', otp)

    let url = `${environment.loginEndpoint}${username}/profile-activate`

    return this.utilityService.postRequest(url, request, options)
  }

  resendOTP = (token, username) => {

    let httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', `Bearer${token}`)

    let options = {
      headers: httpHeaders
    };

    let url = `${environment.loginEndpoint}${username}${environmentConst.endpoints.login.resendOTP}`

    return this.utilityService.postRequest(url, '', options)
  }

  fetchOrderDetails = (token, username) => {

    let httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer${token}`)

    let options = {
      headers: httpHeaders
    };
    let loginUrl = environment.loginEndpoint + username + '/orders'

    return this.utilityService.getRequest(loginUrl, options)
  }


  get customerRoles() {
    return this._customerRoles;
  }

  set customerRoles(roles) {
    this._customerRoles = roles;
  }

  removeLocalStorage = () => {
    this.utilityService.removeCookie('isAuthenticated');
    this.utilityService.removeLocalStorage('displayName')
    this.utilityService.removeLocalStorage('displayUid');
    this.utilityService.removeCookie('userName');
    this.utilityService.removeCookie('cartId');
    this.utilityService.removeCookie('isSuperAdmin');
    this.utilityService.removeCookie('adminType');
    this.utilityService.removeCookie('superAdminOrders');
    this.utilityService.removeCookie('financeSuperUser');
    this.utilityService.removeCookie('customerRoles');
  }
  get powerBiData() {
    return this._powerBiData;
  }

  set powerBiData(roles) {
    this._powerBiData = roles;
  }
}

export class UserContext {
  constructor(public displayUID: string, public displayName: string, public isAuthenticated: boolean, public isSuperUser: boolean) {
    this.displayUID = displayUID;
    this.displayName = displayName;
    this.isAuthenticated = isAuthenticated;
    this.isSuperUser = isSuperUser
  }
}

export const INVALID_CREDENTIAL = 'Invalid User Or Password';
export const INVALID_OTP = 'Invalid OTP';
export const EMAIL_LINK_EXPIRED = 'Email link expired';
export const USER_NOT_FOUND = 'Cannot find the User';
