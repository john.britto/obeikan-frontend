export const PAYMENT_TERMS = [
    '010-15 days from the date of inv',

    '011-PROG Payment',

    '012-50% Advance & bal after 30 day',

    '030-30 DAYS',

    '031-30 DAYS CREDIT AFTER SUB INVOI',

    '035-35 DAYS CREDIT',

    '050-50% w/PO, Balance Against PI',

    '050-50 Days Credit',

    '051-50%adv 50%After Del',

    '060-60 DAYS CREDIT',

    '061-60 DAYS AFTER SUBMITTED INVOIC',

    '069-69 Days Credit',

    '080-80% adv, 20% after 6months',

    '100-100% After Completion of Job',

    '101-100% after invoice date',

    '102-100% ADV',

    '105-105 days from Invoice Date',

    '150-150 days credit',

    '400-40% Adv 60% after delivery',

    '515-50% advance 50% after 15days',

    '90D-90DAYS D/A from date of B/L',

    '91D-90DAYS AVAL DFT FROM B/L DATE',

    'ADV-Advance Payment',

    'C00-30 DAYS CREDIT',

    'C01-30 Days Credit From Invoice D.',

    'C02-60 Days Credit From Invoice D.',

    'C03-90 Days Credit From Inv.Date',

    'C04-120 Days Credit From Invoice D',

    'C05-75%ADVANCE 25% AFTER DELIVERY.',

    'C06-20% ADV 70%PIOR SHIP,10%ACCEPT',

    'C07-45 Days Credit from Invoice Dt',

    'C08-AFTER COMPLETE DELIVERY',

    'C09-25%Advance- 75%After Delivery',

    'C10-7 DAYS FROM INVOICE RECEIVING',

    'C11-75 DAYS FROM THE INVOICE DATE',

    'C12-150 days credit',

    'C17-ADVANCE AGAISNT GUARANTEE',

    'C18-14 days from recipt goods',

    'C19-15 DAYS FROM INVOCIE',

    'C20-15 DAYS FROM INV DATE',

    'C21-90 Days Credit from B/L Date',

    'C22-120 DAYS DRAFT FROM B/L DATE',

    'C23-60 Days Credit from B/L Date',

    'C24-150 Days Credit From Invoice D',

    'C25-90 DAYS CREDIT / END OF MONTH',

    'C26-30 DAYS CREDIT FROM MAT RECPT',

    'C27-90 DAYS FROM MATERIAL RECEIPT',

    'C28-1st Lot Immediate &Bal 60 DAYS',

    'C29-120DAYS CREDIT FROM B/L DATE',

    'C30-75 Days Credit from Invoice Da',

    'C31-150 DAYS  D/A FROM DATE OF B/L',

    'C32-70% ADV & 30% AFTER DELIVERY',

    'C33-60%adv40%prior shipment',

    'C40-60% adv,40%after completion',

    'COD-Cash On Delivery',

    'CON-CONSIGNMENT',

    'D01-Cash Against Document',

    'D02-50% ADV & 50% CASH AGAINST DOC',

    'D10-50% Advance & Balance',

    'D11-50%ADV.BALANCE UPON DELIVERY',

    'D12-50%with PO & 50% before Delive',

    'D18-50% ADVANCE 50% AFTER JOB COMP',

    'D19-30%ADVANCE W/PO&70%PROG WORK',

    'D20-50% ADV, 40%ON DEL & 10%INSTAL',

    'D21-30% Adv, 50% As Prog & 20% Com',

    'D22-35%Adv,30%onInst&30%Comm&5%HO',

    'D23-50% ADV & BAL 50% AFTER 6 MTHS',

    'D24-60 DAYS BANK A/D FROM B/L DATE',

    'D25-OFFSET AGAINST CREDIT NOTE',

    'D26-OFFSETTING WITH OPI',

    'D32-60 DAYS D/A FROM B/L DATE',

    'D33-CASH AGASINT DOCUMENTS 60 DAYS',

    'D34-50% adv, 50% after 6months.',

    'D35-75% advance 25% after 6 month',

    'D36-60%adv,35%del,5%test/comm',

    'D37-CAD 90 DAYS FROM B/L DATE',

    'D38-50% CAD AND 50% PROGRESSIVE',

    'L01-letter of Credit At Sight',

    'L03-L/C Pay at 60 Days From B/L',

    'L04-L/C Pay at 90 Days From B/L',

    'L05-L/C Pay at 120 Days From B/L',

    'L07-L/C Pay at 180 Days From B/L',

    'LC-LETTER OF CREDIT',

    'LO8-L/C PAY AT 20% ADV,60%SHIP,20%',

    'MON-Monthly Basis',

    'QAT-Quarterly Payment'
]

export const DELIVERY_TERMS = [
"CFR-Cost and Freight",
"CIF-Cost, Insurance and Freight",
"CIP-Carriage and Insurance Paid to",
"CPT-Carriage Paid To",
"DAF-Delivered At Frontier",
"DDP-Delivered Duty Paid",
"DDU-Delivered Duty Unpaid",
"DEQ-Delivered Ex Quay",
"DES-Delivered Ex Ship",
"OTH-Other Delivery Terms",
"EXW-Ex Works",
"FAS-Free Alongside Ship",
"FCA-Free Carrier",
"CIF-Cost, Insurance and Freight",
"FOB-Free On Board",
"ODT-Other Delivery Terms"
]