// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  hostName: 'https://api.c050ygx6-obeikanin2-d1-public.model-t.cc.commerce.ondemand.com/',
  leaveManagment: 'https://hcmsaned.obeikan.com.sa:443/webservices/rest/EmployeeLeave/',
  headerEndpoint: 'osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=headerpage',
  logoUrl: 'medias/site-logo.png?context=bWFzdGVyfGltYWdlc3wxMTEyOXxpbWFnZS9wbmd8aGRmL2hmYi84Nzk3MTQ2OTA2NjU0L3NpdGUtbG9nby5wbmd8YWNmYmZhYzAwZmEyNjkxMjMyODg1OTM1N2YzZTliMGQwM2IzYTQ4MjM4ZmJjZTlmOWNhYWMzZmE4NmQ0MTk4Ng',
  homePageEndPoint: '/osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=homepage',
  pdpCMSApi: '/osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=',
  oAuthAPI: 'authorizationserver/oauth/token',
  registerUserAPI: 'osanedcommercewebservices/v2/osaned/users',
  footerEndPoint: 'osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=footerpage',
  tokenEndpoint: 'authorizationserver/oauth/token',
  loginEndpoint: 'osanedcommercewebservices/v2/osaned/users/',
  pdpEndPoint: 'osanedcommercewebservices/v2/osaned/products/',
  usersEndpoint: 'osanedcommercewebservices/v2/osaned/users/',
  cartAPI: 'osanedcommercewebservices/v2/osaned/users/',
  subscriptionAPI: 'osanedcommercewebservices/v2/osaned/subscription/users/',
  payrollApi: 'osanedcommercewebservices/v2/osaned/hr-portal/users/',
  policyApi: 'osanedcommercewebservices/v2/osaned/hr-policy/users/',
  enumAPI: 'osanedcommercewebservices/v2/osaned/osaned-enums',
  companyAPI: 'osanedcommercewebservices/v2/osaned/users/',
  aboutUS: 'osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=aboutUsPage',
  contactUS: 'osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=contactUsPage',
  contactUsPostApi: 'osanedcommercewebservices/v2/osaned/contactus/apicall?',
  adminDownloadApi: 'osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=hrservicepage',
  glDownloadApi: 'osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=financeGeneralLedgerServicePage',
  inventoryApi: 'osanedcommercewebservices/v2/osaned/inventory/',
  lookupApi: 'osanedcommercewebservices/v2/osaned/lookup/users/',
  superAdmin: {
    hrPortalAPI: 'osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=hrportalpage',                 
    hrGetCustomerAPI: 'osanedcommercewebservices/v2/osaned/hr-portal/users/',
    hrCreateUserAPI: 'osanedcommercewebservices/v2/osaned/hr-portal/users/',
    hrRequestAPI: 'osanedcommercewebservices/v2/osaned/hr-request/users/',
    financePortalAPI: 'osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=financeportalpage',
    financeGetCustomerAPI: 'osanedcommercewebservices/v2/osaned/finance-portal/users/',
    financeCreateUserAPI: 'osanedcommercewebservices/v2/osaned/finance-portal/users/',
    financialEntryAPI: 'osanedcommercewebservices/v2/osaned/financial-entry/users/',
    financeManagerAPI: 'osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=financemanagerservicepage',
    conversionAPI: 'osanedcommercewebservices/v2/osaned/finance-portal/users/'
   },
   verificationAdmin:{
    subEmpApi: 'osanedcommercewebservices/v2/osaned/finance-portal/users',
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
